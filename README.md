npm run dev -- --env.lang=cn --env.production

#运行
npm run dev  默认运行 dev
npm run dev -- --base=DEV
npm run dev -- --base=TEST
npm run dev -- --base=UAT
npm run dev -- --base=PROD

#打包
npm run build  默认打包 dev
npm run build -- --base=DEV
npm run build -- --base=TEST
npm run build -- --base=UAT
npm run build -- --base=PROD


Logo     string json:"logo"      // 游戏logo
PcRec    string json:"pc_rec"    // pc推荐位背景图
PcSec    string json:"pc_sec"    // pc二级页面背景图
H5Figure string json:"h5_figure" // h5人物卡片背景图
H5Filter string json:"h5_filter" // h5游戏筛选背景图
H5Sec    string json:"h5_sec"    // h5二级页面背景图

#埋点索引
1-游戏选择,2-联赛勾选,3-公告,4-推荐赛事,5-推荐赛事盘口投注点击,6-今日标签,7-滚球标签,8-早盘标签,9-串关标签,
10-收藏标签,11-赛果标签,12-主播标签,13-冠军标签,14-战队搜索,15-首页时间选择,16-个人中心,17-隐藏余额,18-赛事列表盘口投注点击,
19-赛事局数切换,20-局内串关开关,21-赛事赛果照片,22-盘口投注点击,23-关闭页面,
24-赛果时间选择,25-赛事点击,26-赛果局数切换,27-赛果赛果照片
28-头像更换,29-隐藏/显示余额,30-历史注单,31-账变记录,32-查看公告,33-玩法规则,
34-金额输入框,35-快捷金额,36-删除注单,37-投注按钮,38-自动接受赔率选择,39-清空注单,40-局内串关,41-投注记录,42-收起,
43-新增单注投注单,44-单注成功投注,45-串注盘口点击,46-串注成功投注,47-局内串关盘口点击,48-局内串关成功投注,

is_winner 
 0-输 1-赢 2-赢半 3-输半 4-平(走水)

#埋点页面
1-首页,2-赛事页面,3-赛果页面,4-个人中心页面,5-购物车点击页面,6-购物车注单页面,7-游戏点击页面

