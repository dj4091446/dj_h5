import { mapGetters } from "vuex";
import * as virtualSportsApi from "@/api/virtualSports.js";
export default {
        data(){
            return {
                firstMaxDuration: 0, // 首轮最大播放时间
            }
        },
        computed:{
            ...mapGetters([
                "virtualMatchList",
                "serverTime",
                "localTime",
            ]),
            matchList(){
                return this.virtualMatchList[0]?.game_data || [];
            },
        },
        methods: {
            isFirstRoundAllEnd(){
                const {start_time, batch_no, game_id, tournament_id:tour_id} = this.virtualMatchList[0].game_data[0];
                const playedTime = (this.serverTime + (new Date().getTime() - this.localTime) - start_time * 1000) / 1000;
                if(playedTime >= 0 && this.firstMaxDuration === 0){
                    virtualSportsApi.requestVirtualReplay({batch_no, game_id, tour_id}).then(res=>{
                        for(const key of Object.keys(res.data.data)){
                            const {duration} = res.data.data[key];
                            if(duration && duration > this.firstMaxDuration){
                                this.firstMaxDuration = Number(duration);
                            }
                        }
                    });
                }
                return playedTime >= this.firstMaxDuration && this.firstMaxDuration !== 0;
            } 
        },
}