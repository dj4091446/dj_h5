import CryptoJS from 'crypto-js/crypto-js'
import {requestGameFestival, getOssUrl} from "@/api/game.js";
import lodash from 'lodash';

export default {
    data() {
        return {
            key: "panda1234_1234ob",
            DECRYPT_KEY: {},
            envs: {
                dev: ["https://api-json.sportxxx25o1bmw.com/dev.json"],
                fat: ["https://api-json.sportxxx25o1bmw.com/test.json"],
                uat: ["https://api-json.sportxxx25o1bmw.com/lspre.json"],
                prod: [
                    "https://xbnhjktbwggfvyok.ybgjhb.com/prod.json",
                    "https://aukukktsxfauannt.zyakxf.com/prod.json",
                    "https://xbnhjktbwggfvyok.chinazjyh.com/prod.json",
                    "https://xbnhjktbwggfvyok.lcjzgt.com/prod.json"
                ]
            },
            oss_data: {}
        }
    },
    created(){
        // 解密使用的key
        this.DECRYPT_KEY = CryptoJS.enc.Utf8.parse(this.key);
        this.ossUrl()
    },
    methods: {
        /**
        * @desc 获取节庆赛事资源
        * @params { }
        */
         getFestival(){
            requestGameFestival().then(res =>{
                if( res.status == 200 ){
                    const data = lodash.get(res,"data.data")
                    const date = new Date().getTime()
                    const oss_data = lodash.get(this.oss_data,"img[0]")
                    let show_festival =  data.state && date <= data.end_time && date >= data.start_time && oss_data
                    this.$store.dispatch("setTyOssData", {
                        ty_img_domain: oss_data,
                        pendant: show_festival && data.img4 ? `${oss_data}/${data.img4}` : false,
                        dialog: show_festival && data.img5 ? `${oss_data}/${data.img5}` : false,
                        festival_list: show_festival && lodash.get(res,"data.data")
                    })
                }
            })
        },
        /**
        * @desc 获取oss加密域名
        * @params { }
        */
        ossUrl(){
            let env = this.envs[isDevMode?"fat": ENV],
                urls = []
            env.forEach(item =>{
                urls.push(getOssUrl(item))
            })

            Promise.any(urls).then(res =>{
                if(res.status == 200 && res.data){
                    this.get_decrypt_oss_data(res.data)
                }
            })
        },
        /**
         * @description: 字符串解密函数
         * @param {*} word 加密字符串
         * @return {*}  明码字符串
         */
        get_oss_decrypt_str(word) {
            let ret = '';
            if (word) {
                try {
                    // 解密
                    var decrypt = CryptoJS.AES.decrypt(word, this.DECRYPT_KEY, {
                        mode: CryptoJS.mode.ECB,
                        padding: CryptoJS.pad.Pkcs7
                    });
                    // 转字符串
                    ret = CryptoJS.enc.Utf8.stringify(decrypt).toString();
                    // 去除左右空格
                    ret = ret.replace(/(^\s*)|(\s*$)/g, "");
                    // 删除结尾的/
                    if (ret && (ret[ret.length - 1] == '/')) {
                        ret = ret.substr(0, ret.length - 1);
                    }
                } catch (error) {
                    console.error('get_oss_decrypt_str:', error);
                }
            }
            return ret;
        },

        /**
         * @description: 解密数组中的加密数据,并进行赋值操作(获取解密后的信息)
         * @param {*} obj
         * @return {*}
         */
        get_oss_decrypt_obj(obj) {
            if (lodash.isArray(obj)) {
                for (let i = 0; i < obj.length; i++) {
                    // 解密数据,重新设置数据
                    obj[i] = this.get_oss_decrypt_str(obj[i]);
                }

            } else if (lodash.isObject(obj)) {
                for (const key in obj) {
                    if (Object.hasOwnProperty.call(obj, key)) {
                        // 解密数据,重新设置数据
                        obj[key] = this.get_oss_decrypt_str(obj[key]);
                    }
                }
            }
        },

        /**
         * @description: 获取oss文件中的明码数据
         * @param {*} data oss文件加密数据
         * @return {*} oss文件中的明码数据
         */
        get_decrypt_oss_data(data) {
            if (data) {
                //解密 img
                this.get_oss_decrypt_obj(lodash.get(data, 'img'));
                //解密 static
                this.get_oss_decrypt_obj(lodash.get(data, 'static'));
                //解密 live_domains
                this.get_oss_decrypt_obj(lodash.get(data, 'live_domains'));
                //解密 GAA.api
                this.get_oss_decrypt_obj(lodash.get(data, 'GAA.api'));
                //解密 GAA.api
                this.get_oss_decrypt_obj(lodash.get(data, 'GAB.api'));
                //解密 api
                this.get_oss_decrypt_obj(lodash.get(data, 'api'));
            }
            console.log('最优oss url文件信息:', data);
            this.oss_data = data
            this.getFestival()
        }
    }
}