import { mapGetters } from "vuex";
import { oddStrTrans, hasJiZhunFen } from "@/assets/scripts/utils";
import userOdds from "./userOdds.js";
// 全场-波胆， 趣味 类型 隐藏投注项
// round 0-全局 1-单 BO1-BOX...
// option_type 选项类型 选项类型 1-输赢 2-让分 3-大小 4-趣味 5-波胆

export default {
    mixins:[ userOdds ],
    computed: {
        ...mapGetters([
            "matchTeams"
        ]),
    },
    methods: {
    
         // 盘口下面投注项目全部隐藏  则隐藏盘口
        hideVisible(odds){
            const visible = odds.every( ele => ele.visible == 0 )
            return visible
        },
        
        // 主播盘 设置空框
        hostIsVible(row, category){
            if ( row && category == 5 && !row.visible){
                return true
            }
            return false
        },

        // 投注项是否被暂停锁住
        issuspended(row, markets, match) {
            const  market = markets || {};
            
            // 赛事暂停
            if( match.suspended == 1){
                return true;
            }
            // 盘口暂停
            if ( market.suspended == 1) {
                return true;
            } 
            // 投注项暂停
            if( row.suspended == 1 ){
                return true
            }
            return false
        },

        // 是否是“—”的隐藏效果
        isLineHide(markets, item = {}) {
            const  market = markets || {};
            // 胜负平盘口，隐藏时，显示“-”
            // option_type=6 胜负平盘口
            if( !market.visible ) {
                return true;
            }

            if(!item.visible) {
                return true;
            }
            return false
        },

        // 购物车投注项是否被暂停
        cartIssuspended(item){
            const { suspended, market_suspended, match_suspended } = item;
            const issuspended = Boolean( this.issuspended(
                {suspended:suspended},
                {suspended: market_suspended },
                {suspended: match_suspended}
            ) || item.match_visible != 1
              || item.market_visible != 1
              || item.match_status != 5
              || item.market_status != 6
              || item.visible != 1 
              || (Number(item.odds) < 1.001 ) );
            // console.log( issuspended )
            return issuspended
        },
        
        // 是否显示bo
        hideBo(category){
            const arr = [4, 5, 6]; // 篮球，主播，足球
            return arr.includes(Number(category))
        },
        // 是否显示比分
        hideScore(category){
            // const arr = []; // 主播
            // return arr.includes(Number(category))
            return false
        },
        // 是否显示计时器
        hideTimer(match, round){
            let category = match.category,
                game_id = match.game_id
            // console.log("---ty",match,round)

            if( category == 6 ){
                return  round == "middle_half" || round == "mid_rest"; // 足球中场不显示时间
            }else{
                const arr = [4, 5]; // 篮球，主播
                // nba2k 不隐藏倒计时
                if (game_id === "257733146228414") {
                    return  ['rest1','rest2','rest3'].indexOf(round) > -1; // 休息时间
                }
                return arr.includes(Number(category))
            }
        },
        // 计时器类型
        timerType(match){
            const {game_id} = match;
            // nba2k 倒计时
            if (game_id === "257733146228414") {
                return 'countdown'
            } else {
                return 'timing'
            }
        },
        // 是否显示主播赛事
        showHost(category){
            const arr = [5]; // 主播
            return arr.includes(Number(category))
        },

        // 替换投注项战队信息
        getTeams(match, str){
            if ((typeof str).toLowerCase() !== 'string') {
                str = this.i18nTransGameNameKey(str)
            }
            // 英雄，队员让分
            str = str.replace(/\$\$/gi,' ') ;
            str = oddStrTrans(str);
            if( match.category == 2 ){
                const teams = this.matchTeams && (this.matchTeams[match.id || match.match_id] || []);
                if( teams ){
                    const str1 = str.split("@T");
                    if( str1.length > 1 ){
                        const strIdx = Number(str1[1].substr(0,1));
                        const team = teams[strIdx - 1] || {};
                        return str.replace(`@T${strIdx}`,this.i18nTransGameNameKey(team,'team_'))
                    }else{
                        return str
                    }
                }else{
                    return str
                }
            }else{
                if( match.match_cn_team ){
                    const matchTeam = this.i18nTransMatchTeam(match)
                    const team = matchTeam.split(",");
                    return str.replace(/@T1/g,team[0]).replace(/@T2/g,team[1])
                }else{
                    return str;
                }
            }
        },



        // 投注项的宽
        getWidth(match, i){
            const position = this.getPostionClass(match.m.odds.length, i);
            if(position == "middle"){
                return 67
            }else{
                return 92
            }
        },

        getPostionClass(lens, i) {
            if (lens == 2) {
                return i % 2 == 0 ? "left" : "right";
            }
            if (lens == 3) {
                if( i == 0 ){
                    return "left"
                }
                if( i == 1 ){
                    return "middle"
                }   
                if( i == 2 ){
                    return "right"
                }
            }
        },

        // 待结算盘口
        marketClosed(market){
            return  market.status == 7 || market.status == 8 || market.status == 10 ? true : false;
        },

    }
};