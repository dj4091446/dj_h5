/***
 *  用户赔率分组js
 * 
*/

import { mapGetters } from "vuex";
export default {
    computed:{
        ...mapGetters(['oddGroup'])
    },
    methods: {
        // 赔率分组
        getOddGroup(match, mkt, item){
            if( match.category == 2 ){ // 冠军盘不参与赔率分组计算
                return Math.formatAmount(item.odd,3)
            }
            if( mkt.option_type == 12 || mkt.option_type == 23 ){ // 复合玩法不参与赔率分组计算
                return Math.formatAmount(item.odd,3)
            }
            if( item.odd_group && item.odd_group[this.oddGroup.group] ){ // 判断是否有值，兼容赛事老数据
                if( this.oddGroup.group == "g0" ){
                    return Math.formatAmount(item.odd,3)
                }else{
                    return Math.formatAmount(item.odd_group[this.oddGroup.group], 3)
                }
            }else{
                return Math.formatAmount(item.odd,3)
            }
        },

        // 购物车赔率分组
        getOddGroupCart(item){
            return this.getOddGroup( 
                { category: item.category },
                { option_type: item.option_type },
                { 
                    odd: item.relodds,
                    odd_group: item.odd_group
                }
            )
        }
    },
}