import {i18nTransGameNameKeyUtil} from '@/assets/scripts/utils.js'
const cls = {
    "0": {
        logo: "all"
    },
    "257154660915053": { // 英雄联盟
        logo: "LOL"
    },
    "257289795134339": { // DOTA2
        logo: "DOTA2"
    },
    "257366452854033": { // 任天堂全明星大乱斗
        logo: "Smash"
    },
    "257393813865855": { // 彩虹6号
        logo: "R6"
    },
    "257421187099429": { // 使命召唤
        logo: "COD"
    },
    "257445934728386": { // 堡垒之夜
        logo: "Fortnite"
    },
    "257474469784520": { // 绝地求生
        logo: "PUBG"
    },
    "257532676759026": { // 守望先锋
        logo: "OW"
    },
    "257561197207055": { // 王者荣耀
        logo: "KoG"
    },
    "257578064923863": { // CSGO
        logo: "CSGO"
    },
    "257594184113856": { // 星际争霸
        logo: "SC"
    },
    "257620516983156": { // 街头霸王
        logo: "SF"
    },
    "257647180647306": { // 万智牌
        logo: "MTG"
    },
    "257672679460102": { // 雷神之锤
        logo: "Quake"
    },
    "257697372071315": { // 篮球
        logo: "Basketball"
    },
    "257719680296134": { // 修改足球
        logo: "Soccer"
    },
    "257733146228414": { // NBA2K
        logo: "NBA2K"
    },
    "258003077224510": { // 魔兽争霸 III
        logo: "Warcraft3"
    },
    "258006510157846": { // 星际争霸 2
        logo: "SC2"
    },
    "258143683216896": { // 神之浩劫
        logo: "SMITE"
    },
    "258172821136137": { // 火箭联盟
        logo: "RL"
    },
    "258336811299100": { // 魔兽世界
        logo: "WoW"
    },
    "258337013509741": { // FIFA
        logo: "FIFA"
    },
    "271192272576750": { // 无畏契约
        logo: "Valorant"
    },
    "614598069861073": { //  Hearthstone
        logo: "Hearthstone"
    },
    "1376613872342718": { // 传说对决
        logo: "AoV"
    },
    "1377323217127607": { // 皇室战争
        logo: "CRL"
    },
    "1377370776735090": { // 穿越火线
        logo: "CF"
    },
    "21363218739142692":{ // 激门峡谷
        logo: "Wild_Rift"
    },
    "31476623018398892":{ // 刺激战场
        logo: "PUBGM_CN"
    },
    "101258749512819384":{ // 无尽对决
        logo: "ML",
    },
    "00":{ // 全部虚拟赛事
        logo: "V",
    },
    "1001":{ // 虚拟足球
        logo: "V_Soccer",
    },
    "159003446163958838":{ // 无人机冠军联盟
        logo: "DCL",
    },
    "10158428472914228":{ // 电竞彩-英雄召唤
        logo: "YXZH",
    },
    "257289795134800":{ // 梦三国
        logo: "M3G",
    }
}

export default {
    methods:{
        logoCls(id){
            const obj = cls[id] || cls['0']
            return obj.logo
        },
        gameIdGroups(){
            let ids = [];
            for( let key in cls ){
                ids.push(key)
            };
            return ids;
        },
        // 跳转链接，超连接 跳转使用
        jumpLink(data, i) {
          // 顶部导航，类型 取imgType4，   首页弹框 类型 取 imgType6
          let imgType = 'imgType'+ String(i)
          // 顶部导航，跳转链接 取imgUrl4，  首页弹框 跳转链接 取 imgUrl6
          let imgUrl = 'imgUrl'+ String(i)
          // console.error(data.festival_list[imgType]);
          // console.error(data.festival_list[imgUrl]);
          // imgType: 1：内部导航，2：外部链接，否则就是 不用跳转
          if(data.festival_list[imgType] == 1){
            // 如果是亚运会，直接跳转到亚运会专题
            if(data.festival_list[imgUrl] && data.festival_list[imgUrl].trim() == 'asianGames'){
              this.$router.push("/asiad");
            }else{
            this.$store.commit("SET_MATCH_ID", data.festival_list[imgUrl]);
            // 这里进入详情页，有 两个区分，带数据源 和 不带 数据源
            this.$router.push(`/markets/${data.festival_list[imgUrl]}`);
            }
          }else if(data.festival_list[imgType] == 2){
            window.open(data.festival_list[imgUrl], "_blank");
          }
        },
        // 转换多语言取值的key
        i18nTransGameNameKey(data, prefix = '', suffix = '') {
            return i18nTransGameNameKeyUtil(data, prefix, suffix)
        },
        i18nTransMatchTeam(data) {
            return i18nTransGameNameKeyUtil({
                cn_name: data.match_cn_team,
                en_name: data.match_en_team || data.match_cn_team,
            })
        }
    }
}