import i18n from "@/assets/i18n/index.js"
import userOdds from "./userOdds.js";
const round = [
    // { key: -1, value:"全部" },
    { key: 0, value: i18n.t('common.mixins.Global')},
    {key: 1, value: i18n.t('common.mixins.Mapnum', {num: 1})},
    {key: 2, value: i18n.t('common.mixins.Mapnum', {num: 2})},
    {key: 3, value: i18n.t('common.mixins.Mapnum', {num: 3})},
    {key: 4, value: i18n.t('common.mixins.Mapnum', {num: 4})},
    {key: 5, value: i18n.t('common.mixins.Mapnum', {num: 5})},
    {key: 6, value: i18n.t('common.mixins.Mapnum', {num: 6})},
    {key: 7, value: i18n.t('common.mixins.Mapnum', {num: 7})},
    {key: 8, value: i18n.t('common.mixins.Mapnum', {num: 8})},
    {key: 9, value: i18n.t('common.mixins.Mapnum', {num: 9})},
    {key: 10, value: i18n.t('common.mixins.Mapnum', {num: 10})},
    {key: 11, value: i18n.t('common.mixins.Mapnum', {num: 11})}
];

const stage = [
    { key: 1, value: i18n.t('common.mixins.early') },
    { key: 2, value: i18n.t('common.mixins.live') },
]

const basketball = [
    { key: 0, value: i18n.t('common.mixins.FullTime') },
    { key: 100, value: i18n.t('common.mixins.HalfTime') },
    { key: 1, value: i18n.t('common.mixins.OneQ') },
    { key: 2, value: i18n.t('common.mixins.TwoQ') },
    { key: 3, value: i18n.t('common.mixins.ThreeQ') },
    { key: 4, value: i18n.t('common.mixins.FourQ') },
];

const football = [
    { key: 0, value: i18n.t('common.mixins.FullTime') },
    { key: 100, value: i18n.t('common.mixins.HalfTime')},
];

const isHideVideo = localStorage.getItem('hideVideo') === 'true';

import { mapGetters } from "vuex";
export default {
    mixins:[ userOdds ],
    computed:{
        ...mapGetters(["matchs", "bifenBtnStatus"]),
        roundList(){
            const { visible, category } = this.matchs;
            if( visible == 0 ){
                return []
            };
            if( category == 4 ){
                return basketball;
            }
            if( category == 6 ){
                return football
            }
            let bo = this.matchs ? Number(this.matchs.bo || 0) : 0 ;
            bo === 0 && (bo = 9) // BO0的赛制默认显示9局。与BO9显示一样
            return this.round.filter(ele => ele.key <= bo);
        },
    },
    filters:{
        getStage(value){
            const live = stage.find( ele => ele.key == value )
            return live ? live.value : '--'
        }
    },
    data(){
        return {
            basketball,
            football,
            round,
            stage,
            // 只有四个游戏有玩法标签
            playGameArr:[
                "257154660915053", // 英雄联盟
                "257289795134339", // DOTA2
                "257561197207055", // 王者荣耀
                "257578064923863", // CSGO
            ]

        }
    },
    methods: {

        // 滚球第几局
        getLiveRound(val, category){
            let str = "";
            val = String(val);
            if( category == 2 ){
                return i18n.t('common.mixins.champion');
            }
            switch (val){
                case "overtime":
                    str = i18n.t('common.mixins.overtime')
                    break;
                case "middle_half":
                    str = i18n.t('common.mixins.middleHalf')
                    break;
                case "second_half":
                    str = i18n.t('common.mixins.secondHalf')
                    break;
                case "first_half":
                    str = i18n.t('common.mixins.HalfTime')
                    break;
                case "0":
                    const arr= [4, 6];
                    str = arr.includes(Number(category)) ? i18n.t('common.mixins.FullTime'): i18n.t('common.mixins.Global');
                    break;
                case "1":
                    str = category == 4 ? i18n.t('common.mixins.OneQ') : i18n.t('common.mixins.Mapnum', {num: 1})
                    break;
                case "2":
                    str = category == 4 ? i18n.t('common.mixins.TwoQ') : i18n.t('common.mixins.Mapnum', {num: 2})
                    break; 
                case "3":
                    str = category == 4 ? i18n.t('common.mixins.ThreeQ') : i18n.t('common.mixins.Mapnum', {num: 3})
                    break; 
                case "4":
                    str = category == 4 ? i18n.t('common.mixins.FourQ') : i18n.t('common.mixins.Mapnum', {num: 4})
                    break; 
                case "5":
                    str = i18n.t('common.mixins.Mapnum', {num: 5});
                    break; 
                case "6":
                    str = i18n.t('common.mixins.Mapnum', {num: 6});
                    break; 
                case "7":
                    str = i18n.t('common.mixins.Mapnum', {num: 7});
                    break; 
                case "8":
                    str = i18n.t('common.mixins.Mapnum', {num: 8});
                    break;
                case "9":
                    str = i18n.t('common.mixins.Mapnum', {num: 9});
                    break; 
                case "10":
                    str = i18n.t('common.mixins.Mapnum', {num: 10});
                    break; 
                case "11":
                    str = i18n.t('common.mixins.Mapnum', {num: 11});
                    break; 
                case "100":
                    str = i18n.t('common.mixins.HalfTime');
                    break;
                case "rest1":
                    str = i18n.t('common.mixins.rest1');
                    break;
                case "rest2":
                    str = i18n.t('common.mixins.rest2');
                    break;
                case "rest3":
                    str = i18n.t('common.mixins.rest3');
                    break;
                case "overtime_first_half":
                    str = i18n.t('common.mixins.overtime_first_half');
                    break;
                case "overtime_second_half":
                    str = i18n.t('common.mixins.overtime_second_half');
                    break;
                case "mid_rest":
                    str = i18n.t('common.mixins.mid_rest');
                    break;
            }
            return str
        },

        /****
         * 蚂蚁赛事 视频是否显示
         * */       
        showAntVideo(match){
            return  (
                        (match.video_type != 5 && ( match.admin_video_url || match.user_video_url ))  // 无直播 
                        || (
                            this.bifenBtnStatus.anime_video_status // 总开关直播按钮开启
                            && match.animated_video_lock // 赛事直播按钮开启
                        )  
                        || match.priority_live != 0 // 赛事数据源直播按钮开启
                    ) 
                    && match.is_live == 2  // 滚球
                    && match.ant_match_id != 0  // 关联数据源
                    && match.status == 5 // 赛事开盘
                    && !isHideVideo // 试玩场馆 隐藏视频
        },

        /****
         * 蚂蚁赛事 视频是否显示
         * */       
        showAntVideogary(match){
            return  (
                        (match.video_type != 5 && ( match.admin_video_url || match.user_video_url ))  // 无直播 
                        || (
                            this.bifenBtnStatus.anime_video_status // 总开关直播按钮开启
                            && match.animated_video_lock // 赛事直播按钮开启
                        )  
                        || match.priority_live != 0  // 赛事数据源直播按钮开启
                    ) 
                    && match.is_live == 1  // 滚球
                    && match.ant_match_id != 0  // 关联数据源
                    && match.status == 5  // 赛事开盘
                    && !isHideVideo // 试玩场馆 隐藏视频
        },

        // 蚂蚁数据  只显示正常直播按钮
        showVideoLS(match){
            return  match.video_type != 5  // 无直播 
                    && match.is_live == 2  // 滚球
                    && match.ant_match_id != 0  // 关联数据源
                    && match.status == 5 // 赛事开盘
                    && !isHideVideo // 试玩场馆 隐藏视频
        },

        // 蚂蚁数据 只显示动画直播按钮
        showAnimationLS(match){
            return  this.bifenBtnStatus.anime_video_status // 总开关直播按钮开启
                    && match.animated_video_lock // 赛事直播按钮开启
                    && match.is_live == 2  // 滚球
                    && match.ant_match_id != 0  // 关联数据源
                    && match.status == 5 // 赛事开盘
                    && !isHideVideo // 试玩场馆 隐藏视频
        },
  
        // 正常赛事 视频是否显示
        shownormalVideo(match){
            return (( match.video_type != 5 && match.is_live == 2 && match.status == 5 && ( match.admin_video_url || match.user_video_url )) 
                || (match.sid !== '0' && match.is_live == 2 && match.video_type != 5 && match.is_open_video && match.status == 5 && ( match.admin_video_url || match.user_video_url )))
                && !isHideVideo
        },

        // 正常赛事 视频是否显示
        shownormalVideogary(match){
            return (( match.video_type != 5 && match.is_live == 1 && match.status == 5 && ( match.admin_video_url || match.user_video_url ) ) 
                || (match.sid !== '0' && match.is_live == 1 && match.video_type != 5 && match.is_open_video && match.status == 5 && ( match.admin_video_url || match.user_video_url )))
                && !isHideVideo // 试玩场馆 隐藏视频
        },
        
        // 是否显示比分判定中   category 赛事类型 正常-1 冠军-2 大逃杀-3 篮球-4 主播盘-5 足球-6
        showBIfen(match){ 
            const { id, home_score, away_score, category, is_live } = match;
            if( category == 1 && is_live == 2){ // 正常盘口，滚盘中
                const timerRound = this.matchTimer[id] || {}
                return Number(home_score) + Number(away_score) != Number(timerRound.round) - 1
            }else{
                return false
            }
        },

        // 足球，篮球，FIFA没有复合玩法，默认不显示复合按键。
        showFuhe(game_id, type){
            if( type == 12 ){
                const ids = [ "258337013509741", "257697372071315", "257719680296134" ];
                return !(ids.includes(String(game_id)))
            }else{
                return true
            }
        }

    },
}
