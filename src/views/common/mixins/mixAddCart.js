// 添加盘口到购物栏操作
import { mapGetters, mapState } from 'vuex';
import lodash from 'lodash'
import * as gameApi from "@/api/game.js";
import i18n from "@/assets/i18n/index.js";
import userOdds from "./userOdds.js";
export default {
    mixins:[ userOdds ],
    computed: {
        ...mapGetters([
            'betCart',
            'userInfo',
            'stationOpen',
            'stationID',
            'stationList',
            'isUseSingleQuick',
            'isUseCorssQuick',
            'isUseStationQuick',
            'betLimit',
            'cross',
            'crossOddMax',
            'isFullscreen'
        ]),
        ...mapState({
            collect: state => state.game.collect,
            stateStationList: state => state.game.stationList,
        }),
        isOVerLimtOdd(){
            const obj = this.cross.find( ele => ele.alwaysShow ) || {};
            return obj.isOVerLimtOdd;
        },
        
        crossCurOdd(){
            const cross =[ ...this.betCart.filter( ele => !ele.same_match && !this.cartIssuspended(ele) && ele.bet_type === 'isCross'  )]; // 串注所有满足条件订单
            let odd = 0;
            if( cross.length == 0 ){
                odd = 0;
            }else if( cross.length == 1 ){
                odd = cross[0].odds
            }else{
                const obj = this.cross.find( ele => ele.alwaysShow ) || {};
                odd = obj.odds;
            }
            return odd
        },

        maxOdd(){
            return Number(this.crossOddMax)
        }
    },
    methods: {
         // 生成注单数据
         orderData(item, markets, matches, bet_type){
            const { 
                match_cn_team,
                match_en_team,
                game_id, 
                is_live, 
                tournament_id,
                is_pass_off,
                rnd_comp_odd_dscnt,
                batch_no,
                round_no,
                start_time,
            } = matches;
            const { 
                cn_name, 
                en_name, 
                round, 
                option_type,
                odd_type_id,
                tag,
                comp_sub_num,
                score_benchmark,
                name
            } = markets;
            let matchTeam = this.i18nTransMatchTeam({match_cn_team, match_en_team}).split(',');
            return {

                item_id: item.id, // 投注项ID
                item_cn_name: item.name, // 投注项中文名称
                item_en_name: item.en_name, // 投注项英文名称
                relodds: item.odd, // 投注项赔率
                visible: item.visible, // 投注项显示
                suspended: item.suspended, // 投注项暂停

                match_id: matches.id , // 赛事ID
                match_suspended: matches.suspended, // 赛事显示暂停
                match_status: matches.status, // 赛事状态
                match_visible: matches.visible, // 赛事显示隐藏

                cn_name, // 盘口中文名称
                en_name, // 盘口英文名称
                name, // 盘口名称(多语言动态数据新加字段)
                market_id: item.market_id, // 盘口ID 因为大小让分盘口合并盘口，取投注项盘口id
                market_suspended: markets.suspended,// 盘口显示暂停
                market_status: markets.status, // 盘口状态
                market_visible: markets.visible, // 盘口显示隐藏
                market_pass_off: markets.is_pass_off, // 盘口是否串关 0:不能串; 1:赛事串关; 2:局内串关和赛事串关;
                
                game_id, // 游戏ID

                tournament_name:matches.tournament_name , // 联赛名称
                tournament_id, // 联赛ID

                match_cn_team, // 赛事战队
                match_en_team, // 赛事英文战队
                T1:matchTeam[0], // 主队
                T2:matchTeam[1], // 客队

                round, // BO几
                is_live, // 是否为滚球
                is_pass_off: is_pass_off, // 是否允许串关
                option_type, // 玩法类型

                odd_type_id, // 同盘口

                bet_amount:"", // 投注金额
                profitable:0, // 预计返还
                relProfitable: 0,  // 实际预计返还

                odd_rate: rnd_comp_odd_dscnt, // 返还率
                category: matches.category, // 盘口类型 1:普通盘; 4:篮球盘 

                tag, // 是否可形成串关
                bet_type: bet_type,//组成单注注单还是串注注单
                comp_sub_num, //子盘口数量
                score_benchmark, //基准分
                batch_no, // 期数 只要虚拟体育才有此字段 
                round_no, // 轮数 只要虚拟体育才有此字段
                start_time, // 比赛开始时间   虚拟体育使用

                odd_group: item.odd_group, // 赔率分组
            }
        },

        /**
         * @description 生成普通注单
         * 
         * @param {Object} item     投注项信息
         * @param {Object} markets  盘口信息
         * @param {Object} matches  赛事信息
         * @param {Object} bet_type  单注single 串注cross 
         * */ 
        addCart(item, markets, matches, bet_type){
            // 是否已经结算
            if(markets.status == 9){
                return 
            }
            // 是否可以添加到购物车
            const canAdd = this.issuspended(item, markets, matches) || this.marketClosed(markets) || this.isLineHide(markets, item)
            if(canAdd){
                return
            }
            const id = item.id 
            const isAdd = this.betItemId.includes(id);

            // 串注达到最大赔率，不再添加注单
            const isOverCorssOdds = Number(this.maxOdd) < (Number(this.crossCurOdd) * Number(item.odd)) ;
            console.log( Number(this.maxOdd),Number(this.crossCurOdd),  Number(item.odd)  )
            if( !isAdd && isOverCorssOdds && bet_type == "isCross" ){
                this.$toast({
                    message: i18n.t('common.mixins.upperLimit'),
                    duration: 2000,
                    customClass: 'bet-status-tips',
                });
                return
            }


            if(!isAdd){
                // 新增的单注注单数统计，添加到购物车则默认为新增一单
				// this.$store.commit("SET_TRACK", { tab: 43, page: 6 });
                if(matches.is_pass_off){
                    // 开启串关的盘口点击数，每点击一次串关盘口选项算一次点击
                    // this.$store.commit("SET_TRACK", { tab: 45, page: 6 });
                }
            }
            
            // 单注时清空单注投注项
            if ( bet_type == "isSingle" && this.betCart.length > 0 ) {
                const item_id=this.betCart.find(ele=>ele.bet_type=='isSingle').item_id;
                // 删除购物篮订单
                this.$store.commit("SET_BET_CART", {
                    isAdd: false,
                    row: {
                        item_id: item_id
                    }
                });
            }
            
            //不允许同赛事进行串关 替换掉上一个同赛事选项
            if(bet_type=='isCross' && this.betCart.length > 0 && this.betCart.some(ele=>ele.match_id == matches.id)){
                const item_id = this.betCart.find( ele => ele.match_id == matches.id).item_id;
                // 删除购物篮订单
                this.$store.commit("SET_BET_CART", {
                    isAdd: false,
                    row: {
                        item_id: item_id
                    }
                });
            }
            const maxlens = this.betItemId.length;
            // 第一单弹起 
            if( maxlens == 0 && isAdd == false ){
                this.$store.commit("SET_BETCART_SHOW", true);
            }else{
                this.$store.commit("SET_BETCART_SHOW", false);
            }
            if(maxlens >= 10 && !isAdd ){
                this.$store.commit("SET_BETCART_SHOW", true);
                this.$store.commit("SET_BET_MAXTEN", true);
                return ;
            }else {
                this.$store.commit("SET_BET_MAXTEN", false);
            }

            this.$store.commit("SET_BETCART_TYPE", "betOrder");


            const itemData = this.orderData(item, markets, matches, bet_type)
            this.$store.commit("SET_BET_CART",{
                isAdd: !isAdd,
                row: {
                    ...itemData,
                    odds: this.getOddGroupCart(itemData), // 实际赔率 （赔率缩减之后的赔率）
                    same_match: false,
                    limit_succeed: false,//限额接口是否请求成功
                }
            });
            this.addCross()
        },
        
        addCross(){

           
           
            const maxOdd = this.maxOdd

            const cross =[ ...this.betCart.filter( ele => !ele.same_match && !this.cartIssuspended(ele) && ele.bet_type === 'isCross'  )]; // 串注所有满足条件订单
            // const singleCross = [ ...this.betCart.filter( ele => !this.cartIssuspended(ele)  )]; // 单注所有的满足条件订单
            const lens = cross.length;
     

            const souce = [];   // 返回列表数据格式
            const allList = []; // 总串注
            let allComb = 0; // 总串注 单注被计算次数
            // 当有2有效注 形成串单
            if(lens >= 2){
               
                // 单串注  串注大于6串，取最后三串
                for( let i = lens > 5 ? lens - 2 : 2 ; i <= lens; i++ ){

                    const list = Math.arrange(cross, i);
                    if(i == lens){
                        const odds = list[0].reduce((pre, item) => {
                            return pre * Number(item.odds)
                        }, 1)
                        souce.push({
                            name:`${i}${i18n.t('common.mixins.cross1')}`,     // 注数名称
                            list: list,     // 注数详情
                            bet_num: list.length,   // 注数
                            bet_amount:"",   // 投注金额
                            profitable:0,    // 预计返还
                            relProfitable: 0,  // 实际预计返还
                            odds: odds >= maxOdd ? maxOdd : odds, // 赔率
                            alwaysShow:true,
                            key:Math.random(), // 随机数 为key，确保变化赔率是同一个串注
                            b: this.double(list), // 发送后台的内容
                            type: i, // 串注标志 => 信用盘串注限额使用
                            num: 1, // 投注需要添加字段
                            comb: Math.combination(lens-1, i-1), // 每个单注被计算的次数
                            limitOdd: maxOdd, // 最大赔率
                            isOVerLimtOdd: odds >= maxOdd, // 达到最大赔率限制
                        });
                    }else{
                        souce.push({
                            name:`${i}${i18n.t('common.mixins.cross1')}`,     // 注数名称
                            list: list,     // 注数详情
                            bet_num: list.length,   // 注数
                            bet_amount:"",   // 投注金额
                            profitable:0,    // 预计返还
                            relProfitable: 0,  // 实际预计返还
                            b: this.double(list), // 发送后台的内容
                            type: i, // 串注标志 => 信用盘串注限额使用
                            num: 1, // 投注需要添加字段
                            comb: Math.combination(lens-1, i-1), // 每个单注被计算的次数
                            limitOdd: maxOdd, // 最大赔率
                        });
                    }
                    allComb = allComb + Math.combination(lens-1, i-1);
                    allList.push( ...list );
                }
                
                // 总串注  显示： 大于2串， 小于等于5串
                if(lens <= 5 && lens > 2){
                    souce.push({
                        name:`${lens}${i18n.t('common.mixins.cross')}${allList.length}`,     // 注数名称
                        list: allList,     // 注数详情
                        bet_num: allList.length,   // 注数
                        bet_amount:"",   // 投注金额
                        profitable:0,    // 预计返还
                        relProfitable: 0,  // 实际预计返还
                        bet_corss: lens, // 几串几
                        isAllCross: true, // 是否是总串注
                        b: this.double(allList), // 发送后台的内容
                        num: allList.length, // 投注需要添加字段
                        type: lens, // 串注标志 => 信用盘串注限额使用type: lens, // 串注标志 => 信用盘串注限额使用
                        comb: allComb, // 每个单注被计算的次数
                        limitOdd: maxOdd, // 最大赔率
                    })
                }                
            };
            console.log( "设置赛事串关", souce, maxOdd )
            this.$store.commit("SET_BET_CROSS", souce);
        },

        // 单注拆分 格式 1  单注普通投注
        single(cross){
            const bt = []
            cross.forEach(element => {
                const {
                    match_id,
                    market_id,
                    item_id,
                    odds,
                } = element;
                bt.push({
                    mch: match_id,
                    mkt: market_id,
                    oid: item_id,
                    odd: odds,
                    bt: 1,
                })
            });

            return bt;
        },

        // 串注拆分 格式
        double(cross){
            const b = cross.map( ele => ele.map( item =>  `${item.match_id},${item.market_id},${item.item_id},${item.odds}` ) )
            const bt = []
            b.forEach(ele => {
                bt.push( {
                    bt: 2,  // 投注类型 1普通投注 2 串关投注
                    b: ele.map( item => item ).join('|'), // 投注内容,竖线分割,
                    t: ele.length, // 串关类型 0:单注  1:2串1  2:3串1 3:4串1 4:5串1 5:6串1 6:7串1 7:8串1
                })
            });
            return bt;
        },

        // 添加收藏 
        addCollect(match){
            const hasId = this.collect.find(ele => match.id == ele.id)
            if (hasId) {
                this.$store.commit("SET_COLLECT_LIST", this.collect.filter(ele => ele.id != match.id));
            } else {
                if( this.collect.length >= 30 ) {
                    this.$toast({
                        message: i18n.t('common.mixins.max30'),
                        duration: 1500,
                    });
                } else { 
                    this.$store.commit("SET_COLLECT_LIST", [
                        ...this.collect,
                        match
                    ])
                }
            }
        },

        // 添加局内串关
        addStationCart(item, markets, matches){
            // 是否已经结算
            if(markets.status == 9){
                return 
            }
         
            const isAdd = this.stationID.includes(item.id);
            const nowList = this.stationList[markets.round];

            if(!isAdd){
                // 局内串关盘口点击数统计，每点击一次盘口选项算一次点击
				// this.$store.commit("SET_TRACK", { tab: 47, page: 6 });
            }

            // 是否可以添加到购物车
            const canAdd = this.issuspended(item, markets, matches ) || this.marketClosed(markets) || this.isLineHide(markets, item)
            if(canAdd){
                return
            };

            // 判断添加数据是否可以继续添加
            if( !isAdd && nowList && Number(markets.tag) && nowList.list.find( ele => ele.tag != 0 && ele.tag == markets.tag )){
                this.$toast({
                    message: i18n.t('common.mixins.same'),
                    duration: 2000,
                    getContainer: () => this.isFullscreen ? document.querySelector('#full') : document.body
                });
                return;
            }

            const betData = this.orderData( item, markets, matches );
            this.$store.commit("SET_BETCART_TYPE", "stationOrder")
            
            const keysArr = Object.keys( this.stationList )
            
            // 第一单弹起 
            if( keysArr.length == 0  ){
                this.$store.commit("SET_BETCART_SHOW", true);
            }else{
                this.$store.commit("SET_BETCART_SHOW", false);
            }

            // 判断添加数据个数 不超过十单
            
           
            if( !isAdd && nowList && nowList.list.length >= 10 ){
                this.$store.commit("SET_BETCART_SHOW", true);
                this.$store.commit('SET_STATION_MAX', true)
                return;
            }else {
                this.$store.commit("SET_BET_MAXTEN", false);
            }

            const validData = this.stationBetList({
                key: betData.round,
                val: {
                    ...betData,
                    odds: this.getOddGroupCart(betData), // 实际赔率 （赔率缩减之后的赔率）
                    complex_max: 1000,
                    isFull: false,
                    
                },
                limit:{
                    complex_min: 10,
                    complex_max: 1000,
                    rate_max_prize: Infinity
                }
            });
            if( validData ){
                this.$store.commit("SET_BET_STATION", validData);
            }else{
                this.$toast({
                    message: i18n.t('common.mixins.upperLimit'),
                    duration: 2000,
                    customClass: 'bet-status-tips',
                });
                return
            }

            // 测试账户不限红
            //  if( this.userInfo.tester == 1 ){ return };

            // 请求限红 测试账户不限红
            // if( !isAdd ){
            //     gameApi.requestGameLimit({
            //         match_id: matches.id,
            //         market_id: markets.id,
            //         odd_id: item.id,
            //     }).then( res => {
            //         if( res.data.status == "true" ){

            //             const limit = res.data.data;

            //             // 信用盘账户 无 赛事串关赔付 和 盘口串关赔付
            //             if( this.userInfo.tester == 2 || this.userInfo.tester == 3  ){
            //                 // limit.simple_max = Infinity;
            //                 // limit.mix_prize_limit = Infinity;
            //                 // limit.match_level_comp_limit = Infinity;

            //                 limit.match_comp_limit = Infinity;
            //                 limit.mkt_comp_prize_limit = Infinity;

            //             }


            //             // 添加局内串数据
            //             let complex_max =  Number(limit.simple_max)/( Number(item.odd) - 1 ); // 单注串关赔付/（总赔率-1）
            //             complex_max = Number(limit.prize_static_profit) > complex_max ? complex_max : Number(limit.prize_static_profit);
            //             const rnd_comp_left = limit.rnd_comp_limit_status == 1 ? limit.rnd_comp_left : Infinity; // 会员当日局内串关亏损剩余金额

            //             this.$store.commit("SET_CREDIT_LIMIT", {
            //                 credit_limit_2: limit.credit_limit_2,
            //                 credit_limit_3: limit.credit_limit_3,
            //                 credit_limit_4: limit.credit_limit_4,
            //                 credit_limit_5: limit.credit_limit_5,
            //                 credit_limit_6: limit.credit_limit_6,
            //                 credit_limit_7: limit.credit_limit_7,
            //                 credit_limit_8: limit.credit_limit_8,
            //                 credit_limit_9: limit.credit_limit_9,
            //                 credit_limit_10: limit.credit_limit_10,
            //                 credit_agent_match_limit: limit.credit_agent_match_limit,
            //                 credit_simple_max: limit.credit_simple_max,
            //                 rnd_comp_left: rnd_comp_left
            //             });

            //             this.$store.commit("UPDATE_STATION_MAXODDS", limit.rnd_comp_max_odd);

            //             const win_balance = limit.win_balance; // 会员当日派彩剩余金额
            //             this.$store.commit("UPDATE_STATION_LIMIT", {
            //                 key: betData.round,
            //                 item_id: item.id,
            //                 complex_min: parseInt(limit.complex_min),
            //                 complex_max: complex_max,
            //                 isFull: complex_max < parseInt(limit.complex_min),
            //                 rate_max_prize: limit.rnd_comp_max_prize < 0 ? 0 : limit.rnd_comp_max_prize,
            //                 win_balance: win_balance, 
            //             })
            //         }
            //     })
            // }
            
        },

        // 生成局内串关列表
        stationBetList(data){
            let list = lodash.cloneDeep(this.stateStationList)
            const { key, val, limit } = data;
            const hasObj = list[key];
            if( hasObj ){
                // 判断是否是重新添加
                const hasItem = hasObj.list.find( ele => ele.item_id == val.item_id );
                if( hasItem ){
                    list[key].list = hasObj.list.filter( ele => ele.item_id != val.item_id );
                    // 清除十局提示
                    if( list[key].list.length == 9 ){
                        this.$store.commit("SET_STATION_MAX", false)
                    }
                    // 删除完所有订单，删除整局
                    if( list[key].list.length == 0 ){
                        list = lodash.omit( list, [key] )
                    }

                }else{
                    if( this.stationList[key].canStation.includes(4) ){
                        return false
                    }

                    list[key].list = [
                        val,
                        ...list[key].list,
                    ]
                }
            }else{
                list = {
                    ...list,
                    [key]: {
                        bet_amount:"", // 投注金额
                        profitable:0, // 预计返还
                        relProfitable: 0,  // 实际预计返还
                        odds: val.odds, // 赔率
                        tournament_name: val.tournament_name,
                        game_id: val.game_id,
                        round: val.round,
                        T1: val.T1,
                        T2: val.T2,
                        cn_name: val.cn_name,
                        en_name: val.en_name,
                        item_cn_name: val.item_cn_name,
                        item_en_name: val.item_en_name,
                        game_name: val.game_name,
                        odd_rate: val.odd_rate,
                        match_id:  val.match_id,
                        complex_min: limit.complex_min,
                        rate_max_prize: limit.rate_max_prize,
                        is_live: val.is_live,
                        oddLimit: false, // 赔率超过最大，限制投注框
                        category: val.category,
                        list: [{
                            ...val,
                            complex_max: limit.complex_max,
                        }]
                    }
                }    
            }
            return list;
        },

         // 投注
         submitBet: lodash.throttle(async function(betData, types, callback) {
            
            // 注单数量
            const lens = betData.length;

            // 是否形成有效注单
            if (!lens) { return; }

            // 提交按钮显示
            this.submitload = true;
            
            // 普通注单数
            const singleLen = betData.filter(item=>item.indexOf("bt=1") !==-1 ).length;

            // 投注是否包含串注单
            const hasCorssBet = JSON.stringify(betData).includes("bt=2");
            const corrsLen = betData.filter(item=>item.indexOf("bt=2") !==-1 ).length;

            // 投注是否包含串单句串关
            const hasStationBet = JSON.stringify(betData).includes("bt=3");
            const stationLen = betData.filter(item=>item.indexOf("bt=3") !==-1 ).length;

            await gameApi.requestBet({ c: lens, b: betData, types }).then(res => {
                if (res.data.status == "true") {
                    this.$message({
                        message: i18n.t('common.mixins.finish'),
                        duration: 2000,
                        getContainer: this.isFullscreen ? document.querySelector('#full') : ''
                    });

                    if(hasCorssBet){
                        this.$store.commit("SET_DEL_CART"); // 包含串注注单 删除全部注单
                        // 投注成功的串注投注注单数
                        // this.$store.commit("SET_TRACK", { tab: 46, page: 6, number: corrsLen });
                        this.$store.commit("SET_CORSS_QUICK", false);
                    }else if(hasStationBet){
                        // 投注成功的局内串关注单数
                        // this.$store.commit("SET_TRACK", { tab: 48, page: 6, number: stationLen });
                        this.$store.commit("SET_STATION_QUICK", false);
                    }else{
                        this.$store.commit("SET_PART_CART");// 只有普通注单 删除已经投注的注单
                        // 投注成功的串注投注注单数
                        // this.$store.commit("SET_TRACK", { tab: 44, page: 6, number: singleLen });
                        this.$store.commit("SET_SINGLE_QUICK", false);
                    }
                    
                   
                    if(callback){
                        callback(betData[betData.length-1], betData)
                    }

                    gtag('event', 'purchase', {
                        currency: 'CNY',
                        transaction_id: `T_${Math.random()}`,
                        value: 1,
                    });
                } else {
                    console.log( res.data.data, this.isFullscreen )
                    this.$toast({
                        message: res.data.data,
                        duration: 2000,
                        customClass: 'bet-status-tips',
                        getContainer: () => this.isFullscreen ? document.querySelector('#full') : document.body
                    });
                }
            }).catch(err => {
                console.error( err )
                this.$toast({
                    message: i18n.t('common.mixins.betFail'),
                    duration: 2000,
                    customClass: 'bet-status-tips',
                    getContainer: () => this.isFullscreen ? document.querySelector('#full') : document.body
                });

            });
            setTimeout( () => {
                this.submitload = false;
            }, 200);
        },1000)
    }
};



// 串注格式
// c:2
// b[0]:a=20&bt=2&t=2&b=赛事1,盘口1,投注项1,投注项赔率1|赛事2,盘口2,投注项2,投注项赔率2|赛事3,盘口3,投注项3,投注项赔率3
// b[1]:a=20&bt=2&t=2&b=赛事1,盘口1,投注项1,投注项赔率1|赛事2,盘口2,投注项2,投注项赔率2|赛事3,盘口3,投注项3,投注项赔率3

// 格式说明：
// c:投注总注数                                                              
// b:投注内容,竖线分割,各个基本信息逗号分割  如： |赛事,盘口,投注项,投注项赔率|赛事,盘口,投注项,投注项赔率
// a:金额                                                                                
// bt:投注类型 1普通投注 2 串关投注
// t:串关类型 1:单注  2:2串1  3:3串1 4:4串1 5:5串1 6:6串1 7:7串1 8:8串1

// 例如：
// c: 3
// b[0]: bt=2&t=2&a=300&b=13644913956544994,13645073014582404,13645073014611962,6.000|13645022853392896,13645335882060726,13645335882099845,1.900
// b[1]: bt=2&t=2&a=300&b=13644913956544994,13645073014582404,13645073014611962,6.000|13644949559408871,13645129703549290,13645129703626530,1.900
// b[2]: bt=2&t=2&a=300&b=13645022853392896,13645335882060726,13645335882099845,1.900|13644949559408871,13645129703549290,13645129703626530,1.900
