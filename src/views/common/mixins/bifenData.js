import subHead from '@/views/markets/data/subHead'
import Echarts from "@/views/markets/data/echarts";
import { mapGetters } from 'vuex'
export default {
    components: {
        Echarts,
        subHead,
    },
    computed:{
        ...mapGetters(['bifenMatchData', 'matchs']),
        t1(){
            return this.$lodash.get(this.bifenMatchData, 'team[0]')
        },
        t2(){
            return this.$lodash.get(this.bifenMatchData, 'team[1]')
        },
    },
    data(){
        return {
            viewId: null,
            global,
        }
    },
    methods:{
        viewDatail(id) {
            id == this.viewId? this.viewId = null : this.viewId = id
        },
        // 输出占比
        getDamage(num, type){
            const total = this.$lodash.get(this.bifenMatchData, `team[${type}].damage`)
            return !total? 0 : (num/total*100).toFixed(2)
        },
        // 承伤占比
        getTanking(num, type){
            const total = this.$lodash.get(this.bifenMatchData, `team[${type}].tanking`)
            return !total? 0 : (num/total*100).toFixed(2)
        },
        // 参团率
        getTeam_fight(num){
            return (num*100).toFixed(2)
        },
        // dota总经验
        getEXP(players){
            let total = 0
            if(players) {
                players.forEach( item => {
                    total += item.exp
                })
            }

            // for(var i = 0; i < players.length; i++) {
            //     total += players[i].exp
            // }
            return (total/1000).toFixed(1)
        },
        changeLableLang(data){
            const lang = localStorage.getItem('language') || 'cn'
            return ['cn', 'zh'].includes(lang)? this.$lodash.get(data, "name") : this.$lodash.get(data, "name_en")
        }
    }
}