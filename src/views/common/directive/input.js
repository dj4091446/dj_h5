const trigger = (el, type) => {
    const e = document.createEvent('HTMLEvents');
    e.initEvent(type, true, true);
    el.dispatchEvent(e)
};
const changeCurrentValue = (val, digits) => {
    let sNum = val.toString(); //先转换成字符串类型
    if (sNum.indexOf('.') === 0) {
        //第一位就是
        sNum = '0' + sNum
    }
    sNum = sNum.replace(/[^\d.]/g, ''); //清除“数字”和“.”以外的字符
    sNum = sNum.replace(/\.{2,}/g, '.'); //只保留第一个. 清除多余的
    sNum = sNum
        .replace('.', '$#$')
        .replace(/\./g, '')
        .replace('$#$', '.');
    let reg = '^(\\-)*(\\d+)\\';
    if (digits > 0) {
        reg += `.(\\d{0,${digits}})`
    }
    reg += '.*$';
    sNum = sNum.replace(new RegExp(reg), digits > 0 ? '$1$2.$3' : '$1$2'); //只能输入两个小数
    //以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
    if (sNum.indexOf('.') < 0 && sNum !== '') {
        sNum = parseFloat(sNum)
    }
    return sNum
};

// 输入宽输入小数点和整数
const digit = {
        bind: function(el, binding) {
            let digits = binding.value.digits
            let maxMin = binding.value.maxMin;
            const keyupFn = function() {
                const value = Number(el.value);
                el.value = changeCurrentValue(el.value, digits);
                // 大于最大值
                if( value > maxMin.max ){
                    el.value = maxMin.max
                }
                trigger(el, 'input');
          
            };
            const blurFn = function(){
                const value = Number(el.value);
                el.value = changeCurrentValue(el.value, digits);
                // 小于最小值
                // if (value && value < maxMin.min) {
                //     el.value = maxMin.min
                // }
                trigger(el, 'input');
            
            }
            el.onkeyup = keyupFn;
            el.onblur = blurFn;
        },
};

export {
    digit
}



