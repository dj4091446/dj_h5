import modal from './modal.vue'
const sysModal = {};
sysModal.install = Vue => {
    // 扩展 vue 插件
    const ToastCon = Vue.extend(modal)
    const ins = new ToastCon()
    // 挂载 dom
    ins.$mount(document.createElement('div'))
    // 给 vue 原型添加 toast 方法
    Vue.prototype.$sysModal = (msg) => {
        const { message, getContainer, btnName, title, link, cancel, center, overlay } = msg;
        // 添加到 body 后面
        const dom = getContainer || document.body;
        dom.appendChild(ins.$el);
        ins.message = message;
        ins.visible = true;
        ins.btnName = btnName;
        ins.title = title;
        ins.link = link;
        ins.cancel = cancel;
        ins.center = center;
        ins.overlay = overlay;
    }
}
export default sysModal