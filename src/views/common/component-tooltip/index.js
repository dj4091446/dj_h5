import CommonPopup from './common-popup.vue'
import i18n from '@/assets/i18n/index'

const componentTooltip = {
    install(Vue) {
        Vue.component('CommonPopup', CommonPopup)

        // 实例方法
        Vue.prototype.$toolTipShow = function(content, options) {
            // 1. 创建构造函数
            const ComponentClass = Vue.extend(CommonPopup)
            // 2. 创建组件
            const instance = new ComponentClass({
                i18n,
                propsData: {
                    trigger: options.container || this.$el,
                    isFixed: options.isFixed || false, // 如果目标元素有定位不在文档流里 回影响定位的准确性 需要特殊处理
                    content: content,
                    ...options,
                },
            })
            // 3. 挂在组件
            document.body.appendChild(instance.$mount().$el)
            // 显示组件 a. 先挂载，后显示 -- 动画
            instance.show = true
        }
    },
}

export default componentTooltip
