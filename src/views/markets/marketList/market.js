import { mapActions, mapGetters } from 'vuex';
import lodash from "lodash";

import mixAddCart from "@/views/common/mixins/mixAddCart.js";
import mixMatchStatus from "@/views/common/mixins/mixMatchStatus.js";
import mixBetStatus from "@/views/common/mixins/mixBetStatus.js";
import commonNotData from "@/views/common/commonNotData";
import { groupingData,timetrans, getAntDataGameId} from '@/assets/scripts/utils'
import listLoad from "@/views/common/listLoad"

import bigPicture from "../bigPicture"
import listItem from "../listItem"
import marketHeader from "../market-header"
import foresight from "../foresight";
import data from "../data"
import schedule from "../schedule"
import { requestBifenTabDataStatus } from "@/api/data.js";



export default {
    mixins: [ mixAddCart, mixMatchStatus, mixBetStatus],
    components:{ commonNotData, marketHeader, listItem, listLoad, bigPicture},
    data(){
        const {device, os, browser} = this.Browser
        const MA = device === 'Mobile' && os === 'Android'
        const browsers = ['UC', 'Quark', 'Safari']
        const AndroidOS = os === 'Android'
        return{
            UPLOAD_URL,
            activeMenu: 0,
            smallHeader: false,
            noCoverBtn: MA && browsers.includes(browser),
            descriptionShow: false,
            description: this.$t('marketPage.stationtips'),
            marketLoading: true,

            // 只有四个游戏有玩法标签
            playGameArr:[
                "257154660915053", // 英雄联盟
                "257289795134339", // DOTA2
                "257561197207055", // 王者荣耀
                "257578064923863", // CSGO
            ],
            lockVideo: false,//锁定视频
            AndroidOS,
            bigPictureShow: false,
            descriptionShow1: false,
            bigPictureImages: [],
            // 主播盘口是否显示图片
            showPic:{
                1: true,
                2: true,
                3: true,
                4: true,
                5: true,
                6: true,
                7: true,
                8: true,
                9: true,
                10: true,
                11: true,
            },
            // 正常盘口控制待结算已结算盘口显示隐藏
            showMkt:{
                0: false,
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false,
                10: false,
                11: false,
                100: false,
            },

            // 局内串关控制待结算已结算盘口显示隐藏
            showMktStation:{
                0: false,
                1: false,
                2: false,
                3: false,
                4: false,
                5: false,
                6: false,
                7: false,
                8: false,
                9: false,
                10: false,
                11: false,
                100: false,
            },
            // marketRoundFinish: false,
            fromName: '', // 详情页入口页面
            i18n: this.$t('marketPage.index'),
            i18nItem:  this.$t('homePage.listItem'),
            i18nA: this.$t("antDataSource"),
            showPlayType: false, // 是否显示玩法分类
            playList:[ // 玩法分类
                { val: 0, name: this.$t("homePage.listItem.common") },
                { val: 1, name: this.$t("homePage.listItem.trend") },
                { val: 2, name: this.$t("homePage.listItem.special") },
                // { val: 3, name: this.$t("homePage.listItem.mix") },
                { val: 4, name: this.$t("homePage.listItem.hoPTarlay") },
            ],
            
            showPlayListGame: [
                "257154660915053", // 英雄联盟
                "257289795134339", // DOTA2
                "257561197207055", // 王者荣耀
                "257578064923863", // CSGO
                // "257697372071315", // 篮球
                // "21363218739142692", // 激斗峡谷
            ],
            activeRightNav: 0, // 激活的菜单
            win_rate:{ // 奔驰胜率
                t1:"",
                t2:""
            } ,
            isShowForesightTab: false, // 前瞻tab显隐状态
            isShowDatatTab: false, // 数据tab显隐状态
            isShowScheduleTab: false, // 实时tab显隐状态
        }
    },

    watch:{
        $route(v){
            const { id } = v.params;
            this.getMarketList(id)
        },
        matchs(n) {
            // 获取数据tab是否有数据
            this.getDataTabStatus()
            if (!this.dataTabStatusTimer) {
                this.dataTabStatusTimer = setInterval(() => this.getDataTabStatus(), 60000)
            }
        },
        "matchs.is_open_match"(v, ov) {
            if(!v){
                this.$store.commit("SET_GAME_ID", 0);
                // 清楚数据
                this.$router.push("/home");
            }
        },
        rightNav(n, o) {
            // 动态导航，如果删掉是导航栏第一个tab，则激活第二个tab
            if( n.length && n.length < o.length ){
                const delCurNav = n.find( ele => ele.key == this.activeRightNav );
                if( !delCurNav ){
                    this.activeRightNav = n[0].key;
                }
            }
        }
    },

    created(){
        this.getDataTabStatus()
    },



    computed:{
        ...mapGetters(["matchs", "marketList", "betItemId", "stationOpen" ,"stationList", "betCart", "playVideo", "anchorInfo", "showHostTips", "activePlay", "gameList", "matchTimer", "bifenBtnStatus"]),

        // 是否又热门玩法        
        hasPlayList1(){
            let arr = this.marketList.filter( ele => ele.detail_type == 1);
            return arr.length > 0
        },

        // 是否又特殊玩法        
        hasPlayList2(){
            let arr = this.marketList.filter( ele => ele.detail_type == 2);
            return arr.length > 0
        },

        // 是否又复合玩法       
        hasPlayList3(){
            let arr = this.marketList.filter( ele => ele.detail_type == 3);
            return arr.length > 0
        },

        // 是否又复热门局内串关     
        hasPlayList4(){
            let arr = this.marketList.filter( ele => ele.detail_type == 4);
            return arr.length > 0
        },

  
        stationID(){
            let arr = [];
            for( let key in this.stationList ){
                arr = [
                    ...arr,
                    ...this.stationList[key].list
                ]
            };
            return arr.map( ele => ele.item_id );
        },
        
        // 是否可以局内串关
        canStation(){
            const matchs = this.matchs;
            return matchs && matchs.is_pass_off==2 && matchs.visible==1 && matchs.status ==5
        },

        rightNav({ bifenBtnStatus, matchs, isShowForesightTab, isShowDatatTab, isShowScheduleTab }) {
            const nav = [{val: this.$t('antDataSource.Bet'), key: "0"}];
            if( this.matchs ){
                if (bifenBtnStatus.analyse_status && matchs && matchs.prospective_analysis_lock && isShowForesightTab ) {
                    nav.push({ val: this.$t('antDataSource.Prescient'), key: "1" })
                }
                if (bifenBtnStatus.real_time_status && matchs && matchs.real_time_data_lock && matchs.is_live == 2 && isShowDatatTab) {
                    nav.push({ val: this.$t('antDataSource.Data'), key: "2" })
                }
                if( bifenBtnStatus.live_status && matchs.live_lock && matchs.is_live == 2 && isShowScheduleTab){
                    nav.push({ val: this.$t('antDataSource.Scene'), key: "3" })
                }
            }
            return nav
        },


        // 当前渲染    
        componentId() {
            const path = this.activeRightNav;
            switch(path){
                case '1':
                    return foresight;
                case '2':
                    return data;
                case '3':
                    return schedule;       
            }
        },
    },

    methods:{
        timetrans,

        ...mapActions(["fetchMarketList", "fetchAnchorInfo",]),

        // 获取数据源各tab类型是否有数据
        getDataTabStatus() {
            if (this.matchs && this.matchs.id) {
                requestBifenTabDataStatus({
                    id: this.matchs.id,
                    stage_id: '1', // 第几局（默认1）
                    game_id: getAntDataGameId(this.matchs.game_id),
                    team_ids: this.matchs.home_team + ',' + this.matchs.away_team
                }).then((res) => {
                    if (res.data.status === "true") {
                        const _data = res.data.data
                        // 此页签重新显示后,无需再次隐藏页签
                        if (!this.isShowForesightTab) {
                            this.isShowForesightTab = _data.micro_analysis || _data.complete_analysis || _data.analysis_fight
                        }
                        if (!this.isShowDatatTab) {
                            this.isShowDatatTab = _data.match_data
                        }
                        if (!this.isShowScheduleTab) {
                            this.isShowScheduleTab = _data.match_event
                        }
                        if (this.isShowForesightTab && this.isShowDatatTab && this.isShowScheduleTab && this.dataTabStatusTimer && this.dataTabStatusTimer) {
                            clearInterval(this.dataTabStatusTimer)
                        }
                    }
                }).catch((err) => {
                    console.error(err);
                });
            }
        },

        // 背景图片
        h5sec(){
            const img = global.GameImages[this.matchs.game_id];
            return UPLOAD_URL + (img ? img.h5_sec : "")
        },
        getDuration() {
            const len = this.matchs.count || 0
            if (len <= 30) return 0.3
            if (len <= 50) return 0.2
            if (len <= 70) return 0.1
            if (len > 70) return 0.05
        },

        


        //切换视频锁定
        lockSwitch:lodash.throttle(function() {
            this.lockVideo = !this.lockVideo;
            window.scrollTo(0,0)
            if( this.lockVideo ){
                const $height1 = document.querySelector('.mkt-nav').clientHeight;
                document.querySelector(".market-line").style.marginTop=`${$height1}px`
            }else{
                document.querySelector(".market-line").style.marginTop=`0px`
            }
           
        }, 300),

        setLock(){
            this.lockVideo = false;
            const marketlineDom = document.querySelector(".market-line");
            if(marketlineDom){
                document.querySelector(".market-line").style.marginTop=`0px`
            }
            
        },

        offsetTop(){
            const isHost = this.matchs.category == 5 && this.showHostTips && this.matchs.is_live == 2 && this.matchs.category == 5 && this.matchs.status == 5; 
            const isAnt = this.rightNav.length > 1 && this.matchs.ant_match_id != 0;
            const antNum = isAnt ? 9.6 : 0;
            if(this.lockVideo){
                if(isHost){
                    return `${76.53334 + 9.6 + antNum}vw`;
                }else{
                    return `${76.53334 + antNum}vw`;
                }
            }else{
                if(isHost){
                    return (this.playVideo || this.canStation) ? `${20.53334 + 9.6 + antNum}vw` : `${13.06667 + 9.6 + antNum}vw`;
                }else{
                    return (this.playVideo || this.canStation) ? `${20.53334 + antNum}vw` : `${13.06667 + antNum}vw`;
                }
            }
        },

        //主播盘信息
        getAnchorInfo(matchID){
            this.fetchAnchorInfo({ids: matchID})
        },

        getAnchorDetail(key){
            const obj = this.anchorInfo.find( ele => ele.round == key );
            return obj || {};
        },

        // 主播盘每局时间
        getHostTimer(key){
            const timeNum = Number(this.getAnchorDetail(key).start_time);
            if(timeNum){
                const time = timetrans(timeNum)
                return time.y + time.d
            }else{
                return ""
            }
        },

        chunkFn(val){
            return lodash.chunk(val,2)
        },

        picList(key){
            const anchorInfo = lodash.cloneDeep(this.anchorInfo);
            let pic = (anchorInfo.find( ele => ele.round == key ) || {}).images;
            (pic || []).sort( (a, b) => {
                return a.sort_code - b.sort_code
            })
            return pic
        },

        bigPictureFn(obj){
            this.trackPic();
            this.bigPictureImages = obj
            this.bigPictureShow = true
        },

         // 图片显示隐藏
        handlerPic(key){
            this.trackPic();
            this.showPic[key] = !this.showPic[key];
        },

        trackPic(){
            if(this.matchs.status === 7){
                // 赛果赛果照片的点击次数统计
                // this.$store.commit("SET_TRACK", { tab: 27, page: 3 });   
            }else{
                // 赛事赛果照片的点击次数统计
                // this.$store.commit("SET_TRACK", { tab: 21, page: 2 });
            }
        },


        handlerStaMkt(key){
            const isShow =  this.showMktStation[key]
            this.$set(this.showMktStation, key, !isShow )
        },



        changeShowHostTips(){
            this.$store.commit("SET_SHOW_HOSTTIPS", !this.showHostTips)
        },


        // 是否显示玩法分类项
        showPlayItem(val){
            if( val == 1 ){
                return this.hasPlayList1
            }
            if( val == 2 ){
                return this.hasPlayList2
            }
            if( val == 3 ){
                return this.hasPlayList3
            }
            if( val == 4 ){
                return this.hasPlayList4
            }
            return true
        },
        changeRightNav(key){
            this.smallHeader = false;
            this.activeRightNav = key
        },
    },

    beforeDestroy() {
        this.$store.commit("SET_MATCH_ID", 0);
        this.$store.commit("SET_MARKES_LIST",[])
        this.$store.commit("SET_STATION_STATUS", false);
        this.$store.commit("SET_VIDEO_PLAY", false);
        this.$store.commit("SET_ANCHOR_INFO", []);
        this.$store.commit("SET_MATCH_GAME_ID", 0);
        this.$store.commit("SET_ACTIVE_PLAY", 0)
        this.$bus.$off('foresightData')
        this.dataTabStatusTimer && clearInterval(this.dataTabStatusTimer)
    },
    beforeRouteEnter(to, from, next) {
        next((vm) => {
            vm.fromName = from.name;
        });
    },

}