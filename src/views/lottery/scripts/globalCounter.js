import store from '@/store'
import { allLotteryList, getTeSeTicketType } from '@/views/lottery/util/lottery'

const activedCounter = []
const deactivedCounter = []
const lotteryCounterMap = store.state.lottery.lotteryCounterMap
const counterInstanceVMMap = {}
let intervalHandle = null
const dataLodingStateMap = {}
/**
 * 注册定时器
 * @param {*} counterObj 
 */
export function register(ticketId, vm) {
    if (!ticketId) {
        throw Error('register error, ticket is not right ', ticketId)
    }
    // console.info(`GlobalCounter register ${ticketId}`)

    if (!activedCounter.includes(ticketId)) {
        activedCounter.push(ticketId)
    }
    addCounterInstance(ticketId, vm)

    const counterObj = lotteryCounterMap[ticketId]
    if (!counterObj) {
        const isLoding = dataLodingStateMap[ticketId]
        if (isLoding) {
            // 数据加载中
            return
        }

        dataLodingStateMap[ticketId] = true
        setTimeout(() => {
            store.dispatch('fetchCurrentIssue', {
                ticketIds: ticketId,
            }).finally(() => {
                dataLodingStateMap[ticketId] = false
            })
        }, 501)
    }
}

/**
 * 取消定时器
 * @param {*} counterObj 
 */
export function unregister(ticketId, vm) {
    if (!ticketId) {
        return;
        // throw Error('unregister error, ticket is not right ', ticketId)
    }
    // console.info(`GlobalCounter unregister ${ticketId}`)

    // 去激活
    const idx = activedCounter.indexOf(ticketId)
    if (idx != -1) {
        activedCounter.splice(idx, 1)
    }
    removeCounterInstance(ticketId, vm)
}

function addCounterInstance(ticketId, vm) {
    if (!counterInstanceVMMap[ticketId]) {
        counterInstanceVMMap[ticketId] = []
    }

    const addedVMs = counterInstanceVMMap[ticketId]
    if (!addedVMs.includes(vm)) {
        counterInstanceVMMap[ticketId].push(vm)
    }
}

function removeCounterInstance(ticketId, vm) {
    if (!counterInstanceVMMap[ticketId]) {
        return
    }

    const vmArr = counterInstanceVMMap[ticketId]
    // console.info(`GlobalCounter before removeCounterInstance `, vmArr.length, vmArr)
    const idx = vmArr.indexOf(vm)
    if (idx !== -1) {
        vmArr.splice(idx, 1)
    }
    // console.info(`GlobalCounter after removeCounterInstance `, vmArr.length, vmArr)
}

export function startTick() {
    const ticketIds = Object.keys(lotteryCounterMap)
    ticketIds.forEach(ticketId => {
        // 无实例 不执行
        if(!(counterInstanceVMMap[ticketId] && counterInstanceVMMap[ticketId].length)) {
            return
        }
        const counterObj = Object.assign({},lotteryCounterMap[ticketId]);
        counterObj.remain--;
        if (counterObj.remain < 0) {
            counterObj.remain = 0;
        }
        if (counterObj.remain == 3) {
            triggerEvent(ticketId, 'lastThreeSeconds')
        }
        if (counterObj.remain == 0 && counterObj.total > 0 && !counterObj.ended) {
            if(counterObj.defaultOpen) {
                if(!counterObj.fengpanRemain) {
                    counterObj.fengpanRemain = counterObj.advanceStopBetTime
                    counterObj.fengpaning = true
                } else {
                    counterObj.fengpanRemain--
                    if(counterObj.fengpanRemain<=0) {
                        counterObj.ended = true
            
                        if(ticketId) {
                            loadData(ticketId, true)
                        }
                    }
                }
            } else {
                counterObj.ended = true
                if(ticketId) {
                    loadData(ticketId)
                }
            }
        }
        store.commit('setLotteryCounterMap', counterObj)
    })
}

function loadData(ticketId, fengpan) {
    const isLoding = dataLodingStateMap[ticketId]
    if (isLoding) {
        // 数据加载中
        return
    }

    triggerEvent(ticketId, 'countDownEnd')
    dataLodingStateMap[ticketId] = true
    const loadIssueFn = function () {
        setTimeout(() => {
            store.dispatch('fetchCurrentIssue', {
                ticketIds: ticketId,
            }).catch(e => {
                loadIssueFn()
            }).finally(() => {
                dataLodingStateMap[ticketId] = false
            })
        }, fengpan ? 1501 : 501)
    }
    loadIssueFn()
}

function triggerEvent(ticketId, eventName) {
    const VMs = counterInstanceVMMap[ticketId]

    VMs && VMs.forEach(vm => {
        vm.$emit(eventName, ticketId)
    })
}

export function init() {
    // console.info(`GlobalCounter 全局定时器初始化`)
    
    // console.info(`GlobalCounter 启动定时器`, new Date())
    clearInterval(intervalHandle)
    // 整毫秒数启动定时器，多个页面上同步定时器心跳，一起改变时间
    const milliSeconds = new Date().getMilliseconds()
    setTimeout(() => {
        // console.info(`GlobalCounter 时间差为 ${window.serverTimerGap}`)
        startTick()

        intervalHandle = setInterval(() => {
            startTick()
        }, 1000)
    }, milliSeconds)
}

// TEST
window.GlobalCounter = {
    counterInstanceVMMap,
    activedCounter,
    lotteryCounterMap,
}
