import Audios from '@/assets/scripts/Audios.js'
import store from '@/store'
let isIos = document.querySelector("#root")?.classList.contains('ios');
/**
 * 初始化，背景音乐，和点击音效
 * 避免太多卡主流程
 */
const AudioInit = () => {
  let arr = arr = [
    // {
    //   id: 'lol-bg',//背景音乐
    //   autoplay: false,
    //   loop: true,
    //   volume: 0.0,
    //   src: require('../../assets/audios/lol/bgm.mp3').default,
    //   path:"/assets/audios/lol/bgm.mp3",//用来避免出现重复路径，id却不一样的情况的，必填项
    //   isBg:true//和其他音效做区分
    // },
  ]
  if (isIos) {
    arr = [
      {
        id: 'lol-bg',//背景音乐
        autoplay: false,
        loop: true,
        volume: 0.0,
        src: require('../../assets/audios/lol/bgm.mp3').default,
        path:"/assets/audios/lol/bgm.mp3",//用来避免出现重复路径，id却不一样的情况的，必填项
        isBg:true//和其他音效做区分
      }
    ]
  }

  for (let i = 0; i < arr.length; i++) {
    let item = { ...arr[i] }
    if (item && !Audios.pools[item.id]) {
      Audios.create(item)
    }
  }
}
/**
 * 初始化非背景音乐
 */
const AudioInitNoBgMusic = () => {
  let arr = [];
  if (!isIos) {
    arr = [
      {
        src: require(`../../assets/audios/lol/wait-open.mp3`).default,
        id: 'lol-wait-open',
        path:"/assets/audios/lol-wait-open.mp3"
      },
      {
        src: require(`../../assets/audios/lol/jt.mp3`).default,
        id: 'lol-jt',
        path:"/assets/audios/lol-jt.mp3"
      },
    ]
    for(let i = 2; i <= 13; i++) {
      if(store.state?.setting?.lang === 'cn' || store.state?.setting?.lang === 'zh') {
        arr.push({
          src: require(`../../assets/audios/lol/select/${i}1/zh_cn.mp3`).default,
          id: `lol-${i}1`,
          path:`/assets/audios/lol/select/${i}1/zh_cn.mp3`
      }, {
          src: require(`../../assets/audios/lol/select/${i}2/zh_cn.mp3`).default,
          id: `lol-${i}2`,
          path:`/assets/audios/lol/select/${i}2/zh_cn.mp3`
        })
      } else {
        arr.push({
          src: require(`../../assets/audios/lol/select/${i}1/en.ogg`).default,
          id: `lol-${i}1`,
          path:`/assets/audios/lol/select/${i}1/en.ogg`
        }, {
          src: require(`../../assets/audios/lol/select/${i}2/en.ogg`).default,
          id: `lol-${i}2`,
          path:`/assets/audios/lol/select/${i}2/en.ogg`
        })
      }
    }
  }
  for (let i = 0; i < arr.length; i++) {
    let item = { ...arr[i] }
    if (item && !Audios.pools[item.id]) {
      Audios.create(item)
    }
  }
}
//是否已经执行过播放动作
const canPlay = (id) => {
  try {
    let canplaynow = String(Audios.pools()[id].getAttribute('canplaynow'));
    return canplaynow == 'true';
  } catch (e) {
    console.error("canplay函数报错", e)
    return false;
  }

}
//在正确页面里面
const isRightHash = function () {
  return /#\/lottery/ig.test(location.hash);
}
//预播放
const bgPlayPrev = (volume = 0.0) => {
  //通过自定义标签属性，判断是否已经播放过
  //如果播放过说明，已经可以直接使用，不再调用预播放

  if (!canPlay('lol-bg')) {
    Audios.play('lol-bg', {
      volume: 0.0,
      muted: true
    })
  }
}
const playPrev = (volume = 0.0) => {
  if (isIos) {
    return;
  }
}

//选择筹码
export const AudioClickChip = () => {
  if (!isIos && isRightHash()) {
    // Audios.play('sicbo-button-click', {
    //   volume: 1.0,
    //   currentTime: 0,
    //   muted: false,
    // })
  }
}
//错误提示
export const AudioError = () => {
  if (!isIos && isRightHash()) {
    // Audios.play('sicbo-err', {
    //   volume: 1.0,
    //   currentTime: 0,
    //   muted: false,
    // })
  }
}
//投注
export const AudioBetting = () => {
  if (!isIos && isRightHash()) {
    // Audios.play('sicbo-Betting', {
    //   volume: 1.0,
    //   currentTime: 0,
    //   muted: false,
    // })
  }
}
//投注
export const AudioBackUp = () => {

  if (!isIos && isRightHash()) {
    // Audios.play('sicbo-chip-back', {
    //   volume: 1.0,
    //   currentTime: 0,
    //   muted: false,
    // })
  }
};
//最好5S倒计时
export const AudioTimeDown = () => {

  if (!isIos && isRightHash()) {
    // Audios.play('sicbo-time-down', {
    //   volume: 1.0,
    //   currentTime: 0,
    //   muted: false,
    // }) 
  }

};
export const AudioPauseAll = () => {
  let audioMap = Audios.pools();
  for (let key in audioMap) {
    let item = audioMap[key];
    item.pause();
  }
};

export default {
  init: AudioInit,
  AudioInitNoBgMusic: AudioInitNoBgMusic,
  tools: Audios,
  playPrev: playPrev,
  bgPlayPrev: bgPlayPrev,
  AudioClickChip,
  AudioError,
  AudioBetting,
  AudioBackUp,
  AudioTimeDown,
  AudioPauseAll
};