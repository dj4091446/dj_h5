let results = []
let result = []
function combination(arr, index, position) {
    for (var i = 0; i < arr[index].length; i++) {
        result[index] = arr[index][i]
        if (index !== arr.length - 1) {
            combination(arr, index + 1, position)
        } else {
            if (position) {
                // 判断是否有重复的数字
                const newArr = [...new Set(result)]
                if (newArr.length < result.length) {
                    continue
                }
            }

            results.push(result.join(','))
        }
    }

    return Math.sort(results)
}
// combination(ballNumArr, 0)

/**
 * 返回排列组合数组
 * @param {*} list 需要排列的数组
 * @param {*} position 是否区分位置 SYXW需要区分位置
 */
export function combinationList(list, size, position) {
    // reset
    results = []
    result = []

    if (list.length > 1) {
        return combination(list, 0, position)
    } else if (list.length === 1) {
        // 如果是一组,则需要从数组中选取几个数字进行排序
        return global.groupSplit(list[0], size)
    } else {
        console.error('排列数组不能为空')
    }
}

/**
 * 根据玩法返回排列组合结果
 * @param {Array} list 需要排列的数字
 * @param {*} play 玩法
 */
export function getCombinateListByPlay(list, play) {
    let result = null
    switch (play.playCode) {
        // SYXW
        case 'qianerzhixuan':
            result = combinationList(list, null, true)
            break
        case 'qiansanzhixuan':
            result = combinationList(list, null, true)
            break
        case 'renxuanyi':
            result = combinationList(list, 1, true)
            break
        case 'renxuaner':
        case 'erlianxiao':
        case 'erlianwei':
        case 'erquanzhong':
        case 'erzhongte':
        case 'techuang':
        case 'hexiao':
        case 'qianerzuxuan':
            result = combinationList(list, 2, true)
            break
        case 'renxuansan':
        case 'sanlianxiao':
        case 'sanlianwei':
        case 'sanzhonger':
        case 'sanquanzhong':
        case 'qiansanzuxuan':
        case 'zuliu':
            result = combinationList(list, 3, true)
            break
        case 'renxuansi':
        case 'silianwei':
        case 'silianxiao':
        case 'siquanzhong':
            result = combinationList(list, 4, true)
            break
        case 'renxuanwu':
        case 'wulianwei':
        case 'wulianxiao':
            result = combinationList(list, 5, true)
            break
        case 'renxuanliuzhongwu':
            result = combinationList(list, 6, true)
            break
        case 'renxuanqizhongwu':
            result = combinationList(list, 7, true)
            break
        case 'renxuanbazhongwu':
            result = combinationList(list, 8, true)
            break
        case 'zusan':
            result = combinationList(list, null, true)
            break
        default:
            result = combinationList(list, null, false)
            break
    }

    return result
}
