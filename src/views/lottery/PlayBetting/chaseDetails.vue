<template>
<div class="chase-detail-lol">
    <header>
        <span @click="$emit('hiddenDetails')">
            <i class="icon-back"></i>
        </span>
        <span>{{ i18n.tab.traceDetail}}</span>
        <span></span>
    </header>
    <div class="game-details trace-details">
        <ul class="details-list">
            <li>
                <span>{{i18n.tab.orderNo}}：</span>
                <span>{{ gameDetails.ticketChaseNum | nameToMit }}</span>
                <span class="list-btn" @click="copyText(gameDetails.ticketChaseNum)">{{$t('yxzh.chaseRecords.copy')}}</span>
            </li>
            <li>
                <span>{{$t('yxzh.chaseRecords.chaseStartTime')}}：</span>
                <span>{{ getZoneTime(gameDetails.chaseStarttime) }} ({{zoneGMTStr}}) </span>
            </li>
            <li>
                <span v-if="['zh_cn', 'zh_tw'].includes(lang)">{{$t('yxzh.chaseRecords.lottery[0]')}}&#12288;&#12288;{{$t('yxzh.chaseRecords.lottery[1]')}}：</span>
                <span v-else>{{$t('yxzh.chaseRecords.lottery[0]')}}{{$t('yxzh.chaseRecords.lottery[1]')}}：</span>
                <span>{{ gameDetails.ticketName }}</span>
            </li>
            <li>
                <span>{{$t('yxzh.chaseRecords.startPeriod')}}：</span>
                <span>{{ gameDetails.chaseStart }}</span>
            </li>
            <li>
                <span v-if="['zh_cn', 'zh_tw'].includes(lang)">{{$t('yxzh.chaseRecords.wanfa[0]')}}&#12288;&#12288;{{$t('yxzh.chaseRecords.wanfa[1]')}}：</span>
                <span v-else>{{$t('yxzh.chaseRecords.wanfa[0]')}}{{$t('yxzh.chaseRecords.wanfa[1]')}}：</span>
                <span>{{ gameDetails.ticketPlay }}</span>
            </li>
            <li  class="ticketBetNum">
                <span>{{$t('yxzh.chaseRecords.chaseContent')}}：</span>
                <span ref="betContent" v-html="gameDetails.ticketBetNum">  </span>
                <span 
                    class="list-btn" 
                    v-show="showViewBtn"
                    @click="seeMoreBetNum(gameDetails.ticketBetNum)">{{$t('yxzh.chaseRecords.chakan')}}</span>
            </li>

            <li>
                <span>{{$t('yxzh.chaseRecords.chaseAllPeriods')}}：</span>
                <!-- <i style="opacity:0">期</i> 兼容oppo默认数字不对齐 -->
                <span>{{ gameDetails.chaseAllPeriods }} <i style="opacity:0">{{$t('yxzh.qi')}}</i> </span>
            </li>

            <li>
                <span>{{$t('yxzh.chaseRecords.chaseTotalMoney')}}：</span>
                <span>{{ gameDetails.chaseAllAmount | toFormatMoney }}{{$t('yxzh.yuan')}}</span>
            </li>
            <li>
                <span>{{$t('yxzh.chaseRecords.chaseFinishedPeriod')}}：</span>
                <!-- <i style="opacity:0">期</i> 兼容oppo默认数字不对齐 -->
                <span>{{ gameDetails.chaseDonePeriods }}<i style="opacity:0">{{$t('yxzh.qi')}}</i></span>
            </li>
            <li>
                <span>{{$t('yxzh.chaseRecords.chaseFinishedAmount')}}：</span>
                <span>{{ gameDetails.chaseDoneAmount | toFormatMoney }}{{$t('yxzh.yuan')}}</span>
            </li>
            <li>
                <span>{{$t('yxzh.chaseRecords.chaseCanceledPeriod')}}：</span>
                <!-- <i style="opacity:0">期</i> 兼容oppo默认数字不对齐 -->
                <span>{{ gameDetails.chaseCancelPeriods }}<i style="opacity:0">{{$t('yxzh.qi')}}</i></span>
            </li>
            <li :class="['list-state', 'list-state-trace']">
                <span>{{$t('yxzh.chaseRecords.chaseStatus')}}：</span>
                <span class="list-red">
                    {{ gameDetails.chaseStatusDes }}
                </span>
            </li>
            <li>
                <span>{{$t('yxzh.chaseRecords.chaseCancelAmount')}}：</span>
                <span>{{ gameDetails.chaseCancelAmount | toFormatMoney }}{{$t('yxzh.yuan')}}</span>
            </li>
            <li class="list-rule">
                <span>{{$t('yxzh.chaseRecords.chaseRule')}}：</span>
                <span>{{ gameDetails.isSuppend | toIsSuppend }}</span>
            </li>
            <li class="list-money" v-show="gameDetails.winAmount">
                <span> {{$t('yxzh.chaseRecords.winAmount')}}：</span>
                <span> {{ gameDetails.winAmount }}{{$t('yxzh.yuan')}} </span>
            </li>
        </ul>
        <div class="details-canCancel">
            <span class="canCancel-trace" @click="goEachTrace">{{$t('yxzh.chaseRecords.eachDetails')}}</span>
            <span v-show="cancelOptions == '1' && gameDetails.canCancel" @click="cancelOrderChase">{{$t('yxzh.chaseRecords.cancelChase')}}</span>
        </div>
    </div>
    <VanPopup class="lol-reportDetails-modal" position="right" v-model="showEachTrace">
        <each-trace @close="showEachTrace = false"></each-trace>
    </VanPopup>
</div>
    
</template>
<script>
import { mapState } from 'vuex'
import betNumModal from './betNumModal'
import orderStatus from '@/views/lottery/configure/mixin/orderStatus'
import { NavBar } from 'vant'
import eachTrace from './eachTrace'
import cancelOrder from '@/views/lottery/configure/mixin/cancelOrder'
export default {
    mixins: [orderStatus, cancelOrder],
    components: {
        NavBar,eachTrace
    },
    data() {
        return {
            showViewBtn: false,
            zoneGMTStr: window.zoneGMTStr,
            i18n: this.$t('yxzh'),
            showEachTrace: false,
        }
    },
    methods: {
        getZoneTime,
        cancelOrderChase() {
            this.traceCancelOrder()
        },
        goEachTrace() {
            this.showEachTrace = true
        },
        // 查看投注详情
        seeMoreBetNum(betNum) {
            this.$commonModal(betNumModal, {
                data: betNum,
                theme: 'down-close',
                onConfirm: e => {},
            })
        },
    },
    computed: {
        ...mapState({
            cancelOptions: state => state.lottery.cancelOptions,
        }),
        betScrollWidth() {
            return this.$refs.betContent.scrollWidth
        },
        betClientWidth() {
            return this.$refs.betContent.clientWidth
        },
        lang() {
            return this.$store.state.setting.lang;
        },
        gameDetails() {
            return this.getGameDetail
        },
    },
    async updated() {
        // await this.fetchTraceDetail(this.getCurrentOrder.chaseId || this.getCurrentOrder.orderId)
        this.$nextTick(() => {
            this.showViewBtn = this.betScrollWidth > this.betClientWidth
        })
    },
}
</script>
<style lang="less">
.chase-detail-lol {
    background-image: url(~@/assets/images/tese-lol/lol-bg.png);
    background-repeat: no-repeat;
    background-size: 100% 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
.details-list {
        font-size: 14px;
        li {
            overflow: hidden;
            padding: 11px 13px;
            border-bottom: 1px solid #445063;
            display: flex;
            align-items: center;
            position: relative;
            // &::after {
            //     content: '';
            //     position: absolute;
            //     height: 1px;
            //     width: 100%;
            //     bottom: 0px;
            //     left: 0;
            //     background: #bcc5da;
            //     transform: scaleY(0.2);
            // }
            span {
                font-size: 14px;
                &:nth-child(1) {
                    width: 75px;
                    text-align: right;
                    color: #8A93A2;
                }
                &:nth-child(2) {
                    width: 220px;
                    color: #fff;
                    overflow: hidden;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    // display: -webkit-box;
                    // -webkit-line-clamp: 2;
                    // word-break: break-all;
                    // -webkit-box-orient: vertical;
                    /*! autoprefixer: off */
                }
                &.list-btn {
                    position: absolute;
                    right: 13px;
                    top: 50%;
                    transform: translateY(-50%);
                    width: 60px;
                    height: 27px;
                    color: #ffffff;
                    line-height: 27px;
                    text-align: center;
                    font-size: 14px;
                    object-fit: contain;
                    background-image: linear-gradient(180deg, rgba(0,149,255,0.16) 2%, rgba(0,149,255,0.30) 100%);
                    border: 1px solid rgba(0,149,255,1);
                }
                &.list-red {
                    color: #e45e5e;
                }
            }
            &.list-state {
                & > span:nth-child(2) {
                    width: auto;
                    padding: 5px 8px;
                    border-radius: 4px;
                    background-color: #dddddd;
                    // ff9307
                    color: #666666;
                }
            }
            &.list-state-red {
                & > span:nth-child(2) {
                    color: #ffffff;
                    background-color: #fe6464;
                }
            }
            &.list-state-trace {
                & > span:nth-child(2) {
                    color: #ffffff;
                    background: rgba(231,187,2,0.10);
                    border: 0.5px solid rgba(231,187,2,1);
                    font-family: PingFangSC-Medium;
                    font-size: 14px;
                    color: #E7BB02;
                    text-align: center;
                    font-weight: 500;
                    border-radius: 0px;
                    line-height: 100%;
                    padding: 4px 3px;
                    text-align: center;
                    box-sizing: border-box;
                }
            }

            &.list-rule,
            &.list-money {
                & > span:nth-child(2) {
                    color: #e45e5e;
                }
            }
            &.odds {
                b {
                    display: block;
                    font-weight: normal;
                }
            }
            &.ticketBetNum {
                color: #333333 !important;
                i {
                    color: #333333 !important;
                }
            }
        }
    }
    .details-canCancel {
        padding: 10px;
        span {
            display: block;
            width: 348.3px;
            height: 42.7px;
            border-radius: 4px;
            color:#fff;
            font-size: 17px;
            text-align: center;
            line-height: 42.7px;
            margin: 5px auto;
            object-fit: contain;
            background-image: url(~@/assets/images/tese-lol/tb/bet-btn-big.png);
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }
        .canCancel-trace {
            background-color: #2E354A;
            background-image: url(~@/assets/images/tese-lol/tb/bet-btn-big.png);
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            color:#fff;
        }
    }
    
    .trace-details {
        overflow-y: auto;
        .details-list li span:first-child {
            width: 85px;
            text-align: right;
            white-space: nowrap;
        }
    }
    .icon-back {
        display: inline-block;
        width: 12px;
        height: 16px;
        background-image: url('~@/assets/images/tese-lol/tb/return.png');
        background-size: 100%;
        background-repeat: no-repeat;
    }
    .lol-reportDetails-modal {
        background-color: transparent;
        height: 100%;
    }
}
    
</style>