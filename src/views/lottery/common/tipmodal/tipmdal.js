import PopModalContainer from './popmodal-container'
import store from '@/store'
import i18n from "@/assets/i18n"

let instance = null
const TipModal = {
    install(Vue) {
        Vue.component('PopModalContainer', PopModalContainer)

        // 实例方法
        Vue.prototype.$tipModal = function(component, options) {
            if (instance) {
                instance.$destroy(true)
                document.body.removeChild(instance.$el)
            }

            // 1. 创建构造函数
            const ComponentClass = Vue.extend(PopModalContainer)
            // 2. 创建组件
            // console.info(`tipModal options ${JSON.stringify(options, null, 2)}`)
            instance = new ComponentClass({
                i18n,
                store,
                propsData: {
                    ...options,
                    content: component,
                },
            })

            // 3. 挂在组件
            document.body.appendChild(instance.$mount().$el)
            // 显示组件 a. 先挂载，后显示 -- 动画
            instance.show = true

            return instance
        }
        Vue.prototype.$tipModal.closeInstance = () => instance.show = false
    },
}

export default TipModal
