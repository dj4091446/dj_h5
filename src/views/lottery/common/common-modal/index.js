import CommonPopup from './common-popup.vue'
import store from '@/store'
import i18n from '@/assets/i18n/index'

let instance = null
const CommonModal = {
    install(Vue) {
        Vue.component('CommonPopup', CommonPopup)

        // 实例方法
        Vue.prototype.$commonModal = function(component, options) {
            if (instance) {
                instance.$destroy(true)
                document.body.removeChild(instance.$el)
            }

            // 1. 创建构造函数
            const ComponentClass = Vue.extend(CommonPopup)
            // 2. 创建组件
            instance = new ComponentClass({
                i18n,
                store,
                propsData: {
                    content: component,
                    ...options,
                },
            })

            // 3. 挂在组件
            document.body.appendChild(instance.$mount().$el)
            // 显示组件 a. 先挂载，后显示 -- 动画
            instance.show = true
        }
    },
}

export default CommonModal
