import MKTooltip from './index.vue'
import i18n from "@/assets/i18n"

const Tooltip = {
    install(Vue) {
        Vue.component('Tooltip', MKTooltip)

        /**
         * subComponent 子组件
         * options 参数
         *     data     传递的参数
         *     listener 监听的事件
         */
        Vue.prototype.$tooltip = (subComponent, options) => {
            // 1. 创建构造函数
            const ComponentClass = Vue.extend(MKTooltip)
            // 2. 创建组件
            const instance = new ComponentClass({
                i18n,
                propsData: {
                    trigger: options.trigger || this.$el,
                    subComponent,
                    ...options,
                },
            })
            // 3. 挂在组件
            // 挂载在container里
            options.container && options.container.appendChild(instance.$mount().$el)
            // document.body.appendChild(instance.$mount().$el)
            // 显示组件 a. 先挂载，后显示 -- 动画
            instance.show = true
            return instance
        }
    },
}

export default Tooltip
