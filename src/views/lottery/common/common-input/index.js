import commonInput from './commonInput.vue'
const commonInputplugIn = {
    install(Vue) {
        Vue.component('commonInput', commonInput)
    },
}

export default commonInputplugIn
