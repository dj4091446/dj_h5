import Loading from './index.vue'

const commonLoad = {
    install(Vue) {
        Vue.component('LoadingComponent', Loading)
    },
}
export default commonLoad
