import MKNumberKeyboard from './index.vue'

const NumberKeyboard = {
    install(Vue) {
        Vue.component('MKNumberKeyboard', MKNumberKeyboard)
    },
}

export default NumberKeyboard
