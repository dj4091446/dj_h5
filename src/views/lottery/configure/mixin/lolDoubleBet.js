import { mapState, mapGetters, mapActions } from 'vuex'
import * as LotteryApi from '@/api/teselol.js'
import * as SicboApi from '@/api/teselol.js'
import CheckFuShiModal from '@/views/lottery/bet/double/fushi/checkFuShiModal.vue'
import DoubleCheckModal from '@/views/lottery/PlayBetting/checkModal.vue'
import axios from 'axios'
import i18n from '@/assets/i18n';

const lottCom = i18n.t('yxzh');

export default {
    data() {
        return {
            submiting: false,
            timerHandler: null,
            cancelHandler: null,
            timeout: 120 * 1000, // 投注接口超时时间
            finalToast: null, // 投注流程结束时的toast，需要点击任何地方经行关闭，避免影响用户操作
        }
    },

    computed: {
        ...mapState({
            ticketId: state => state.lottery.ticketId,
            clearDoubleData: state => state.lottery.clearDoubleData,
            currentStatus: state => state.lottery.currentStatus,
            doubleBetQuickConfirmDialog: state => state.setting.doubleBetConfirm,
            dhistoryBetData: state => state.lottery.dhistoryBetData,
            betEntrance:state => state.lottery.betEntrance,
            openMmcDraw: state => state.lottery.openMmcDraw,
        }),
        ...mapGetters(['balance', 'isJKC']),
    },

    created() {
        document.addEventListener('click', this.handleToast, true)
    },
    destoryed() {
        document.removeEventListener('click', this.handleToast, true)
    },

    methods: {
        // ...mapActions(['fetchRecentPlay']),
        handleToast() {
            if (this.finalToast) {
                this.finalToast.clear()
                this.finalToast = null
            }
        },

        /**
         * 双面盘投注确认 二次确认
         * @param {Object} options 额外投注信息
         *                 issue      当前奖期
         *                 ticketName 彩种名称
         * @param {Function} cb 回调方法，不能使用promise替代（只能触发一次）
         */
        openDoubleCheckModal(options, cb) {
            const vm = this
            if (vm.doubleBetQuickConfirmDialog) {
                // 组和确认框
                if (vm.isFuShi) {
                    const playName = vm.betData.list.length
                        ? vm.betData.list[0].playMethodName
                        : vm.catalog.playName
                    vm.$tipModal(CheckFuShiModal, {
                        title: lottCom.bet.queren,
                        height: 'auto',
                        contentOpts: {
                            betData: {
                                ...vm.betData,
                                ...options,
                            },
                            methodName: playName,
                            defaultMoney: vm.amount,
                            onConfirm: betData => {
                                if (this.submiting) {
                                    // 避免重复提交
                                    return
                                }

                                // 点击确认投注
                                vm.doubleSubmit(betData)
                                    .catch(err => {
                                        cb(err, null)
                                    })
                                    .then(result => {
                                        cb(null, result)
                                    })
                            },
                        },
                    })
                } else {
                    this.$tipModal(DoubleCheckModal, {
                        title: lottCom.bet.queren,
                        rightText: lottCom.wayMap.empty,
                        height: 'lol',
                        contentOpts: {
                            betData: {
                                ...vm.betData,
                                ...options,
                            },
                            onConfirm: (betData, checkModal) => {
                                if (this.submiting) {
                                    // 避免重复提交
                                    return
                                }

                                // 点击确认投注
                                vm.doubleSubmit(betData)
                                    .then(result => {
                                        cb(null, result)
                                        // 关闭二次确认框
                                        checkModal.close()
                                    })
                                    .catch(err => {
                                        cb(err, null)
                                    })
                            },
                        },
                    })
                }
            } else {
                if (this.submiting) {
                    // 避免重复提交
                    return
                }
                return vm.doubleSubmit(
                    Object.assign(vm.betData, {
                        totalBets: vm.totalBets,
                        totalAmount: vm.totalAmount
                    }, options)
                )
                    .catch(err => {
                        cb(err, null)
                    })
                    .then(result => {
                        cb(null, result)
                    })
            }
        },

        doubleSubmit: async function(betData) {
            const isJKC = betData.isJKC
            const vm = this
            if (this.submiting) {
                return this.$toast(lottCom.bet.dontRepeat)
            }

            this.submiting = true
            // 重置定时器句柄
            clearTimeout(this.timerHandler)
            this.checkTimeout()

            var submitList = betData.list.map(item => {
                // prettier-ignore
                let playName = item.playItemName, realName
                let pDom = document.createElement('div')
                pDom.innerHTML = playName
                if(pDom.children.length){
                    realName = pDom.innerText
                } else {
                    realName = playName
                }

                // prettier-ignore
                return {
                    playId   : item.playItemId || item.id,   // 玩法ID
                    betNum   : item.betNum,                  // 投注号码
                    betAmount: parseFloat(item.amount),      // 投注金额
                    betCount : item.betCount || 1,           // 注单数
                    content  : realName,                     // 投注内容
                }
            })
            // prettier-ignore
            var data = {
                ticketId: betData.ticketId || this.ticketId,   // 彩种ID
                planNo  : betData.issue || 'null',             // 期数ID
                bet     : submitList,                          // 投注数据
            }

            return new Promise((resolve, reject) => {
                this.checkCurrentStatus(
                    betData.ticketId || this.ticketId,
                    betData.ticketName,
                    data.planNo,
                    isJKC
                ).then(planId => {
                    console.log('plan id:', planId)
                    // 立即投注没有传递期号，使用最新期号
                    data.planNo = planId
                       // 请求前重置句柄
                    this.cancelHandler = null
                    // prettier-ignore
                    if(!isJKC && this.dhistoryBetData.bet
                        &&data.bet.length===this.dhistoryBetData.bet.length
                        &&this.betEntrance===this.dhistoryBetData.currentPostButton
                        &&this.dhistoryBetData.planNo===data.planNo&&data.bet.every((item,index)=>{
                            const targetCompare = this.dhistoryBetData.bet[index]
                            const itemAomunt= item.amount || item.betAmount
                            const compareAomunt =  targetCompare.amount || targetCompare.betAmount

                            if(targetCompare
                                &&item.betNum===targetCompare.betNum
                                &&item.playId===targetCompare.playId
                                &&(new Date().getTime()-this.dhistoryBetData.timeer)<10000
                                &&Number(itemAomunt)===Number(compareAomunt)) {
                                vm.submiting = false;
                                return true
                            }
                    })) {
                        this.$comfirm({
                            title:'<div>' + lottCom.bet.repeatTitle + '</div>',
                            content:'<div>' + lottCom.bet.repeatContent + '</div>'
                        }).then(()=>{
                            vm.submiting = true;
                            resolve(this.postBetAction(data, isJKC))
                        }).catch((err)=>{
                            this.submiting = false
                            reject(lottCom.bet.giveUp)
                            console.warn("error",err)
                        })
                    } else {
                        resolve(this.postBetAction(data, isJKC))
                    }
                }).catch( err => {
                    this.submiting = false
                    // 1. 过期，点击‘放弃投注’
                    console.warn("error",err)
                    reject(err)
                })
            })
        },
        postBetAction(data, isJKC){
            return SicboApi.create(data, cancel => {
                // 初始化取消句柄
                this.cancelHandler = cancel
            }, isJKC).catch(e => {
                // this.fetchRecentPlay(); // 最近玩过
                if (axios.isCancel(e)) {
                    this.$toast.clear()
                    this.finalToast = this.$toast.fail({
                        message: lottCom.bet.orderFailed,
                        forbidClick: false,
                    })
                } else {
                    this.finalToast = this.$toast.fail({
                        message: lottCom.bet.submitFailed,
                        forbidClick: true,
                    })
                }
                return Promise.reject(Error('请求失败，网络故障'))
            }).then(res => {
                // this.fetchRecentPlay() // 最近玩过
                // 关闭所有toast
                this.$toast.clear({
                    clearAll: true,
                })
                const { code, msg } = res.data
                if (code === 0) {
                    this.$emit('close')
                    this.clearData()
                    if (!isJKC) {
                        // 不允许存在多个toast，避免投注中立马消失
                        setTimeout(() => {
                            this.finalToast = this.$toast.success({
                                message: lottCom.bet.succeed,
                                forbidClick: false,
                            })
                        }, 300)
                        return Promise.resolve(res.data)
                    } else {
                        if(!this.openMmcDraw) {
                            setTimeout(() => {
                                this.finalToast = this.$toast.success({
                                    message: lottCom.bet.succeed,
                                    forbidClick: false,
                                })
                            }, 300)
                        }
                        // 单独投注推送中奖
                        const WinAmount = res.data.data ? res.data.data.list.reduce((prev, value) => {
                            return prev + value.winAmount
                        }, 0).toFixed(4) : 0
                        if(WinAmount > 0 && !res.data.data.hasRisk) {
                            // setTimeout(() => {
                            //     this.$store.commit('setPushData', {
                            //         type:  'EVENT_PRIZE',
                            //         data: {
                            //             WinAmount,
                            //             ticketId: res.data.data.ticketId,
                            //             ticketName: res.data.data.ticketName,
                            //             openNum: res.data.data.ticketResult,
                            //             ticketPlanNo: res.data.data.ticketPlanNo,
                            //         },
                            //     })
                            // }, mmcPushTime)
                        }
                        return Promise.resolve(res.data)
                    }
                } else {
                    // 余额不足
                    if (code === 10020) {
                        this.$dialog.alert({
                            title: lottCom.wayMap.tipsTitle,
                            message: lottCom.wayMap.tipsText,
                            forbidClick: false,
                        })
                        return Promise.reject(msg)
                    } else if (code === 10069) {
                        // 限红区间，在成注区处理
                        // eslint-disable-next-line prefer-promise-reject-errors
                        return Promise.reject({ code, msg })
                    } else {
                        this.finalToast = this.$toast.fail({
                            message: msg,
                            forbidClick: false,
                        })
                        
                        return Promise.reject(msg)
                    }
                }
            }).finally(() => {
                // 不管投注是否成功, 更新余额
                this.$store.dispatch('fetchUserInfo')

                // 近期注单推送 创建注单没有推送
                if(isJKC) {
                    setTimeout(() => {
                        this.$store.dispatch('fetchOrdersTop', 0)
                        this.$store.dispatch('fetchOrdersTop', 1)
                    }, mmcPushTime)
                } else {
                    this.$store.dispatch('fetchOrdersTop', 0)
                    this.$store.dispatch('fetchOrdersTop', 1)
                }

                // 延迟设置状态, 避免在投注成功后确认框消失的过程中被点击
                setTimeout(() => {
                    this.submiting = false
                }, 1000)
            })
        },
        realSubmit(data) {
         
        },

        checkTimeout() {
            // 请求超时
            this.timerHandler = setTimeout(() => {
                if (this.submiting) {
                    // 取消请求
                    if (this.cancelHandler) {
                        this.cancelHandler()
                    }
                    this.submiting = false
                    this.$toast.clear()
                    this.$toast(lottCom.bet.timeout)
                }
            }, this.timeout)
        },
        checkCurrentStatus(ticketId, ticketName, planNo, isJKC=false) {

            if(isJKC) {
                return new Promise((resolve) => resolve(undefined))
            }
            return LotteryApi.loadTime({
                ticketIds: ticketId,
            }).then(ticketPlanInfo => {
                const { planId, sale } = (ticketPlanInfo && ticketPlanInfo.data) || {}
                console.log('ticket plan info:', ticketPlanInfo)
                // 判断彩种的销售状态
                if (!sale) {
                    this.submiting = false
                    // 重置定时器句柄
                    clearTimeout(this.timerHandler)
                    this.$toast.clear()

                    this.$toast(lottCom.bet.notSell)
                    throw new Error('该彩种暂停销售，请稍后再次尝试')
                }
                if (!planId) {
                    this.submiting = false
                    // 重置定时器句柄
                    clearTimeout(this.timerHandler)
                    this.$toast.clear()

                    this.$toast(lottCom.foot.noPeriod)
                    throw new Error('尚未获取到当期期号')
                }

                // 奖期变化 给出提示
                if (planNo !== planId && planNo !== 'null') {
                    this.submiting = false
                    // 重置定时器句柄
                    clearTimeout(this.timerHandler)
                    this.$toast.clear()

                    return this.$dialog
                        .confirm({
                            message: `${ticketName}，${lottCom.trace.changed}${planId}${lottCom.trace.qiNo}`,
                            confirmButtonText: lottCom.trace.continue,
                            cancelButtonText: lottCom.bet.giveUp,
                            className: "hasLogo_dialog",
                        })
                        .then(async () => { 
                            console.log('confirm')
                            return this.checkCurrentStatus(ticketId, ticketName, planId, isJKC)
                        })
                } else {
                    return planId
                }
            })
        },

        clearData() {
            this.$store.commit('setClearDoubleData', !this.clearDoubleData)
        },
    },

    watch: {
        submiting(v) {
            // 提交投注 forbidClick 导致垃圾收集不显示元素问题

            if (v) {
                if (this.isJKC) {
                    this.$store.commit('setMmcStarting', true)
                }
                if (!(this.isJKC && this.openMmcDraw)) {
                    this.$toast.loading({
                        message: lottCom.bet.tijiao,
                        // forbidClick: true,
                        duration: 2000,
                        loadingType: 'spinner',
                    })
                }
                var betNotClick = document.createElement('div');
                betNotClick.className = "bet-not-click"
                document.body.appendChild(betNotClick)
            } else {
                // if (this.isJKC) {
                //     this.$store.commit('setMmcStarting', false)
                // }
                this.$toast.clear()
                var betNotClick = document.querySelector('.bet-not-click')
                document.body.removeChild(betNotClick)
            }
        },
    },
}
