import { mapGetters } from 'vuex'
import i18n from '@/assets/i18n/index'
import store from '@/store'

// alert(i18n.t('yxzh.orderStatusJs.yes'))
// alert(i18n.t('yxzh.orderStatusJs.statusMap[0]'))

export default {
    computed: {
        ...mapGetters([
            'getBetSatus',
            'getTraceSatus',
            'moneyModeList',
            'getMoneyStatus',
            'getMoneyDetail',
        ]),
    },
    filters: {
        /**
         *  状态转文字过滤器
         *  待开奖 0; 未中奖 1; 已中奖 2；系统撤销 3； 个人撤销 4； 追中撤单 5； 不中撤单 6
         *  default --
         * */
        toBetStatus(value, status) {
            const nowStatus = status.find(ele => Number(value) === ele.key)
            return nowStatus ? nowStatus.value : '--'
        },
        /**
         *  状态转文字过滤器
         *  追号中 0; 已完成 1;  个人停追 中奖停追 3；系统停追  4；不中停追  5
         *  default --
         * */
        toTraceStatus(value, status) {
            const nowStatus = status.find(ele => Number(value) === ele.key)
            return nowStatus ? nowStatus.value : '--'
        },
        /**
         *  布尔值过滤器
         *  true 1 : 是 ; false 0: 否 ;
         * */
        toIsStatus(value) {
            return value ? i18n.t('yxzh.orderStatusJs.yes') : i18n.t('yxzh.orderStatusJs.no')
        },

        /**
         *  金额转换过滤器
         *  1元 1; 1角 0.1; 1分 0.01；1厘 0.001；2元 2；2角 0.2；2分 0.02；2厘 0.002
         *  default --
         * */
        toMoneyText(value, moneyList = store.state.lottery.moneyListMap) {
            if (!moneyList || !value) {
                return '--'
            }
            const nowMoney = moneyList.find(ele => {
                return Number(value) === Number(ele.value)
            })
            return nowMoney ? nowMoney.name : '--'
        },

        toIsSuppend(data) {
            switch (Number(data)) {
                case 1:
                    data = i18n.t('yxzh.orderStatusJs.non')
                    break
                case 2:
                    data = i18n.t('yxzh.orderStatusJs.zhuizhongjiting')
                    break
                case 3:
                    data = i18n.t('yxzh.orderStatusJs.buzhongjiting')
                    break
            }
            return data
        },

        // 获取追号模式
        toChaseMethod(data) {
            switch (Number(data)) {
                case 0:
                    data = i18n.t('yxzh.orderStatusJs.fanbeizhuihao')
                    break
                case 1:
                    data = i18n.t('yxzh.orderStatusJs.tongbeizhuihao')
                    break
                case 2:
                    data = i18n.t('yxzh.orderStatusJs.lirunlvzhuihao')
                    break
            }
            return data
        },

        /**
         * 投注状态：0.待开奖,1.未中奖,2.已中奖,3.系统撤单,4.个人撤单,5.追中撤单,6.不中撤单,7.挂起,8.锁定
         * @param {Number} status 状态码
         */
        toSicboBetState(status) {
            const statusMap = {
                0: i18n.t('yxzh.orderStatusJs.statusMap[0]'),
                1: i18n.t('yxzh.orderStatusJs.statusMap[1]'),
                2: i18n.t('yxzh.orderStatusJs.statusMap[2]'),
                3: i18n.t('yxzh.orderStatusJs.statusMap[3]'),
                4: i18n.t('yxzh.orderStatusJs.statusMap[4]'),
                5: i18n.t('yxzh.orderStatusJs.statusMap[5]'),
                6: i18n.t('yxzh.orderStatusJs.statusMap[6]'),
                7: i18n.t('yxzh.orderStatusJs.statusMap[7]'),
                8: i18n.t('yxzh.orderStatusJs.statusMap[8]'),
            }
            const ret = statusMap[status]
            if (!ret) {
                console.warn(`[App] 骰宝状态码未定义 ==> ${status}`)
            }

            return ret ||  i18n.t('yxzh.orderStatusJs.systemException')
        },

        /**
         *  金额过滤器
         * */
        toFormatMoney(value) {
            return Math.formatAmount5(value)
        },
        // 追号状态
        toPlanStatus(data) {
            switch (Number(data)) {
                case 0:
                    data = i18n.t('yxzh.orderStatusJs.weixiadan')
                    break
                case 1:
                    data = i18n.t('yxzh.orderStatusJs.yixiadan')
                    break
                case 2:
                    data = i18n.t('yxzh.orderStatusJs.userCanceled')
                    break
                case 3:
                    data = i18n.t('yxzh.orderStatusJs.systemCanceled')
                    break
            }
            return data
        },

        // 资金状态
        toMoneyStatus(value, data) {
            if (value == 18) {
                return i18n.t('yxzh.eventCash')
            }
            const nowStatus = data.find(ele => Number(value) === ele.tradeType)
            return nowStatus ? nowStatus.name : '--'
        },

        // 订单超过19个字符串...
        nameToMit: function(value) {
            if (!value) return
            if (value.length > 19) {
                value = `${value.substring(0, 19)}...`
            }
            return value
        },

        toTicketBetStatus(data) {
            switch (Number(data)) {
                case 1:
                    data = i18n.t('yxzh.orderStatusJs.statusMap[0]')
                    break
                case 2:
                    data = i18n.t('yxzh.orderStatusJs.statusMap[1]')
                    break
                case 3:
                    data = i18n.t('yxzh.orderStatusJs.statusMap[2]')
                    break
                case 4:
                    data = i18n.t('yxzh.orderStatusJs.statusMap[7]')
                    break
                case 5:
                    data = i18n.t('yxzh.orderStatusJs.yijiesuan')
                    break
            }
            return data
        },
    },

    methods: {
       
        // copyText(text) {
        //     const input = document.createElement('input')
        //     document.body.appendChild(input)
        //     input.setAttribute('value', text)
        //     input.select()
        //     console.log( document.execCommand('copy') )
        //     if (document.execCommand('copy')) {
        //         document.execCommand('copy')
        //         this.$toast('已复制')
        //     }
        //     document.body.removeChild(input)
        // },

        // 复制功能 兼容IOS
        copyText (text, callback){
            // 数字没有 .length 不能执行selectText 需要转化成字符串
            const textString = text.toString();
            let input;
            input = document.createElement('input');
            input.id = "copy-input";
            input.readOnly = "readOnly";        // 防止ios聚焦触发键盘事件
            input.style.position = "absolute";
            input.style.left = "-1000px";
            input.style.zIndex = "-1000";
            document.body.appendChild(input)
            input.value = textString;
            // ios必须先选中文字且不支持 input.select();
            selectText(input, 0, textString.length);
            if (document.execCommand('copy')) {
              document.execCommand('copy');
              if (callback) {
                callback()
              } else {
                this.$toast(i18n.t('yxzh.orderStatusJs.yifuzhi'))
              }
            }
            input.blur();
            document.body.removeChild(input)
            // input自带的select()方法在苹果端无法进行选择，所以需要自己去写一个类似的方法
            // 选择文本。createTextRange(setSelectionRange)是input方法
            function selectText(textbox, startIndex, stopIndex) {
              if (textbox.createTextRange) {//ie
                const range = textbox.createTextRange();
                range.collapse(true);
                range.moveStart('character', startIndex);//起始光标
                range.moveEnd('character', stopIndex - startIndex);//结束光标
                range.select();//不兼容苹果
              } else {//firefox/chrome
                textbox.setSelectionRange(startIndex, stopIndex);
                textbox.focus();
              }
            }
        }
    },
}
