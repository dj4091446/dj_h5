import { mapState } from 'vuex'

export default {
    computed: {
        ...mapState({
            lotteryCounterMap: state => state.lottery.lotteryCounterMap,
        }),
    },
    methods: {
        getCurrentIssue(ticketId) {
            return this.$store.dispatch('fetchCurrentIssue', {
                ticketIds: ticketId,
            })
        },

        getCounterObj(ticketId) {
            return this.lotteryCounterMap && this.lotteryCounterMap[ticketId]
        },

        getCounterTime(ticketId) {
            return this.lotteryCounterMap && this.lotteryCounterMap[ticketId] && this.lotteryCounterMap[ticketId].remain
        },
    },
}
