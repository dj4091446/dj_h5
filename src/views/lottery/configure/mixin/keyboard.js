export default {
    data() {
        // prettier-ignore
        return {
            keyboardValue  : '',     // 键盘本次输入的内容
            currentInputObj: null,   // 当前输入的对象
            showNumberKeyboard: false,
        }
    },

    computed: {
        globalShowNumberKeyboard: {
            get() {
                return this.$store.state.setting.showNumberKeyboard
            },

            set(v) {
                this.$store.commit('setShowNumberKeyboard', v)
            },
        },

        numberKeyboardOrder: {
            get() {
                return this.$store.state.setting.numberKeyboardOrder
            },

            set(v) {
                this.$store.commit('setNumberKeyboardOrder', v)
            },
        },

        numberKeyboardValue: {
            get() {
                return this.$store.state.setting.numberKeyboardValue
            },

            set(v) {
                this.$store.commit('setNumberKeyboardValue', v)
            },
        },

        deleteFlag: {
            get() {
                return this.$store.state.setting.deleteFlag
            },

            set(v) {
                this.$store.commit('setDeleteFlag', v)
            },
        },

        checkInputNumberValue() {
            const keys = Object.keys(this.inputNumberValue)
            return keys.some(key => {
                return !this.inputNumberValue[key]
            })
        },
    },

    watch: {
        showNumberKeyboard(v) {
            if (!v) {
                if (this.localBasket && this.localBasket.length) {
                    this.localBasket.forEach(basketItem => {
                        let amount = String(basketItem.amount)
                        amount = amount.replace(/^[0]+/, '')
                        basketItem.amount = amount
                        if (Number(basketItem.amount) < 1) {
                            basketItem.amount = '1'
                        }
                    })
                }
                this.currentInputObj = null
                this.keyboardValue = ''
                this.numberKeyboardValue = ''
            }
        },

        numberKeyboardValue(v, ov) {
            this.deleteFlag = ov.length > v.length
        },
    },
}
