/* global SXC_NAME */
import { mapState, mapGetters } from 'vuex'

export default {
    computed: {
        ...mapState({
            handicap: state => state.lottery.handicap,
        }),
        ...mapGetters(['isSXC']),
        isShengXiao: {
            get() {
                return this.$store.state.setting.isShengXiao
            },

            set(isShengXiao) {
                this.$store.commit('setIsShengXiao', isShengXiao)
            },
        },
    },

    created() {
        // 如果是双面盘的话，只展示数字
        if (this.handicap==='dobule') {
            this.isShengXiao = false
        }
    },

    methods: {
        getSX(num) {
            return SXC_NAME[num] || num
        },
    },
}
