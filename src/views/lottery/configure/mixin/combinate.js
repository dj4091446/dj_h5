import { mapState } from 'vuex'
import { getCombinateListByPlay } from '@/views/lottery/bet/double/fushi/lottery'

export default {
    computed: {
        ...mapState({
            subCatalog: state => state.lottery.subCatalog,
        }),
    },

    methods: {
        getCombinateListByPlay,
    },
}
