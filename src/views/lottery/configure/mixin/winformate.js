import {  C } from '@/views/lottery/util'
export default {
    data() {
        return {
            cmv:{
                renxuanyi:{
                    m:1,
                    limit:5
                },
                renxuaner:{
                    m:2,
                    limit:5
                },
                renxuansan:{
                    m:3,
                    limit:5
                },
                renxuansi:{
                    m:4,
                    limit:5
                },
                erlianxiao:{
                    m:2,
                    fn:'erlianxiaoFn'
                },
                sanlianxiao:{
                    m:3,
                    fn:'sanlianxiaoFn'
                },
                silianxiao:{
                    m:4,
                    fn:'silianxiaoFn'
                },
                wulianxiao:{
                    m:5,
                    fn:'wulianxiaoFn'
                },
                erlianwei:{
                    m:2,
                    fn:'erlianweiFn'
                },
                sanlianwei:{
                    m:3,
                    fn:'sanlianweiFn'
                },
                silianwei:{
                    m:4,
                    fn:'silianweiFn'
                },
                wulianwei:{
                    m:5,
                    fn:'wulianweiFn'
                },
                sanquanzhong:{
                    m:3,
                    limit:6
                },
                erquanzhong:{
                    m:2,
                    limit:6
                },
                siquanzhong:{
                    m:4,
                    limit:6
                },
                sanzhonger:{
                    m:3,
                    fn:'sanzhongerFn',
                },
                erzhongte:{
                    m:3,
                    fn:'erzhongteFn',
                    limit:7
                },
                techuang:{
                    m:3,
                    fn:'techuanFn',
                    limit:7,
                },
                renxuanwu:'profitFourFn',
                renxuanliuzhongwu:'renxuanliuFn',
                renxuanqizhongwu:'renxuanqiFn',
                renxuanbazhongwu:'renxuanbaFn',
                qiansanzhixuan:'profitFourFn',
                qianerzhixuan:'profitFourFn',
                qianerzuxuan:'profitFourFn',
                qiansanzuxuan:'profitFourFn',
                liangzidingwei:'profitFourFn',
                baishidingwei:'profitFourFn',
                baigedingwei:'profitFourFn',
                shigedingwei:'profitFourFn',
                sanzidingwei:'profitFourFn',
                zusan:'profitFourFn',
                zuliu:'profitFourFn',
            }
        }
    },    
    methods: {
        erlianxiaoFn(item,betBallsNum){
            const benMingInfo = item.benMingInfo
            if(benMingInfo.isBen) {
                if(betBallsNum<=7) {
                   return Math.formatAmount3(item.defaultAmount *benMingInfo.benOdds*(betBallsNum-1)+item.defaultAmount *C(betBallsNum-1,2)* benMingInfo.odds-  item.amount, 4)
                } else {
                    betBallsNum=7
                    return Math.formatAmount3(item.defaultAmount *C(betBallsNum,2)* benMingInfo.odds-  item.amount, 4)
                }
            } else {
              if(betBallsNum>7) betBallsNum=7
              return Math.formatAmount3(item.defaultAmount *C(betBallsNum,2)* benMingInfo.odds-  item.amount, 4)
            }
          },
          sanlianxiaoFn(item,betBallsNum){
            const benMingInfo = item.benMingInfo
         
            if(benMingInfo.isBen) {
                if(betBallsNum<=7) {
                    return Math.formatAmount3(item.defaultAmount *C(betBallsNum-1,2)*benMingInfo.benOdds+item.defaultAmount *C(betBallsNum-1,3)* benMingInfo.odds-  item.amount, 4)
                } else {
                    betBallsNum=7
                    return Math.formatAmount3(item.defaultAmount *C(betBallsNum,3)* benMingInfo.odds-  item.amount, 4)
                }
            } else {
              if(betBallsNum>7)betBallsNum=7
              return Math.formatAmount3(item.defaultAmount *C(betBallsNum,3)* benMingInfo.odds-  item.amount, 4)
            }
          },
          silianxiaoFn(item,betBallsNum){
            const benMingInfo = item.benMingInfo
            if(benMingInfo.isBen) {
                if(betBallsNum<=7) {
                    return Math.formatAmount3(item.defaultAmount *C(betBallsNum-1,3)*benMingInfo.benOdds+item.defaultAmount *C(betBallsNum-1,4)* benMingInfo.odds-  item.amount, 4)
                } else {
                    betBallsNum=7
                    return Math.formatAmount3(item.defaultAmount *C(betBallsNum,4)* benMingInfo.odds-  item.amount, 4)
                }
            } else {
                if(betBallsNum>7)betBallsNum=7
                return Math.formatAmount3(item.defaultAmount *C(betBallsNum,4)* benMingInfo.odds-  item.amount, 4)
            }
          },
          wulianxiaoFn(item,betBallsNum){
            const benMingInfo = item.benMingInfo
            if(benMingInfo.isBen) {
                if(betBallsNum<=7) {
                    return Math.formatAmount3(item.defaultAmount *C(betBallsNum-1,4)*benMingInfo.benOdds+item.defaultAmount *C(betBallsNum-1,5)* benMingInfo.odds-  item.amount, 4)
                } else {
                    betBallsNum=7
                    return Math.formatAmount3(item.defaultAmount *C(betBallsNum,5)* benMingInfo.odds-  item.amount, 4)
                }
            } else {
               if(betBallsNum>7)betBallsNum=7
               return Math.formatAmount3(item.defaultAmount *C(betBallsNum,5)* benMingInfo.odds-  item.amount, 4)
            }
          },
          erlianweiFn(item,betBallsNum){
            const benMingInfo = item.benMingInfo
            if(betBallsNum>7)betBallsNum=7
            if(benMingInfo.isBen) {
                return Math.formatAmount3(item.defaultAmount *(betBallsNum-1)*benMingInfo.benOdds+item.defaultAmount *C(betBallsNum-1,2)* benMingInfo.odds-  item.amount, 4)
            } else {
              return Math.formatAmount3(item.defaultAmount *C(betBallsNum,2)* benMingInfo.odds-  item.amount, 4)
            }
          },
          sanlianweiFn(item,betBallsNum){
            const benMingInfo = item.benMingInfo
            if(betBallsNum>7)betBallsNum=7
            if(benMingInfo.isBen) {
                return Math.formatAmount3(item.defaultAmount *C(betBallsNum-1,2)*benMingInfo.benOdds+item.defaultAmount *C(betBallsNum-1,3)* benMingInfo.odds-  item.amount, 4)
            } else {
              return Math.formatAmount3(item.defaultAmount *C(betBallsNum,3)* benMingInfo.odds-  item.amount, 4)
            }
          },
          silianweiFn(item,betBallsNum){
            const benMingInfo = item.benMingInfo
            if(betBallsNum>7)betBallsNum=7
            if(benMingInfo.isBen) {
                return Math.formatAmount3(item.defaultAmount *C(betBallsNum-1,3)*benMingInfo.benOdds+item.defaultAmount *C(betBallsNum-1,4)* benMingInfo.odds-  item.amount, 4)
            } else {
              return Math.formatAmount3(item.defaultAmount *C(betBallsNum,4)* benMingInfo.odds-  item.amount, 4)
            }
          },
          wulianweiFn(item,betBallsNum){
            const benMingInfo = item.benMingInfo
            if(betBallsNum>7)betBallsNum=7
            if(benMingInfo.isBen) {
                return Math.formatAmount3(item.defaultAmount *C(betBallsNum-1,4)*benMingInfo.benOdds+item.defaultAmount *C(betBallsNum-1,5)* benMingInfo.odds-  item.amount, 4)
            } else {
              return Math.formatAmount3(item.defaultAmount *C(betBallsNum,5)* benMingInfo.odds-  item.amount, 4)
            }
          },
          renxuanliuFn(item,betBallsNum){
              return Math.formatAmount3(item.defaultAmount *item.playItemOdds*C(betBallsNum-5,1)-  item.amount, 4)
          },
          renxuanqiFn(item,betBallsNum){
              return Math.formatAmount3(item.defaultAmount *item.playItemOdds*C(betBallsNum-5,2)-  item.amount, 4)
          },
          renxuanbaFn(item,betBallsNum){
              return Math.formatAmount3(item.defaultAmount *item.playItemOdds*C(betBallsNum-5,3)-  item.amount, 4)
          },
          profitFourFn(item,betBallsNum) {
              return Math.formatAmount3(item.defaultAmount *item.playItemOdds-  item.amount, 4)
          },
           sanzhongerFn(item,betBallsNum){
              const zhongerOdds = item.playItemOdds[0].odds;
                const zhongsanOdds = item.playItemOdds[1].odds;
              if(betBallsNum<=6) {
                   return Math.formatAmount3(item.defaultAmount * C(betBallsNum,3)*zhongsanOdds-  item.amount, 4)
              }
              return Math.formatAmount3(item.defaultAmount * 20*zhongsanOdds+item.defaultAmount*(betBallsNum-6)*C(6,2)*zhongerOdds-  item.amount, 4)
          },
          erzhongteFn(item,betBallsNum){
              const zhongerOdds = item.playItemOdds[0].odds;
              const zhongteOdds = item.playItemOdds[1].odds;
              if(betBallsNum>=7) {
                  betBallsNum=7
                  return Math.formatAmount3(item.defaultAmount* C(6,2)*zhongerOdds+item.defaultAmount*6*zhongteOdds-item.amount, 4)
              }
              return Math.formatAmount3(item.defaultAmount * C(betBallsNum,2)*zhongerOdds-  item.amount, 4)
          },
          techuanFn(item,betBallsNum){
              return Math.formatAmount3(item.defaultAmount *item.playItemOdds*(betBallsNum-1)-item.amount, 4)
          }
    }
}