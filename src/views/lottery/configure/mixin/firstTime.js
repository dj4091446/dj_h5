import { mapState } from 'vuex'

export default {
    computed: {
        firstBetFlag: {
            get() {
                return this.$store.state.setting.firstBetFlag
            },

            set(flag) {
                this.$store.commit('setFirstBetFlag', flag)
            },
        },

        firstQuickMoney: {
            get() {
                return this.$store.state.setting.firstQuickMoney
            },

            set(flag) {
                this.$store.commit('setFirstQuickMoney', flag)
            },
        },

        firstLongDragon: {
            get() {
                return this.$store.state.setting.firstLongDragon
            },

            set(flag) {
                this.$store.commit('setFirstLongDragon', flag)
            },
        },

        ...mapState({
            orderTopListTotal: state => state.record.orderTopListTotal, // 未结注单数量 最大20
        }),
    },

    methods: {},
}
