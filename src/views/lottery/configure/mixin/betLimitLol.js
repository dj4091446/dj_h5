import { mapState } from 'vuex'
import limitBetRange from '@/views/lottery/bet/double/limitBetRange'

// 双面限红，平台封锁值，个人封锁值判断
export default {
    data() {
        return {
            checkBetData: null,
            currentEvent: null,
            type: null,
            limitBetRange,
        }
    },

    computed: {
        ...mapState({
            featureLimitStatus: state => state.setting.featureLimitStatus,
            doubleLimitStatus: state => state.setting.featureLimitStatus,
            doubleBetRangeList: state => state.setting.featureBetRangeList,
        }),
        doubleBetRange() {
            return (
                (this.doubleBetRangeList &&
                    this.doubleBetRangeList.filter(item => item.current)[0]) ||
                {}
            )
        },
    },

    methods: {
        /**
         * 返回boolean 表示是否被限制
         * @param {*} betDate 投注数据
         * @param {*} event 触发事件
         * @param {*} type 展示类型
         */
        checkLimit(betDate, event, type) {
            if(this.featureLimitStatus) return false
            // init
            this.checkBetData = betDate
            this.currentEvent = event
            this.btnParent = this.$refs.btnParent
            this.showType = type

            // 双面盘限红
            let { isLimited, betData } = this.checkDouleLimitRange()
            if (isLimited) {
                return betData
            }
            // TODO 个人封锁值
            ;({ isLimited, betData } = this.checkPersonalLimit(betDate))
            if (isLimited) {
                return betData
            }
            // TODO 平台封锁值

            // 所有检查都通过
            return false
        },

        /**
         * 返回结构
         * { isLimited: boolean, betData: Object }
         * isLimited 是否被限制
         * betData   标记后的投注数据
         *        isLimited 表示是否被限制
         */
        checkDouleLimitRange() {
            // doubleLimitStatus 双面盘限红未开启 不限制限红区间
            if (
                !this.doubleLimitStatus ||
                !this.doubleBetRange.doublePlayBetMin ||
                !this.doubleBetRange.doublePlayBetMax
            ) {
                return {
                    isLimited: false,
                    betData: null,
                }
            }

            // 过滤不符合的投注项
            const limitedData = []
            const unlimitedData = []
            this.checkBetData &&
                this.checkBetData.list &&
                this.checkBetData.list.forEach &&
                this.checkBetData.list.forEach(item => {
                    const amount = Number(item.amount)
                    if (item.isLimit) {
                        if (
                            Number(this.doubleBetRange.doublePlayBetMin) > amount ||
                            Number(this.doubleBetRange.doublePlayBetMax) < amount
                        ) {
                            limitedData.push(item)
                        } else {
                            unlimitedData.push(item)
                        }
                    } else {
                        unlimitedData.push(item)
                    }
                })
            if (limitedData.length) {
                this.$toolTipShow(
                    limitBetRange,
                    {
                        trigger: this.currentEvent,
                        isFixed: true,
                        doubleBetRange: this.doubleBetRange,
                        betData: { list: limitedData },
                        type: 3,
                        OnClickOnSetting: () => {
                            this.$emit('clickOnSetting')
                        },
                    }
                )
                return {
                    isLimited: true,
                    betData: unlimitedData,
                }
            }

            return {
                isLimited: false,
                betData: null,
            }
        },

        /**
         * 返回结构
         * { isLimited: boolean, betData: Object }
         * isLimited 是否被限制
         * betData   标记后的投注数据
         *        isLimited 表示是否被限制
         */
        checkPersonalLimit() {
            return false
        },

        /**
         * 展示提示气泡
         * @param {*} betDate 经过标注的投注数据 这是不符合的
         */
        showTooltip(limitedBetDate) {},
    },
}
