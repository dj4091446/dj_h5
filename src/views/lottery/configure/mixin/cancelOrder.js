import * as api from '@/api/teselol'
import { mapActions, mapGetters } from 'vuex'
import i18n from '@/assets/i18n/index'

// alert(i18n.t('yxzh.cancelOrderJs.cancelConfirmMessage2', [10]))

export default {
    computed: {
        ...mapGetters(['getGameDetail', 'getCurrentOrder']),
    },
    methods: {
        ...mapActions(['fetchOrderDetail', 'fetchTraceDetail']),
        /***
         *  普通投注 取消投注订单
         * */
        async cancelOrder(callback) {
            this.$dialog
                .confirm({
                    message: i18n.t('yxzh.cancelOrderJs.cancelConfirmMessage'),
                    confirmButtonText: i18n.t('yxzh.cancelOrder'),
                    cancelButtonText: i18n.t('yxzh.canceBtnText'),
                })
                .then(async () => {
                    const { orderId } = this.getCurrentOrder
                    const res = await api.cancelOrder(orderId)
                    if (res.data.code === 0) {
                        this.fetchOrderDetail({
                            orderId,
                        })
                        // 更新余额
                        this.$store.dispatch('fetchUserInfo')
                        this.$toast.success({
                            message: i18n.t('yxzh.cancelOrderJs.cancelSuccess'),
                            forbidClick: true,
                            duration: 1000
                        })

                        this.$store.commit('setPushData', {
                            type:  'EVENT_BET_CANCEL',
                            data: null,
                        })

                        if (callback) callback()
                    } else {
                        this.$toast.fail({
                            message: res.data.msg,
                            forbidClick: true,
                            duration: 1000
                        })
                    }
                })
                .catch(() => {})
        },
        /***
         *  追号投注 单个取消追号订单   第一次确认框
         * */
        traceCancelOrder() {
            const { orderId } = this.getCurrentOrder
            const addChaseId = this.getCurrentOrder.addChaseId
            const params = {
                chaseId: addChaseId ? addChaseId : orderId, // 追号单号
            }
            api.chasePlanStatus(params).then(res => {
                if (res.data.code === 0) {
                    let { status, msg, currentPlanNo, nextPlanNo } = res.data.data;
                    // 1:可撤；2：追号单状态不合法；3：不可撤
                    if (status === 1) {
                        this.$dialog
                        .confirm({
                            message: i18n.t('yxzh.cancelOrderJs.cancelConfirmMessage2', [currentPlanNo]),
                            confirmButtonText: i18n.t('yxzh.cancelOrderJs.confirmButtonText'),
                            cancelButtonText: i18n.t('yxzh.canceBtnText'),
                        })
                        .then(() => {
                            this.confirmCancelTrack({ ...params, startPlanNo: currentPlanNo })
                        }).catch(() => {})
                    } else if (status === 3) {
                        if (nextPlanNo) {
                            this.$dialog
                            .confirm({
                                message: i18n.t('yxzh.cancelOrderJs.cancelConfirmMessage3', [currentPlanNo]),
                                confirmButtonText: i18n.t('yxzh.cancelOrderJs.confirmButtonText'),
                                cancelButtonText: i18n.t('yxzh.canceBtnText'),
                            })
                            .then(() => {
                                this.confirmCancelTrack({ ...params, startPlanNo: nextPlanNo })
                            }).catch(() => {})
                        } else {
                            this.$toast.fail({
                                message: i18n.t('yxzh.cancelOrderJs.lastPeriodNonCancel'),
                                duration:2000,
                                forbidClick: true,
                            })
                        }
                    } else {
                        this.$toast.fail({
                            message: res.data.data.msg,
                            forbidClick: true,
                        })
                    }
                } else {
                    this.$toast.fail({
                        message: i18n.t('yxzh.cancelOrderJs.cancelFail'),
                        forbidClick: true,
                    })
                }
            })
        },

        /***
         *  追号投注 批量取消追号订单   第一次确认框 
         *  1208350943968493615 
         *  1208350943955910664
         * */
        batchCancelTrack(data) {
            const { startPlanId, nextPlanId } = data
            const { orderId } = this.getCurrentOrder
            const addChaseId = this.getCurrentOrder.addChaseId
            const params = {
                chaseId: addChaseId ? addChaseId : orderId, // 追号单号
                planNo: startPlanId, // 开始删除的奖期
            }
            api.chaseCancelStatus(params).then(res => {
                if (res.data.code === 0) {
                    const { status } = res.data.data
                    if (status === 1) {
                        this.$dialog
                            .confirm({
                                message: i18n.t('yxzh.cancelOrderJs.cancelConfirmMessage3', [startPlanId]),
                                confirmButtonText: i18n.t('yxzh.cancelOrder'),
                                cancelButtonText: i18n.t('yxzh.canceBtnText'),
                            })
                            .then(() => {
                                this.confirmCancelTrack({ ...params, startPlanNo: nextPlanId })
                            })
                            .catch(() => {})
                        return
                    }
                    if (status === 2) {
                        this.$dialog
                            .confirm({
                                message:i18n.t('yxzh.cancelOrderJs.cancelConfirmMessage2', [startPlanId]),
                                confirmButtonText: i18n.t('yxzh.cancelOrder'),
                                cancelButtonText: i18n.t('yxzh.canceBtnText'),
                            })
                            .then(() => {
                                this.confirmCancelTrack({ ...params, startPlanNo: startPlanId })
                            })
                            .catch(() => {})
                        return
                    }
                    if (status === 4) {
                        this.$dialog
                            .confirm({
                                message: i18n.t('yxzh.cancelOrderJs.cancelConfirmMessage4'),
                                confirmButtonText: i18n.t('yxzh.cancelOrder'),
                                cancelButtonText: i18n.t('yxzh.canceBtnText'),
                            })
                            .then(() => {
                                this.confirmCancelTrack({ ...params, startPlanNo: startPlanId })
                            })
                            .catch(() => {})
                        return
                    }
                }
                this.$toast.success({
                    message: i18n.t('yxzh.cancelOrderJs.cancelFail'),
                    forbidClick: true,
                })
            })
        },
        /***
         *  追号投注 取消追号订单   第二次确认框
         * */
        confirmCancelTrack(params) {
            const { orderId } = this.getCurrentOrder
            const addChaseId = this.getCurrentOrder.addChaseId
            api.chaseBetCancle(params).then(res => {
                if (res.data.code === 0) {
                    // 更新余额
                    this.$store.dispatch('fetchUserInfo')
                    this.$toast.success({
                        message: i18n.t('yxzh.cancelOrderJs.cancelSuccess'),
                        forbidClick: true,
                    })

                    this.$store.commit('setPushData', {
                        type:  'EVENT_BET_CANCEL',
                        data: null,
                    })

                    // 后台状态修改慢，加200毫秒
                    setTimeout( () => {
                        this.fetchTraceDetail(addChaseId ? addChaseId : orderId)
                    },200)
                } else {
                    this.$toast.success({
                        message: res.data.msg,
                        forbidClick: true,
                    })
                }
            })
        },
    },
}
