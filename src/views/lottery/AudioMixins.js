import Audios, { AudioPauseAll } from './audioConfig.js'
import AudiosCore from "@/assets/scripts/Audios.js"
let visibilitychangeCallback;
export default {
  watch: {
    "$route.name"(newV) {
      if (newV !== 'lottery') {
        AudioPauseAll();
      }
    }
  },
  computed: {
    MixinsSicboBgMusic(){
      return this.$store.state.setting.sicboBgMusic;
    }
  },
  mounted() {
    //修改背景音乐禁用状态时候，还控制了对应的背景音乐的播放和停止
    // AudiosCore.updateDisabled('abledBGAudio', this.MixinsSicboBgMusic,'lol-bg');
    //节点渲染完成后再初始化音频
    //避免堵塞主流程
    this.$nextTick(() => {
      this.MixinsAddAudiosEventListener();
    })
    //监听浏览器标签栏的切换，包括【黑频，切换浏览器tab，回到桌面】都有效
    this.addTabChange();
  },
  activated() {
    this.$nextTick(() => {
      //修改背景音乐禁用状态时候，还控制了对应的背景音乐的播放和停止
      //  AudiosCore.updateDisabled('abledBGAudio', this.MixinsSicboBgMusic,'lol-bg');
      this.MixinsAddAudiosEventListener();
      //监听浏览器标签栏的切换，包括【黑频，切换浏览器tab，回到桌面】都有效
      this.addTabChange();
    })
  },
  deactivated() {
    AudioPauseAll();//暂停所有应用
    this.MixinsRemoveAudiosEventListener()
    document.removeEventListener('visibilitychange', visibilitychangeCallback);
  },
  beforeDestroy() {
    AudioPauseAll();//暂停所有应用
    this.MixinsRemoveAudiosEventListener()
    document.removeEventListener('visibilitychange', visibilitychangeCallback);
  },
  methods: {
    addTabChange() {
      let _this = this;
      //监听tab切换
      visibilitychangeCallback = function visibilitychangeCallBack() {
        AudiosCore.updateDocumentHidden(document.hidden);
        if (document.visibilityState == 'visible') { //状态判断：显示（切换到当前页面）
          if (_this.MixinsSicboBgMusic && _this.$route.name == 'lottery') {
            // _this.MixinsPlayBgAudio();
          }

        }
        //AudiosCore.updateDocumentHidden 已经处理不需要再处理
        // else if (document.visibilityState == 'hidden') {
        //   AudioPauseAll();
        // }
      }
      //添加tab切换时候的背景音乐播放监听
      document.addEventListener('visibilitychange', visibilitychangeCallback);
    },
    MixinsAddAudiosEventListener() {
      //如果是访问这个页面 这段代码有效==========>
      // Audios.init() //初始化音效
      Audios.AudioInitNoBgMusic();//初始化非背景音乐
      // //预播放
      document.removeEventListener('click', this.MixinsPrePlay)
      document.addEventListener('click', this.MixinsPrePlay)
      // // 播放背景音乐
      // document.removeEventListener('click', this.MixinsPlayBgAudio)
      // document.addEventListener('click', this.MixinsPlayBgAudio)
      // //<==========如果是访问这个页面

      // //如果是首页过来的话，直接播放
      this.MixinsPlayBgAudio();
    },
    MixinsRemoveAudiosEventListener() {
      document.removeEventListener('click', this.MixinsPrePlay)
      //移除背景音乐播放
      // document.removeEventListener('click', this.MixinsPlayBgAudio)
      // Audios.tools.pause('lol-bg')
    },
    MixinsPrePlay() {
      // Audios.bgPlayPrev();
      Audios.playPrev()
    },
    MixinsPlayBgAudio() {
      // let bgAudio = Audios.tools.pools()['lol-bg']
      // let muted = bgAudio.muted
      // if (bgAudio.audioStatus == 'playing') {

      //   if (muted) {
      //     Audios.tools.play('lol-bg', {
      //       volume: 0.5,
      //       currentTime: 0,
      //       loop: true,
      //       muted: false,
      //     })
      //   }
      // } else if (bgAudio.audioStatus == 'paused' || !bgAudio.audioStatus) {
      //   Audios.tools.play('lol-bg', {
      //     volume: 0.5, 
      //     loop: true,
      //     muted: false,
      //   })
      // }
    },
  },
}