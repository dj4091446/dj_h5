import numeral from 'numeral'
import QS from 'qs'
import moment from 'moment'

/**
 * 动态引入style
 * @param {资源地址} url
 */
export const loadStyle = url => {
    const link = document.createElement('link')
    link.type = 'text/css'
    link.rel = 'stylesheet'
    link.href = url
    const head = document.getElementsByTagName('head')[0]
    head.appendChild(link)
}

/**
 * 动态引入script
 * @param {资源地址} url
 */
export const loadScript = url => {
    const link = document.createElement('script')
    link.src = url
    const head = document.getElementsByTagName('head')[0]
    head.appendChild(link)
}

Math.factorial = function (e) {
    return e <= 0 ? 1 : e * Math.factorial(e - 1)
}
Math.permutation = function (e, t) {
    var a = Math.factorial;
    return t > e ? 0 : a(e) / a(e - t)
};
Math.combination = function (e, t) {
    var a = Math.factorial
    return t > e ? 0 : Math.round(a(e) / (a(t) * a(e - t)))
}
Math.intersection = function (e, t) {
    return e.filter(function (e) {
        return t.indexOf(e) !== -1
    })
}
Math.union = function (e, t) {
    return e.concat(t).filter(function (e, t, a) {
        return a.indexOf(e) === t
    })
}
Math.difference = function (e, t) {
    return e.filter(function (e) {
        return t.indexOf(e) === -1
    })
}
Math.nzn = function (e, t) {
    for (var a = 1, n = 0; t > n; n++) a *= (e - n) / (n + 1)
    return a
}
Math.precision = function (e) {
    e = (e + '').split('.')[1]
    return e ? e.length : 0
}

// 金额格式化  小数保留四位小数，整数保留两位小数
Math.formatAmount5 = function (n) {
    if (!n) {
        return '0.00'
    }

    let a = numeral(n).multiply(10000).divide(10000)
    a = a.value()
    if (!String(a).includes('.')) {
        return a.toFixed(2)
    }

    const splitDigtal = String(a).split('.')
    let lastdigital = splitDigtal[1].substr(0, 4)
    lastdigital = lastdigital.replace(/0+$/, '')
    return splitDigtal[0] + '.' + lastdigital
}

// 金额格式化  小数保留2位小数，整数保留整数
Math.formatAmount2 = function (n) {
    n = parseFloat(Math.subStringNum(n, 2))
    var a = parseInt(n * Math.pow(10, 2)) / 100
    if (!String(a).includes('.')) {
        a = a.toFixed(0)
    }
    return a
}
//可赢金额格式化  小数保留3位小数，整数保留整数
Math.formatAmount3 = function (n) {
    n = parseFloat(Math.subStringNum(n, 3));
    if (!String(n).includes('.')) {
        n = n.toFixed(0)
    }
    return n;
}
// 不预留小数，小数位最多保留4位，不四舍五入
Math.formatAmount4 = function (num) {
    num = parseFloat(parseFloat(num).toFixed(6))
    return Number(String(num).replace(/^(.*\..{4}).*$/, "$1"))
}
// 小数截取，不进行四舍五入
// Math.subStringNum = function(a, num) {
//     var dataType = typeof a
//     var aArr = null
//     if (dataType === 'number') {
//         var aStr = a.toString()
//         aArr = aStr.split('.')
//     } else if (dataType === 'string') {
//         aArr = a.split('.')
//     }

//     if (aArr.length > 1) {
//         a = aArr[0] + '.' + aArr[1].substr(0, num)
//     }
//     return a - 0
// }

//金额格式化  至少展示2位小数，不足补0，超过小数点后3位截断
Math.formatAmount5 = function (n) {
    n = parseFloat(Math.subStringNum(n, 3));
    if (!String(n).includes('.')) {
        n = n.toFixed(2)
    }else{
        var arr = String(n).split('.')[1];
        if (arr.length <= 2) {
            n = n.toFixed(2)
        } else {
            if (arr[2] === 0) {
                n = n.toFixed(2)
            } else {
                n = n.toFixed(3)
            }
        }
    }
    return n;
}

// 小数截取，不进行四舍五入
Math.subStringNum = function (a, num) {
    a = parseFloat(parseFloat(a).toFixed(6))
    var arr = String(a).split('.');
    if (arr.length > 1) {
        a = arr[0] + "." + arr[1].substr(0, num);
    }
    return a - 0;
}
// 去重排序
Math.sort = function (arr) {
    return [...new Set(arr)].sort((x, y) => x - y)
}

// 根据数组中某元素排序
Math.sortByKey = function (arr, key) {
    return arr.sort((x, y) => x[key] - y[key])
}

// 求和
Math.getSumByNum = arr => arr.reduce((prev, curr) => Number(prev) + Number(curr))

// 排列数分解
Math.arrange = function (arr, num) {
    var r = []
        ; (function f(t, a, n) {
            if (n === 0) return r.push(t)
            for (var i = 0, l = a.length; i < l; i++) {
                f(t.concat(a[i]), a.slice(i + 1), n - 1)
            }
        })([], arr, num)
    return r
}

// 计算精度处理
Math.calculation = function () {
    function isInteger(obj) {
        return Math.floor(obj) === obj
    }

    function toInteger(floatNum) {
        var ret = { times: 1, num: 0 }
        if (isInteger(floatNum)) {
            ret.num = floatNum
            return ret
        }
        var strfi  = floatNum + ''
        var dotPos = strfi.indexOf('.')
        var len    = strfi.substr(dotPos + 1).length
        var times  = Math.pow(10, len)
        var intNum = parseInt(floatNum * times + 0.5, 10)
        ret.times  = times
        ret.num    = intNum
        return ret
    }

    function operation(a, b, digits, op) {
        var o1 = toInteger(a)
        var o2 = toInteger(b)
        var n1 = o1.num
        var n2 = o2.num
        var t1 = o1.times
        var t2 = o2.times
        var max = t1 > t2 ? t1 : t2
        var result = null
        switch (op) {
            case 'add':
                if (t1 === t2) {
                    result = n1 + n2
                } else if (t1 > t2) {
                    result = n1 + n2 * (t1 / t2)
                } else {
                    result = n1 * (t2 / t1) + n2
                }
                return result / max
            case 'subtract':
                if (t1 === t2) {
                    result = n1 - n2
                } else if (t1 > t2) {
                    result = n1 - n2 * (t1 / t2)
                } else {
                    result = n1 * (t2 / t1) - n2
                }
                return result / max
            case 'multiply':
                result = (n1 * n2) / (t1 * t2)
                return result
            case 'divide':
                result = (n1 / n2) * (t2 / t1)
                return result
        }
    }

    function add(a, b, digits) {
        return operation(a, b, digits, 'add')
    }
    function subtract(a, b, digits) {
        return operation(a, b, digits, 'subtract')
    }
    function multiply(a, b, digits) {
        return operation(a, b, digits, 'multiply')
    }
    function divide(a, b, digits) {
        return operation(a, b, digits, 'divide')
    }

    return {
        add: add,
        subtract: subtract,
        multiply: multiply,
        divide: divide
    }
}()

// eslint-disable-next-line no-extend-native
Array.prototype.flattenDeep = function () {
    const flat = arr =>
        arr.reduce((acc, val) => (Array.isArray(val) ? acc.concat(flat(val)) : acc.concat(val)), [])
    return flat(this)
}

/**
 * @desc localStorage || sessionStorage
 * @param {[String]} table      [表名, 可选]
 * @param {[Object]} settings   [{key, value, remove}, 可选]
 * @param {[Object]} storage    [sessionStorage || localStorage, 可选]
*/
window.opStorage = function (table, settings, storage) {
    table = table || 'lotteryBase';     // 默认表名 'lotteryBase'
    storage = storage || localStorage;  // 默认使用localStorage

    if (!window.JSON || !window.JSON.parse) return;

    // 如果settings为null，则删除表
    if (settings === 'null') delete storage[table];

    settings = typeof settings === 'object' ? settings : { key: settings };

    let data = {};
    try {
        data = JSON.parse(storage[table]);
    } catch (e) {
        data = {};
    }

    if ('value' in settings) data[settings.key] = settings.value;
    if (settings.remove) delete data[settings.key];
    storage[table] = JSON.stringify(data);

    return settings.key ? data[settings.key] : data;
};


/*******************************************************************************/
window.SXC_NAME = [];

// prettier-ignore
window.sebo_red = [
    '01', '02', '07', '08', '12',
    '13', '18', '19', '23', '24',
    '29', '30', '34', '35', '40',
    '45', '46'
]
// prettier-ignore
window.sebo_blue = [
    '03', '04', '09', '10', '14',
    '15', '20', '25', '26', '31',
    '36', '37', '41', '42', '47',
    '48'
]
// prettier-ignore
window.sebo_green = [
    '05', '06', '11', '16', '17',
    '21', '22', '27', '28', '32',
    '33', '38', '39', '43', '44',
    '49'
]

// prettier-ignore
window.sxNumArray = [
    ['01', '13', '25', '37', '49'],
    ['12', '24', '36', '48'],
    ['11', '23', '35', '47'],
    ['10', '22', '34', '46'],
    ['09', '21', '33', '45'],
    ['08', '20', '32', '44'],
    ['07', '19', '31', '43'],
    ['06', '18', '30', '42'],
    ['05', '17', '29', '41'],
    ['04', '16', '28', '40'],
    ['03', '15', '27', '39'],
    ['02', '14', '26', '38'],
]
// prettier-ignore
window.sxArray = [];
window.sxCode = [
    'shu', 'niu', 'hu', 'tu', 'long', 'she',
    'ma', 'yang', 'hou', 'ji', 'gou', 'zhu',
]

// 前端默认五行
// prettier-ignore
window.wuxingArray = {
    jin: ['05', '06', '19', '20', '27', '28', '35', '36', '49'],                     // 金
    mu: ['01', '02', '09', '10', '17', '18', '31', '32', '39', '40', '47', '48'],   // 木
    shui: ['07', '08', '15', '16', '23', '24', '37', '38', '45', '46'],               // 水
    huo: ['03', '04', '11', '12', '25', '26', '33', '34', '41', '42'],               // 火
    tu: ['13', '14', '21', '22', '29', '30', '43', '44'],                           // 土
}

// prettier-ignore
window.lianweiMap = {
    lingwei: '0尾',
    yiwei: '1尾',
    erwei: '2尾',
    sanwei: '3尾',
    siwei: '4尾',
    wuwei: '5尾',
    liuwei: '6尾',
    qiwei: '7尾',
    bawei: '8尾',
    jiuwei: '9尾',
}

// 获取几天前
window.getNDate = function(n) {
    var now = new Date();
    var date = new Date(now.getTime() - n * 24 * 3600 * 1000);
    var year = date.getFullYear();
    var month = date.getMonth() + 1 > 9 ? date.getMonth() + 1 : "0" + (date.getMonth() + 1);
    var day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
    var date = year + "-" + month + "-" + day + ' 00:00';
    return date;
}
/**
 * @description 排列组合
 * @example  groupSplit([1,2,3],2) => [ [1,2],[1,3],[2,3] ]
 *
 */
window.groupSplit = function (arr, size) {
    var r = []
    function _call(t, a, n) {
        if (n <= 0) {
            r[r.length] = t
            return
        }
        for (var i = 0, l = a.length - n; i <= l; i++) {
            var b = t.slice()
            b.push(a[i])
            _call(b, a.slice(i + 1), n - 1)
        }
    }
    _call([], arr, size)
    return r
}

// 每三位加逗号
window.toThousands = function (num) {
    return (num || 0).toString().replace(num?.toString().indexOf(".") > 1 ? /(\d)(?=(\d{3})+\.)/g : /(\d)(?=(?:\d{3})+$)/g, '$1,');
}

// 数字转 万 亿单位
window.numberFormat = function (value) {
    var lens = String(value).length;
    if (lens < 9) {
        value = `${(value / 10000).toFixed(1)}万`
    } else {
        value = `${(value / 100000000).toFixed(3)}亿`
    }
    return value
}


// 字节数组转16进制
window.ByteArrToHex = function (byteArr) {
    let hexStr = ''
    let tmpHex
    const len = byteArr.length
    for (let i = 0; i < len; i++) {
        if (byteArr[i] < 0) {
            byteArr[i] = byteArr[i] + 256
        }
        tmpHex = byteArr[i].toString(16)
        if (tmpHex.length === 1) {
            tmpHex = '0' + tmpHex
        }
        hexStr += tmpHex
    }
    return hexStr
}

// 16进制转换字节数组
window.HexToByteArr = function(val) {
    for (var i = 0, strArr = [], temp = ''; i < val.length; i += 2) {
        temp = val.substr(i, 2);
        strArr.push(parseInt(temp, 16));
    }
    return strArr;
}

window.mmcPushTime = 2500
window.basicZone = 8
window.targetZone = new Date().getTimezoneOffset() / 60
window.zoneGMTStr = 'GMT' + (targetZone > 0 ? '-' : '+') + Math.abs(targetZone)
window.getZoneTime = function(date) {
    date = (date||'').replace(/-/g, "/");
    return moment(`${date} +8`).zone(targetZone).format('YYYY-MM-DD HH:mm:ss')
}
window.getZoneTimeRe = function(date) {
    date = date.replace(/-/g, "/");
    return moment(`${date}`).utcOffset(8).format('YYYY-MM-DD HH:mm:ss')
}
window.getNDateT = function (n) {
    return getZoneTimeRe(getNDate(n))
}

export const getShortIssue = (type, issue, isJKC) => {
    let shortIssue = null
    const sliceMap = {
        P3P5: 5,
        KLC: 7,
        PK10: 6,
        LHC: 5,
        SD: 5,
        SSC: 6,
    }
    if (issue.indexOf('-') !== -1) {
        shortIssue = issue.split('-')
        if(['YNZBC','YNNBC','YNBBC',].includes(type)){
            shortIssue = shortIssue[2] ? `${shortIssue[0].slice(-2, shortIssue[0].length)}${shortIssue[1]}${shortIssue[2]}` : `${shortIssue[0].slice(-2, shortIssue[0].length)}${shortIssue[1]}`
        }else if(type == "TGC"){
            shortIssue = `${shortIssue[0].slice(-4, shortIssue[0].length)}${shortIssue[1]}`
        } else {
            shortIssue = `${shortIssue[0].slice(-2, shortIssue[0].length)}${shortIssue[1]}`
        }
        if (type == 'SSC' && isJKC) {
            shortIssue = shortIssue.slice(-1 * 7, shortIssue.length)
        }
    } else if (sliceMap[type]) {
        shortIssue = issue.slice(-1 * sliceMap[type], issue.length)
    } else {
        shortIssue = issue
    }
    return shortIssue
}

/**
 * 判断奖期大小
 * @param {*} a
 * @param {*} b
 *            假如a < b 返回 -1
 *                a = b  0
 *                a > b  1
 *   奖期格式
 *   20190621-0xx       时时彩  自营3D
 *                      PK10自营(香港赛马 幸运飞艇 XX赛狗 XX分赛车)
 *                      11选5(自营)
 *                      K3(自营)
 *                      快乐8(自营)
 *   0293745            黑龙江时时彩  北京快乐8
 *   2019168            福彩3D 排列3/排列5 六合彩
 *   7349925期          北京PK10
 *
 */
export const compareIssue = function (a, b) {
    let ret = 0
    const year = new Date().getFullYear()

    if (a.indexOf('-') !== -1) {
        // 1. 假设都是当天的期号
        const aArr = a.split('-')
        const bArr = b.split('-')
        ret = aArr[1] - bArr[1]
    } else if (a.indexOf(year) !== -1) {
        // 1. 假设都是当天的期号
        const aIssue = a.replace(year, '')
        const bIssue = b.replace(year, '')
        ret = aIssue[1] - bIssue[1]
    } else {
        return a - b
    }
    return ret
}

/**
 * 格式化金额
 * @param {*} num 数字
 * @param {*} min 最小保留小数
 * @param {*} max 最大保留小数
 */
export const format = (num, min, max) => {
    const numStr = String(num)
    const numArr = numStr.split('.')
    const len = (numArr[1] && numArr[1].length) || 0

    if (len <= min) {
        return numeral(num).format('0.' + '0'.repeat(min))
    } else {
        return numeral(num)
            .format('0.' + '0'.repeat(max + 1))
            .substr(0, numArr[0].length + max + 1)
    }
}

/**
 * 格式化金额 如果小于最大值，不补0
 * @param {*} num 数字
 * @param {*} min 最小保留小数
 * @param {*} max 最大保留小数
 */
export const formatWithoutPadding = (num, min, max) => {
    const numStr = String(Number(num))
    const numArr = numStr.split('.')
    const len = (numArr[1] && numArr[1].length) || 0

    if (len <= min) {
        return numeral(num).format('0.' + '0'.repeat(min))
    } else if (len < max) {
        return numeral(num).format('0.' + '0'.repeat(len))
    } else {
        return numeral(num)
            .format('0.' + '0'.repeat(max + 1))
            .substr(0, numArr[0].length + max + 1)
    }
}

/**
 * 格式化金额 截取
 * @param {*} num 数字
 * @param {*} max 最大保留小数
 */
export const formatWithCut = (num, max) => {
    const numStr = String(num)
    const numArr = numStr.split('.')
    const len = (numArr[1] && numArr[1].length) || 0

    if (len === 0) {
        return `${numArr[0]}`
    } else {
        return `${numArr[0]}.${numArr[1].substr(0, max)}`
    }
}

/**
 * 格式化金额 最小保留min位小数，不足补0，超过max位截取
 * @param {*} num 数字
 * @param {*} min 最小保留小数位数
 * @param {*} max 最大保留小数位数
 */
export const formatMinToMax = (num, min, max) => {
    const numeralNum = numeral(num).value()
    const numStr = String(numeralNum)
    const numArr = numStr.split('.')
    const xiaoshu = (numArr[1] && parseInt(numArr[1])) || 0
    const xiaoshuLen = (xiaoshu && String(numArr[1]).length) || 0

    if (xiaoshu === 0 || xiaoshuLen < min) {
        return numeral(`${numArr[0]}.${numArr[1]}`).format('0.' + '0'.repeat(min))
    } else {
        return `${numArr[0]}.${numArr[1].substr(0, max)}`
    }
}

/**
 * 格式化金额 <=min位时显示min位小数，不足补0；
 *           >min位时显示max位小数，不足补0；
 * @param {*} num 数字
 * @param {*} min 最小保留小数位数
 * @param {*} max 最大保留小数位数
 */
export const formatMinOrMax = (num, min, max) => {
    const numeralNum = numeral(num).value()
    const numStr = String(numeralNum)
    const numArr = numStr.split('.')
    const xiaoshu = (numArr[1] && parseInt(numArr[1])) || 0
    const xiaoshuLen = (xiaoshu && String(numArr[1]).length) || 0

    if (xiaoshu === 0 || xiaoshuLen <= min) {
        return numeral(`${numArr[0]}.${numArr[1]}`).format('0.' + '0'.repeat(min))
    } else if (xiaoshuLen <= max) {
        return numeral(`${numArr[0]}.${numArr[1]}`).format('0.' + '0'.repeat(max))
    } else {
        return `${numArr[0]}.${numArr[1].substr(0, max)}`
    }
}

/**
 * 格式化金额 不预留小数，超过max位截取
 * @param {*} num 数字
 * @param {*} max 最大保留小数位数
 */
export const format0ToMax = (num, max) => {
    if (!num) num = 0
    const numStr = String(num)
    const numArr_ori = numStr.split('.')

    let numArr = []
    if (String(numArr_ori[1]).length > max) {
        numArr[0] = JSON.parse(JSON.stringify(numArr_ori[0]))
        numArr[1] = JSON.parse(JSON.stringify(String(numArr_ori[1]).substr(0, max)))
    } else {
        numArr = JSON.parse(JSON.stringify(numArr_ori))
    }

    const numeralNum = String(numeral(`${numArr[0]}.${numArr[1]}`).value())
    const numArr_new = numeralNum.split('.')
    const xiaoshu = (numArr_new[1] && parseInt(numArr_new[1])) || 0
    const xiaoshuLen = (xiaoshu && String(numArr_new[1]).length) || 0

    if (xiaoshu === 0) {
        return `${numArr_new[0]}`
    } else if (xiaoshuLen < max) {
        return `${numArr_new[0]}.${numArr_new[1]}`
    } else {
        return `${numArr_new[0]}.${numArr_new[1].substr(0, max)}`
    }
}
//金额格式化 截取小数点后2位数
Math.formatAmountPrecision = function (number, i = 4) {
    if (isNaN(number)) {
        return 0.00;
    }
    //为了避免精度问题，如 number = 1.89999999999999999
    number = String(parseFloat(number).toFixed(6));

    var index = number.indexOf('.') + i + 1;
    number = number.substring(0, index);
    return parseFloat(number);
}
/**
 * 每三位分割一次金额
 * @param {*} num 
 */
function interSplit(num) {
    return (num || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
}
/**
 * 整数每三位用，分割，默认不预留小数，超过decimalLen尾截断
 * @param {Number} decimalMaxLen 
 * @param {Number} num 
 */
export const splitAmoutBycomma = (num, decimalMaxLen = 2) => {
    let numArr = String(num).split(".");
    let int = interSplit(numArr[0]);
    let decimal = numArr[1];
    if (decimal && String(decimal).length) {
        decimal = String(decimal).substr(0, decimalMaxLen)
    }
    //截取小数前N位后，如果不是所有位数都是0
    if (parseInt(decimal) > 0) {
        return `${int}.${decimal}`;
    }
    else {
        return int;
    }

}

/**
 * 浮点数格式化为整数和小数2部分的html
 * @param {Num} digital
 */
export function format2Html(digital, maxLength = 999) {
    // if ((digital + '').length > 10) {
    //     let ipos = (digital + '').indexOf(".");
    //     digital = (digital + '').substring(0, ipos);
    // }
    const digitalArr = digital.split('.')
    console.log(digitalArr)
    if (digitalArr.length > 1 && digital.length < maxLength) {
        return `<span class="num">${toThousands(digitalArr[0])}.<small class="small">${digitalArr[1]}</small></span>`
    } else if(digitalArr[0].length < (maxLength - 3)) {
        return `<span class="num">${toThousands(digitalArr[0])}</span>`
    } else {
        return `<span class="num">${toThousands(digitalArr[0].substr(0, digitalArr[0].length - 3))}K</span>`
    }
}

/**
 * 时时彩大小单双判断
 * @param {*} code 球号
 */
export const getSSCBallStatus = code => {
    let daxiao
    let dashuang
    const intBall = Number(code)
    if (intBall > 4) {
        daxiao = '大'
    } else {
        daxiao = '小'
    }

    if (intBall % 2 === 1) {
        dashuang = '单'
    } else {
        dashuang = '双'
    }
    return [daxiao, dashuang]
}

/**
 * 根据汉字字符宽度获取字符串，一个单字节字符按半个汉字宽度计算
 * @param {Number} maxWidth 汉字字符宽度
 */
export function getStrByHansWidth(str, maxWidth) {
    let strWidthCounter = 0
    let cutedStr = ''
    for (const letter of str) {
        if (strWidthCounter >= maxWidth) {
            // 退出循环
            cutedStr = cutedStr + '...'
            break
        }

        if (letter.codePointAt(0) > 0xff) {
            // 双字节字符
            strWidthCounter = strWidthCounter + 1
        } else {
            // 单字节字符
            strWidthCounter = strWidthCounter + 0.5
        }
        cutedStr = cutedStr + letter
    }

    return cutedStr
}

/**
 * 计算可赢金额 双面盘使用
 * @param {Number} amount 投注金额
 * @param {Number} odds 赔率
 */
export function calcWinMoney(amount, odds) {
    if (!amount) amount = 0
    if (odds instanceof Array) {
        const oddArr = odds
            .map(item => {
                return item.odds
            })
            .sort()
        odds = oddArr[0]
    }
    const wrapAmount = numeral(amount)
    wrapAmount.multiply(odds).subtract(amount)
    return wrapAmount.value()
}

/**
 * 追号子注单 前台显示状态转换
 * @param {*} desc 
 */
export function transformTraceSubListStatus(desc) {
    if (desc == '挂起') {
        return '待开奖'
    }

    return desc
}

const mo = function (n) {
    if (n <= 1) {
        return 1
    }
    return mo(n - 1) * n
}
const A = function (n, m) {
    return mo(n) / mo(n - m)
}
export function C(n, m) {
    if (m > n) return 0
    return A(n, m) / mo(m)
}

export function getReturnUrl() {
    const queryStr = window.location.search
    const param = QS.parse(queryStr, { ignoreQueryPrefix: true })
    let returnUrl = param.returnUrl

    // 解码 base64
    if (returnUrl) {
        try {
            returnUrl = atob(returnUrl)
        } catch (err) {
            // 不做任何处理
        }
    }

    return returnUrl || ''
}

// 返回到app
export function go2App() {
    const returnUrl = getReturnUrl()
    window.location.href = returnUrl
}

/**
 * 获得字符串的字节长度
 * @param {*} str 
 * @returns 
 */
export function getBytesLength(str) {
    if(!str){
        return 0
    }
    var len = 0;  
    for (var i=0; i<str.length; i++) {  
        if (str.charCodeAt(i)>127 || str.charCodeAt(i)==94) {  
            len += 2;  
        } else {  
            len ++;  
        }  
    }
    return len;  
}

/** 
 * @description 日期格式化
 * @example  groupSplit(new Date(),yyyy-mm-dd) => 2019-08-08
 * 
*/  
window.formatDate = function (date, fmt = 'yyyy-mm-dd') {
    var o = {
        "M+": date.getMonth() + 1,                      //月份   
        "d+": date.getDate(),                           //日   
        "h+": date.getHours(),                          //小时   
        "m+": date.getMinutes(),                        //分   
        "s+": date.getSeconds(),                        //秒   
        "q+": Math.floor((date.getMonth() + 3) / 3),    //季度   
        "S":  date.getMilliseconds()                    //毫秒   
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

/**
 * 获取指定时区的时间戳
 * @param {*} str 
 */
window.getTimeForTimeZone = function (timezone = 8) {
    var offset_GMT = new Date().getTimezoneOffset();
    var nowDate = new Date().getTime();
    var targetDate = new Date(nowDate + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000).getTime();
    return targetDate;
}

export default {
    loadStyle,
    loadScript,
}

export * from './coldhotMiss'

export function copyToClipboard(e) {
    let textToCopy = e
    // navigator clipboard api needs a secure context (https)
    if (navigator.clipboard && window.isSecureContext) {
        // navigator clipboard api method'
        return navigator.clipboard.writeText(textToCopy);
    } else {
        // text area method
        let textArea = document.createElement("textarea");
        textArea.value = textToCopy;
        // make the textarea out of viewport
        textArea.style.position = "fixed";
        textArea.style.left = "-999999px";
        textArea.style.top = "-999999px";
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        return new Promise((res, rej) => {
            // here the magic happens
            document.execCommand('copy') ? res() : rej();
            textArea.remove();
        });
    }
}

/**
 * HiDPI Canvas Polyfill (1.0.10)
 *
 * Author: Jonathan D. Johnson (http://jondavidjohn.com)
 * Homepage: https://github.com/jondavidjohn/hidpi-canvas-polyfill
 * Issue Tracker: https://github.com/jondavidjohn/hidpi-canvas-polyfill/issues
 * License: Apache-2.0
*/
// 把人家插件改成单独调用的，直接用改动太多
export function canvasRatio(prototype) {
	var pixelRatio = (function() {
			var canvas = document.createElement('canvas'),
					context = canvas.getContext('2d'),
					backingStore = context.backingStorePixelRatio ||
						context.webkitBackingStorePixelRatio ||
						context.mozBackingStorePixelRatio ||
						context.msBackingStorePixelRatio ||
						context.oBackingStorePixelRatio ||
						context.backingStorePixelRatio || 1;

			return (window.devicePixelRatio || 1) / backingStore;
		})(),

		forEach = function(obj, func) {
			for (var p in obj) {
				if (obj.hasOwnProperty(p)) {
					func(obj[p], p);
				}
			}
		},

		ratioArgs = {
			'fillRect': 'all',
			'clearRect': 'all',
			'strokeRect': 'all',
			'moveTo': 'all',
			'lineTo': 'all',
			'arc': [0,1,2],
			'arcTo': 'all',
			'bezierCurveTo': 'all',
			'isPointinPath': 'all',
			'isPointinStroke': 'all',
			'quadraticCurveTo': 'all',
			'rect': 'all',
			'translate': 'all',
			'createRadialGradient': 'all',
			'createLinearGradient': 'all',
			'createLinearGradient': 'all'
		};

    prototype.pixelRatio = pixelRatio;
	if (pixelRatio === 1) return;

	forEach(ratioArgs, function(value, key) {
		prototype[key] = (function(_super) {
			return function() {
				var i, len,
					args = Array.prototype.slice.call(arguments);

				if (value === 'all') {
					args = args.map(function(a) {
						return a * pixelRatio;
					});
				}
				else if (Array.isArray(value)) {
					for (i = 0, len = value.length; i < len; i++) {
						args[value[i]] *= pixelRatio;
					}
				}

				return _super.apply(this, args);
			};
		})(prototype[key]);
	});

	 // Stroke lineWidth adjustment
	prototype.stroke = (function(_super) {
		return function() {
			this.lineWidth *= pixelRatio;
			_super.apply(this, arguments);
			this.lineWidth /= pixelRatio;
		};
	})(prototype.stroke);

	// Text
	//
	prototype.fillText = (function(_super) {
		return function() {
			var args = Array.prototype.slice.call(arguments);

			args[1] *= pixelRatio; // x
			args[2] *= pixelRatio; // y

			this.font = this.font.replace(
				/(\d+)(px|em|rem|pt)/g,
				function(w, m, u) {
					return (m * pixelRatio) + u;
				}
			);

			_super.apply(this, args);

			this.font = this.font.replace(
				/(\d+)(px|em|rem|pt)/g,
				function(w, m, u) {
					return (m / pixelRatio) + u;
				}
			);
		};
	})(prototype.fillText);

	prototype.strokeText = (function(_super) {
		return function() {
			var args = Array.prototype.slice.call(arguments);

			args[1] *= pixelRatio; // x
			args[2] *= pixelRatio; // y

			this.font = this.font.replace(
				/(\d+)(px|em|rem|pt)/g,
				function(w, m, u) {
					return (m * pixelRatio) + u;
				}
			);

			_super.apply(this, args);

			this.font = this.font.replace(
				/(\d+)(px|em|rem|pt)/g,
				function(w, m, u) {
					return (m / pixelRatio) + u;
				}
			);
		};
	})(prototype.strokeText);
};
export function canvasRatioContext(prototype) {
	prototype.getContext = (function(_super) {
		return function(type) {
			var backingStore, ratio,
				context = _super.call(this, type);

			if (type === '2d') {

				backingStore = context.backingStorePixelRatio ||
							context.webkitBackingStorePixelRatio ||
							context.mozBackingStorePixelRatio ||
							context.msBackingStorePixelRatio ||
							context.oBackingStorePixelRatio ||
							context.backingStorePixelRatio || 1;

				ratio = (window.devicePixelRatio || 1) / backingStore;

				if (ratio > 1) {
					// this.style.height = this.height + 'px';
					// this.style.width = this.width + 'px';
                    this.dataset.height = this.height
                    this.dataset.width = this.width
					this.width *= ratio;
					this.height *= ratio;
				}
			}

			return context;
		};
	})(prototype.getContext);
};