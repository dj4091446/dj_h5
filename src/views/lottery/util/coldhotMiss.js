/**
 * 通过玩法id解析出 [levelId][groupId][playId]
 * id格式为 [mode][ticketId][levelId][groupId][playId]
 * @param {*} id 
 */
export function parsePlayId(id) {
    const levelIdArr = String(id).match(/^(\d)(\d+)(\d{2})(\d{2})(\d{2})$/)
    const levelId = levelIdArr.slice(3, 4).join('')
    const groupId = levelIdArr.slice(3, 5).join('')
    const playId = levelIdArr.slice(3, 6).join('')
 
    return {
       levelId,
       groupId,
       playId,
    }
 }

/**
 * 标准盘 处理冷热遗漏的递归函数
 * @param {*}  row 
 */
export function calcStandardHotOrMissRow(row) {
    let result = [];
    // 获取最大最小值
    let max, min, colorType;
    row.forEach(item => {
        if (item instanceof Array) {
            // 特殊玩法
            item.forEach(ball => {
                max = Math.max.apply(null, item);
                min = Math.min.apply(null, item);
                colorType = {
                    [max]: '1',
                    [min]: '3'
                }[Number(item)] || '2';
                
                result.push({
                    count: ball, 
                    colorType,
                });
            });
        } else {
            // 普通玩法
            max = Math.max.apply(null, row);
            min = Math.min.apply(null, row);
            colorType = {
                [max]: '1',
                [min]: '3'
            }[Number(item)] || '2';
 
            result.push({
                count: item, 
                colorType,
            });
        }
    });
    return result;
 }
 
 export function calcStandardHotOrMissColor(data) {
    const { '20Q': Q20, '50Q': Q50, '100Q': Q100, miss } = data
    const codeHotMap = {
       '20Q': Q20,
       '50Q': Q50,
       '100Q': Q100,
    }
    for (const key in codeHotMap) {
       const coldHot = codeHotMap[key]
       for (const playId in coldHot) {
          let playColdHot = coldHot[playId]
          if (Object.prototype.toString.call(playColdHot) === '[object String]') {
             // 参考其他玩法的数据
             playColdHot = coldHot[playColdHot]
          }
 
          // 按照行来计算颜色值
          // TODO delete 目前数据有问题，出现以为数组，为了调试添加
          if (playColdHot[0] instanceof Array) {
             playColdHot = playColdHot.map(playColdHotRow => {
                if (playColdHotRow[0] && playColdHotRow[0].colorType) {
                   // 已经计算过了
                   return playColdHotRow
                }
 
                checkFormat({playId}, playColdHotRow)
                return calcStandardHotOrMissRow(playColdHotRow)
             })
          } else {
             checkFormat({playId}, playColdHot)
             playColdHot = [calcStandardHotOrMissRow(playColdHot)]
          }
          coldHot[playId] = playColdHot
       }
    }
 
    // 获取遗漏，排除没有返回遗漏的数据
    if (miss) {
       for (const playId in miss) {
          let missList = miss[playId]
 
          if (Object.prototype.toString.call(missList) === '[object String]') {
             // 参考其他玩法的数据
             missList = miss[missList]
          }
 
          // 按照行来计算颜色值
          // TODO delete 目前数据有问题，出现以为数组，为了调试添加
          if (missList[0] instanceof Array) {
             missList = missList.map(missListRow => {
                if (missListRow[0] && missListRow[0].colorType) {
                   // 已经计算过了
                   return missListRow
                }
    
                checkFormat({playId}, missListRow)
                return calcStandardHotOrMissRow(missListRow)
             })
          } else {
             checkFormat({playId}, missList)
             missList = [calcStandardHotOrMissRow(missList)]
          }
 
          miss[playId] = missList
       }
    }
 
    return {
       codeHotMap,
       missMap: miss,
    }
 }
 
 function checkFormat(context, data) {
    if (Object.prototype.toString.call(data) === '[object Object]') {
       throw Error(`返回的数据格式错误 playId=${context.playId} ${JSON.stringify(data, null, 2)}`)
    }
 }
 