/* global sxArray:readonly sxNumArray:readonly */
import moment from "moment"
import i18n from "@/assets/i18n"

const i18nLotteryName = i18n.t('homepage.lotteryName');

export function judgeJKC(ticketId) {
    return false;
}

export const allLotteryList = [
    {
        id: 17,
        name: i18nLotteryName.djc,
        ticketId: [268],
        list: [],
        code: 'DJC',
        seriesKind: 2,
    }
]

export const lolMap1 = {
    2: 0,
    3: 0,
    4: 1,
    5: 1,
    6: 1,
    7: 2,
    8: 2,
    9: 2,
    10: 3,
    11: 3,
    12: 4,
    13: 5,
}
export const lolMap2 = {
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
    7: 0,
    8: 0,
    9: 0,
    10: 1,
    11: 1,
    12: 1,
    13: 1,
}
export function getLolTypes(historyList) {
    return historyList.map(v => ({
        type1: map1[Number(v.slice(0,2))],
        type2: map2[Number(v.slice(0,2))],
        attack: v.slice(2,4),
        gender: v.slice(5),
    }))
}

export class DjPlanTime {
    constructor({time, plan}) {
        if(time) {
            plan = this.toPlan(time)
        }
        if(plan) {
            time = this.toDate(plan)
        }
        this.plan = plan
        this.time = time
    }

    toDate(plan = this.plan) {
        let [date, pl] = plan.split('-')
        return moment(date, "YYYYMMDD").startOf('day').add(Number(pl) * 20 - 5, 'seconds')
    }

    toPlan(time = this.time) {
        let date = moment(time)
        let pl = time.getHours()*180 + time.getMinutes() * 3 + Math.floor(time.getSeconds() / 20)
       
        if(pl <= 0) {
            return date.subtract(1, 'days').format('YYYYMMDD') + '-' + String(4320 + pl).padStart(4,'0')
        }
        return date.format('YYYYMMDD') + '-' + String(pl).padStart(4,'0')
    }

    substract(t) {
        if(typeof t === 'number') {
            let [date, pl] = this.plan.split('-')
            if(Number(pl) - t <= 0) {
                pl = 4320 + Number(pl) - t
                this.time = moment(date, "YYYYMMDD").subtract(1, 'days').add(pl*20, 'seconds')
                this.plan = this.toPlan(this.time.toDate())
            } else {
                this.plan = date + '-' + String(Number(pl) - t).padStart(4,'0')
                this.time = this.toDate(this.plan)
            }
        } else if (t instanceof Date) {

        }
        return this
    }

    add(t) {
        if(typeof t === 'number') {
            let [date, pl] = this.plan.split('-')
            if(Number(pl) + t > 4320) {
                pl = Number(pl) + t - 4320
                this.time = moment(date, "YYYYMMDD").add(1, 'days').add(pl*20, 'seconds')
                this.plan = this.toPlan(this.time.toDate())
            } else {
                this.plan = date + '-' + String(Number(pl) + t).padStart(4,'0')
                this.time = this.toDate(this.plan)
            }

        } else if (t instanceof Date) {
            
        }
        return this
    }
}
