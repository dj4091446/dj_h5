import { findBallsToSX, getLolTypes, lolMap1, lolMap2 } from '@/views/lottery/util/lottery'
//import calendar from '@/assets/scripts/calendar'
import i18n from "@/assets/i18n"
const lolyuan = new Image()
const loljin = new Image()
const lolnan = new Image()
const lolnv = new Image()
lolyuan.src = require('@/assets/images/tese-lol/tb/remote-icon2.png')
loljin.src = require('@/assets/images/tese-lol/tb/melee-icon2.png')
lolnan.src = require('@/assets/images/tese-lol/tb/icon_male.png')
lolnv.src = require('@/assets/images/tese-lol/tb/icon_female.png')


// 求和值
const getSum = function(arr) {
    let sum = 0
    for (let i = 0; i < arr.length; i++) {
        sum += Number(arr[i])
    }
    return sum
}
// 选出号码对应位置的球
const positionSplit = function(arr, splitArr) {
    const newArr = []
    splitArr.map(ele => {
        newArr.push(Number(arr[ele]))
    })
    return newArr
}
// 判断质合
const isPrimeNum = function(num) {
    num = Number(num)
    if (num === 0) {
        num = 10
    }
    for (let i = 2; i < num; i++) {
        if (num % i === 0) {
            return false
        }
    }
    return true
}
// 求和值尾数
const getHeZhiTailNum = function(nums) {
    const weishu = getSum(nums) % 10
    return weishu
}
// 个位十位相加和值
const getGeShiSum = function(nums) {
    const frist = String(nums[0]).slice(0, 1)
    const last = String(nums[0]).slice(1, 2)
    const sum = Number(frist) + Number(last)
    return sum
}
const countYearShengXiao = function(code,saleStartTime) {
    // const shengxiaoValue = findBallsToSX(code)
    // let index = shengxiao.indexOf(shengxiaoValue)
    // if (saleStartTime) {
    //     let now = new Date()
    //     let issueDate = new Date(saleStartTime)
    //     // 公历转农历获取年份-当前
    //     let nowYear = calendar.solar2lunar(
    //         now.getFullYear(),
    //         now.getMonth() + 1,
    //         now.getDate(),
    //     ).lYear;
    //     // 公历转农历获取年份-奖期
    //     let issueYear = calendar.solar2lunar(
    //         issueDate.getFullYear(),
    //         issueDate.getMonth() + 1,
    //         issueDate.getDate(),
    //     ).lYear
    //     if (nowYear !== issueYear) {
    //         index = index - (nowYear - issueYear);
    //         if (index < 0) {
    //             index = index%12 + 12
    //         }
    //     }
    // }
    // return shengxiao[index]
}
// 天地肖
const getTianDiXiao = function(nums,saleStartTime) {
    const nowSX = tianDiXiao.find(ele => ele.list.includes(countYearShengXiao(nums[0],saleStartTime)))
    return nowSX.name
}
// 前后肖
const getQianHouXiao = function(nums,saleStartTime) {
    const nowSX = qianHouXiao.find(ele =>ele.list.includes(countYearShengXiao(nums[0],saleStartTime)))
    return nowSX.name
}
// 家野肖
const getJiaYeXiao = function(nums,saleStartTime) {
    const nowSX = jiaYeXiao.find(ele => ele.list.includes(countYearShengXiao(nums[0],saleStartTime)))
    return nowSX.name
}
// 获取生肖号码
const getSXNum = function(nums, saleStartTime) {
    let newnums = []
    nums.map(ele => {
        newnums.push(countYearShengXiao(ele,saleStartTime))
    })
    newnums = [...new Set(newnums)]
    return newnums.length
}
// 上下盘计算
const getShangXiagPan = function(arr) {
    const [shang, xia] = [[], []]
    arr.forEach(ele => {
        if (Number(ele) > 40) {
            xia.push(ele)
        } else {
            shang.push(ele)
        }
    })
    return {
        shang: shang.length,
        xia: xia.length,
    }
}
// 基偶盘
const getJiOuPan = function(arr) {
    let [ji, ou] = [0, 0]
    arr.map(ele => {
        if (ele % 2 === 0) {
            ou++
        } else {
            ji++
        }
    })
    return {
        ji: ji,
        ou: ou,
    }
}
// 双色球大小多
const getDaXiaoPan = function(arr) {
    let [da, xiao] = [0, 0]
    arr.map(ele => {
        if (ele > 16) {
            da++
        } else {
            xiao++
        }
    })
    return {
        da: da,
        xiao: xiao,
    }
}
// 万字4D大小多
const getDaXiaoPanWZ4D = function(arr) {
    let [da, xiao] = [0, 0]
    arr.map(ele => {
        if (ele*1 > 4999) {
            da++
        } else {
            xiao++
        }
    })
    return {
        da: da,
        xiao: xiao,
    }
}
// 双色球区对比龙虎
const getSSQLongHu = function(arr) {
    let quCount = [0, 0, 0];
    arr.map(ele => {
        if (Number(ele) < 12) {
            quCount[0] += 1;
        } else if (Number(ele) > 22) {
            quCount[2] += 1;
        } else {
            quCount[1] += 1;
        }
    })
    return quCount
}

// 号码三个重复
const isTreeSame = function(arr) {
    let [obj, numlen, moretimes] = [{}, 0, 0]
    for (let i = 0; i < arr.length; i++) {
        if (!obj[arr[i]]) {
            obj[arr[i]] = 1
        } else {
            obj[arr[i]] += 1
        }
    }
    for (const k in obj) {
        numlen += 1
        if (obj[k] === 3) {
            moretimes += 1
        }
    }
    if (moretimes === 1 && numlen === arr.length - 2) {
        return true
    }
    return false
}
// 六合彩判断号码是否包含49
class lottery {

    // lol 远近
    static async setTSLOLYJ(newarr) {
        const arr = []
        newarr.map((ele, i) => {
            let t = lolMap2[Number(ele.slice(0,2))]
            arr.push({
                name: '',
                text: t,
                img: t ? lolyuan : loljin,
                code: ele.code,
                func: 'createdImg',
                position: { x: 0, y: 0 },
                index: i,
            })
        })
        lottery.setLotteryOpenNum.call(this, arr)
        return arr
    }

    // lol 男女
    static async setTSLOLNN(newarr) {
        const arr = []
        newarr.map((ele, i) => {
            arr.push({
                name: '',
                text: ele.slice(6),
                img: ele.slice(6) === '1' ? lolnan : lolnv,
                code: ele.code,
                func: 'createdImg',
                position: { x: 0, y: 0 },
                index: i,
            })
        })
        lottery.setLotteryOpenNum.call(this, arr)
        return arr
    }

    // lol 通用
    static async setTSLOLTY(newarr) {
        const arr = []
        newarr.map((ele, i) => {
            arr.push({
                name: '',
                text: parseInt(ele.slice(0,2)),
                code: ele.code,
                func: 'createdTyArc',
                position: { x: 0, y: 0 },
                index: i,
                fontSize: '12px',
                color: '#fff',
                border: [0.1, '#fff'],
                background: ele.slice(6) === '1' ? '1' : '2'
            })
        })
        lottery.setLotteryOpenNum.call(this, arr)
        return arr
    }

    // 统一处理lotteryOpenNum
    static async setLotteryOpenNum(arr) {
        await lottery.setBigPredict.call(this, arr)
        await lottery.setSmallPredict.call(this, arr)
        // 是否有问路 有 继续问路
        if( this.isBigWay ){
            this.isBigWay = false;
            this.askBigWay && this.askBigWay();
        }
        if( this.isSmallWay ){
            this.isSmallWay = false;
            this.askSmallWay && this.askSmallWay();
        }
    }

    // 问路预测 -- 开大预测
    static setBigPredict(arr) {
        arr = arr.filter(ele => !ele.isPredict)
        const addArr = arr.find(ele => ele.name == 'red')
        // addArr.code = '5 4 3 2 1' // 预测时闪烁不带庄闲对
        arr.unshift({
            isPredict: true,
            map: 'red',
            ...addArr,
        })
        this.lotteryOpenNum = arr
    }

    // 问路预测 -- 开小预测
    static setSmallPredict(arr) {
        arr = arr.filter(ele => !ele.isPredict)
        const addArr = this.lotteryOpenNum.find(ele => ele.name == 'blue')
        // addArr.code = '1 2 3 4 5' // 预测时闪烁不带庄闲对
        arr.unshift({
            isPredict: true,
            map: 'blue',
            ...addArr,
        })
        this.lotteryOpenNum = arr
    }

    // 大问路
    static mapBigWay() {
        // 清除小路问路
        if (this.isSmallWay) {
            this.isSmallWay = false
            this.lotteryOpenNum.shift()
        }
        // 开始大路问路
        if (this.isBigWay) {
            this.isBigWay = false
            this.lotteryOpenNum.shift()
        } else {
            const newOpenNum = this.lotteryOpenNum.filter(ele => !ele.isPredict)
            let bigWay
            if (this.ticketType === 'BJL') {
                bigWay = newOpenNum.find(ele => ele.func === 'createdRedNotFillArc')
            } else {
                bigWay = newOpenNum.find(ele => ele.func === 'createdRedFillArc')
            }
            newOpenNum.map(ele => {
                ele.position = {
                    x: 0,
                    y: 0,
                }
            })
            this.isBigWay = true
            this.lotteryOpenNum = [
                {
                    ...bigWay,
                    isAdd: true,
                    addFunc: 'createYellowWay',
                },
            ].concat(newOpenNum)
        }
    }

    // 小问路
    static mapSmallWay() {
        // 清除小路问路
        if (this.isBigWay) {
            this.isBigWay = false
            this.lotteryOpenNum.shift()
        }
        // 开始大路问路
        if (this.isSmallWay) {
            this.isSmallWay = false
            this.lotteryOpenNum.shift()
        } else {
            const newOpenNum = this.lotteryOpenNum.filter(ele => !ele.isPredict)
            let bigWay
            if (this.ticketType === 'BJL') {
                bigWay = newOpenNum.find(ele => ele.func === 'createdBlueNotFillArc')
            } else {
                bigWay = newOpenNum.find(ele => ele.func === 'createdBlueFillArc')
            }
            this.isSmallWay = true
            newOpenNum.map(ele => {
                ele.position = {
                    x: 0,
                    y: 0,
                }
            })
            this.lotteryOpenNum = [
                {
                    ...bigWay,
                    isAdd: true,
                    addFunc: 'createYellowWay',
                },
            ].concat(newOpenNum)

        }
    }
    // 关闭所有问路
    static mapCloseAskWay(){
        if( this.lotteryOpenNum.length == 0 ){
            return;
        }
        const hasAsk = this.lotteryOpenNum[0].isAdd;
        if(hasAsk){
            this.isSmallWay = false;
            this.isBigWay = false;
            this.lotteryOpenNum.shift()
        }
    }
}

export default lottery
