// canvas 画图工具
import anime from 'animejs'
import { canvasRatio, canvasRatioContext } from '@/views/lottery/util/index'
class createdElement {
    constructor(props) {
        const {
            $refs: { canvas },
            marginXY,
        } = props
        canvas.height = marginXY * 6
        canvas.width = marginXY * 100
        this.ctx = canvas.getContext('2d')
        this.canvas = canvas
        this.marginXY = marginXY
    }

    // 画边界
    createMapXY() {
        this.ctx.beginPath()
        this.ctx.moveTo(0, 0)
        this.ctx.lineTo(this.canvas.width, 0)
        this.ctx.lineTo(this.canvas.width, this.canvas.height)
        this.ctx.lineTo(0, this.canvas.height)
        this.ctx.lineTo(0, 0)
        this.ctx.lineWidth = 1
        this.ctx.strokeStyle = this.borderStrokeStyle
        this.ctx.stroke()
    }

    // 画表格横线
    createdTableRows(gap) {
        this.ctx.beginPath()
        // var rows = this.canvas.height / this.marginXY
        for (let i = gap; i < 6; i+= gap) {
            this.ctx.moveTo(0, i * this.marginXY)
            this.ctx.lineTo(this.canvas.width, i * this.marginXY)
        }
        this.ctx.closePath()
        this.ctx.lineWidth = 0.5
        this.ctx.strokeStyle = this.borderStrokeStyle
        this.ctx.stroke()
    }

    // 画表格竖线
    createdTableCols(gap) {
        this.ctx.beginPath()
        // var cols = this.canvas.width / this.marginXY
        for (let i = gap; i < 100; i+= gap) {
            this.ctx.moveTo(i * this.marginXY, 0)
            this.ctx.lineTo(i * this.marginXY, this.canvas.height)
        }
        this.ctx.closePath()
        this.ctx.lineWidth = 0.5
        this.ctx.strokeStyle = this.borderStrokeStyle
        this.ctx.stroke()
    }

    // 画实心点阵
    createdTablePoint() {
        var cols = this.canvas.width / this.marginXY
        var rows = this.canvas.height / this.marginXY
        for (let n = 0; n < cols; n++) {
            for (let m = 0; m < rows; m++) {
                this.ctx.beginPath()
                this.ctx.arc(
                    n * this.marginXY + this.marginXY / 2,
                    m * this.marginXY + this.marginXY / 2,
                    2,
                    0,
                    2 * Math.PI
                )
                this.ctx.fillStyle = '#DDD'
                this.ctx.strokeStyle = '#DDD'
                this.ctx.fill()
                this.ctx.stroke()
            }
        }
    }

    // 字体大小
    fontSize() {
        return `${(this.marginXY * 12) / 20}px`
    }

    // 红色文字
    createdRedFillArc(item) {
        const { x, y } = item.position
        const text = item.text
        const drawAction = () => {
            this.ctx.textAlign = 'center'
            this.ctx.textBaseline = 'middle'
            this.ctx.beginPath()
            this.ctx.fillStyle = '#e3253a'
            this.ctx.font = this.fontSize() + ' arial'
            this.ctx.fillText(text, x, y)
            this.ctx.stroke()
        }
        drawAction()
    }

    // 蓝色文字
    createdBlueFillArc(item) {
        const { x, y } = item.position
        const text = item.text
        const drawAction = () => {
            this.ctx.textAlign = 'center'
            this.ctx.textBaseline = 'middle'
            this.ctx.beginPath()
            this.ctx.fillStyle = '#367af6'
            this.ctx.font = this.fontSize() + ' arial'
            this.ctx.fillText(text, x, y)
            this.ctx.stroke()
        }
        drawAction()
    }
    // 白色文字
    createdWhiteFillArc(item) {
        const { x, y } = item.position
        const text = item.text
        const drawAction = () => {
            this.ctx.textAlign = 'center'
            this.ctx.textBaseline = 'middle'
            this.ctx.beginPath()
            this.ctx.fillStyle = '#ffffff'
            let t = parseInt(this.fontSize()) * (item.scales || 1)
            this.ctx.font = t + 'px arial'
            this.ctx.fillText(text, x, y)
            this.ctx.stroke()
        }
        drawAction()
    }
    
    // 绿色文字
    createdGreenFillArc(item) {
        const {
            position: { x, y },
            text,
        } = item
        this.ctx.textAlign = 'center'
        this.ctx.textBaseline = 'middle'
        this.ctx.beginPath()
        this.ctx.fillStyle = '#49d174'
        this.ctx.font = this.fontSize() + ' arial'
        this.ctx.fillText(text, x, y)
        this.ctx.stroke()
    }
    // 绿色文字
    createdTyArc(item) {
        const {
            position: { x, y },
            text,
        } = item
        this.ctx.beginPath()
        let radius = ((item.scales || 1) * this.marginXY) / 2 - 2
        if(item.background) {
            if(item.border) {
                this.ctx.arc(x, y, radius + item.border[0], 0, 2 * Math.PI)
                //this.ctx.strokeStyle = item.border[1] || '#000'
                if (item.background == '1') {
                    this.ctx.strokeStyle = '#24589B';
                } else {
                    this.ctx.strokeStyle = '#D04C4C';
                }
                this.ctx.stroke()
            }
            this.ctx.arc(x, y, radius, 0, 2 * Math.PI)
            let startX = x;
            let startY = y - radius;
            let endX = x
            let endY = y + radius;
            // 渐变
            let grd1 = this.ctx.createLinearGradient(startX, startY, endX, endY); // 180deg
            grd1.addColorStop(0, 'rgba(121,34,34,1)');
            grd1.addColorStop(1, 'rgba(121,34,34,0.4)');
            let grd2 = this.ctx.createLinearGradient(startX, startY, endX, endY);// 180deg
            grd2.addColorStop(0, 'rgba(19,68,120,1)');
            grd2.addColorStop(1, 'rgba(19,68,120,0.4)');

            //this.ctx.fillStyle = item.background || '#000'
            //this.ctx.fillStyle = grd1;
            if (item.background == '1') {
                this.ctx.fillStyle = grd2;
            } else{
                this.ctx.fillStyle = grd1;
            }
            this.ctx.fill()
        }
        this.ctx.textAlign = 'center'
        this.ctx.textBaseline = 'top'
        this.ctx.fillStyle = item.color || '#fff'
        this.ctx.font = ((item.fontSize || 14) * (item.scales || 1)) + 'px arial'
        this.ctx.fillText(text, x, y - radius/2)
    }

    // 画红色实心圆
    createdBlueFillArcNotW(item) {
        const {
            position: { x, y },
        } = item
        this.ctx.beginPath()
        this.ctx.arc(x, y, ((item.scales || 1) * this.marginXY) / 2 - 4, 0, 2 * Math.PI)
        this.ctx.fillStyle = this.blueFillStyle
        this.ctx.strokeStyle = this.blueFillStyle
        this.ctx.fill()
        this.ctx.stroke()
    }

    // 画蓝色实心圆
    createdRedFillArcNotW(item) {
        const {
            position: { x, y },
        } = item
        this.ctx.beginPath()
        this.ctx.arc(x, y, ((item.scales || 1) * this.marginXY) / 2 - 4, 0, 2 * Math.PI)
        this.ctx.fillStyle = this.redFillStyle
        this.ctx.strokeStyle = this.redFillStyle
        this.ctx.fill()
        this.ctx.stroke()
    }

    // 画红色空心圆
    createdRedNotFillArc(item) {
        const {
            position: { x, y },
        } = item
        this.ctx.beginPath()
        this.ctx.arc(x, y, this.marginXY / 2 - 5, 0, 2 * Math.PI)
        this.ctx.fillStyle = this.redFillStyle
        this.ctx.strokeStyle = this.redFillStyle
        this.ctx.lineWidth = 5
        this.ctx.stroke()
    }

    // 画绿色空心圆
    createdBlueNotFillArc(item) {
        const {
            position: { x, y },
        } = item
        this.ctx.beginPath()
        this.ctx.arc(x, y, this.marginXY / 2 - 5, 0, 2 * Math.PI)
        this.ctx.strokeStyle = '#367af6'
        this.ctx.lineWidth = 5
        this.ctx.stroke()
    }

    // 红色斜线段
    createLineRed(item) {
        const {
            position: { x, y },
        } = item
        this.ctx.beginPath()
        this.ctx.moveTo(x + this.marginXY / 2 - 4, y - this.marginXY / 2 + 4)
        this.ctx.lineTo(x - this.marginXY / 2 + 4, y + this.marginXY / 2 - 4)
        this.ctx.lineWidth = 4
        this.ctx.lineCap = 'round'
        this.ctx.strokeStyle = '#e3253a'
        this.ctx.stroke()
    }

    // 绿色斜线段
    createLineBlue(item) {
        const {
            position: { x, y },
        } = item
        this.ctx.beginPath()
        this.ctx.moveTo(x + this.marginXY / 2 - 4, y - this.marginXY / 2 + 4)
        this.ctx.lineTo(x - this.marginXY / 2 + 4, y + this.marginXY / 2 - 4)
        this.ctx.lineWidth = 4
        this.ctx.lineCap = 'round'
        this.ctx.strokeStyle = '#367af6'
        this.ctx.stroke()
    }

    // 问路颜色
    createYellowWay(item) {
        const {
            position: { x, y },
        } = item
        this.ctx.beginPath()
        this.ctx.globalAlpha = 0.9
        this.ctx.fillStyle = 'yellow'
        this.ctx.strokeStyle = 'yellow'
        this.ctx.fillRect(
            x - this.marginXY / 2,
            y - this.marginXY / 2,
            this.marginXY,
            this.marginXY
        )
        this.ctx.stroke()
    }

    // 白色文字
    createdImg(item) {
        let ratio = this.ctx.pixelRatio
        const { x, y } = item.position
        const img = item.img
        let w = (item.scales || 1) * 14
        this.ctx.drawImage(img,(x-w / 2)*ratio,(y-w / 2) * ratio,(w || 14) * ratio,(w || 14) * ratio)
    }
}

// canvas 画图规则
class drawCanvasMap extends createdElement {
    constructor(props) {
        const {
            $refs: { canvas },
            getPredict,
            marginXY,
            dataList,
            template,
            changeLeft,
            minXY,
            animation,
            gap,
            borderStrokeStyle,
            redFillStyle,
            blueFillStyle,
        } = props
        super(props)
        this.setMapXY = []
        this.canvas = canvas
        this.isAnimation = animation
        this.animationPoints = []
        this.animationHandle = null
        this.dataList = dataList
        this.template = template
        this.changeLeft = changeLeft
        this.getPredict = getPredict
        this.minXY = minXY
        this.gap = gap
        this.borderStrokeStyle = borderStrokeStyle
        this.redFillStyle = redFillStyle
        this.blueFillStyle = blueFillStyle
        this.drawAction() 
    }

    drawAction() {
        // this.createdTablePoint() // 点
        this.createdTableRows(this.gap.row || 1) // 竖线表格 暂时注释
        this.createdTableCols(this.gap.col || 1) // 横线表格  暂时注释
        this.createMapXY() // 画边界 暂时注释\
        this.setMapXY = this.setXY(100, 6)
        this.mathData()
    }

    setDataList(dataList) {
        this.animationPoints = []
        this.dataList = dataList
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        this.canvas.height = this.canvas.height
        this.canvas.width = this.canvas.width
        this.drawAction()
    }

    animationAction(action) {
        const drawBasicBg = target => {
            this.ctx.beginPath()
            this.ctx.arc(target.position.x, target.position.y, 2, 0, 2 * Math.PI)
            this.ctx.fillStyle = '#FFF'
            this.ctx.strokeStyle = '#FFF'
            this.ctx.fill()
            this.ctx.stroke()
        }
        const clearTarget = target => {
            this.ctx.clearRect(
                target.position.x - this.marginXY / 2,
                target.position.y - this.marginXY / 2,
                this.marginXY,
                this.marginXY
            )
        }
        if (!action && this.animationHandle) {
            clearInterval(this.animationHandle)
            this.animationPoints.forEach(item => {
                clearTarget(item)
                drawBasicBg(item)
            })
            this.animationPoints = []
            return
        }
        let isDraw = true
        this.animationHandle = setInterval(() => {
            this.animationPoints.forEach(item => {
                clearTarget(item, isDraw)
                if (!isDraw) {
                    if (item.addFunc) {
                        this[item.addFunc](item)
                    }
                    this[item.func](item)
                } else {
                    drawBasicBg(item)
                }
                isDraw = !isDraw
            })
        }, 700)
    }
    animationActionLol() {
        if(this.ctx) {
            let om = new Image(this.canvas.width, this.canvas.height)
            om.src = this.canvas.toDataURL()
            let _this = this
            let item = this.setMapXY[0][0]
            let maxIndex = 0
            for(let i = 0; i < 100; i++) {
                for(let j = 0; j < 6; j++) {
                    let obj = this.setMapXY[i][j]
                    if(!obj || !obj.index) {
                        continue
                    }
                    if(obj.index > maxIndex) {
                        maxIndex = obj.index
                        item = obj
                    }
                    if(obj.index == 99) {
                        break
                    }
                }
            }
            item.scales = 1
            anime({
                targets: item,
                keyframes: [
                    {scales: 1.8},
                    {scales: 1.4},
                    {scales: 1.8},
                    {scales: 1.4},
                    {scales: 1.8},
                    {scales: 1},
                ],
                easing: 'linear',
                autoplay: true,
                duration: 3000,
                update: function() {
                    this.ctx.clearRect(0,0,this.canvas.width, this.canvas.height);  
                    this.ctx.drawImage(om,0,0)
                    this.ctx.clearRect(item.position.x - 10,item.position.y - 10,20,20);  
  
                    if (item.addFunc) {
                        this[item.addFunc](item)
                    }
                    this[item.func](item)
                }.bind(_this),
            });
        }
    }

    mathData() {
        
        const data = this.dataList
        if (this.template === 'dalu') {
            this.initAgin(data)
            // 画图
            this.setMapXY.map(ele => {
                ele.map(item => {
                    if (item.selected && !item.isPredict) {
                        if (item.addFunc) {
                            this[item.addFunc](item)
                        }
                        this[item.func](item)
                    }
                })
            })
        }

        if (this.template === 'dayanzai') {
            this.initAgin(this.delHEdata(data))

            const startXY = this.findFristPosition(1, 'dayanzai')
            if (!startXY) {
                return
            }
            this.createdXiaoLuRules(
                startXY,
                'createdRedNotFillArc',
                'createdBlueNotFillArc',
                2,
                data[0].addFunc
            )

            // 画图
            this.setMapXY.map(ele => {
                ele.map(item => {
                    if (item.selected && !item.isPredict) {
                        if (item.addFunc) {
                            this[item.addFunc](item)
                        }
                        this[item.func](item)
                    }
                    if (item.isPredict) {
                        this.getPredict({
                            ...item,
                            codeName: 'dayangzai',
                        })
                    }
                })
            })
        }
        // 小路
        if (this.template === 'xiaolu') {
            this.initAgin(this.delHEdata(data))
            const startXY = this.findFristPosition(2, 'xiaolu')
            if (!startXY) {
                return
            }
            this.createdXiaoLuRules(
                startXY,
                'createdRedFillArcNotW',
                'createdBlueFillArcNotW',
                3,
                data[0].addFunc
            )
            // 画图
            this.setMapXY.map(ele => {
                ele.map(item => {
                    if (item.selected && !item.isPredict) {
                        if (item.addFunc) {
                            this[item.addFunc](item)
                        }
                        this[item.func](item)
                    }
                    if (item.isPredict) {
                        this.getPredict({
                            ...item,
                            codeName: 'xiaolu',
                        })
                    }
                })
            })
        }
        // 曱甴路
        if (this.template === 'yueyou') {
            this.initAgin(this.delHEdata(data))
            const startXY = this.findFristPosition(3, 'yueyou')
            if (!startXY) {
                return
            }
            this.createdXiaoLuRules(startXY, 'createLineRed', 'createLineBlue', 4, data[0].addFunc)
            // 画图
            this.setMapXY.map(ele => {
                ele.map(item => {
                    if (item.selected && !item.isPredict) {
                        if (item.addFunc) {
                            this[item.addFunc](item)
                        }
                        this[item.func](item)
                    }
                    if (item.isPredict) {
                        this.getPredict({
                            ...item,
                            codeName: 'yueyou',
                        })
                    }
                })
            })
        }
        
        // 特色pk10
        if (/tspk10_/.test(this.template)) {
            let arr = this.template.split('_')
            let a = parseInt(arr[1])
            let b = parseInt(arr[2])
            this.initAgin(data.map(v => ({
                ...v,
                text: v.allResult ? v.allResult[a][b] : '',
                addFunc: (a + 1) === (v.allResult ? v.allResult[a][b] : '') ? 'createdRedFillArcNotW' : 'createdBlueFillArcNotW'
            }))) 
            // 画图
            this.setMapXY.map(ele => {
                ele.map(item => {
                    if (item.selected && !item.isPredict) {
                        if (item.addFunc) {
                            this[item.addFunc](item)
                        }
                        this[item.func](item)
                    }
                })
            })
        }

        const { maxX } = this.findMaxXY()
        this.canvas.width = this.marginXY * (maxX >= this.minXY ? maxX + 3 : this.minXY) * (this.ctx.pixelRatio || 1)
        this.createdTableRows(this.gap.row || 1) // 竖线表格 暂时注释
        this.createdTableCols(this.gap.col || 1) // 横线表格  暂时注释
        this.createMapXY() // 画边界 暂时注释

        // 画图到最大尺寸
        if (this.template === 'dalu' || /tspk10_/.test(this.template)) {
            // 画图
            this.setMapXY.map(ele => {
                ele.map(item => {
                    if (item.selected && !item.isPredict) {
                        if (item.addFunc) {
                            this[item.addFunc](item)
                            this.animationPoints.push(item)
                        }
                        this[item.func](item)
                    }
                })
            })
        }

        if (this.template === 'dayanzai') {
            // 画图
            this.setMapXY.map(ele => {
                ele.map(item => {
                    if (item.selected && !item.isPredict) {
                        if (item.addFunc) {
                            this[item.addFunc](item)
                            this.animationPoints.push(item)
                        }
                        this[item.func](item)
                    }
                    if (item.isPredict) {
                        this.getPredict({
                            ...item,
                            codeName: 'dayangzai',
                        })
                    }
                })
            })
        }
        // 小路
        if (this.template === 'xiaolu') {
            // 画图
            this.setMapXY.map(ele => {
                ele.map(item => {
                    if (item.selected && !item.isPredict) {
                        if (item.addFunc) {
                            this[item.addFunc](item)
                            this.animationPoints.push(item)
                        }
                        this[item.func](item)
                    }
                    if (item.isPredict) {
                        this.getPredict({
                            ...item,
                            codeName: 'xiaolu',
                        })
                    }
                })
            })
        }
        // 曱甴路
        if (this.template === 'yueyou') {
            // 画图
            this.setMapXY.map(ele => {
                ele.map(item => {
                    if (item.selected && !item.isPredict) {
                        if (item.addFunc) {
                            this[item.addFunc](item)
                            this.animationPoints.push(item)
                        }
                        this[item.func](item)
                    }
                    if (item.isPredict) {
                        this.getPredict({
                            ...item,
                            codeName: 'yueyou',
                        })
                    }
                })
            })
        }
        // LOL
        if (this.template === 'lolty') {
            this.initAginLolTy(data.map(v => ({...v}))) 
            // 画图
            this.setMapXY.map(ele => {
                ele.map(item => {
                    if (item.selected && !item.isPredict) {
                        if (item.addFunc) {
                            this[item.addFunc](item)
                        }
                        this[item.func](item)
                    }
                })
            })
        }

        this.changeLeft(maxX)
    }

    // 删除所有的和和围
    delHEdata(data) {
        // const fifiterArr = ['和', '围']
        // const newData = data.filter(ele => ele.text !== '和' || ele.text !== '围' )
        const newData = data.filter(ele => !ele.isHe || !ele.isWei);
        return newData
    }

    // 初始化数据 倒叙计算值
    initAgin(data) {
        let newData = []
        data.map((ele, i) => {
            ele.position = {
                x: 0,
                y: 0,
            }
            ele.index = data.length - i - 1
            newData = [ele, ...newData]
        })
        this.createdMapRules(newData)
    }
    
    initAginLolTy(data) {
        let newData = []
        data.map((ele, i) => {
            let idx = data.length - i - 1
            ele.position = {
                x: Math.floor(idx / 6),
                y: idx % 6,
            }
            ele.index = data.length - i - 1
            newData = [ele, ...newData]
        })
        this.setMapXY = this.setXY(100, 6)        
        for (let i = 0; i < data.length; i++) {
            this.updatedMapXY(newData[i])
        }
    }

    // 画法规则
    createdMapRules(data) {
        this.setMapXY = this.setXY(100, 6)
        for (let i = 0; i < data.length; i++) {
            /**
             * [第一步]: 忽略第一颗子跳过计算
             * */
            if (i === 0) {
                this.updatedMapXY(data[i])
                continue
            }
            /**
             * [第二步]: 判断当前棋子属性是否相同
             * */
            const {
                position: { x, y },
                text,
                isHe, isWei
            } = data[i - 1]
            
            if (isHe && data[i - 2]) {
                text = data[i - 2].text;
            }

            /**
             * [第三步]: 判断当前棋子属性是否相同
             * */
             if (isWei) {
                const MAX_X0 = this.findXY_X(x)
                data[i].position = {
                    x: MAX_X0 + 1,
                    y: 0
                }
            }else if ((data[i].text == text || data[i].isHe) && !data[i].isWei) {
                if (y + 1 > 5) {
                    /**
                     *  当前棋子属性与上一颗棋子属性相同时; 并且不为和时
                     *  并且Y轴最大坐标不大于5
                     *  此为:同属性不换列
                     *  同属性不换列 : X+1轴坐标直接下一颗子
                     * */
                    data[i].position = {
                        x: x + 1,
                        y: y,
                    }
                } else {
                    /**
                     *  当前棋子属性与上一颗棋子属性相同时;
                     *  并且Y轴最大坐标大于5
                     *  此为:同属性换列
                     *  同属性换列: [查询下一颗子是否存在] 存在: x + 1 下一颗子  Y 轴不变; 不存在 : y + 1, x 轴不变 下一颗子；
                     * */
                    if (this.findNextSelected(x, y + 1)) {
                        data[i].position = {
                            x: x + 1,
                            y: y,
                        }
                    } else {
                        data[i].position = {
                            x: x,
                            y: y + 1,
                        }
                    }
                }
            } else {
                /**
                 * 当第一颗子为和时 直接 y + 1
                 * **/
                if (isHe && !data[i - 2]) {
                    data[i].position = {
                        x: x,
                        y: y + 1,
                    }
                } else if (
                    isHe && 
                    data[i - data[i - 1].position.y - 1].text === data[i].text
                ) {
                    /**  data[i - data[i - 1].position.y - 1]  当前列第一位子属性
                     *  当前棋子属性与上一颗棋子属性不相同时; 并且为和时
                     *  此为不同属性换列
                     *  不同属性换列: [找到棋盘X轴为0,的最大值MAX_X0]，X轴  MAX_X0 + 1 Y轴为 0; 下一颗子
                     * */
                    if (y + 1 > 5) {
                        /**
                         *  当前棋子属性与上一颗棋子属性相同时;
                         *  并且Y轴最大坐标不大于5
                         *  此为:同属性不换列
                         *  同属性不换列 : X+1轴坐标直接下一颗子
                         * */
                        data[i].position = {
                            x: x + 1,
                            y: y,
                        }
                    } else {
                        /**
                         *  当前棋子属性与上一颗棋子属性相同时;
                         *  并且Y轴最大坐标大于5
                         *  此为:同属性换列
                         *  同属性换列: [查询下一颗子是否存在] 存在: x + 1 下一颗子  Y 轴不变; 不存在 : y + 1, x 轴不变 下一颗子；
                         * */
                        if (this.findNextSelected(x, y + 1)) {
                            data[i].position = {
                                x: x + 1,
                                y: y,
                            }
                        } else {
                            data[i].position = {
                                x: x,
                                y: y + 1,
                            }
                        }
                    }
                } else {
                    const MAX_X0 = this.findXY_X(x)
                    data[i].position = {
                        x: MAX_X0 + 1,
                        y: 0,
                    }
                }
            }
            /**
             * [第四步]: 更新canvas坐标系
             * */
            this.updatedMapXY(data[i])
        }
    }

    /**
     * 小路规则 分析大路
     * @param {Object} start         小路计算第一颗子的位置
     * @param {String} createRed     小路画红色图工具
     * @param {String} createBlue    小路画蓝色图工具
     * @param {Number} $index        看齐整的第几列
     * */
    createdXiaoLuRules(start, createRed, createBlue, $index, addFunc) {
        const newArray = []
        const len = this.setMapXY.length
        const { startX, startY } = start

        for (let n = 0; n < len; n++) {
            /**
             *  [第一步] 找到开始计算的第一列startX;之前的列全部跳过continue
             **/
            if (n < startX) {
                continue
            }
            const lenN = this.setMapXY[n].length
            for (let m = 0; m < lenN; m++) {
                const isPredict = this.setMapXY[n][m].isPredict
                const map = this.setMapXY[n][m].map
                /**
                 *  开始第一列 X轴为0 直接跳过不予计算
                 **/
                if (n === startX && m === 0 && startY != 0) {
                    continue
                }
                /**
                 *  [第二步] 每列第一个计算齐整，其他计算有无
                 **/
                if (!this.setMapXY[n][m].selected) {
                    break // 无子时跳出此循环
                }

                if (m === 0) {
                    // 作为对比列
                    const preLineLen = this.findSelectedLen(
                        n - $index,
                        0,
                        this.setMapXY[n - $index]
                    ).length
                    // 作为当前列
                    const nowLineLen = this.findSelectedLen(n - 1, 0, this.setMapXY[n - 1]).length

                    /**
                     * 判断是否齐整;齐整为红;不齐整为蓝
                     */
                    if (preLineLen === nowLineLen) {
                        newArray.push({
                            position: { x: 0, y: 0 },
                            text: 'red',
                            func: createRed,
                            isPredict: isPredict,
                            map: map,
                        })
                    } else {
                        newArray.push({
                            position: { x: 0, y: 0 },
                            text: 'blue',
                            func: createBlue,
                            isPredict: isPredict,
                            map: map,
                        })
                    }
                } else {
                    /**
                     * 判断有无
                     * 当前列与对比列个数(减掉第一列)
                     * 当前列小于等于对比列全红色
                     *
                     */
                    // 作为对比列
                    const preLineLen = this.findSelectedLen(
                        n + 1 - $index,
                        0,
                        this.setMapXY[n + 1 - $index]
                    )
                    // 作为当前列
                    const nowLineLen = this.findSelectedLen(n, 0, this.setMapXY[n])
                    // 差值
                    const diffV = nowLineLen.length - preLineLen.length

                    // 重新计算带map属性的值
                    // const findMap = nowLineLen.filter( ele => ele.map );
                    // const findNotMap = nowLineLen.filter( ele => !ele.map );
                    // if( findMap.length > 0 ){
                    //     nowLineLen[nowLineLen.length - 1] = {
                    //         ...nowLineLen[nowLineLen.length - 1],
                    //         isPredict: findMap[0].isPredict,
                    //         map: findMap[0].map,
                    //     }
                    // }

                    if (diffV <= 0) {
                        const redV = nowLineLen.length - 1
                        for (let difv = 0; difv < redV; difv++) {
                            newArray.push({
                                position: { x: 0, y: 0 },
                                text: 'red',
                                func: createRed,
                                isPredict: nowLineLen[difv + 1].isPredict,
                                map: nowLineLen[difv + 1].map,
                            })
                        }
                    } else {
                        const redV = preLineLen.length - 1
                        const blueV = nowLineLen.length - preLineLen.length - 1

                        for (let difv = 0; difv < redV; difv++) {
                            newArray.push({
                                position: { x: 0, y: 0 },
                                text: 'red',
                                func: createRed,
                                isPredict: nowLineLen[difv + 1].isPredict,
                                map: nowLineLen[difv + 1].map,
                            })
                        }
                        newArray.push({
                            position: { x: 0, y: 0 },
                            text: 'blue',
                            func: createBlue,
                            isPredict: nowLineLen[redV + 1].isPredict,
                            map: nowLineLen[redV + 1].map,
                        })
                        for (let difv = 0; difv < blueV; difv++) {
                            newArray.push({
                                position: { x: 0, y: 0 },
                                text: 'red',
                                func: createRed,
                                isPredict: nowLineLen[redV + 1 + difv + 1].isPredict,
                                map: nowLineLen[redV + 1 + difv + 1].map,
                            })
                        }
                    }
                    break
                }
            }
        }
        const lens = newArray.length
        if (addFunc) {
            newArray[lens - 1]['addFunc'] = addFunc
        }
        // 清除数据所有不是最后一个的问路选择
        newArray.forEach((ele, i) => {
            if (lens != i + 1) {
                ele.map = ''
                ele.isPredict = ''
            }
        })

        this.createdMapRules(newArray)
    }

    // 找到每列被选中的个数
    findSelectedLen(n, m, data) {
        const filterSelected = data.filter(ele => ele.selected)
        const selectedlens = filterSelected.length
        if (selectedlens < 6 && selectedlens > 1) {
            const lianxuSelected = filterSelected.filter((ele, i) => {
                const nextSelected = filterSelected[i + 1]
                const preSelected = filterSelected[i - 1]
                if (nextSelected) {
                    return ele.index + 1 == nextSelected.index
                }
                if (preSelected) {
                    return ele.index - 1 == preSelected.index
                }
            })
            return filterSelected.slice(0, lianxuSelected.length + 1)
        } else if (selectedlens == 1) {
            return filterSelected
        } else if (selectedlens == 6) {
            const lianxu = this.findLastLianxu(n, m)
            return lianxu
        }
    }

    // 递减查询canvas坐标系;需要换行时找到坐标轴,X轴最右边的X轴坐标
    findXY_X(x) {
        for (let i = x; i >= 0; i--) {
            if (this.setMapXY[i][0].selected) {
                return i
            }
        }
    }

    // 查询下一颗子是否被选中
    findNextSelected(x, y) {
        if (y > 5) {
            y = 5
        }
        if (this.setMapXY[x][y].selected) {
            return true
        } else {
            return false
        }
    }

    // 根据画法规则更新canvas坐标
    updatedMapXY({ position, text, func, index, isPredict, addFunc, map, allResult, img, fontSize, border, background, color }) {
        const { x, y } = position
        this.setMapXY[x][y].selected = true
        this.setMapXY[x][y].text = text
        this.setMapXY[x][y].func = func
        if (map) {
            this.setMapXY[x][y].map = map
        }
        if (index || index === 0) {
            this.setMapXY[x][y].index = index
        }
        if (isPredict) {
            this.setMapXY[x][y].isPredict = isPredict
        }
        if (addFunc) {
            this.setMapXY[x][y].addFunc = addFunc
        }
        if (allResult) {
            this.setMapXY[x][y].allResult = allResult
        }
        if (img) {
            this.setMapXY[x][y].img = img
        }
        if (fontSize) {
            this.setMapXY[x][y].fontSize = fontSize
        }
        if (border) {
            this.setMapXY[x][y].border = border
        }
        if (background) {
            this.setMapXY[x][y].background = background
        }
        if (color) {
            this.setMapXY[x][y].color = color
        }
    }

    // 找到小路开始棋子
    findFristPosition(start, des) {
        let data = null
        const newMap = [...this.setMapXY]
        const findXYFirstRow = newMap[start][1]
        const findXYFirstCol = newMap[start + 1][0]
        if (findXYFirstRow.selected) {
            data = {
                startX: (findXYFirstRow.position.x - this.marginXY / 2) / this.marginXY,
                startY: (findXYFirstRow.position.y - this.marginXY / 2) / this.marginXY,
            }
        } else if (findXYFirstCol.selected) {
            data = {
                startX: (findXYFirstCol.position.x - this.marginXY / 2) / this.marginXY,
                startY: (findXYFirstCol.position.y - this.marginXY / 2) / this.marginXY,
            }
        }
        return data
    }

    // 创建棋盘
    setXY(Xnate, Ynate) {
        const newArr = []
        for (let i = 0; i < Xnate; i++) {
            newArr.push([])
            for (let j = 0; j < Ynate; j++) {
                newArr[i].push({
                    selected: false,
                    position: {
                        x: i * this.marginXY + this.marginXY / 2,
                        y: j * this.marginXY + this.marginXY / 2,
                    },
                    text: '',
                })
            }
        }
        return newArr
    }

    findMaxXY() {
        const childLenList = []
        this.setMapXY.map(ele => {
            return ele.map(item => {
                if (item.selected && !item.isPredict) {
                    childLenList.push(item)
                }
            })
        })
        if (childLenList.length > 0) {
            const maxX =
                (childLenList[childLenList.length - 1].position.x - this.marginXY / 2) /
                this.marginXY
            const maxY =
                (childLenList[childLenList.length - 1].position.y - this.marginXY / 2) /
                this.marginXY
            return {
                maxX: maxX,
                maxY: maxY,
            }
        } else {
            return {
                maxX: 0,
                maxY: 0,
            }
        }
    }

    // 找到连续的个数
    findLastLianxu(n, m, createRed) {
        const newArry = []
        let i = 0
        const { index, text } = this.setMapXY[n][m]
        const newArr = [].concat.apply([], this.setMapXY)
        do {
            i++
        } while (newArr.find(item => item.index === index + i && item.text === text))
        const findlast = newArr.find(item => item.map) || {}
        for (let a = 0; a < i; a++) {
            newArry.push({
                func: createRed,
                position: { x: 0, y: 0 },
                text: 'red',
                isPredict: this.setMapXY[n + a][m].isPredict,
                map: this.setMapXY[n + a][m].map,
            })
        }
        newArry[newArry.length - 1] = {
            ...newArry[newArry.length - 1],
            isPredict: findlast.isPredict,
            map: findlast.map,
        }
        return newArry
    }
}

// vue-canvas
const VueCanvas = {
    name: 'VueCanvas',
    data() {
        return {
            canvans: null,
        }
    },
    methods: {
        changeLeft(left) {
            this.$emit('scrollLeft', {
                left: left,
                ref: this.template,
            })
        },
        getPredict(row) {
            this.$emit('getPredict', row)
        },
    },
    props: {
        isAnimation: {
            type: Boolean,
            default: () => false,
        },
        // 选择渲染路数模板
        template: {
            type: String,
            default() {
                return 'dalu'
            },
        },
        // 数据渲染的列表
        dataList: {
            type: Array,
            default() {
                return []
            },
        },
        // 单个表格大小 20*20
        marginXY: {
            type: Number,
            default() {
                return 20
            },
        },
        // 表格最小列数
        minXY: {
            type: Number,
            default() {
                return 0
            },
        },
        gap: {
            type: Object,
            default() {
                return {
                    row: 1,
                    col: 1,
                }
            },
        },
        borderStrokeStyle: {
            default: '#061420'
        },
        redFillStyle: {
            default: '#e3253a'
        },
        blueFillStyle: {
            default: '#367af6'
        },
        animationType: {
            default: 'animationAction'
        },
        ratioRender: {
            default: false,
        },
    },

    watch: {
        minXY() {
            if(!this.canvans){
                return;
            }
            this.canvans.animationAction(false)
            this.canvans = new drawCanvasMap(this)
            this.canvans.animationAction(this.isAnimation)
        },
        template() {
            if(!this.canvans){
                return;
            }
            this.canvans = new drawCanvasMap(this)
        },
        dataList(newV, oldV) {
            const len = newV.length
            if (len > 0) {
                if (!this.canvans) {
                    if (this.$refs.canvas) {
                        this.canvans = new drawCanvasMap(this)
                    }
                } else {
                    this.canvans.setDataList(this.dataList)
                }
            }
        },
        isAnimation(v) {
            if (this.canvans) {
                this.canvans[this.animationType](v)
            }
        },
    },
    render(createElement) {
        let canvasEle = createElement('canvas', {
            ref: 'canvas',
            })
        return canvasEle
    },
    mounted() {
        if(this.ratioRender) {
            let ref = this.$el
            canvasRatio(ref.getContext('2d'))
            canvasRatioContext(ref)
        }
    }
}

export default VueCanvas
