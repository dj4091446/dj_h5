if (!isDevMode) {
    __webpack_public_path__ = window.APP_CDN_URL + 'h5/';
}

import Vue from "vue";

import toast from "@/views/common/message"
import systemModal from "@/views/common/systemModal"
import componentTooltip from '@/views/common/component-tooltip'

import "@/assets/scripts/vant.js"
import "@/assets/scripts/math.js"
import "@/assets/scripts/Browser.js"
// import "@/assets/scripts/inobounce.js" // 底部或顶部 禁止回弹效果 避免出现空白

import App from "./App.vue";
import store from "./store";
import router from "./router";

import "@/assets/less/icon.less";
import "@/assets/less/vant.less";
import "@/assets/less/commmon.less";
import "@/assets/less/i18n/index.less";
import gameCls from './views/common/mixins/gameCls';
import TeamImage from "@/views/common/teamImage";

var VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo)

import lodash from 'lodash'
Vue.prototype.$lodash = lodash

let echarts = require('echarts');
Vue.prototype.$echarts = echarts

// import VConsole from 'vconsole';
import i18n from '@/assets/i18n/index.js';

// 全局变量，用于存放赛事背景图、战队logo
global.GameImages = {};
global.TeamImages = {};


Vue.use(componentTooltip)
Vue.component('TeamImage', TeamImage)
Vue.mixin(gameCls);
Vue.prototype.Browser = new Browser()
Vue.prototype.$bus = new Vue();
// Vue.prototype.$vconsole = new VConsole();


let lang = i18n.locale;
import(/* webpackChunkName: "[request]" */ `@/assets/i18n/${lang}/index.js`).then(res => {
    i18n.setLocaleMessage(i18n.locale, res.default)
    // fix 多语言提示
    Vue.use(toast);
    Vue.use(systemModal)
    global.rootVM = new Vue({
        i18n,
        router,
        store,
        render: h => h(App)
    }).$mount("#root");
});

// 添加语言className到 #root，比如繁体lang_zh_tw,简体lang_zh_cn.
// 加到body上，因为有的弹窗会超出#root之外
document.body.classList.add('lang_' + i18n.locale);