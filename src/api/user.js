import request from "./axios";
import Api from "./api";
import qs from "qs";

/**
 * @登陆
 * @param {*} data = {username，pwd}
 */
export function requestUserLogin(data) {
    data = qs.stringify(data);
    return request({
        url: Api.user.login,
        method: "POST",
        data: data
    });
}

/**
 * @注册
 * @param {*} data = {username，pwd，parent_merchant_id，merchant_id，merchant_account}
 */
export function requestUserRegister(data) {
    data = qs.stringify(data);
    return request({
        url: Api.user.register,
        method: "POST",
        data: data
    });
}

/**
 * @获取用户信息
 * @param {*} data = {id}
 */
export function requestUserConfig() {
    return request({
        url: Api.user.userConfig,
        method: "get"
    });
}
/**
 * @获取帐变记录
 * @param {*}
 */
export function requestFundList(data) {
    data = qs.stringify(data);
    return request({
        url: `${Api.user.fundList}?${data}`,
        method: "get"
    });
}

/**
 * @获取公告列表
 * @param {*}
 */
export function requestNoticeList(data) {
    data = qs.stringify(data);
    return request({
        url: `${Api.user.noticeList}?${data}`,
        method: "get",
    });
}
/**
 * @设置用户接收自动变赔的协议
 * @param {*}
 */
export function getUpdateType(data) {
    data = qs.stringify(data);
    return request({
        url: `${Api.user.updateType}?${data}`,
        method: "get",
    });
}

/**
 * @设置用户获取服务器时间
 * @param {*}
 */
 export function requestSystemTime() {
    return request({
        url: `${Api.user.systemTime}`,
        method: "get",
    });
}



/**
 * @用户新版本后首次登录
 * @param {*}
 */
 export function requestUserver(data) {
    data = qs.stringify(data);
    return request({
        url: `${Api.user.userver}?${data}`,
        method: "get",
    });
}
/**
 * @获取获取会员下次盲盒时间戳，当前盲盒堆数量，当前奖券数
 * @param {*}
 */
 export function requestActivityStatus() {
    return request({
        url: `${Api.activity.activityStatus}`,
        method: "get"
    });
}
/**
 * @获取活动是否开启
 * @param {*} data = {merchant} 商户会员id
 */
 export function requestActivityState(data) {
    data = qs.stringify(data);
    return request({
        url:`${Api.activity.activityEntrance}?${data}`,
        method: "get"
    });
}
/**
 * @获取盲盒数量
 * @param {*}
 */
 export function requestActivityCount() {
    return request({
        url:`${Api.activity.activityInventory}`,
        method: "get"
    });
}
/**
 * @获取每日任务/成长任务
 * @param {*} data = {task_type} 0-每日任务  1-成长任务
 */
 export function requestActivityQuest(data) {
    data = qs.stringify(data);
    return request({
        url:`${Api.activity.activityQuest}?${data}`,
        method: "get"
    });
}
/**
 * @获取成长任务里面的会员周，月 统计数据
 * @param {*}
 */
 export function requestActivityStatistic() {
    return request({
        url:`${Api.activity.activityStatistic}`,
        method: "get"
    });
}
/** 
 * @兑换盲盒
 * @param {*}
 */
 export function requestActivityClaimBox(data) {
    data = qs.stringify(data);
    return request({
        url: `${Api.activity.activityClaimBox}`,
        method: "POST",
        data
    });
}
/** 
 * @盲盒历史记录
 * @param {*}
 */
 export function requestActivityBoxRecords(data) {
    data = qs.stringify(data);
    return request({
        url: `${Api.activity.activityBoxRecords}?${data}`,
        method: "get",
    });
}
/** 
 * @领取任务奖励
 * @param {*}
 */
 export function requestActivityClaimBonus(data) {
    data = qs.stringify(data);
    return request({
        url: `${Api.activity.activityClaimBonus}`,
        method: "POST",
        data
    });
}
/** 
 * @奖券记录
 * @param {*}
 */
 export function requestActivityBonusRecords(data) {
    data = qs.stringify(data);
    return request({
        url: `${Api.activity.activityBonusRecords}?${data}`,
        method: "get",
    });
}

/**
+ * @用户设置语言
+ * @param {*} data = {lang}
+ */
export function langSet(data) {
    data = qs.stringify(data);
    return request({
       url: `${Api.user.langSet}?${data}`,
       method: "get",
   });
}


/**
 * @切换皮肤
 * @param {*} data = {lang}
 */
export function themeSet(theme) {
    return request({
        url: `${Api.user.themeSet}?theme=${theme}`,
        method: "get",
    });
}

/**
 * @切换皮肤开关
 * @param {*} data = {lang}
 */
export function getThemeStatus(theme) {
    return request({
        url: `${Api.user.themeStatus}`,
        method: "get",
    });
}
