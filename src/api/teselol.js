import request from "./axios";
import axios from 'axios';
import Api from "./api";
import qs from "qs";
import { getParams } from '@/assets/scripts/utils'
import {transformTraceSubListStatus} from '@/views/lottery/util'

// 获取彩票的token和api域名
export function getTokenAndDomain(){
    return request({
        url: `${Api.teselol.getTokenAndDomain}`,
        method: "post"
    });
}

// 获取开奖倒计时
export function currentSaleIssue(data) {
    data = qs.stringify(data);
    return request({
        url: `${Api.teselol.currentSaleIssue}?${data}`,
        method: "get",
        data: data
    });
}

// 近期开奖
export function getRecentlylottery(data) {
    return request({
        url: `${Api.teselol.recentlylottery}`,
        method: "post",
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
        data: data
    });
}

// 投注记录列表
export function getOrderList(param) {
    let data = qs.stringify(param);
    return request({
        url: `${Api.teselol.getOrderList}`,
        method: "post",
        data: data
    });
}

// 追号记录列表
export function getTraceList(param) {
    let data = qs.stringify(param, { allowDots: true });
    return request({
        url: `${Api.teselol.getTraceList}`,
        method: "post",
        data: data
    });
}

// 投注撤单
export function cancelOrder(orderId) {
    let data = qs.stringify({ orderId: orderId }, { allowDots: true })
    return request({
        url: `${Api.teselol.cancelOrder}`,
        method: "post",
        data: data
    });
}
// 电竞彩 投注撤单
export function cancelOrderByIssue(planNo) {
    let data = qs.stringify({ planNo: planNo }, { allowDots: true })
    return request({
        url: `${Api.teselol.cancelOrderByIssue}`,
        method: "post",
        data: data
    });
}

// 获取彩种配置信息
export function getTicketCfg(ticketId) {
    return request({
        url: `${Api.teselol.getTicketCfg}?ticketId=${ticketId}`,
        method: "get",
    });
}

// 追号期数列表
export function getTraceIssues(ticketId) {
    return request({
        url: `${Api.teselol.getTraceIssues}`,
        method: "post",
        data: {
            ticketId
        }
    });
}
// 双面盘追号
export function doubleTrace(data) {
    // data.orderSource = global.orderSource
    data = qs.stringify(data, { allowDots: true })
    return request({
        url: `${Api.teselol.doubleTrace}`,
        method: "post",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        data: data
    });
}
// 电竞开奖号码
export function getHisOpenCodesDj(data) {
    data = qs.stringify(data, {allowDots: true});
    return request({
        url: `${Api.teselol.hisOpenCodesDj}?${data}`,
        method: 'get',
    });
}
// 写入用户设置
export function setUserConfig(data) {
    data = qs.stringify(data, { allowDots: true })
    return request({
        url: `${Api.teselol.setUserConfig}`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        method: 'post',
        data,
    });
}

export function getFrontuserInfo() {
    return request({
        url: `${Api.teselol.frontuserInfo}`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        method: 'post',
    });
}

// 读取用户设置
export function getUserConfig() {
    return request({
        url: `${Api.teselol.getUserConfig}`,
        method: 'get'
    })
}


/**
 * 获取彩种倒计时
 * @param {JSON} param
 *      ticketId 彩种Id
 */
export function loadTime(param) {
    return request
        .get(`${Api.teselol.loadTime}?${qs.stringify(param)}`)
        .then(res => {
            const { code, msg } = res.data
            if (code !== 0) {
                return {
                    code,
                    msg,
                    data: {},
                }
            }

            let data = res.data.data

            // 转换sale为true/false
            data = data.map(ele => {
                return {
                    ...ele,
                    sale: ele.sale === 0, // 0 在售  1 停售
                    startTime: Number(ele.startTime),
                    endTime: Number(ele.endTime),
                }
            })

            if (data && data.length === 1) {
                // 兼容修改前的代码
                return {
                    code,
                    msg,
                    data: data[0],
                }
            } else {
                return {
                    code,
                    msg,
                    data,
                }
            }
        })
}

/**
 * 追号撤单确定
 * @param {*} params
 *            chaseId     追号单号
 *            startPlanNo 开始撤销的奖期
 */
export function chaseBetCancle(params) {
    var data = qs.stringify({ type: 1, ...params }, { allowDots: true })
    return request({
        method: 'post',
        url: `${Api.teselol.chaseBetCancle}`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        data: data,
    })
}
export function configQuery(){
    return request({
        method: 'get',
        url: Api.teselol.configQuery,
    });
}

/**
 * 取消追号二次确认(追号详情)
 * @param {*} params
 *              chaseId   追号单号
 *              planNo    奖期
 */
export function chaseCancelStatus(params) {
    var data = qs.stringify(params, { allowDots: true })
    return request({
        method: 'post',
        url: `${Api.teselol.chaseCancelStatus}`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        data: data,
    })
}


/**
 * 追号撤单二次确认(列表)
 * @param {*} params
 * chaseId 追号定单id
 */
export function chasePlanStatus(params) {
    var data = qs.stringify(params, { allowDots: true })
    return request({
        method: 'post',
        url: `${Api.teselol.chasePlanStatus}`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        data: data,
    })
}

 /**
 * 投注
 * @param {Object} params 
 */
export function create(data, cancleExecutor) {
    data.orderSource = global.orderSource 
    // 取消请求token的工厂函数
    const CancelToken = axios.CancelToken

    let cancelToken = null
    if (cancleExecutor) {
        cancelToken = new CancelToken(cancleExecutor)
    }
    return request({
        method: 'post',
        url: `${Api.teselol.create}`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        data: qs.stringify(data, { allowDots: true }),
        cancelToken,
    })
  }
  

// 盈利列表
export function getDailyProfit(params) {
    var data = qs.stringify(params, { allowDots: true })
    return request({
        method: 'post',
        url: `/boracay/member/front/report/memberReportByDate`,
        // headers: {
        //     'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        // },
        data: data,
    })
}

// 帐变类型接口
export function getFrontTradeTypes() {
    return request({
        method: 'post',
        url: `/boracay/member/front/getFrontTradeTypes`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
    })
}


// 资金记录
export function getMoneyRecord(params) {
    var data = qs.stringify({ ...params }, { allowDots: true })
    return request({
        method: 'POST',
        url: `/boracay/member/front/queryBalanceRecords`,
        data: data,
    })
}


/**
 * 投注详情
 * @param {*} orderId
 * @param {*} isTrace  0 代表查询注单详情 1代表 查询追号-->子注单详情
 */
export function getOrderDetail(orderId, isTrace = 0) {
    var data = qs.stringify({ orderId: orderId, isTrace }, { allowDots: true })
    return request({
        method: 'post',
        url: `/coron/ticketmod/orderDetail`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        data: data,
    })
}

/**
 * 查询近期开奖
 * @param {FormData} status
 *         status 0未结算 1已结算
 */
export function getOrdersTop(status) {
    var data = qs.stringify({ status: status }, { allowDots: true })
    return request({
        method: 'post',
        url: `/coron/ticketmod/ordersTop`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        data: data,
    })
}

// 追号详情
export function getTraceDetail(orderId) {
    var data = qs.stringify({ chaseId: orderId }, { allowDots: true })
    return request({
        method: 'post',
        url: `/coron/ticketmod/traceDetail`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        data: data,
    }).then(res => {
        const { code } = res.data

        if (code === 0) {
            const data = res.data.data

            // 转换数据
            data.chasePlanVoList = data.chasePlanVoList.map(subDetail => {
                return {
                    ticketPlanId: subDetail.planNo, // 奖期
                    ticketBetStatus: transformTraceSubListStatus(subDetail.prizeStatusDes), // 中奖状态
                    ticketBetMoney: subDetail.betMoney, // 投注金额
                    planStatus: subDetail.planStatusCode, // 追号状态
                    planStatusDes: subDetail.planStatusDes, // 追号状态
                    ticketBetMultiple: subDetail.betMultiple,
                    winAmount: subDetail.winAmount, // 中奖金额
                    canCancel: subDetail.canCancel, // 是否可以取消追号
                    hasDetail: subDetail.hasDetail, // 是否已经下单
                }
            })

            const convertedData = {
                ticketChaseNum: data.id, // 订单编号
                chaseStarttime: data.chaseStartTime, // 追号时间
                ticketName: data.ticketName, // 彩种
                chaseStart: data.chaseStart, // 开始期号
                ticketPlay: data.playName, // 玩法
                ticketBetNum: data.chaseBetContent, // 追号内容
                singleGame: data.singleGame, // 是否是单式玩法
                ticketBetNum_LZMA: data.chaseBetNum, // 加密后的完整追号内容
                chaseAllPeriods: data.allPeriods, // 追号期数
                ticketBetMethod: data.betMode, // 投注模式
                chaseAllAmount: data.allAmount, // 总投金额
                chaseDonePeriods: data.donePeriods, // 完成期数
                chaseDoneAmount: data.doneAmount, // 完成金额
                chaseCancelPeriods: data.cancelPeriods, // 取消期数
                chaseCancelAmount: data.cancelAmount, // 取消金额
                chaseStatus: data.chaseStatus, // 追号状态
                chaseStatusDes: data.chaseStatusDes, // 追号状态文字
                isSuppend: data.suspend, // 追号规则
                chasePlanDetailLst: data.chasePlanVoList, // 追号子项
                winAmount: data.winAmount, // 中奖金额
                canCancel: data.canCancel, // 是否可以取消整比订单
            }

            res.data.data = convertedData
        }

        return res
    })
}

/**
 * 特色 keno投注详情
 * @param {ticketId
 * ticketPlanNo:
 * playId:} param
 */
export function kenoOrderDetail(param) {
    var data = qs.stringify(param, { allowDots: true })
    return request({
        method: 'post',
        url: `/coron/ticketmod/kenoOrderDetail`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        data: data,
    })
}

 /**
 * 特色 keno投注列表
 * @param {Object} param
 *      seriesId:
 *      startDate:
 *      endDate:
 *      pageNum:
 *      pageSize:
 */
export function kenoOrderList(param) {
    var data = qs.stringify(param, { allowDots: true })
    return request({
        method: 'post',
        url: `/coron/ticketmod/kenoOrderList`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        data: data,
    })
}
