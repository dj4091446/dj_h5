let Api = {
    user: {
        userConfig: "/game/balance", //获取用户信息
        fundList: "/game/fundList", //帐变记录
        noticeList: "/game/notice", //公告列表
        updateType: '/game/odd/updateType', // 用户是否自动接收自动赔率变化
        systemTime: '/game/systemTime', // 用户获取服务器时间
        userver: '/game/userver', // 用户新版后首次登录
        langSet: "/game/langSet", // 设置用户语言
        themeSet: "/game/themeSet", // 设置皮肤
        themeStatus: "/game/zkBtnStatus", // 切换皮肤开关
    },
    game: {
        gameList: "/game/nav", // 游戏列表
        matchList: "/game/index", // 赛事列表
        bet: "/game/bet", // 投注
        order: "/game/orderList", //游戏订单
        marketList: "/game/view", // 盘口列表
        gameLimit: "/game/bet/quota", // 限红
        matchMeun: "/game/stat", // 赛事菜单
        getTimer: "/game/getTimer", // 获取滚球赛事计时器
        getTournamentName: "/game/tourName", // 根据联赛ID 查找联赛名称
        images: "/upload/json/pc.json", // 所有图片
        matchState: "/game/matchList", //赛事状态
        matchRecommend:"/game/recommend", // 获取推荐赛事
        gameTour: "/game/tour", // 联赛筛选
        anchorInfo: '/game/anchorInfo', //主播盘信息
        getGameTeam: "/game/team", // 获取战队名称
        gameWinPrize: "/game/member/prize", // 获取中奖金额
        parlaySet: "/game/parlay/settings", // 获取串注超限设置
        festival: "/game/festival", // 节庆赛事UI
        matchResult: "/game/matchResult", // 赛果详情
        updateDomain: "/game/updateDomain", // 获取资源域名
        userOddGroup: "/game/userOddGroup", // 获取用户赔率分组组别
        merchantJson: "/game/merchant/tabSetting", // 商户名称、商户logo
        asianGamesRecommend: "/game/asianGamesRecommend", // 亚运会赛事列表
        matchTeamSearch: "/game/matchTeamSearch", // 战队搜索
        teamIndex: "/game/teamIndex", // 队伍名称搜索
    },
    // 活动接口
    activity: {
        activityStatus: "/activity/status", // 获取会员下次盲盒时间戳，当前盲盒堆数量，当前奖券数
        activityEntrance: "/activity/entrance", // 获取活动入口的显示状态
        activityInventory: "/activity/inventory", // 获取盲盒数量
        activityQuest: "/activity/quest", // 每日任务/成长任务
        activityStatistic: "/activity/statistic", // 成长任务里面的会员 周，月 统计数据
        activityClaimBox: "/activity/claimBox", // 兑换盲盒拆盒
        activityBoxRecords: "/activity/boxRecords", // 历史记录
        activityBonusRecords: "/activity/bonusRecords", // 领奖记录
        activityClaimBonus: "/activity/claimBonus", // 领取
    },
    //虚拟体育
    virtualSports: {
        commonScore: "/game/commonScore", // 一般联赛积分榜
        groupScore: "/game/groupScore", // 小组赛积分榜
        disuseScore: "/game/disuseScore", // 淘汰赛积分榜
        sportsList: "/game/sports/index", // 虚拟足球列表
        sportsTour:"/game/sports/tour", // 联赛列表
        sportsResult: "/game/sports/indexResult",// 虚拟体育赛果
        virtualReplay: "/game/virtualRpl",// 虚拟赛事视频详情
        sportsView: "/game/sports/view",// 虚拟赛事详情
        batchNoResult: "/game/sports/batchNoResult", // 赛事列表当前轮赛果
    },
    // 比分网数据
    bifenData: {
        bifenBtnStatus: "/game/zkBtnStatus", // 比分网按钮开关
        listForesightData: "/game/stats/analysisStat", // 列表前瞻数据
        foresightComplete: "/game/stats/analysisStatComplete", // 详情前瞻近期数据
        foresightFight: "/game/stats/analysisStatFight", // 详情前瞻历史交手
        tournamentHistory: "/game/tournamentHistory", // 历史联赛
        tournamentHistoryData: "/game/tournamentHistoryData", // 联赛数据
        matchData: "/game/stats/matchData", // 详情小局数据
        antLines: "/game/stats/antLines", // 经济差
        antEvents: "/game/ant/events", // 赛事赛程事件
        antVideoUrl: "/game/stats/getVideoList", // 赛事视频直播地址
        csgoEvents: "/game/stats/csgoEvents", //  CSGO实况栏事件数据
        csgoMatchData: "/game/stats/csgoMatchData", //  CSGO数据栏小局数据
        tabDataStatus: "/game/stats/checkShowData", // 比分网各tab有无数据
    },
    // 英雄召唤
    teselol: {
        getTokenAndDomain: "/game/lottery/config", // 获取彩票的token和api域名
        setUserConfig:"/boracay/member/front/save", // 写入用户设置
        currentSaleIssue: "/coron/ticketmod/currentSaleIssue.json", // 开奖倒计时
        recentlylottery: "/coron/api/ticketSourceResult/ticketSourceResultList.json", // 近期开奖
        getOrderList:"/coron/ticketmod/orderList", // 投注记录列表
        getTraceList:"/coron/ticketmod/traceList", // 追号记录列表
        cancelOrder: "/coron/ticketmod/memberBetCancel", // 投注撤单
        cancelOrderByIssue: "/coron/ticketmod/djc/plan/memberBetCancel", // 电竞彩 投注撤单
        getTicketCfg:"/coron/ticketmod/ticketcfg.json", // 获取彩种配置信息
        getTraceIssues: "/coron/ticketmod/traceIssues", // 追号期数列表
        doubleTrace:"/coron/order/double/chase", // 双面盘追号
        hisOpenCodesDj: "/coron/api/ticketSourceResult/djc/getHisOpenCodes", // 开奖号码
        getUserConfig: "/boracay/member/front/query",
        loadTime: "/coron/ticketmod/currentSaleIssue.json", // 开奖时间
        chaseBetCancle: '/coron/ticketmod/chase/memberBetCancel',
        chaseCancelStatus: '/coron/ticketmod/chase/canCancelByPlanNo',
        chasePlanStatus: '/coron/ticketmod/chase/canCancelByChaseId',
        create: '/coron/order/features/create',
        configQuery: `/coron/system/config/query`,
        frontuserInfo:`/boracay/member/front/userInfo`
    }
};
export default Api;
