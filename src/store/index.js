import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        user: require("./user").default,
        game: require("./game").default,
        data: require("./antData").default,
        setting: require('./setting').default,
        lottery: require('./lottery').default,
        record: require('./record').default,
    },
    plugins: [
        createPersistedState({
            storage: window.localStorage,
            paths: [
                "user.token",
                "user.showHostTips",
                "user.userBetAcount",
                "user.userBetOpen",
                "user.userInfo.maintain_time", // 持久化弹窗时间
                "game.matchID",
                "game.leagueCheckeds",
                "game.collect",
                "game.gameID",
                "game.matchFlag",
                "game.dailyTab",
                "game.tournamentName",
                "game.matchGameID",
                "game.betActiveType",
            ],
        }),
        store => {
            // mutation 被调用触发 
            store.subscribe((mutation, state) => {
                const { type } = mutation;
                const {  matchFlag, resultType, searchValueTemp } = state.game;
                switch (type) {
                    // case "SET_GAME_LIST":  // 第一次请求游戏列表
                    //     store.dispatch("fetchMatchNumber");
                    //     break;
                    case "INIT_GAMEID_FAG": // 初次进入初始化数据 gameid matchFlag
                    case "SET_GAME_ID":    // 切换游戏
                    case "SET_VSPORTS_BACK": // 电子游戏回退
                        store.dispatch("fetchMatchNumber");
                    case "SET_MATCH_FLAG": // 切换盘口类型
                    case "SET_EARLY_DATE":  // 切换早盘日期
                    case "SET_ENDGAME_DATE": // 切换赛果日期
                    case "SET_CROSS_DATE": // 切换串关日期
                        // 以上操作，均需要重新更新战队搜索框关键字
                        store.dispatch("updateSearchValue", searchValueTemp); 

                        if( matchFlag == 9 ||matchFlag == 10 || (matchFlag == 5 && resultType != 'djMatch')){
                            break;
                        }
                        store.dispatch("updateMatchList"); // 查询赛事列表
                        break;
                }
            });
            // action被调用触发
            store.subscribeAction((action, state) => {});
        }
    ],

    // 开启严格模式 状态变更必须是由 mutation函数引起 否则会抛出错误
    strict: isDevMode
});

export default store;
