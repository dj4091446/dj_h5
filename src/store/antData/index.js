import mutations from "./mutations.js";
import getters from "./getters.js";
import actions from "./actions.js";

const state = {
    
};

export default {
    state,
    getters,
    mutations,
    actions
};
