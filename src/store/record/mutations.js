const mutations = {
    // 设置单式位置记录danshiCheckList
    SET_DANSHI_CHECKLIST: (state, data) => {
        state.danshiCheck = { ...state.danshiCheck, ...data }
    },
    // 记录时时彩玩法位置
    SET_DANSHI_CHECKPOSITION: (state, data) => {
        state.sscPlayPositions = { ...state.sscPlayPositions, ...data }
    },
    // 设置投注记录列表
    SET_BETRECORD: (state, data) => {
        const { betRecord } = state
        if ( !data.list.list ) { return };
        if( data.isPull ){
            state.betRecord = data.list
        }else{
            state.betRecord = {
                ...data.list,
                list: [...betRecord.list, ...data.list.list],
            }
        }
    },
    // 设置追号记录列表
    SET_TRACERECORD: (state, data) => {
        const { traceRecord } = state
        if ( !data.list.list ) { return };
      
        if( data.isPull ){
            state.traceRecord = data.list
        }else{
            state.traceRecord = {
                ...data.list,
                list: [...traceRecord.list, ...data.list.list],
            }
        }
    },

    // 设置资金记录列表
    SET_MONEYRECORD: (state, data) => {
        const { moneyRecord } = state
        if ( !data.list.list ) { return };
        if( data.isPull ){
            state.moneyRecord = data.list
        }else{
            state.moneyRecord = {
                ...data.list,
                list: [...moneyRecord.list, ...data.list.list],
                totalSum:data.totalSum
            }
        }
    },

    // 设置每日盈利报表
    SET_DAILYRECORD: (state, data) => {
        const { dailyRecord } = state
        if (data.list) {
            state.dailyRecord = {
                ...data,
                list: [...dailyRecord.list, ...data.list],
            }
        }
    },
    // 设置用户详情
    SET_GAMEDETAIL: (state, data) => {
        state.gameDetail = data
    },
    // 设置当前查询详情的id
    SET_CURRENTORDER: (state, orderId) => {
        state.currentOrder = orderId
    },
    // 设置资金状态
    SET_MONEYTYPES: (state, data) => {
        state.moneyStatus = data
    },
    // 资金记录详情
    SET_MONEYDEYAIL: (state, data) => {
        state.moneyDetail = data
    },
    SET_DAYDEYAIL: (state, data) => {
        state.dayDetails = data
    },
    SET_ORDERSTOPLIST: (state, data) => {
        if (data.list) {
            state.orderTopList = data.list
        }
    },
    SET_ORDERSTOPLISTOVER: (state, data) => {
        if (data.list) {
            state.orderTopListOver = data.list
        }
    },
    // 未结注单数量
    SET_ORDERSTOPLIST_Total: (state, data) => {
        if (data.list) {
            state.orderTopListTotal = data.list.length || 0
        }
    },
    // 修改报表记录当前激活菜单
    SET_RECORDACTIVE: (state, data) => {
        state.activeMeun = data
    },
    // 清除游戏记录列表
    REMOVE_GAMERECORD: (state, name) => {
        if( name){
            state[name] = {
                list: [],
            }
        }else{
            state.betRecord = {
                list: [],
            }
            state.traceRecord = {
                list: [],
            }
            state.moneyRecord = {
                list: [],
            }
            state.dailyRecord = {
                list: [],
            }
        }
    },
    // 特色keno注单详情
    SET_TSORDERS: (state, data) => {
        if (data) {
            state.tsOrders = data
        }
    },

    // 保存当前游戏记录列表到本地存储 记录游戏列表位置
    SAVE_RECORD:(state, data) => {
        state.storageRecord = data
    },

    // 保存当前游戏记录列表状态
    SAVE_RECORDSTATE: (state,data) => {
        state.recordState = {
            ...state.recordState,
            ...data
        }
    },

    // 改变当前保存的游戏记录 9 20
    CHANGE_RECORD:(state,data) => {
        state[data.name] =  {
            ...state[data.name],
            list:data.list
        }
    },

    SET_HASFROMPATH:(state, data) => {
        state.hasfromPath = data
    },
    
    SET_CURRENTTAB:(state, data) => {
        state.currentTab = data
    },
}
export default mutations
