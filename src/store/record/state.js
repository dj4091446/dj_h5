import i18n from "@/assets/i18n/index"


const state = {
    betRecord: {
        // 投注记录列表
        list: [],
    },

    danshiCheck: {}, // 单式位置记录
    sscPlayPositions: {},

    traceRecord: {
        // 追号记录列表
        list: [],
    },
    moneyRecord: {
        // 资金记录列表
        list: [],
    },
    dailyRecord: {
        // 每日盈利记录列表
        list: [],
    },
    orderTopList: [], // 未结注单
    orderTopListTotal: 0, // 未结注单数量
    orderTopListOver: [], // 已结注单
    moneyStatus: [], // 接口返回资金状态
    currentOrder: {}, // 当前查询详情
    gameDetail: {}, // 游戏详情(投注，追号，资金，每日)
    moneyDetail: {}, // 资金记录详情 保存在storege中
    dayDetails: {}, // 每日盈利详情 保存在storege中
    activeMeun: 'betRecord', // 报表记录当前激活菜单 保存在storege中
    tsOrders: [], // 特色注单列表

    hasfromPath:'', // 从那个页面进入的游戏记录

    storageRecord:{
        list:[]
    },  // 保存当前游戏记录列表

    recordState:{
        isFinished: true,
        isLoading: true,
        pageNum: 1,
        defaultValue:{},
        search:{},
        isFirst:false,
        name: ''
    }, // 保存当前游戏记录列表下拉刷新状态


    // 投注状态
    betSatus: [
        { key: 1, value: i18n.t('yxzh.watingOpen') },
        { key: 2, value: i18n.t('yxzh.unWin') },
        { key: 3, value: i18n.t('yxzh.winned') },
        { key: 4, value: i18n.t('yxzh.personalCancel') },
        { key: 5, value: i18n.t('yxzh.systemCancel') },
    ],
    // 追号状态
    traceSatus: [
        { key: 1, value: i18n.t('betRecord.status.traceing') },
        { key: 2, value: i18n.t('betRecord.status.finished') },
        { key: 3, value: i18n.t('betRecord.status.personalStopTrace') },
        { key: 4, value: i18n.t('betRecord.status.systemStopTrace') },
    ],
    // 记录列表-对应opath
    recordList: [
        { 
            name: i18n.t('yxzh.tab.betRecord'), 
            titleDet: i18n.t('yxzh.tab.betDetail'), 
            details: 'betDetails', 
            path: 'betRecord' 
        },
        {
            name: i18n.t('yxzh.tab.traceRecord'),
            titleDet: i18n.t('yxzh.tab.traceDetail'),
            details: 'traceDetails',
            path: 'traceRecord',
        },
        { 
            name: i18n.t('yxzh.tab.dailyProfit'), 
            titleDet: i18n.t('yxzh.tab.dailyProfitDetail'), 
            details: 'dayDetails', 
            path: 'dayRecord' },
        {
            name: i18n.t('yxzh.tab.moneyRecord'),
            titleDet: i18n.t('yxzh.tab.moneyRecordDetail'),
            details: 'moneyDetails',
            path: 'moneyRecord',
        },
        { 
            name: i18n.t('yxzh.tab.geqizhuihao'), 
            titleDet: i18n.t('yxzh.tab.geqizhuihaoDetail'), 
            details: 'eachTrace', 
            path: 'eachTrace' 
        },
        {
            name: i18n.t('yxzh.tab.totalProfit'),
            titleDet: i18n.t('yxzh.tab.totalProfit'),
            details: 'totleLossDetails',
            path: 'totleLossDetails',
        },
        {
            name: i18n.t('yxzh.tab.dayProfit'),
            titleDet: i18n.t('yxzh.tab.dayProfit'),
            details: 'dayLossDetails',
            path: 'dayLossDetails',
        },
    ],
    // 当前的一级菜单
    currentTab: 'double',
}
export default state
