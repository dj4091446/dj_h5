import * as api from '@/api/teselol'

const actions = {
    // 获取用户投注记录
    async fetchOrderList({ commit }, params) {
        const { isPull }  = params;
        delete params.isPull;
        const res = await api.getOrderList({ ...params })
        if (!res.data.data) {
            return
        }
        commit('SET_BETRECORD', {
            list: res.data.data,
            isPull: isPull
        })
    },

    // 获取用户追号记录
    async fetchTraceList({ commit }, params) {
        const { isPull }  = params;
        delete params.isPull;
        const res = await api.getTraceList({ ...params })
        if (!res.data.data) {
            return
        }
        commit('SET_TRACERECORD', {
            list: res.data.data,
            isPull: isPull
        })
    },
    
    // 获取用户资金记录
    async fetchMoneyRecord({ commit }, params) {
        const { isPull }  = params;
        delete params.isPull;
        const res = await api.getMoneyRecord({ ...params })
        if (!res.data.data) {
            return
        }
        commit('SET_MONEYRECORD', {
            totalSum:res.data.totalSum.totalSum,
            list: res.data.data,
            isPull: isPull
        })

    },

    // 获取用户投注记录详情
    async fetchOrderDetail({ commit }, params) {
        const res = await api.getOrderDetail(params.orderId, params.isTrace)
        if (!res.data.data) {
            commit('SET_GAMEDETAIL', null)
            return
        }
        commit('SET_GAMEDETAIL', res.data.data)
    },

    // 获取用户追号记录详情
    async fetchTraceDetail({ commit }, params) {
        const res = await api.getTraceDetail(params)
        if (!res.data.data) {
            commit('SET_GAMEDETAIL', null)
            return
        }
        commit('SET_GAMEDETAIL', res.data.data)
    },


    // 获取用户资金状态
    async fetchFrontTradeTypes({ commit }) {
        const res = await api.getFrontTradeTypes()
        if (!res.data.data) {
            return
        }
        commit('SET_MONEYTYPES', res.data.data)
    },

    // 获取用户每日盈利列表
    async fetchDailyProfit({ commit }, params) {
        const res = await api.getDailyProfit({ ...params })
        if (!res.data.data) {
            return
        }
        commit('SET_DAILYRECORD', res.data.data)
    },

    // 获取已结注单 1 ; 未结注单 0
    async fetchOrdersTop({ commit }, params) {
        const res = await api.getOrdersTop(params)
        if (!res.data.data) {
            return
        }
        // 单独保存 未结注单
        if (params === 0) {
            commit('SET_ORDERSTOPLIST_Total', res.data.data)
            commit('SET_ORDERSTOPLIST', res.data.data)
        } else {
            commit('SET_ORDERSTOPLISTOVER', res.data.data)
        }
    },

    // keno 注单详情
    async kenoOrderDetail({ commit }, params) {
        const res = await api.kenoOrderDetail(params)
        if (!res.data.data) return
        commit('SET_TSORDERS', res.data.data)
    },

    // keno 投注列表
    async kenoOrderList({ commit }, params) {
        const res = await api.kenoOrderList({ ...params })
        if (!res.data.data) return
        commit('SET_BETRECORD', res.data.data)
    },

    // 修改报表记录当前激活菜单
    setRecordActiveMeun({ commit }, params) {
        commit('SET_RECORDACTIVE', params)
    },
}
export default actions
