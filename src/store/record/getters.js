// prettier-ignore
const getters = {
    getBetRecord       : state => state.betRecord,          // 返回投注记录
    getTraceRecord     : state => state.traceRecord,        // 返回追号记录
    getMoneyRecord     : state => state.moneyRecord,        // 返回资金记录
    getMoneyStatus     : state => state.moneyStatus,        // 返回资金状态
    getBetSatus        : state => state.betSatus,           // 返回投注状态
    getGameDetail      : state => state.gameDetail,         // 返回游戏详情
    getCurrentOrder    : state => state.currentOrder,       // 返回当前游戏详情
    getTraceSatus      : state => state.traceSatus,         // 返回追号状态
    getRecordList      : state => state.recordList,         // 返回游戏导航列表
    getMoneyDetail     : state => state.moneyDetail,        // 返回资金记录详情
    geDailyRecord      : state => state.dailyRecord,        // 返回资金记录详情
    geDailyDetail      : state => state.dayDetails,         // 返回资金记录详情 
    getOrderTopList    : state => state.orderTopList,       // 返回未结注单
    getOrderTopListOver: state => state.orderTopListOver,   // 返回已结注单
    getActiveMeun      : state => state.activeMeun,         // 返回报表记录当前菜单
    getStorageRecord   : state => state.storageRecord,      // 返回保存的游戏记录
    getRecordState     : state => state.recordState,        // 返回保存的游戏记录列表状态
    getHasfromPath     : state => state.hasfromPath,        // 返回保存的游戏记录列表的路径
}
export default getters
