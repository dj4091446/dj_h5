import * as userApi from "@/api/user.js";
import * as gameApi from "@/api/game.js";

const actions = {
    // 用户信息
    async fetchUserInfo({ commit }) {
        return new Promise((resolve, reject) => {
            userApi.requestUserConfig().then(response => {
                let data = response.data.data

                const merchantArr = ['5575856692509362', '5583488003396830', '5575172007572006']; // 截取删除前三位用户名的商户
                const showAct = merchantArr.includes(String(data.merchant_id)) // 是否显示截取后的用户名
                const act = data.account.slice(3);
                data = {
                  ...data,
                  act: showAct ? act : data.account
                }

                commit("SET_USERINFO", data);
                commit('SET_ACEPT_ODDS', data.odd_update_type || 1)
                resolve(response);
            });
        });
    },
    
    // 活动入口显示/隐藏
    async fetchActivityState({ commit },params) {
        return new Promise((resolve) => {
            userApi.requestActivityState(params).then(response => {
                let data = response.data.data
                commit("SET_ACTIVITY_ENTRANCE", data !== '0');
                // 维护信息
                commit("SET_ACTIVITY_MAINTAIN_INFO", {
                    title: data.title,  // 维护标题
                    content: data.content, // 维护内容
                    maintain_status: data.maintain_status, // 维护状态
                    h5_maintain_url: '/' + data.h5_maintain_url, // 维护背景图
                    maintain_end_time: data.maintain_end_time, // 维护结束时间
                });
                resolve(response);
            });
        });
    },

   
    
    // 串关超限参数设置 
    getParlaySet({commit}){
        gameApi.requestParlaySet().then( res => {
            if(res.data.status == "true"){
                commit("SET_CROSS_PARLAY", res.data.data)
            }
        })
    }
};

export default actions;
