import lodash from 'lodash'
const mutations = {
    SET_TOKEN(state, data) {
        state.token = data;
    },
    SET_USERINFO(state, data) {
        if(data.maintain_time.length){
            data.maintain_time = JSON.parse(data.maintain_time)
        }else{
            data.maintain_time = {
                maintain_time: 0, // 开始维护时间
                reminder_time: 0, // 提示用户时间
            }
        }
        state.userInfo = data;
    },
    // 更新余额
    UPDATA_BALANCE(state, data) {
        // if (state.userInfo.account == data.account) {
        state.userInfo.balance = data.balance;
        // }
    },
    UPDATE_SECRET_KEY(state, data){
        state.userInfo.secret_key = data;
    },
    // 改变用户头像
    SET_USER_AVATAR(state, data) {
        state.userAvatar = data;
        window.localStorage.setItem('userAvatar', data)
    },

    SET_BET_LIMITDELL(state, data){
        state.betLimit = {};
    },

    // 设置限红
    SET_BET_LIMIT(state, data){
        state.betLimit = data;
    },


    SET_STATIONBET_LIMIT(state, data){
        const { name, resJson } = data;
        state.stationBetLimit = {
            ...state.stationBetLimit,
            [name]: resJson
        };
    },

    DEL_STATIONBET_LIMIT(state, data){
        state.stationBetLimit = lodash.omit(state.stationBetLimit, [data])
    },

    // 设置中奖特效状态
    SET_AWARD_ANIMATE_STATUS(state, data){
        state.showAwardAnimate = data;
    },

    // // 更新最大限额
    // UPDATE_BETLIMIT(state, data){
    //     const { id, odds } = data;
    //     const { root_maxLimit, root_compMaxLimit } = state.betLimit[id];

    //     let { prize_static_profit } = state.betLimit[id];
    //     prize_static_profit = Number(prize_static_profit);

    //     const simple_max_1 = root_maxLimit > 0 ? parseInt( root_maxLimit / (odds - 1 ) ): 0;
    //     const simple_max = prize_static_profit > simple_max_1 ? simple_max_1: prize_static_profit;
    //     const mix_prize_limit_1 = root_compMaxLimit > 0 ? parseInt( root_compMaxLimit / (odds - 1) ): 0;
    //     const mix_prize_limit = prize_static_profit > mix_prize_limit_1 ? mix_prize_limit_1: prize_static_profit;
    //     state.betLimit[id] = {
    //         ...state.betLimit[id],
    //         simple_max: simple_max,
    //         simple_max_1: simple_max_1,
    //         mix_prize_limit,
    //         mix_prize_limit_1,
    //     }
    // },
    // 设置接受赔率变化规则
    SET_ACEPT_ODDS(state, data){
        state.acceptOddType = data;
    },

    BET_CART_VISIBLE(state, data){
        state.betCartVisible = data;
    },
    
    SET_STATION_MAX(state, data){
        state.stationOrderMax = data
    },


    SET_BETCART_TYPE(state, data){
        state.betType = data;
    },

    SET_BETCART_SHOW(state, data){
        state.showBetCart = data;
    },

    SET_CREDIT_LIMIT(state, data){
        state.corssCreditLimit = data
    },
    SET_ACTIVE_TIME(state, data){
        state.activeTime = data
    },

    SET_DURATION(state, data){
        state.duration = data;
    },

    SET_OFFLINE(state, data){
        state.offline = data
    },

    SET_OFFLINE_TAG(state, data){
        state.offlineTag = data
    },

    SET_TRACK(state, data){
        const {tab, page, number = 1, gameId} = data;
        let hasdTarget = false;
        state.track.forEach((item) => {
            if (tab && item.click_tab === tab) {
                hasdTarget = true;
                item.click_number += number;
            }

            if (gameId && item.game_id === gameId) {
                hasdTarget = true;
                item.click_number += number;
            }
        });
        if (!hasdTarget) {
            state.track.push({ click_tab: tab, click_number: 1, page, game_id: gameId });
        }

        // 有效统计 更新在线时长
        const { offline, offlineTag, activeTime } = state;
        let { duration } = state;
        
        if (offline) {
            // 用户离线，有效点击算作登录
            this.commit("SET_OFFLINE", false);
        }else{
            // 计算上一个时间段的在线时长, 并累加
            // console.log('前',duration, new Date().getTime(), activeTime);
            duration = duration + (new Date().getTime() - activeTime);
            // console.log('后',duration, new Date().getTime(), activeTime);
        }
        this.commit("SET_ACTIVE_TIME", new Date().getTime());
        this.commit("SET_DURATION", duration);
        console.info('更新前端在线时长', `${Math.ceil(duration/(1000))}s`); // , new Date().getTime()
    
        if (offlineTag) {
            // 清除旧的在线时长定时器
            clearTimeout(offlineTag);
        }
        // 更新定时器，60分钟后变更为离线，发送请求保存在线时长
        const newOfflineTag = setTimeout(() => {
           
            console.info('离线了，保存时长', `60min`); // , new Date().getTime()
            this.commit("SET_OFFLINE", true);
        }, 1000 * 60 * 60);
        this.commit("SET_OFFLINE_TAG", newOfflineTag);

        console.info("埋点", `tab:${tab}`, `gameId:${gameId}`, state.track);
    },

    CLEAR_TRACK(state, data){
        state.track = data;
    },

    SET_TRACK_TAG(state, data){
        state.trackTag = data
    },

    SET_SHOW_HOSTTIPS(state, data){
        state.showHostTips = data
    },
    
    SET_USER_BETACOUNT(state, data){
        state.userBetAcount = data
    },

    SET_USER_BETOPEN(state, data){
        state.userBetOpen = data;
        if( !data ){
            state.userBetAcount = 0
        }
    },
    
    SET_ACTIVITY_ENTRANCE(state, data) {
        state.activityEntrance = data;
    },
    // 设置运营活动我的奖券总数
    SET_CURRENT_TOTAL(state, data) {
        state.currentTotal = data;
    },

    // 串关超限参数设置
    SET_CROSS_PARLAY(state, data){
        state.crossParlaySet = data;
    },
    // 限额超限推送
    UPDATE_OVERRUN(state, data){
        const { exchange_rate } = state.userInfo;
        const keys = Object.keys(data)[0];
        if( keys.indexOf('bet_max') == -1 ){
            state.crossParlaySet[keys] = data[keys];
        }else{
            state.crossParlaySet[keys] = parseInt(Number(data[keys])/Number(exchange_rate));
        }
    },
    // 赛事串关最大投注额
    UPDATE_CROSS_MAXODDS(state, data){
        for( let key in state.betLimit ){
            state.betLimit[key].complex_max_odd_limit = data;
        }
    },
    // 将当前mqtt推送过来的值覆盖到userInfo对象
    SET_USERINFO_MAIN_REMIN(state, data){
        let keys = Object.keys(data)
        keys.map((key)=>{
            state.userInfo.maintain_time[key] = data[key]  
        })
    },
    // 设置活动维护页面信息
    SET_ACTIVITY_MAINTAIN_INFO(state, data){
        state.maintainInfo = data;
    },
    SET_KICK_OUT(state){
        state.kickOut = true;
    },
};
export default mutations;
