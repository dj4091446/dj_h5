export default {
    token: state => state.token,
    userInfo: state => state.userInfo,
    userAvatar: state => state.userAvatar,
    betLimit: state => state.betLimit,
    stationBetLimit: state => state.stationBetLimit,
    acceptOddType: state => state.acceptOddType,
    betCartVisible: state => state.betCartVisible,

    showBetCart: state => state.showBetCart,
    betType: state => state.betType,
    corssCreditLimit: state => state.corssCreditLimit,
    activeTime: state => state.activeTime,
    duration: state => state.duration,
    offline: state => state.offline,
    offlineTag: state => state.offlineTag,
    track: state => state.track,
    trackTag: state => state.trackTag,

    showHostTips: state => state.showHostTips,
    userBetAcount: state => state.userBetAcount,
    userBetOpen: state => state.userBetOpen,
    activityEntrance: state => state.activityEntrance,
    currentTotal: state => state.currentTotal, // 获取运营活动我的奖券总数

    crossParlaySet: state => state.crossParlaySet,
    maintainInfo: state => state.maintainInfo, // 维护页面信息
    kickOut: state => state.kickOut, 
};

