import mutations from "./mutations.js";
import getters from "./getters.js";
import actions from "./actions.js";

const state = {
    token: "",
    userInfo: {},
    userAvatar: !!window.localStorage.getItem('userAvatar') ? window.localStorage.getItem('userAvatar'): 'avatar01',
    betLimit:{},
    stationBetLimit:{},  // 局内限额
    acceptOddType: 1, // 1: 自动接受最新赔率； 2： 自动接受更高赔率；3: 永不接受赔率变动；

    betCartVisible: false, // 注单弹框


    showBetCart: false, // 注单是否弹框
    betType: "betOrder",  // 当前显示的注单弹框

    corssCreditLimit:{}, // 串关信用盘限额
    activeTime: new Date().getTime(), // 活跃点击时间
    duration: 0, // 在线时长
    offline: false, // 离线
    offlineTag: null, // 离线定时器标签
    track: [], // 埋点数据
    trackTag: null, // 埋点定时器标签

    showHostTips: true, // 是否显示主播盘提示文字

    userBetAcount: 0, 
    userBetOpen: false,
    activityEntrance: false, // 是否显示活动入口
    currentTotal: 0, // 运营活动我的奖券总数
    showAwardAnimate: false, // 是否显示获奖动画

    crossParlaySet: {}, // 串关超限参数设置
    maintainInfo: {}, // 活动页面维护信息
    kickOut: false, // 一键强制刷新
};

export default {
    state,
    getters,
    mutations,
    actions
};
