import * as gameApi from "@/api/game.js";
import * as userApi from "@/api/user.js";
import * as dataApi from "@/api/data.js";
import * as virtualSportsApi from "@/api/virtualSports.js";
import { 
    itemSort, 
} from "@/assets/scripts/sort.js";
import { i18nTransGameNameKeyUtil  } from "@/assets/scripts/utils";
import i18n from "@/assets/i18n/index.js"
// 多语言转换matchTeam
const i18nTransMatchTeam = (data) => {
    return i18nTransGameNameKeyUtil({
        cn_name: data.match_cn_team,
        en_name: data.match_en_team || data.match_cn_team,
    })
}
function transMatch(sourceData){
    let data = [];
    if( !(sourceData instanceof Array) ) { return data };
    sourceData = sourceData.filter( ele => ele );
    sourceData.forEach( ele => {
        // 盘口
        let m =  ele.default_market || { odds:[] };
        let ms = []; // 冠军盘
        // 赛事
        let source = ele;
        // 比分
        source["home_score"] = Number(ele.score.split(":")[0]);
        source["away_score"] = Number(ele.score.split(":")[1]);
        source['rel_bo'] = source.bo;
        if( source.status > 5 ){  // 赛事 取消 关盘 结算
            const bo = source["home_score"] + source["away_score"];
            source.bo = bo ? source["home_score"] + source["away_score"] : source.bo
        }
        // 兼容以前赛事没有 match_cn_team;
        ele['match_cn_team'] = ele.match_cn_team || ele.match_team;
        const matchTeam = i18nTransMatchTeam(ele)
        // 战队 转小写模糊查询
        source["lower_team"] = matchTeam.toLowerCase();

        // 对象初始化属性，方便被computed计算
        source["h5_bg"] = ""; 

        // 战队id
        if(ele.team_id){
            source["home_team"] = ele.team_id.split(",")[0];
            source["away_team"] = ele.team_id.split(",")[1];
        }
        // 战队名称
        if( matchTeam ){
            source["home_name"] = matchTeam.split(",")[0];
            source["away_name"] = matchTeam.split(",")[1];
        }

        // 投注项
        let odds = [];
        for( let key in m.odds) {
            if( m.odds.hasOwnProperty(key) ){ 
                odds.push(
                    m.odds[key]
                )
            }
        }
        m.odds = odds.length > 0 ? odds: [
            { name: "@T1", en_name: "@T1"},
            { name: "@T2", en_name: "@T2"},
        ];  
        m.odds = odds.sort( (a, b) => a.sort_id - b.sort_id );
        source["hasping"] = m.odds.length == 3;

        // 冠军盘赛事投注项;
        for( let key in ele.champ_markets ){
            if( ele.champ_markets.hasOwnProperty(key) ){ //  for...in 循环会把某个类型的原型(prototype)中方法与属性给遍历出来  渲染组件时 导致页面报错
                const mkt = ele.champ_markets[key];
                let betOdds = [];
                for( let i in mkt.odds) {
                    betOdds.push(
                        mkt.odds[i]
                    )
                }
                if( [10, 11].includes(Number(mkt.option_type)) ){
                    mkt.odds = betOdds.sort((a, b) => Number(a.odd) - Number(b.odd));
                }else{
                    mkt.odds = betOdds
                }
                ms.push(mkt)
            }
        }
        
        delete ele.default_market;
        delete ele.champ_markets;
        ele.duration = 0
        data.push({
            ...source,
            m: m,
            ms: ms
        })
    });
    return data;
}

// 转化盘口列表
function transMarket(sourceData){
    let data = [];
    for( let key in  sourceData){
        let odds = [];
        let market = sourceData[key]
        for( let i in market.odds ){
            odds.push( market.odds[i] )
        };
        
        // 特殊处理：复合玩法替换字符串
        if( market.option_type == 23 ){
            // market.name = market.name.replaceAll("$", market.market_value)
            const marketName = market.name.split('$');
            const marketValue = market.market_value.split(',');
            let str = "";
            if( marketName.length > 1 ){
                marketValue.forEach( (ele, i) => {
                    str += marketName[i]+ele;
                });
                market.name = str + marketName[marketName.length - 1];
            }
        }
        

        // 无投注项盘口 不显示
        if( market.odds && Object.keys( market.odds ).length ){
            // 投注项排序 option_type
            odds = itemSort( odds , market.option_type )
            data.push({
                ...market,
                odds: odds
            })
        }
       
        delete market.odds;
    }
    
    // 盘口排序
    data.sort((a, b) => {
        return a.round - b.round || a.status - b.status || a.sort_code - b.sort_code;
    })

    return data
}

// 转化盘口列表
function transMarketVSports(sourceData, matchStatus){
    let data = [];
    for( let key in  sourceData){
        let odds = [];
        let market = sourceData[key]
        for( let i in market.odds ){
            odds.push( market.odds[i] )
        };
        odds.sort((a, b) => {
            return a.sort_id - b.sort_id;
        })
        if(market.option_type === 5 && matchStatus !== 2){// 波胆
            const len = market.round === 100 ? 4 : 12; // 4:半场，12:全场
            const winOdds = odds.splice(0,len);
            const loseOdds = odds.splice(0,len);
            odds = [
                winOdds,
                odds,
                loseOdds,
            ]
        }
        delete market.odds;
        data.push({
            ...market,
            odds: odds
        })
    }

    // 盘口排序
    data.sort((a, b) => {
        return a.sort_code - b.sort_code;
    })
    
    return data
}

// 拆分赛事数据
function matchSplit(data){
    let mqMatch = {};
    let mqMarket = {};
    let mqOdds = {};
    
    data.forEach( ele => {
        const { m, ms, ...paramsMatch } = ele;
        
        mqMatch = {
            ...mqMatch,
            [ele.id]: paramsMatch
        };
        if( m.id ){
            const { odds, ...paramsMarket } = m;
            mqMarket = {
                ...mqMarket,
                [m.id]:  paramsMarket,
            };
            odds.forEach( row => {
                mqOdds = {
                    ...mqOdds,
                    [row.id]: row,
                };
            })
        };
        if( ms.length > 0 ){
            ms.forEach( item => {
                const { odds, ...paramsMarket } = item;
                mqMarket = {
                    ...mqMarket,
                    [item.id]:  paramsMarket,
                }
                odds.forEach( row => {
                    mqOdds = {
                        ...mqOdds,
                        [row.id]: row,
                    }
                });
            })
        };
    });

    // console.log( mqMatch )
    // console.log( mqMarket )
    // console.log( mqOdds )

    return JSON.parse(JSON.stringify( {
        mqMatch,
        mqMarket,
        mqOdds
    }))
}


// 拆分赛事详情(盘口)数据
function marketSplit(data){
    let mqMatch = {};
    let mqMarket = {};
    let mqOdds = {};
    console.log(data)
    data.forEach( m => {
        const { odds, ...paramsMarket } = m;
        mqMarket = {
            ...mqMarket,
            [m.id]:  paramsMarket,
        };
        odds.forEach( row => {
            mqOdds = {
                ...mqOdds,
                [row.id]: row,
            };
        })
    })
    // console.log( mqMarket )
    // console.log( mqOdds )
    return JSON.parse(JSON.stringify( {
        mqMatch,
        mqMarket,
        mqOdds,
    }))
}



const awaitWrap = (promise) => {
    return promise
        .then(data => data)
        .catch(err => err)
}


const actions = {
    async updataVRList({ commit }, data){
        commit("SET_SLOT_GAME_LIST", data); 
    },
    
    // 游戏列表
    async fetchGameList({ commit, dispatch }) {
        const res = await awaitWrap(gameApi.requestGameList());
        
        let gameList = res.data.data || [];

        // 存储游戏序号
        let gameSortCode = {};
        let mainGameList = [], slotGameList = []
        gameList = gameList.filter( ele => Number(ele.is_open_entrance) == 0 && ele.status == 1 ); // 显示游戏 并且 游戏开启状态
        gameList.forEach( ele => {
            //英雄召唤和虚拟足球归类到电子游戏
            if(['10158428472914228', '1001'].includes(ele.id)) {
                slotGameList.push(ele)
            } else {
                let code = ele.sort_code  == 0 ? -1: ele.sort_code;
                gameSortCode[ele.id] = code
                mainGameList.push(ele)
            }
        });
        commit("SET_GAME_SORTCODE", gameSortCode);
        commit("SET_GAME_LIST", mainGameList); 
        commit("SET_SLOT_GAME_LIST", slotGameList); 
    },

    // 联赛筛选
    async fetchMatchTour({ commit, dispatch }, gameID){
        return new Promise( async (resolve, reject) => {
            gameApi.requestGameTour({ game_id: gameID }).then( res => {
               
                if( res.data.status == "true" ){
                    const tour = res.data.data;
        
                    // 有联赛，去请求联赛赛事，
                    const tourId = Object.keys(tour);
                    if( tourId.length > 0 ){
                        dispatch("fetchTournamentName", {ids: tourId.join(",")}) 
                    };
                    
                    let arr = []; // 转化为数组
                    for( let key in tour ){
                        if( tour[key] ){
                            const sort_code = tour[key].sort_code ? tour[key].sort_code : 99999999999
                            arr.push({
                                tourId: key,
                                matchId: tour[key].matches,
                                checked: false,
                                gameID: gameID,
                                sort_code: sort_code
                            });
                        }
                    };
                    commit("UPDATE_LEAGUECHECKEDS", tour);
                    commit("SET_TOUR_LIST", { arr: arr, key: gameID });
                }
                resolve(res);
            })
        });
    },

    // 查询当前联赛的赛事列表
    fetchMatchTourList({ commit }, data){
        return new Promise( async (resolve, reject) => {
            const { matchId } = data;
            gameApi.requestMatchState({
                ids: matchId.join(",").trim()
            }).then( res => {
                if( res.data.status == "true" ){
                    const data = transMatch(res.data.data) || [];
                    commit("SET_MQTT_OBJECT", matchSplit(data))
                    commit("SET_AllMATCH_LIST", data);
                }
                resolve(res)
            })
        })
    },

    // 赛事列表
    fetchMatchList({ commit, dispatch }, params) {
        if(params.flag==10){
            return
        }
        return new Promise( (resolve, reject) => {
            gameApi.requestMatchList(params).then( async (res) => {
                resolve(res)
                if(res.data.status == "true"){
                    let data = transMatch(res.data.data) || [];
                    commit("SET_MQTT_OBJECT", matchSplit(data))
                    // 赛果:已关盘-6 已结算-7 比赛取消-9; 其他: 开盘-5
                    if( params.flag == 5 ){
                        const filterArr = [6, 7, 9];
                        data = data.filter( ele => filterArr.includes(Number(ele.status)) )
                    }else if( params.flag == 8 ){
                        const filterArr = [5, 6, 7, 9];
                        data = data.filter( ele => filterArr.includes(Number(ele.status)) )
                    }else{
                        data = data.filter( ele => ele.status == 5 )
                    }
                    commit("SET_MATCH_LIST", data);
                    
                    // 获取赛事战队名称 ==> 冠军盘用
                    if( params.flag == 8 ){
                        const teams = await awaitWrap(gameApi.requestGameTeam( { ids: data.map( ele => ele.id).join(",")} ));
                        if( teams.data.status == "true" ){
                            commit("SET_MATCH_TEAMS", teams.data.data || {})
                        }
                    }else{
                        dispatch("fetchTimer");
                    }
                }
            });
        });
    },


    // 赛事搜索
    fetchMatchTeamSearch({ commit, dispatch }, params) {
   
        return new Promise( (resolve, reject) => {
            gameApi.requestMatchTeamSearch(params).then( async (res) => {
                resolve(res)
                if(res.data.status == "true"){
                    let data = transMatch(res.data.data) || [];
                    commit("SET_MQTT_OBJECT", matchSplit(data))
                    // 赛果:已关盘-6 已结算-7 比赛取消-9; 其他: 开盘-5
                    if( params.flag == 5 ){
                        const filterArr = [6, 7, 9];
                        data = data.filter( ele => filterArr.includes(Number(ele.status)) )
                    }else if( params.flag == 8 ){
                        const filterArr = [5, 6, 7, 9];
                        data = data.filter( ele => filterArr.includes(Number(ele.status)) )
                    }else{
                        data = data.filter( ele => ele.status == 5 )
                    }
                    commit("SET_MATCH_LIST", data);
                    
                    // 获取赛事战队名称 ==> 冠军盘用
                    if( params.flag == 8 ){
                        const teams = await awaitWrap(gameApi.requestGameTeam( { ids: data.map( ele => ele.id).join(",")} ));
                        if( teams.data.status == "true" ){
                            commit("SET_MATCH_TEAMS", teams.data.data || {})
                        }
                    }else{
                        dispatch("fetchTimer");
                    }
                }
            });
        });
    },

    // 获取比分网数据开关状态
    async fetchBifenBtnStatus({ commit, dispatch, state }){
        const res = await awaitWrap(dataApi.requestBifenBtnStatus());
        // 设置赛事列表
        if(res.data.status == "true" && res.data.data){
            const data = res.data.data;
            commit("SET_BIFEN_BTN_STATUS",  data);
        }
    },

    // 请求时间
    async fetchTimer({commit, state}){
        const json = await gameApi.requestTimer();
        if(json.data.status == "true"){
            let newdata = {};
            const data = json.data.data;
            const currentSysTime = state.serverTime + (new Date().getTime()- state.localTime);
            for( let key in data ){
                const { start_time, time_ticket, status } = data[key];
                const match_id = key.replace("$","");
                newdata[match_id] = {
                    ...data[key],
                    "time_ticket": status == 1 ? currentSysTime/1000 - Number(start_time) + Number(time_ticket) : time_ticket
                }
            }
            commit("SET_MATCH_TIMER", newdata || {});
        }
    },

    // 收藏列表赛事状态列表
    async fetchMatchState({ commit, state, dispatch }, data){
        if( data.type == "collect" ){
            const ids = state.collect.map( ele => ele.id );
            if(ids.length > 0){
                const res = await awaitWrap(gameApi.requestMatchState({
                    ids: ids.join(",").trim()
                }));
                if( res.data.status == "true" ){
                    const json = res.data.data;
                    const matchData = transMatch(json)
                    commit("SET_MQTT_OBJECT", matchSplit(matchData))
                    commit("SET_COLLECT_LIST", matchData)
                };
            }
        };
        if( data.type == "market" ){
            const res = await awaitWrap(gameApi.requestMatchState({
                ids: data.ids.trim()
            }));
            if( res.data.status == "true" ){
                let json = res.data.data;
                const matchData = transMatch(json)
                commit("SET_MQTT_OBJECT", matchSplit(matchData))
                commit("SET_MARKET_DETAIL", matchData);
                // 更新赛事时间
                dispatch("fetchTimer");
            };
        };
    },


    

    // 查询盘口详情
    async fetchMarketDetails({ commit, dispatch }, data) {
        return new Promise( async (resolve, reject) => {
            const res = await awaitWrap(gameApi.requestMatchState({
                ids: data.match_id.trim()
            }));
            if( res.data.status == "true" ){
                let json = res.data.data;
                const matchData = transMatch(json)
                commit("SET_MQTT_OBJECT", matchSplit(matchData))
                commit("SET_MARKET_DETAIL", matchData);
                // 更新赛事时间
                await dispatch("fetchTimer");
                resolve(res)
            };
        })

    },
    
    // 查询盘口列表
    async fetchMarketList({ commit, dispatch }, data) {
        return new Promise( async (resolve, reject) => {
            let arr = []
            const res = await awaitWrap(gameApi.requestMarketList(data));
            arr = res.data.data || [];
            if(res.data.status == "true"){
                arr = transMarket(arr) || []
                commit("SET_MQTT_OBJECT", marketSplit(arr));
                commit("SET_MARKES_LIST",  arr);
            }else{
                commit("SET_MARKES_LIST", []);
            }
            resolve();
        })
    },

    // 获取联赛名称
    async fetchTournamentName({ commit }, params){
        const res = await awaitWrap(gameApi.requestTournamentName(params));
        if(res.data.status == "true"){
            commit("SET_TOURNAMENT_NAME", res.data.data || {});
        } else {
            commit("SET_TOURNAMENT_NAME", {});
        }
    },

    // 获取所有图片 
    async fetchAllImages({ commit, state }, data){
        const res = await awaitWrap(gameApi.requestImages(data));
        let gameImg = {};
        let teamImg = {};
        
        if(res.status == 200){
            gameImg = res.data.game_imag || {};
            teamImg = res.data.team_imag || {};
        }
        commit( "SET_IMAGES", {
            url: UPLOAD_URL
        });

        global.GameImages = gameImg;
        global.TeamImages = teamImg;
    },

    // 赛事数量
    async fetchMatchNumber({ commit, state }){
        const id = state.gameID;
        let game_id = "";
        if( id != 0 ){
            game_id = id;
        }

        const res = await awaitWrap(gameApi.requestMatchMeun({ game_id: game_id }));
        if( res.data.status == "true" ){
            commit("SET_MATCH_NUMBER", res.data.data )
        } else {
            commit("SET_MATCH_NUMBER", {} );
        }
    },


    // 推荐赛事
    async fetchMatchRecommend({ commit, dispatch, state }){
        const res = await awaitWrap(gameApi.requestMatchRecommend());
        // 设置赛事列表
        if(res.data.status == "true"){
            const data = transMatch(res.data.data) || [];
            commit("SET_MQTT_OBJECT", matchSplit(data))
            commit("SET_RECOMMEND_LIST",  data);
        }
    },

    // 亚运会赛事列表  requestAsianGamesRecommend 调试数据用：requestMatchRecommend
    fetchAsianGamesRecommend({ commit, dispatch, state }){
        return new Promise( async (resolve, reject) => {
            const res = await awaitWrap(gameApi.requestAsianGamesRecommend());
            if(res.data.status == "true"){
                const data = transMatch(res.data.data) || [];
                commit("SET_MQTT_OBJECT", matchSplit(data));
                commit("SET_ASIANRECOMMEND_LIST",  data);
                resolve()
            } 
        });
    },

    // 查询主播盘信息
    async fetchAnchorInfo({ commit, dispatch }, data) {
        return new Promise( async (resolve, reject) => {
            // await dispatch('fetchMatchState', { ids: data.match_id, type: "market" });
            
            const res = await gameApi.requestAnchorInfo(data);
            if(res.data.status == "true"){
                commit("SET_ANCHOR_INFO", res.data.data[data.ids] || []);
            }
            // resolve(res)
        })

    },

    // 虚拟赛事列表
    async fetchVirtualMatches({ commit, state }, params){
        const res = await awaitWrap(virtualSportsApi.requestSportsList(params));
        if( res.data.status == "true" ){
            let list = res.data.data || [];
            list.map(el=>{
                el.game_data = transMatch(el.game_data)
                el.maxDuration = ''
            })
            commit("SET_VIRTUAL_MATCH_LIST", list);
        } else {
            commit("SET_VIRTUAL_MATCH_LIST", [] );
        }
    },


    // 虚拟赛事视频详情
    async fetchVirtualReplay({ commit, state }, params){
        const res = await awaitWrap(virtualSportsApi.requestVirtualReplay(params));
        const {sid} = state.virtualMatch;
        if(!res.data.data[`$${sid}`]){
            console.log('没有视频数据', sid, params)
        }
        if( res.data.status == "true" && res.data.data[`$${sid}`]){
            const {nextBatchNo, nextRound, nextStartTime,} = res.data.data;
            const {list:detail, url, duration,} = res.data.data[`$${sid}`];
            const virtualMatch = {
                ...state.virtualMatch,
                detail,
                url,
                duration,
                nextBatchNo, 
                nextRound, 
                nextStartTime,
            }
            commit("SET_VIRTUAL_MATCH", virtualMatch );
            commit("SET_VIRTUAL_MATCH_LIST_TIME", {batch_no: params.batch_no,data: res.data.data} );
        }
    },

    // 虚拟赛事详情
    async fetchVirtualMatchDetail({ commit, state }, params){
        console.log('获取赛事详情')
        const {match_id, matchStatus = 0, video_status = 0} = params;
        const res = await awaitWrap(virtualSportsApi.requestVirtualMatchDetail({match_id, video_status}));
        if( res.data.status == "true"){
            const {match_detail, match_data} = res.data.data;
            const {round_no} = match_data;
            // 处理点球比分
            const scoreArr = match_data.score.split(',');
            match_data.score = scoreArr[0];
            match_data.penaltyScore = scoreArr[1];
            const matchData = transMatch([match_data]) || [];
            delete res.data.data.match_data;
            match_detail.forEach(item => {
                item.market_data = transMarketVSports(item.market_data, matchStatus) || [];
            });
            const virtualMatch = {
                ...state.virtualMatch,
                ...res.data.data,
                ...matchData[0],
                round_no,
                matchStatus,
            }
            commit("SET_VIRTUAL_MATCH", virtualMatch );
            return virtualMatch;
        } else {
            commit("SET_VIRTUAL_MATCH", {} );
            return {};
        }
    },

    // 当前轮数赛果列表
    async fetchVirtualResult({ commit, state }, params){
        console.log('params',params)
        const {batch_no, tour_id, game_id, matchId} = params;
        const res = await awaitWrap(virtualSportsApi.requestBatchNoResult({batch_no, game_id, tour_id, }));
        if( res.status == 200 &&res.data.data) {
            if( batch_no == res.data.data.batch_no) {
                res.data.data.game_data.forEach(match=>{
                    const scoreArr = match.score.split(',');
                    match.score = scoreArr[0];
                    match.penaltyScore = scoreArr[1];
                })
                // 处理点球比分
                if(matchId){
                    const {penaltyScore} = res.data.data.game_data.find(item=>item.id === matchId);
                    const virtualMatch = {
                        ...state.virtualMatch,
                        penaltyScore,
                    }
                    commit("SET_VIRTUAL_MATCH", virtualMatch );
                }
                commit("SET_VIRTUAL_MATCH_LIST_STATE", {game_data:transMatch(res.data.data.game_data),batch_no:batch_no} );
            }

        }
    },

    // 获取服务器时间
    async getSystemTime({ commit }){
        const res = await awaitWrap(userApi.requestSystemTime());
        if( res.status == 200 ){
            commit("SET_SERVER_TIME", res.data.data * 1000);
        }
    },

    // 体育节庆UI oss域名
    setTyOssData({ commit }, data) {
        commit("SET_TY_OSS_DATA", data)
    },

     // 获取比分网数据模块
     async fetchBifenMatchData({ commit, dispatch, state }, params){
        const res = params.isCSGO ? await awaitWrap(dataApi.requestBifenCsgoMatchData(params.params)) : await awaitWrap(dataApi.requestBifenMatchData(params.params))
        if(res.data.status == "true" && res.data.data){
            const data = {
                ...res.data.data,
                isCSGO: params.isCSGO,
                stage: params.params.stage_id
            };
            commit("SET_BIFEN_MATCH_DATA",  data);
        } else {
            commit("SET_BIFEN_MATCH_DATA",  {}); 
        }
    },

    // 获取比分网echarts数据
    async fetchBifenEchartsData({ commit, dispatch, state }, params){
        const res = await awaitWrap(dataApi.requestBifenAntEvents(params));
        if(res.data.status == "true" && res.data.data){
            const data = res.data.data;
            commit("SET_BIFEN_ECHARTS_DATA", data);
        } else {
            commit("SET_BIFEN_ECHARTS_DATA",  {}); 
        }
    },

    // 获取资源域名
    async fetchUpdateDomain({ commit }, params){
        const res = await awaitWrap(gameApi.requestUpdateDomain(params));
        return res.data.data.pc;
    },

    // 更新赛事搜索
    updateSearchMatchList({ dispatch, state }, obj = {}) {
        const { gameID, matchFlag, page_size, teamWords, earlyDate, crossDate, endDate } = state;
        let datares = {  
            game_id: gameID ? gameID : '0',
            flag: matchFlag,
        };
        switch (matchFlag){
            case 1: // 今日 
                datares = {
                    ...datares,
                    day: 1
                };
                dispatch("fetchMatchList", {
                    ...datares,
                    ...obj
                }); // 查询赛事列表
                break;
            case 5: // 赛果   
                datares = {
                    ...datares,
                    page: 1,
                    page_size: page_size,
                    team_name: teamWords,
                    date: endDate
                };
                dispatch("fetchMatchTeamSearch", {
                    ...datares,
                    ...obj
                }); // 查询赛事列表
                break;
            case 2: // 早盘
                datares = {
                    ...datares,
                    page: 1,
                    page_size: page_size,
                    team_name: teamWords,
                    date: earlyDate
                };
                dispatch("fetchMatchTeamSearch", {
                    ...datares,
                    ...obj
                }); // 查询赛事列表
                break;
            case 4: // 串关 
                datares = {
                    ...datares,
                    ...obj,
                };
                if( crossDate == -1 ){
                    dispatch("fetchMatchTeamSearch", {
                        ...datares,
                        page: 1,
                        page_size: page_size,
                        team_name: teamWords,
                        date: crossDate
                    }); // 查询赛事列表
                }else{
                    dispatch("fetchMatchList", {
                        ...datares,
                        day: crossDate
                    }); // 查询赛事列表
                }
                break; 
            case 3: // 滚球     
                datares = {
                    ...datares,
                    page_size: 0,
                };
                dispatch("fetchMatchList", {
                    ...datares,
                    ...obj
                }); // 查询赛事列表
                break; 

            case 7:
                dispatch("fetchMatchList", {
                    ...datares,
                }); // 查询
                break;                 
            case 8:
                datares = {
                    ...datares,
                };
                dispatch("fetchMatchList", {
                    ...datares,
                    ...obj
                }); // 查询赛事列表
                break;              
        }

    },


    // 更新赛事列表
    updateMatchList({ dispatch, state }, obj = {}) {
    
        if( state.teamWords ){
            dispatch("updateSearchMatchList")
        }else{
            const { gameID, matchFlag, earlyDate, crossDate, endDate, page_size } = state;
            let datares = {  
                game_id: gameID ? gameID : '0',
                flag: matchFlag,
            };
            switch (matchFlag){
                case 1: // 今日
                    datares = {
                        ...datares,
                        day: 1
                    };
                    break;
                case 2: // 早盘
                    datares = {
                        ...datares,
                        day: earlyDate,
                        page_size: page_size,
                    };
                    break;
                case 5: // 赛果
                    datares = {
                        ...datares,
                        day: endDate,
                        page_size: page_size,
                    };
                    break;
                case 4: // 串关
                    datares = {
                        ...datares,
                        day: crossDate,
                    };
                    
                    // 全部日期的时候分页
                    if (datares.day == -1) {
                        datares.page_size = page_size;
                    }
                    break;
                case 3: // 滚球     
                    datares = {
                        ...datares,
                        page_size: 0,
                    };
                    break; 
                case 7:
                    dispatch("fetchMatchList", {
                        ...datares,
                    }); // 查询
                    break;                     
                case 8:
                    datares = {
                        ...datares,
                        // page_size: page_size,
                    };
                    break;              
            }
            dispatch("fetchMatchList", {
                ...datares,
                ...obj
            }); // 查询赛事列表
        }
    },

    // 更新未点击搜索关键字
    updateSearchValue({ commit }, params){
        commit("SET_SEARCH_WORDS",  params ); 
    },

    // 获取用户赔率分组组别
    async fetchUserOddGroup({ commit }){
        const res = await awaitWrap(gameApi.requestUserOddGroup());
        if(res.data.status == "true"){
            commit("SET_ODD_GROUP", res.data.data );
        } 
    },

    

};

export default actions;