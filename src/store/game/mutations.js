import lodash from "lodash";
import { 
    championMatchSort, 
    matchSort, 
    recommendSortMatch, 
    gameSort,
    marketRoundSort,
    marketSort,
    asiaDRecommendSortMatch
} from "@/assets/scripts/sort.js";
import i18n from "@/assets/i18n/index.js";

// 赔率分组 --- 赔率更新事用 
function getOddGroup(match, mkt, item, oddGroup){
    if( match.category == 5 ){ // 冠军盘不参与赔率分组计算
        return Math.formatAmount(item.odd,3)
    }
    if( mkt.option_type == 12 || mkt.option_type == 23 ){ // 复合玩法不参与赔率分组计算
        return Math.formatAmount(item.odd,3)
    }
    if( item.odd_group && item.odd_group[oddGroup] ){ // 判断是否有值，兼容赛事老数据
        if( oddGroup == "g0" ){
            return Math.formatAmount(item.odd,3)
        }else{
            return Math.formatAmount(item.odd_group[oddGroup], 3)
        }
    }else{
        return Math.formatAmount(item.odd,3)
    }
}

// 购物车赔率分组 --- 赔率更新事用 
function getOddGroupCart(item, oddGroup){
    return getOddGroup( 
        { category: item.category },
        { option_type: item.option_type },
        { 
            odd: item.relodds,
            odd_group: item.odd_group
        },
        oddGroup
    )
}

const Mqtt = {


    SET_MQTT_OBJECT(state, data){
        const { mqMatch, mqMarket, mqOdds } = data;
        state.mqMatch = {
            ...state.mqMatch,
            ...mqMatch
        }
        state.mqMarket = {
            ...state.mqMarket,
            ...mqMarket
        }
        state.mqOdds = {
            ...state.mqOdds,
            ...mqOdds
        }
    },

    DEL_MQTT_OBJECT(state, data){
        state.mqMatch = {};
        state.mqMarket = {};
        state.mqOdds = {};
    },


    // 投注项赔率变更
    SET_MARKET_ODDS(state, data) {
      
        const { stationList, betCart, mqOdds  } = state;

        // 赛事变化赔率
        data.forEach( ele => {
            if( mqOdds[ele.id] ){
                mqOdds[ele.id].odd = ele.odd;
                mqOdds[ele.id].odd_group = ele.odd_group;
            }
        })
        
        // 购物车变化赔率
        betCart.forEach((element) => {
            const res = data.find((ele) => ele.id == element.item_id);
            if (res) {
                element.relodds = res.odd;
                element.odd_group = res.odd_group;
                element.odds = getOddGroupCart(element, state.oddGroup.group);
                if( element.profitable == element.relProfitable ){
                    element.profitable = Math.formatAmount(Number(element.bet_amount) * Number(element.odds));
                    element.relProfitable = Math.formatAmount(Number(element.bet_amount) * Number(element.odds));
                }
            }
        });


        // 局内串关
        for( let key in  stationList ){
            stationList[key].list.forEach((element) => {
                const res = data.find((ele) => ele.id == element.item_id);
                if (res) {
                    element.relodds = res.odd;
                    element.odd_group = res.odd_group;
                    element.odds = getOddGroupCart(element, state.oddGroup.group);
                }
            });
        }

    },

    // 盘口暂停取消
    SET_MARKET_SUSPENDED(state, data) {
        // 赛事暂停取消
        const { stationList, betCart, mqMarket  } = state;

        data.forEach( ele => {
            if( mqMarket[ele.market_id] ){
                mqMarket[ele.market_id].suspended = ele.suspended;
            }
        })

        // 购物车
        betCart.forEach((element) => {
            const res = data.find((ele) => ele.market_id == element.market_id);
            if (res) {
                element.market_suspended = res.suspended;
            }
        });

        // 局内串关
        for( let key in  stationList ){
            stationList[key].list.forEach((element) => {
                const res = data.find((ele) => ele.market_id == element.market_id);
                if (res) {
                    element.market_suspended = res.suspended;
                }
            });
        }

    },

    // 盘口显示隐藏
    SET_MARKET_VISIBLE(state, data) {
        const { stationList, betCart, mqMarket  } = state;
       
        data.forEach( ele => {
            if( mqMarket[ele.market_id] ){
                // mqMarket[ele.market_id] = Object.assign( {}, mqMarket[ele.market_id], { visible: ele.visible } )
                mqMarket[ele.market_id].visible = ele.visible;
            }
        })
    
        // 购物车显示隐藏
        betCart.forEach(element => {
            const res = data.find(ele => ele.market_id == element.market_id);
            if(res){
                element.market_visible = res.visible;
            }
        });

        // 局内串关
        for( let key in  stationList ){
            stationList[key].list.forEach((element) => {
                const res = data.find(ele => ele.market_id == element.market_id);
                if(res){
                    element.market_visible = res.visible;
                }
            });
        }

    },

     // 赛事暂停取消
    SET_MATCH_SUSPENDED(state, data) {
        // 赛事 , 
        data.forEach( item => {
            if(state.mqMatch[item.match_id]){
                state.mqMatch[item.match_id].suspended = item.suspended
            }
        });

        // 购物车
        state.betCart.forEach((element) => {
            const res = data.find((ele) => ele.match_id == element.match_id);
            if (res) {
                element.match_suspended = res.suspended;
            }
        });

        // 局内串关
        for( let key in  state.stationList ){
            state.stationList[key].list.forEach((element) => {
                const res = data.find((ele) => ele.match_id == element.match_id);
                if (res) {
                    element.match_suspended = res.suspended;
                }
            });
        }

    },

    // 赛事隐藏显示
    SET_MATCH_VISIBLE(state, data) {
        // 赛事
        data.forEach( item => {
            if(state.mqMatch[item.match_id]){
                state.mqMatch[item.match_id].visible = item.visible
            }
        });
        
        const marketIdList = data.map((ele) => {
            if (!ele.visible) {
                return ele.match_id || 0;
            }
        });

        // 购彩蓝
        state.collect = state.collect.filter(
            (item) => !marketIdList.includes(item.id)
        );
        
        // 购物车
        state.betCart.forEach(element => {
            const res = data.find(ele => ele.match_id == element.match_id);
            if(res){
                element.match_visible = res.visible;
            }
        });

        // 局内串关
        for( let key in  state.stationList ){
            state.stationList[key].list.forEach((element) => {
                const res = data.find(ele => ele.match_id == element.match_id);
                if(res){
                    element.match_visible = res.visible;
                }
            });
        }

    },

    // 投注项显示隐藏
    SET_MARKET_BET_VISIBLE(state, data) {
        const { market_id, odd_id, visible } = data;
    
        const { stationList, betCart, mqOdds  } = state;

        if( mqOdds[odd_id] ){
            mqOdds[odd_id].visible = visible;
        }
        

        // 购物车
        betCart.forEach((element) => {
            if (odd_id == element.item_id) {
                element.visible = visible;
            }
        });

        // 局内串关
        for( let key in  stationList ){
            stationList[key].list.forEach((element) => {
                if (odd_id == element.item_id) {
                    element.visible = visible;
                }
            });
        }
    },
    
    // 投注项显示隐藏
    SET_MARKET_BET_SUSPENDED(state, data) {

        const { market_id, odd_id, suspended } = data;
        const { stationList, betCart, mqOdds  } = state;

        if( mqOdds[odd_id] ){
            mqOdds[odd_id].suspended = suspended;
        }

        // 购物车
        betCart.forEach((element) => {
            if (odd_id == element.item_id) {
                element.suspended = suspended;
            }
        });

        // 局内串关
        for( let key in  stationList ){
            stationList[key].list.forEach((element) => {
                if (odd_id == element.item_id) {
                    element.suspended = suspended;
                }
            });
        }
    },

    // 赛事是否支持滚球
    SET_MATCH_LIVESUPPORT(state, data){
        const { mqMatch } = state;

        data.forEach( ele => {
            if(mqMatch[ele.match_id]){
                mqMatch[ele.match_id].live_support = ele.live_support
            }
        });
    },

    // 串关变更
    SET_MATCH_PASS(state, data) {
        const { mqMatch, betCart,  stationList } = state;
        data.forEach( ele => {
            if(mqMatch[ele.match_id]){
                mqMatch[ele.match_id].is_pass_off = ele.is_pass_off
            }
        });    

        betCart.forEach((ele) => {
            const res = data.find((item) => ele.match_id == item.match_id);
            if (res) {
                ele.is_pass_off = res.is_pass_off;
                if(res.is_pass_off != 0 && res.market_pass_off != 0){
                    ele.same_match = false
                }else{
                    ele.same_match = true
                }
            }
        });

        for( let key in  stationList ){
            stationList[key].list.forEach((ele) => {
            const res = data.find((item) => ele.match_id == item.match_id);
                if (res) {
                    ele.is_pass_off = res.is_pass_off;
                }
            });
        }
    },
    
    // 取消关联赛事数据源
    SET_MACTH_ANT_LINK(state, data){
        const { mqMatch } = state;
        const { match_id, ant_match_id } = data;

        if( mqMatch[match_id] ){
            mqMatch[match_id].ant_match_id = ant_match_id
        }
    },

    // 盘口数量更新
    SET_MARKET_COUNT(state, data){
        const { mqMatch } = state;
        const { match_id, count } = data;
        if( mqMatch[match_id] ){
            mqMatch[match_id].count = count
        }
    },


    // 比分网数据 按钮显示隐藏
    SET_BIFEN_LOCK(state, data){
        const { mqMatch } = state;
        const { match_id, animated_video_lock, real_time_data_lock, prospective_analysis_lock, live_lock } = data;
        if( mqMatch[match_id] ){
            mqMatch[match_id].animated_video_lock = animated_video_lock;
            mqMatch[match_id].real_time_data_lock = real_time_data_lock
            mqMatch[match_id].prospective_analysis_lock = prospective_analysis_lock
            mqMatch[match_id].live_lock = live_lock
        }       
    },



    // 比赛变更滚球状 不可逆操作 
    SET_MATCH_LIVE(state, data) {
        const { mqMatch, betCart, stationList} = state;
        const { match_id, is_live} = data;
        
        
        if( mqMatch[match_id] ){
            mqMatch[match_id].is_live = is_live
        }

        betCart.forEach( ele => {
            if(ele.match_id == match_id){
                ele.is_live = is_live
            }
        });


        // 局内串关
        for( let key in stationList ){
            stationList[key].list.forEach(ele => {
                if(ele.match_id == match_id){
                    ele.is_live = is_live
                }
            });
        }
    },

    // 赛事比分更新
    SET_MATCH_SCORE(state, data){
        const { mqMatch } = state;
        const { match_id, score} = data

        if( mqMatch[match_id] ){
            mqMatch[match_id].home_score = score.split(":")[0];
            mqMatch[match_id].away_score = score.split(":")[1];
            mqMatch[match_id].score = score
        }
    },

    // 赛事取消 不可逆操作
    SET_MATCH_UPDATE(state, data){

        data.forEach( item => {

            if( state.mqMatch[item.match_id] ){
                state.mqMatch[item.match_id].status = item.status
            }

            state.betCart.forEach(ele => {
                if( ele.match_id == item.match_id ){
                    ele.match_status = item.status
                }
            })

            // 局内串关
            for( let key in state.stationList ){
                state.stationList[key].list.forEach(ele => {
                    if( ele.match_id == item.match_id ){
                        ele.match_status = item.status
                    }
                });
            }
        });
    },

    // 赛事返还率 局内串关
    SET_MATCH_RNDDSCUT(state, data){
        const { rnd_comp_odd_dscnt, match_id } = data;
        const { stationList, mqMatch} = state;
        if(  rnd_comp_odd_dscnt ){
            // 局内串关
            for( let key in stationList ){
                if( match_id == stationList[key].match_id ){
                    stationList[key].odd_rate = rnd_comp_odd_dscnt
                };
                break;
            }

            if( mqMatch[match_id] ){
                mqMatch[match_id].rnd_comp_odd_dscnt = rnd_comp_odd_dscnt
            }
        }
    },

    // 盘口串关推送
    SET_MARKET_PASS(state,data){

        const markets= data[0].markets || [];

        const { mqMarket, betCart,  stationList } = state;

        markets.forEach( ele => {
            if( mqMarket[ele.market_id] ){
                mqMarket[ele.market_id] .is_pass_off = ele.is_pass_off;
            }
        })

        betCart.forEach( element => {
            const res = markets.find( ele => ele.market_id == element.market_id);
            if(res){
                element.market_pass_off = res.is_pass_off
                if(element.is_pass_off != 0 && element.market_pass_off != 0){
                    element.same_match = false
                }else{
                    element.same_match = true
                }
            }
        });

        for( let key in stationList ){
            stationList[key].list.forEach(element => {
                const res = markets.find(ele => ele.market_id == element.market_id);
                if( res ){
                    element.market_pass_off = res.is_pass_off
                }
            });
        }
    },

    // 推荐赛事 排序变更
    SET_RECOMMEND_UPDATE(state, data){
        const { match_id, rec } = data
        const { mqMatch } = state;
        if( mqMatch[match_id] ){
            mqMatch[match_id].rec =rec
        }
    },

    SET_MATCH_VIDEO_TYPE(state, data) {
        const { mqMatch } = state;
        const { match_id, video_type } = data;

        if( mqMatch[match_id] ){
            mqMatch[match_id].video_type = video_type
        }
    },

    // 赛事直播地址变更 
    SET_MATCH_VIDEO_URL(state, data) {
        const { mqMatch } = state;
        const { match_id, video_url, flv_url, video_type } = data;

        if( mqMatch[match_id] ){
            mqMatch[match_id].user_video_url = video_url
            mqMatch[match_id].admin_video_url = flv_url
            mqMatch[match_id].video_type = video_type
        }
    },

    // 比赛时间更新
    SET_MATCH_STARTTIME(state, data){
        const { mqMatch } = state;
        const { match_id, start_time, close_time } = data;

        if( mqMatch[match_id] ){
            mqMatch[match_id].start_time = start_time;
            mqMatch[match_id].close_time = close_time;
        }
    },

    // 盘口排序
    SET_MARKET_SORTCODE(state, data){
        const { mqMarket } = state;
        const { sort_code } = data;

        for( let id in sort_code ){
            if(mqMarket[id]){
                mqMarket[id].sort_code = sort_code[id];
            }
        }
    },

    // 盘口重新开盘
    SET_MARKET_REOPEN(state, data){

        const { mqMarket } = state;
        for( let id in data.market_id ){
            if( mqMarket[id] ){
                mqMarket[id].status = 6;
                mqMarket[id].visible = 0;
            }
        } 
    },

    // 盘口名称更新和基准分更新
    SET_MARKET_NAME_UPDATE(state, data){
        const { mqMarket } = state;
        const {market_id, match_id, cn_name, name, score_benchmark} = data;

        if( mqMarket[market_id] ){
            let _name = name || cn_name;
            _name = _name.replace(/\([^\)]*\)/g,"("+score_benchmark+")");
            mqMarket[market_id].score_benchmark = score_benchmark;
            mqMarket[market_id].name = _name
        }
    },

    /**
     * 盘口状态发生变化：已开盘、关盘、待结算、结算、盘口取消
     * @param {*} state 
     * @param { Array } data 发生状态变化的盘口数组
     */
    SET_MARKET_UPDATE(state, data){
        // mqMarket 当前已获取的所有盘口
        const { mqMarket, mqOdds } = state;

        data.forEach( ele => {
            // 如果存在这个盘口
            if( mqMarket[ele.market_id] ){
                mqMarket[ele.market_id].status = ele.status;

                // 判断胜负平
                if( ele.odd_winner_status ){
                    Object.keys(ele.odd_winner_status).forEach((el)=>{
                        if( mqOdds[el] ){
                            mqOdds[el].is_winner = ele.odd_winner_status[el]
                        }
                    })
                }

                // 购彩蓝
                state.betCart.forEach( element => {
                    if(element.market_id == ele.market_id) {
                        element.market_status = ele.status;
                    }
                });

                // 局内串关
                for( let key in state.stationList ){
                    state.stationList[key].list.forEach(element => {
                        if(element.market_id == ele.market_id) {
                            element.market_status = ele.status;
                        }
                    });
                }  

            }
        })
    },












    /****************以上是经过优化的mQTT******************/




    // 冠军盘新增盘口
    SET_MATCH_INSERTODD(state, data){
        const { sourceMatch } = state;
        const { match_id, market_id } = data;
        sourceMatch.forEach( element => {
            if( element.id == match_id ){
                element.ms.forEach( market => {
                    if( market.id == market_id ){
                        market.odds = [
                            ...market.odds,
                            data,
                        ]
                    }
                })
            }
        });
    },




 








    // 首页赔率分组更新
    SET_BASIC_ODDGROUP(state){
        const oddGroup = state.oddGroup.group;
        state.betCart.forEach(element => {
            element.odds = getOddGroupCart(element, oddGroup);     
            if( element.profitable == element.relProfitable ){
                element.profitable = Math.formatAmount(Number(element.bet_amount) * Number(element.odds));
                element.relProfitable = Math.formatAmount(Number(element.bet_amount) * Number(element.odds));
            }
        });
    },



    // 冠军盘 盘口排序
    SET_MARKET_SORTREC(state, data){
        const { sourceMatch } = state;
        const { sort_code, market_id } = data;
        sourceMatch.forEach( element => {
            element.ms.forEach( market => {
                if (market.id == market_id) {
                    market.sort_code = sort_code;
                }
            })
        });
    },

    

    
















    // 赛事比赛时间更新 
    SET_TIMER_UPDATE(state, data) {
        const { matchTimer } = state;
        const { match_id } = data;
        if (matchTimer[match_id]) {
            state.matchTimer = {
                ...matchTimer,
                [match_id]: data,
            };
        }
    },




    // 赛事直播地址变更 {"match_id": 赛事id,"video_url": 直播源}
    SET_MATCH_VIDEO_URL(state, data) {
        const { sourceMatch, collect,  matchs, recommend, allMatchList} = state;
        const { match_id, video_url, flv_url, video_type } = data;
        sourceMatch.forEach((ele) => {
            if (ele.id == match_id) {
                ele.user_video_url = video_url;
                ele.admin_video_url = flv_url
                ele.video_type = video_type
            }
        });

        matchs.forEach((ele) => {
            if (ele.id == match_id) {
                ele.user_video_url = video_url;
                ele.admin_video_url = flv_url
                ele.video_type = video_type
            }
        });

        recommend.forEach((ele) => {
            if (ele.id == match_id) {
                ele.user_video_url = video_url;
                ele.admin_video_url = flv_url
                ele.video_type = video_type
            }
        });

        allMatchList.forEach((ele) => {
            if (ele.id == match_id) {
                ele.user_video_url = video_url;
                ele.admin_video_url = flv_url
                ele.video_type = video_type
            }
        });

        collect.forEach((ele) => {
            if (ele.id == match_id) {
                ele.user_video_url = video_url;
                ele.admin_video_url = flv_url
                ele.video_type = video_type
            }
        });
    },


    // 局内串关最大赔率推送
    UPDATE_STATION_MAXODDS(state, data){
        state.stationOddMax = data;
    },

    
    UPDATE_ODDS_LIMIT(state, data){
        state.crossOddMax = data;
    },


    // 更新游戏列表赛事数量
    SET_MATCH_STAT(state, data){
        state.gameList.forEach( ele => {
            const row = data[ele.id];
            if(row){
                ele.count =  Number(row.total)
            }else{
                ele.count = 0;
            }
        });
        const col = data[state.gameID];
        if(col){
            state.matchNum = col
        }else{
            state.matchNum = {
                ...state.matchNum,
                anchor: 0,
                champ: 0,
                complex: 0,
                early: 0,
                live: 0,
                today: 0,
            }
        }
    },

    // 联赛数量
    SET_MATCH_GAMETOUR(state, data){
        const { game_id, tours } = data;
        for( let key in tours ){
            if( state.tour[game_id] ){
                const hasKey = state.tour[game_id].find( ele => ele.tourId == key);
                if( hasKey ){
                    hasKey.matchId = tours[key];
                    if( tours[key].length == 0 ){
                        hasKey.checked = false;
                    }
                }else{
                    state.tour[game_id] = [
                        ...state.tour[game_id],
                        {
                            checked: false,
                            gameID: game_id,
                            matchId: tours[key],
                            tourId: key,
                        }
                    ]
                    this.dispatch("fetchTournamentName", {ids: key}) 
                }
            }
        }
    },



    UPDATE_RECOMMEND_LIST(state){
        let arr = [];
        const mqMatch = state.mqMatch;
        state.recommend.forEach( (ele) => {
            arr.push({
                ...ele,
                ...mqMatch[ele.id]
            })
        });
        state.recommend = recommendSortMatch(arr);
    },
    
    UPDATE_MATCH_LIST(state){
        let arr = [];
        const mqOdds = state.mqOdds;
        const mqMarket = state.mqMarket;
        state.marketList.forEach( (ele) => {
            let odds = []
            ele.odds.forEach( item => {
                odds.push( mqOdds[item.id] )
            })
            arr.push({
                ...mqMarket[ele.id] ,
                odds: odds
            });
        });
        
        if( state.stationOpen ){
            state.marketList = marketRoundSort(arr);
        }else{
            state.marketList = marketSort(arr);
        }
    },









}


const mutations = {

    // 联赛筛选 联赛
    SET_TOUR_LIST(state, data){
        const { key, arr } = data;
        state.tour = {
            ...state.tour,
            [key]: arr,
        }
    },


    // 设置游戏列表
    SET_GAME_LIST(state, data) { 
        let gameList = [];
        // 计算全部有多少赛事
        const matches_count = data.reduce((pre, item) => {
            return pre + Number(item.count)
        }, 0)

        gameList = [
            {
                id: 0,
                name: i18n.t("common.menu.all"),
                cn_name: i18n.t("common.menu.all"),
                count: matches_count,
            },
            ...gameSort(data),
        ];

        state.gameList = gameList;
    },


    // 设置推荐列表
    SET_RECOMMEND_LIST(state, data){
        state.recommend = recommendSortMatch(data);
    },

    // 亚运会赛事
    SET_ASIANRECOMMEND_LIST(state, data){
        state.asianRecommend = asiaDRecommendSortMatch(data, state.gameSortCode);
    },

    // 改变赛事列表
    SET_MATCH_LIST(state, data) {
        const mqMatch = state.mqMatch;
        const  includesArr = [ 2, 5 ]; // 早盘 赛果 不需要排序；
        // 串关并且选择全部日期
        const isCrossAllDate = Number(state.matchFlag) == 4 && state.crossDate == -1;

        // 是否需要排序
        const needSort = !includesArr.includes(Number(state.matchFlag)) // 早盘、赛果时不做排序
                && !isCrossAllDate; // 串关选择全部日期 不做排序

        let arr = data.filter( ele =>  {
            if( state.matchFlag == 8 || state.matchFlag == 2 || isCrossAllDate || state.matchFlag == 5 ){ // 冠军盘不进行战队搜索
                return ele
            }else{
                return ele.lower_team.includes(state.teamWords.toLowerCase())
            }
        });



        if(needSort){
            if( state.matchFlag == 8 ){
                arr = championMatchSort( arr,  state.gameSortCode, mqMatch )
            }else{
                arr = matchSort( arr,  state.gameSortCode, mqMatch)
            }
        }; 
        // 赛果: 已关盘-6 已结算-7 比赛取消-9 其他: 开盘-5
        if( state.matchFlag == 5 ){
            const filterArr = [6, 7, 9];
            arr = arr.filter( ele => filterArr.includes(Number(ele.status)) )
        }else if( state.matchFlag == 8 ){
            const filterArr = [5, 6, 7, 9];
            arr = arr.filter( ele => filterArr.includes(Number(ele.status)) )
        }else {
            arr = arr.filter( ele => ele.status == 5 )
        }
        // 早盘的今日不显示滚球
        if (state.matchFlag === 2 && state.earlyDate === 0) {
            arr = arr.filter(ele => ele.is_live === 1)
        }
        state.matchLoading = false;
        console.log( arr )
        state.matchList = arr;
    },

    // 设置盘口
    SET_MARKES_LIST(state, data) {
        if( state.stationOpen ){
            state.marketList = marketRoundSort(data);
        }else{
            state.marketList = marketSort(data);
        }
    },




    // 更新联赛筛选
    UPDATE_TOUR_LIST(state, data){
        state.tour = data;
    },

    // 设置电子游戏列表
    SET_SLOT_GAME_LIST(state, data) {
        state.slotGameList = data;
    },
    // 更新游戏排序code
    UPDATE_GAME_SORT_CODE(state, data) {
        if(['10158428472914228', '1001'].includes(data.id)) {
            state.slotGameList = state.slotGameList.map(v => {
                if (v.id === data.id) {
                    v.sort_code = +data.sort_code
                }
                return v
            });
        } else {
            state.gameList = state.gameList.map(v => {
                if (v.id === data.id) {
                    v.sort_code = +data.sort_code
                }
                return v
            });
            state.gameSortCode[data.id] = data.sort_code  == 0 ? -1: data.sort_code
        }
    },

    
    // 赛事数量
    SET_MATCH_NUMBER(state, data){
        state.matchNum = data
    },

    // 设置游戏序号
    SET_GAME_SORTCODE(state, data){
        state.gameSortCode = data;
    },

    // 改变总赛事列表
    SET_AllMATCH_LIST(state, data) {
        state.allMatchList = data;
    },



    SET_TOURNAMENT_NAME(state, data){
        state.tournamentName = {
            ...state.tournamentName,
            ...data
        }
    },



    // 改变游戏id
    SET_GAME_ID(state, gameID) {
        state.gameID = state.gameID !== gameID ? gameID : 0;
        state.matchLoading = true;
    },

    // 设置赛事id
    SET_MATCH_GAME_ID(state, data) {
        state.matchGameID = data;
    },

    // 设置赛事id
    SET_MATCH_ID(state, data) {
        state.matchID = data;
    },

    // 设置搜索的盘口类型 matchFlag 0-全部；1-今日；2-早盘；3-滚球；4-冠军；
    SET_MATCH_FLAG(state, data) {
        state.matchFlag = data;
        state.earlyDate = -1; // 初始化早盘选中时间
        state.crossDate = -1;  // 初始化串关选中时间
        state.endDate = -1;  // 初始化赛果选中时间
        state.matchLoading = true; // 赛事列表请求状态
    },
    SET_VSPORTS_BACK(state, data){
        const { gameId, tab, flag } = data;
        state.matchFlag = flag;
        state.dailyTab = tab;
        state.gameID = gameId
    },
    // 设置早盘被选中的日期
    SET_EARLY_DATE(state, data) {
        state.earlyDate = data;
        state.matchLoading = true;
    },
    // 设置串关被选中的日期
    SET_CROSS_DATE(state, data) {
        state.crossDate = data;
        state.matchLoading = true;
    },
    // 设置赛果日期
    SET_ENDGAME_DATE(state, data) {
        state.endDate = data;
        state.matchLoading = true;
    },

    // 赛事时间
    SET_MATCH_TIMER(state, data) {
        state.matchTimer = data;
    },


    SET_BET_CART(state, data) {
        // 添加操作
        const { isAdd, row } = data;
        if (isAdd) {
            state.betCart = [
                row,
                ...state.betCart,
            ]
            // state.betCart.push(row);
        } else {
            state.betCart=state.betCart.filter((el,index)=> {
                return index != state.betCart.findIndex(ele=>ele.item_id == row.item_id && ele.bet_type == state.betActiveType)
            });
        }
        // 是否有相同赛事操作 same_match: same_match, // 是否有相同赛事

        state.betCart.forEach((ele) => {
            const same_match = state.betCart.filter( item => item.match_id == ele.match_id && item.bet_type == state.betActiveType ).length >= 2 || ele.is_pass_off == 0 || ele.market_pass_off == 0;
            ele.same_match = same_match;
        });

        state.activeBet = "betOrder"; // 菜单切换到投注单
    },

    // 更新输入框内容，和返还奖金 Amount
    SET_BET_AMOUNT(state, data) {
        const { bet_amount, profitable, i, item_id, relProfitable } = data;
        const obj = state.betCart.find(ele => ele.item_id == item_id && ele.bet_type == state.betActiveType);
        obj.bet_amount = bet_amount ? bet_amount : "";
        obj.profitable = profitable;
        obj.relProfitable = relProfitable;
    },

    // 串关预计返还变化
    SET_CROSS_AMOUNT(state, data) {      
        const { bet_amount, i, limitBlance } = data;
        state.cross[i].bet_amount =  bet_amount ? bet_amount : '';
        let profitable =  state.cross[i].list.reduce((pre, item) => {
            let odds = item.reduce( (next,ele) => {
                return next*Number(ele.odds)
            }, 1);  
            // odds = odds <= Number(maxOdd) ? odds : Number(maxOdd);
            // console.log(data)
            return pre + Number(Math.formatAmount(odds,3)) * Number(bet_amount)
        }, 0);
        state.cross[i].profitable = profitable >  limitBlance  + bet_amount ? limitBlance  + bet_amount : profitable;
        state.cross[i].relProfitable  = profitable;

    },

    // 串关赔率变化
    SET_CROSS_ODDS(state, data) {
        state.cross.forEach((ele) => {
            if (ele.odds) {
                ele.odds = data.reduce((pre, item) => {
                    return pre * Number(item.odds);
                }, 1);
            }

            // 赔率变化改变提交后台的投注项
            ele.list.forEach((item, n) => {
                const b = item.map(
                    (row) =>`${row.match_id},${row.market_id},${row.item_id},${row.odds}`
                );
                ele.b[n].b = b.map((col) => col).join("|");
            });
        });
    },

    // 添加串注
    SET_BET_CROSS(state, data) {
        state.cross = data;
    },
    // 清空购物车
    SET_DEL_CART(state) {
        state.betCart =  state.betCart.filter(item=> item.bet_type != state.betActiveType)
    },

    // 删除不合法购物车
    UPDATE_BET_CART(state){
        
        state.betCart = state.betCart.filter(ele => 
            ele.match_visible == 1 &&  // 赛事显示
            ele.market_visible == 1 && // 盘口显示
            ele.match_status == 5 && // 赛事开盘
            ele.market_status == 6 && // 盘口开盘
            ele.visible == 1 // 盘口显示
            && (ele.batch_id && ele.suspended== 1) // 虚拟体育暂停盘口
        );
    },
        
    // 清空已经投注的购物车 bet_amount
    SET_PART_CART(state) {
        state.betCart = state.betCart.filter((ele) => !ele.bet_amount);
    },

    // 修改左侧菜单
    SET_ACTIVE_BET(state, data) {
        state.activeBet = data;
    },

    // 保存勾选的联赛ID
    SET_LEAGUECHECKEDS(state, data) {
        state.leagueCheckeds = data;
    },

    // 更新选择的联赛排序号
    UPDATE_LEAGUECHECKEDS(state, data){
        state.leagueCheckeds.forEach( ele =>  {
            if( data[ele.tourId] ){
                const sort_code = data[ele.tourId].sort_code ? data[ele.tourId].sort_code : 99999999999
                ele.sort_code = sort_code
            }
        })
    },

    SET_DAILY_TAB(state, data) {
        state.dailyTab = data;
        if(data=='cross'){
            state.betActiveType='isCross'
        }else{
            state.betActiveType='isSingle'
        }
    },
    //联赛筛选页购物车状态
    SET_BET_ACTIVE_TYPE(state, data) {
        if(data) {
            state.betActiveType = data
        } else {
            state.betActiveType =  state.dailyTab =='cross' ? 'isCross' : 'isSingle' 
        }
    },

    // 添加到搜藏 塞选状态为5
    SET_COLLECT_LIST(state, data){
        state.collect =  data.filter( ele => ele.status == 5 && ele.visible == 1 );
    },

    // 设置全部图片
    SET_IMAGES(state, data) {
        state.images = data;
    },

    // 设置下拉加载页面大小
    SET_PAGE_SIZE(state, data){
        state.page_size = data;
    },

    // 超过十单
    SET_BET_MAXTEN(state, data){
        state.isbetMax10 = data;
    },

    SET_SEARCH_WORDS(state, data){
        state.teamWords = data;
    },
    SET_SEARCH_WORDSINPUT(state, data){
        state.searchValueTemp = data;
    },


    // 搜索框状态
    SET_SEARCH_STATUS(state, data){
        state.searchActive = data;
    },

    SET_MARKET_DETAIL(state, data){
        // 电竞接入体育数据源
        // NBA2K游戏类型从正常改为篮球盘，客户端以‘篮球盘模版’显示
        // FIFA游戏类型从正常改为足球盘，客户端以‘足球盘模版’显示
        if(data.length) {
            let details = data[0]
            const nba2kGameId = "257733146228414"; // NBA2K 游戏id
            const fifaGameId = "258337013509741"; // FIFA 游戏id
            if(details.game_id === nba2kGameId) {
                details.category = 4 // 篮球盘
            }

            if(details.game_id === fifaGameId) {
                details.category = 6 // 足球盘
            }

            data[0] = details
        }

        state.matchs = data;
    },

    ...Mqtt,
    

    // 局内串关

    SET_STATION_STATUS(state,data){
        state.stationOpen = data;
    },

    // 添加到局内串关 
    SET_BET_STATION(state, data){
        state.stationList = data;
    },

    // 删除不合法购物车
    UPDATE_STATION_CART(state){

        for( let key in state.stationList ){
            state.stationList[key].list = state.stationList[key].list.filter(ele => 
                ele.match_visible == 1 &&  // 赛事显示
                ele.market_visible == 1 && // 盘口显示
                ele.match_status == 5 && // 赛事开盘
                ele.market_status == 6 && // 盘口开盘
                ele.is_pass_off == 2 && // 赛事允许局内串关
                ele.market_pass_off == 2 // 盘口允许串关
            );
            
            // 特殊处理 包胆
            const boDan = state.stationList[key].list.find( ele => (ele.option_type == 5 && ele.visible == 0 ));
            if( boDan ){
                state.stationList[key].list = state.stationList[key].list.filter( ele =>  ele.item_id != boDan.item_id );
            }

            // 清楚十局提示
            if( state.stationList[key].list.length == 9 ){
                this.commit("SET_STATION_MAX", false)
            }

            // 删除完所有订单，删除整局
            if( state.stationList[key].list.length == 0 ){
                this.commit("DEL_STATION_GROUPS", key)
            }
        }
    },


    // 更新投注金额
    UPDATE_STATION_AMOUNT(state, data){
        const { key, amount, odds, win_balance } = data;
        state.stationList[key].bet_amount = parseInt(amount) || '';
        const limitBlance = win_balance + amount
        const profitable = Number( Math.formatAmount(odds,3) ) * Number(amount); // 计算第几局总返还
        state.stationList[key].profitable = limitBlance > profitable ? profitable : limitBlance;
        state.stationList[key].relProfitable = profitable;
    },

    // 删除已投注局内串关注单
    SET_PART_STATION(state, data){
        state.stationList = data;
    },

    // 删除局内串关一单
    DEL_STATION_ONE(state, data){
        const { key, id } = data;
        const list = state.stationList[key].list;
        state.stationList[key].list = list.filter( ele => ele.item_id != id );

        // 删除完所有订单，删除整局
        if( state.stationList[key].list.length == 0 ){
            this.commit("DEL_STATION_GROUPS", key)
        }

         // 清楚十局提示
         if( state.stationList[key] && state.stationList[key].list.length == 9 ){
            this.commit("SET_STATION_MAX", false)
        }
    },

    SET_STATION_MAX(state, data){
        state.stationOrderMax = data
    },

    // 删除所有局内串关
    DEL_STATION_ALL(state){
        state.stationList = {};
    },

    // 删除局内串关一局订单
    DEL_STATION_GROUPS(state, key){ 
         // 清除十局提示
         if( state.stationList[key].list.length == 9 ){
            this.commit("SET_STATION_MAX", false)
        }
        state.stationList = lodash.omit( state.stationList, [key] )
    },

    // 判断当前赛事ID 是否是局内串关赛事
    SET_STATION_DELL(state){
        const { matchID, stationList } = state;
        const key = Object.keys(stationList)[0];
        const item = stationList[key];
        
        if( key &&  item && item.match_id != matchID ){
            this.commit("DEL_STATION_ALL")
            this.commit("SET_STATION_MAX", false)
        }
    },
    SET_BAT_ACTIVE_BET(state, data){
        state.betActiveBet = data;
    },
    // 更新局内串关限额
    UPDATE_STATION_LIMIT(state, data){
        const { key, item_id, complex_min, complex_max, isFull, rate_max_prize, win_balance  } = data;
        for( let i in state.stationList ){
            state.stationList[i].rate_max_prize = rate_max_prize;
            state.stationList[i].win_balance = win_balance;
            if( key == i ){
                state.stationList[i].complex_min = complex_min;
                state.stationList[i].complex_max = complex_max;
            }
            state.stationList[i].list.forEach( ele => {
                if( ele.item_id == item_id ){
                    ele.complex_max = complex_max;
                    ele.isFull = isFull
                }
            })
        }
    },

    //视频关闭开启
    SET_VIDEO_PLAY(state, data){
        state.playVideo = data
    },

    //主播盘赛果信息
    SET_ANCHOR_INFO(state, data){
        state.anchorInfo = data
    },

    //主播盘赛果图片修改
    UPDATE_ANCHOR_INFO(state, data){
        if(state.matchID==data.match_id){
            state.anchorInfo.forEach((element) => {
                if (data.round == element.round) {
                    element.images = data.data;
                }
            });
        }
    },

    //主播盘小局时间修改
    UPDATE_ANCHOR_TIME(state, data){
        if(state.matchID==data.match_id){
            state.anchorInfo.forEach((element) => {
                const res = data.data.find( ele => ele.round == element.round );
                if (res) {
                    element.start_time = res.start_time;
                }else{
                    element.start_time = 0;  
                }
            });
        }
    },
    
    SET_MATCH_TEAMS(state, data){
        state.matchTeams = {
            ...state.matchTeams,
            ...data
        };
    },

    //改变购物车里限额是否请求成功字段
    SET_BET_CART_LIMIT_SUCCEED(state, data){
        const { item_id }=data;
        state.betCart.forEach(element => {
            if(element.item_id==item_id){
                element.limit_succeed = true;
            }
        });
    },

    // 更新体育视频源
    SET_TY_VIDEO_URL(state, data){
        const { matchs } = state;
        const { match_id, admin_video_url } = data;
        if( matchs[0] && matchs[0].id == match_id ){
            matchs[0].admin_video_url = admin_video_url;
        }
    },
    // 页面URL token改变，gameId改变刷新页面初始化状态
    INIT_GAMEID_FAG(state, data){
        const { gameID, matchFlag } = data;
        state.gameID = gameID;
        state.matchFlag = matchFlag;
        state.earlyDate = -1; // 初始化早盘选中时间
        state.crossDate = -1;  // 初始化串关选中时间
        state.endDate = -1;  // 初始化赛果选中时间
        state.matchLoading = true; // 赛事列表请求状态
        // if (matchFlag === 2 || matchFlag === 4) { // 早盘，串关默认全部

        // }
    },

    // 更新虚拟赛事列表
    SET_VIRTUAL_MATCH_LIST(state, data){
        state.virtualMatchList = data;
        const {round_no = ''} = data[0] || {};
        state.matchType = round_no.match(/^Q8|Q4|SEMIFINAL|FINAL/)?.length > 0 ? 'groupKnockout' : 'groupMatches';
        state.knockoutType = round_no.indexOf('Q8') !== -1 ? 0 : round_no.indexOf('Q4') !== -1 ? 1 : round_no.indexOf('SEMIFINAL') !== -1 ? 2 : 3;
        console.log('小组淘汰', state.matchType, state.knockoutType)
    },
    // 更新小组赛，淘汰赛
    SET_MATCH_TYPE(state, data){
        state.matchType = data;
    },
    // 更新淘汰赛内部赛
    SET_KNOCKOUT_TYPE(state, data){
        state.knockoutType = data;
    },
    // 虚拟赛事 10S前锁盘
    SET_MTACH_LOCK(state, data){
        // state.virtualMatchList = data;
        const { batch_no } = data;
        if( !batch_no ) {
            return   
        }
        // 虚拟赛事列表
        state.virtualMatchList.forEach(item=>{
            if(item.batch_no == batch_no){
                item.game_data.forEach((element) => {
                    element.m.odds.forEach((el) => {
                        el.suspended = 1;
                    });
                });
            }
        });

        // 虚拟赛事详情
        if( state.virtualMatch.match_detail && state.virtualMatch.batch_no == batch_no){
            state.virtualMatch.match_detail.forEach((item) => {
                item.market_data.forEach((element) => {
                    element.suspended = 1;
                });
            });
        }

        //更新购物车
        state.betCart.forEach((element) => {
            if(batch_no == element.batch_no) {
                element.market_suspended = 1
                element.suspended = 1
            }
        });

    },
    // 设置虚拟赛事菜单栏位显示/隐藏
    SET_VIRTUAL_GAME_SHOW(state, data){
        state.virtualGameShow = data;
    },
    // 更新虚拟游戏状态
    SET_VIRTUAL_GAME(state, data){
        state.virtualGame = data;
    },
    // 更新虚拟游戏状态
    SET_VIRTUAL_GAMES_STATUS(state, data){
        const {is_open_match} = data;
        state.virtualGame.is_open_match = is_open_match;
    },
    // 更新虚拟联赛
    SET_VIRTUAL_LEAGUE(state, data){
        state.virtualLeague = {
            ...data,
            updata: false,
        };
    },
    // 根据该字段更新虚拟体育赛果
    SET_VIRTUAL_LEAGUE_STATUS(state){
        state.virtualLeague.updata = !state.virtualLeague.updata;
    },
    // 更新虚拟赛事详情
    SET_VIRTUAL_MATCH(state, data){
        state.virtualMatch = data;
    },
    // 更新虚拟赛事状态
    SET_VIRTUAL_MATCH_STATUS(state, data){
        state.virtualMatch.matchStatus = data;
    },
    // 更新系统时间
    SET_SERVER_TIME(state, data){
        state.serverTime = data;
        state.localTime = new Date().getTime();
    },
    // 游戏赛事显隐
    SET_GAME_MATCH_VISIBLE(state, data){
        const { sourceMatch, collect, matchs, allMatchList, virtualGame} = state;
        const { game_id, is_open_match, } = data;
        allMatchList.forEach( element => {
            if( element.game_id == game_id ){
                element.is_open_match = is_open_match;
            }
        });
        sourceMatch.forEach( element => {
            if( element.game_id == game_id ){
                element.is_open_match = is_open_match;
            }
        });
        collect.forEach( element => {
            if( element.game_id == game_id ){
                element.is_open_match = is_open_match;
            }
        });
        if( matchs[0] && matchs[0].game_id == game_id ){
            matchs[0].is_open_match = is_open_match;
        }
        // 虚拟赛事
        if(virtualGame.game_id == game_id){
            virtualGame.is_open_match = is_open_match;
        }
    },

    // 游戏赛事视频显隐
    SET_GAME_VIDEO_VISIBLE(state, data){
        const { sourceMatch, collect, matchs, allMatchList } = state;
        const { game_id, is_open_video } = data;
        allMatchList.forEach( element => {
            if( element.game_id == game_id ){
                element.is_open_video = is_open_video;
            }
        });
        sourceMatch.forEach( element => {
            if( element.game_id == game_id ){
                element.is_open_video = is_open_video;
            }
        });
        collect.forEach( element => {
            if( element.game_id == game_id ){
                element.is_open_video = is_open_video;
            }
        });
        if( matchs[0] && matchs[0].game_id == game_id ){
            matchs[0].is_open_video = is_open_video;
        }
    },

    SET_VIRTUAL_MATCH_LIST_STATE(state, data){
        const { game_data, batch_no}=data
        state.virtualMatchList.forEach(item=>{
            if(item.batch_no == batch_no) {
                item.game_data = game_data
            }
        })
    },

    // 更新列表总视频播放时长
    SET_VIRTUAL_MATCH_LIST_TIME(state, data){
        let maxDuration = 0;
        state.virtualMatchList.forEach(item=>{
            if(item.batch_no == data.batch_no) {
                item.game_data.forEach(match=>{
                    const {duration} = data.data[`$${match.id}`] || {};
                    match.duration = duration;
                    if(duration > maxDuration){
                        maxDuration = duration;
                    }
                })  
                item.maxDuration = maxDuration;
            }
        })
        
    },

    // 虚拟足球赛事列表滚动距离
    SET_VIRTUAL_LIST_TOP(state, data){
        state.listScrollTop = data;
    },

    //节庆赛事OSS域名
    SET_TY_OSS_DATA(state, data){
        state.tyOssData = data;
    },

    // 切换赛果页面展示的内容
    SET_RESULT_TYPE(state, data){
        state.resultType = data;
    },
    
    SET_ACTIVE_PLAY(state, data){
        state.activePlay = data;
    },
    // 设置单注弹出框是否使用过任意快捷金额
    SET_SINGLE_QUICK(state, data){
        state.isUseSingleQuick = data;
    },
    // 设置串注弹出框是否使用过任意快捷金额
    SET_CORSS_QUICK(state, data){
        state.isUseCorssQuick = data;
    },
    // 设置局内串关弹出框是否使用过任意快捷金额
    SET_STATION_QUICK(state, data){
        state.isUseStationQuick = data;
    },
    // 比分网数据模块
    SET_BIFEN_MATCH_DATA(state, data){
        state.bifenMatchData = data;
    },
    //比分网echarts数据
    SET_BIFEN_ECHARTS_DATA(state, data){
        state.bifenEchartsData = data;
    },
    // 比分网数据按钮显隐
    SET_BIFEN_BTN_STATUS(state, data){
        state.bifenBtnStatus = data;
    },
    // 赔率分组组别 oddGroup
    SET_ODD_GROUP(state, data){
        state.oddGroup = data;
    },
    // 是否是全屏投注
    SET_FULL_SCREEN(state, data){
        state.isFullscreen = data;
    }
};


export default mutations;
