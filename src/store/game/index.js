import mutations from "./mutations.js";
import getters from "./getters.js";
import actions from "./actions.js";

const state = {
    gameList: [], // 所有游戏
    slotGameList: [], // 电子游戏
    matchTimer: {}, // 比赛时间

    sourceMatch:[], // 源数据所有赛事 => 联赛筛选赛事
    allMatchList: [], // 所有赛事(包括今日，早盘，赛后)

    marketList: [], // 所有盘口

    leagueCheckeds: [], // 被选中的联赛ID

    tour:{}, // 游戏对应联赛
    matchTeams:{}, // 冠军盘联赛名称

    stationOpen: false, // 局内串关是否开启
    stationList:{}, // 局内串关列表
    gameID: '', // 当前选择的游戏ID
    matchGameID: 0, // 当前查看盘口的游戏ID
    matchID: 0, // 当前查看盘口的赛事ID

    matchFlag: 3, // 搜索当前游戏的类型 默认滚球
    matchLoading: false, // 赛事列表请求状态
    betCart:[], // 普通注单列表
    cross:[], // 串关注单列表

    dailyTab:"daily", // 赛事切换 默认滚球
    activeBet:"betOrder", // 左侧菜单切换
    earlyDate: -1, // 早盘查询的时间 默认全部不日期
    endDate: 0, // 赛果查询的时间 默认全部不日期
    crossDate: -1, // 串关查询的时间 默认全部不日期

    gameSortCode:{}, // 游戏排序序号

    collect:[], // 收藏列表
    tournamentName:{},
    images: {}, // 全部图片

    isbetMax10: false, // 添加注单超十单

    stationOrderMax:false,//局内串关最多十单提示

    matchNum: {}, // 赛事数量

    page_size: 30,
    
    teamWords: "", // 点击搜索赛事 搜索得关键字
    searchValueTemp: "", // 未点击搜索赛事 搜索得关键字
    
    recommend:[], // 推荐赛事
    asianRecommend:[],  // 亚运会赛事列表
    
    matchs:[], // 盘口详情
    betActiveBet:'betOrder',//购物车tab栏展示

    stationOddMax: 0 , // 串关最大赔率限制
    crossOddMax: 0 , // 串关最大赔率限制
    playVideo: false, // 视频是否播放
    anchorInfo:[], // 主播盘信息
    betActiveType: 'isSingle', // 当前注单类型

    virtualGameShow: true, // 是否显示虚拟足球
    virtualGame: {}, // 虚拟游戏
    virtualLeague:{}, // 虚拟联赛
    virtualMatchList:[], //虚拟赛事列表
    virtualMatch:{}, // 虚拟赛事详情
    serverTime: new Date().getTime(),
    localTime: new Date().getTime(),
    resultType: 'djMatch', // 赛果页面展示的内容  djMatch:普通赛事 VSoccer:虚拟足球

    tyOssData: {}, //体育节庆UI oss域名

    activePlay: 0,  // 当前选择玩法 0 全部 1热门 2特殊 3复合
    isUseSingleQuick: false,   // 单注投注成功后是否使用过任意快捷金额
    isUseCorssQuick: false,    // 串注投注成功后是否使用过任意快捷金额
    isUseStationQuick: false,  // 局内串关投注成功后是否使用过任意快捷金额
    bifenBtnStatus: { // 比分网按钮开关
        analyse_status: 1, //前瞻分析 0-关闭, 1-开启
        real_time_status: 1, //实时数据 0-关闭, 1-开启
        anime_video_status: 1 //动画视频 0-关闭, 1-开启
    },
    bifenMatchData: {},
    bifenEchartsData: {},
    searchActive: false, // 搜索框状态
    matchType: 'groupMatches', // 小组赛，淘汰赛类型
    knockoutType: 0, // 小组赛，淘汰赛类型
    listScrollTop:0, // 虚拟足球赛事列表滚动距离

    oddGroup: {
        group: "g0",
        switch: 0
    }, // 赔率分组组别

    // MQ 优化：赛事，盘口，投注项单独对象 存储，更新，读取；
    mqMatch: {},
    mqMarket: {},
    mqOdds: {},
    isFullscreen: false,
    matchList:[],
};

export default {
    state,
    getters,
    mutations,
    actions
};
