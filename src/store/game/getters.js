import { formatH5Bg } from '@/assets/scripts/utils'
import lodash from "lodash";
import i18n from "@/assets/i18n/index.js"
import { 
    championMatchSort, 
    matchSort,
} from "@/assets/scripts/sort.js";


export default {
    isFullscreen: state => state.isFullscreen,
    sourceMatch: state => state.sourceMatch,
    images: (state) => state.images,
    gameID: state => state.gameID,
    matchGameID: (state) => state.matchGameID,
    matchID:state => state.matchID,
    earlyDate: state => state.earlyDate,
    crossDate: state => state.crossDate,
    endDate: state => state.endDate,
    dailyTab: state => state.dailyTab,
    betCart: (state) => state.betCart.filter(item=>item.bet_type==state.betActiveType),
    cross: state => state.cross,
    activeBet: state => state.activeBet,
    tournamentName: state => state.tournamentName,
    matchTimer: state => state.matchTimer,
    gameSortCode: state => state.gameSortCode,
    page_size: state => state.page_size,
    isbetMax10: state => state.isbetMax10,
    stationOrderMax: (state) => state.stationOrderMax,
    matchNum: state => state.matchNum,
    teamWords: state => state.teamWords,
    recommend: state => state.recommend.filter( ele =>  ele.is_open_match ),
    asianRecommend: state => state.asianRecommend,
    matchLoading: state => state.matchLoading,
    betActiveBet: state => state.betActiveBet,
    stationOddMax: (state) => state.stationOddMax,
    crossOddMax: (state) => state.crossOddMax,
    playVideo: (state) => state.playVideo,
    anchorInfo: (state) => state.anchorInfo,
    matchTeams: (state) => state.matchTeams,
    betActiveType: (state) => state.betActiveType,
    matchFlag: (state) => state.matchFlag,
    tyOssData:(state) => state.tyOssData, // 体育节庆UI oss域名
    activePlay: (state) => state.activePlay,
    bifenBtnStatus: (state) => state.bifenBtnStatus,
    bifenMatchData: (state) => state.bifenMatchData,
    bifenEchartsData: (state) => state.bifenEchartsData,
    gameList: (state) => state.gameList,
    virtualGameShow: state => state.virtualGameShow, // 是否显示虚拟足球
    virtualGame: state => state.virtualGame,
    virtualLeague: state => state.virtualLeague,
    virtualMatchList: state => state.virtualMatchList,
    virtualMatch: state => state.virtualMatch,
    serverTime: state => state.serverTime,
    localTime: state => state.localTime,
    resultType: state => state.resultType,
    isUseSingleQuick: state => state.isUseSingleQuick,  // 单注投注成功后是否使用过任意快捷金额
    isUseCorssQuick: state => state.isUseCorssQuick,    // 串注投注成功后是否使用过任意快捷金额
    isUseStationQuick: state => state.isUseStationQuick,// 局内串关投注成功后是否使用过任意快捷金额
    searchActive: state => state.searchActive, // 首页搜索框打开状态
    matchType: state => state.matchType,
    knockoutType: state => state.knockoutType,
    listScrollTop: state => state.listScrollTop,
    oddGroup: state => state.oddGroup,
    mqMatch: state => state.mqMatch,
    mqMarket: state => state.mqMarket,
    mqOdds: state => state.mqOdds,
    stationOpen: (state) => state.stationOpen,
    matchList: (state) => state.matchList,



    marketList: (state) => {
        let arr = [];
        const mqOdds = state.mqOdds;
        const mqMarket = state.mqMarket;
        state.marketList.forEach( (ele) => {
            let odds = []
            ele.odds.forEach( item => {
                odds.push( mqOdds[item.id] )
            })
            arr.push({
                ...mqMarket[ele.id] ,
                odds: odds
            });
        });

        // 筛选合法盘口: status = 6: 盘口开盘, status = 9:已经结束, status = 7:已关盘, status = 8:待结算, status = 10:赛果驳回;
        return arr.filter( ele => [6, 9, 7, 8, 10].includes(Number(ele.status)) && !ele.odds.every( ele => !ele.visible ) && ele.visible != 0 );
    },

    tour: (state) => {  
        const tour = lodash.cloneDeep(state.tour);
        for( let key in tour){
            tour[key] = tour[key].filter( ele => ele.matchId.length );
            // 排序联赛
            tour[key].sort((a, b) => {
                const aSortCode = a.sort_code == 0 ? 99999999999 : a.sort_code;
                const bSortCode = b.sort_code == 0 ? 99999999999 : b.sort_code;
                return aSortCode - bSortCode || b.matchId.length - a.matchId.length || Number(b.tourId) - Number(a.tourId);
            });
          
        }
        return tour;
    },

    leagueCheckeds: (state) => {
        const tour = lodash.cloneDeep( state.leagueCheckeds);
        tour.sort((a, b) => {
            const aSortCode = a.sort_code == 0 ? 99999999999 : a.sort_code;
            const bSortCode = b.sort_code == 0 ? 99999999999 : b.sort_code;
            return aSortCode - bSortCode || b.matchId.length - a.matchId.length || Number(b.tourId) - Number(a.tourId);
        });
        return tour
    },
    
    slotGameList: (state, getters, rootState) => {
        let _slotGameList = state.slotGameList.filter( ele => Number(ele.is_open_entrance) == 0 && ele.status == 1 ); // 显示游戏 并且 游戏开启状态
        const _userInfo = getters.userInfo
        let isShowVirtualGame = true
        if([2, 3].includes(+_userInfo.tester) || !getters.virtualGameShow){
            isShowVirtualGame = false
        }
        if (!isShowVirtualGame) {
            _slotGameList = _slotGameList.filter(v => v.id !== '1001')
        }
        console.log( _slotGameList )
        return _slotGameList.sort( (a, b) => {
            return Number(a.sort_code) - Number(b.sort_code) || Number(a.id) - Number(b.id)
        });
    },
    
    collects: state => {
        const gameId = state.gameID;
        if( !gameId ){
            return state.collect.filter( ele =>  ele.is_open_match );
        }else{
            return state.collect.filter( ele =>  gameId === ele.game_id && ele.is_open_match )
        }
    },
    
    collectID: state => {
        let arr = [];
        state.collect.forEach( ele=> {
            arr.push( ele.id )
        })
        return arr
    },
    
    // 当前赛事
    matchs: (state) => {
        const id = state.matchs[0].id;
        return state.mqMatch[id] || {};
    },
    
    // 被选中投注项ID
    betItemId: state => {
        let betItemId = [];
        const betCart = state.betCart.filter(item=>item.bet_type==state.betActiveType);
        betCart.forEach(element => {
            betItemId.push(element.item_id)
        });
        return betItemId || []
    },

    // 总投注金额 
    tolBetAcount: state=> {
        if ( state.betActiveType === 'isSingle' ) {
            let normal = state.betCart.reduce((pre, item) => {
                return pre + Number(item.bet_amount)
            }, 0)
            return normal;
        } else {
            let cross = state.cross.reduce((pre, item) => {
                return pre + Number(item.bet_amount) * Number(item.bet_num)
            }, 0)
            return cross;
        }
    },

    // 总返还金额
    tolProfitable: state=>{
        if ( state.betActiveType === 'isSingle' ) {
            let normal = state.betCart.reduce((pre, item) => {
                return pre + Number(item.profitable)
            }, 0)
            return normal;
        } else {
            console.log(state.cross)
            let cross = state.cross.reduce((pre, item) => {
                return pre + Number(item.profitable)
            }, 0)
            return cross;
        }
    },

    // 联赛筛选的赛事
    allMatchList: state => {
        const mqMatch = state.mqMatch
        let newarr = lodash.cloneDeep(state.allMatchList);
        const normalMatch = newarr.filter( match => match.category != 2);  // 正常赛事
        const championMatch = newarr.filter( match => match.category == 2); // 冠军盘赛事
        return [
            ...matchSort( normalMatch,  state.gameSortCode, mqMatch),
            ...championMatchSort( championMatch,  state.gameSortCode, mqMatch),
        ];
    },

    stationID: (state) => {
        let arr = [];
        for( let key in state.stationList ){
            arr = [
                ...arr,
                ...state.stationList[key].list
            ]
        };
        return arr.map( ele => ele.item_id );
    },

    stationList: (state) => {
        let stationList = lodash.cloneDeep(state.stationList);
        for( let key in stationList ){
            let arr = []; // 不能形成串关类型
            const { list, bet_amount, odd_rate } = stationList[key];
            /**
             * 类型定义
             *  1: 单局串关注单数量需≥2。判断条件list的长度 = 1;
             *  2: 同局中同名盘口，不能进行局内串关。判断条件option_type相同
             *  3: 同盘口的投注项之间，暂不支持局内串关。判断条件market_id相同
             *  4: 本局注单总赔率已达到上限，不能添加更多选项了！
            */
            if( list.length <= 1 ){
                arr.push(1)
            }else{
                list.forEach( ele => {

                    ele['type_not_cross'] = list.filter( 
                        item => item.odd_type_id == ele.odd_type_id  && 
                        item.market_id != ele.market_id).length >= 1;

                    ele['market_not_cross'] = list.filter( 
                        item => item.market_id == ele.market_id ).length >= 2;

                    if( ele['type_not_cross'] ){
                        arr = [
                            ...arr,
                            2,
                        ]
                    }
                    if( ele['market_not_cross'] ){
                        arr = [
                            ...arr,
                            3,
                        ]
                    }
                });
            }
           
            // 计算第几局总赔率

            const newList = list.filter( ele => 
                !ele.market_not_cross && 
                !ele.type_not_cross &&
                ele.match_visible == 1 &&  // 赛事显示
                ele.market_visible == 1 && // 盘口显示
                ele.match_status == 5 && // 赛事开盘
                ele.market_status == 6 && // 盘口开盘
                ele.market_suspended == 0 && // 盘口不暂停
                ele.match_suspended == 0 && // 赛事不暂停
                ele.suspended == 0 && // 投注项暂停
                ele.visible == 1 && // 投注项显示
                ele.is_pass_off == 2 &&// 可以串关
                ele.market_pass_off == 2 && // 盘口允许串关
                Number(ele.odds) >= 1.001  // 赔率大于1.001 
            );
                
            // 赔率计算
            let odds = 0;
            let maxOdds = false;
            let relOdds = 0;
            if(newList.length >= 2){
                odds = newList.reduce((pre, item) => {
                    return pre * ((Number(item.relodds)-1)*Number(odd_rate/100) + 1)
                }, 1);
            }else{
                odds = 0;
            }
            // odds = (odds - 1) * (Number(odd_rate/100)) + 1; // 总赔率 * 返还率 = 真实赔率

            relOdds = odds;
            // 限制赔率
            if( odds >= state.stationOddMax ){
                arr = [ ...arr, 4 ]
                odds = state.stationOddMax;
                maxOdds = true;
            }

            // 限额计算 Math.min.apply(Math,this.list.map(item => { return item.id }))
            // complex_max
            let complex_max = Math.min.apply( Math, list.map(item => Number(item.complex_max)  ))
            const order_max = Number(stationList[key].rate_max_prize) / (odds - 1); // 单局串关赔付 /（总赔率-1）
            complex_max = complex_max > order_max ? order_max : complex_max;
            
            stationList[key].odds = odds;  // 总赔率
            stationList[key]['canCross'] = newList.length >= 2; // 可以形成串注
            stationList[key]['complex_max'] = parseInt(complex_max); // 最大投注额
            stationList[key]['cross_name'] = newList.length; // 几串几

            

            // const win_balance = Number(stationList[key].win_balance);
            // const relProfitable =  odds * Number(bet_amount);
            // 计算第几局总返还
            // stationList[key].relProfitable = relProfitable; // 预计返还
            // stationList[key].profitable = win_balance +  Number(bet_amount) < relProfitable ? win_balance +  Number(bet_amount) :relProfitable; // 实际返还             
            // stationList[key].profitable = odds * Number(bet_amount); // 计算第几局总返还
            stationList[key]['canStation'] = arr;
            stationList[key]['maxOdds'] = maxOdds; 
            stationList[key]['relOdds'] = relOdds; 

        };
        // console.log( stationList )
        return stationList
    },
    
};
