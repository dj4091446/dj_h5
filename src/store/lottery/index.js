import mutations from './mutations.js'
import getters from './getters.js'
import actions from './actions.js'

// prettier-ignore
const userInfo = {
    userName       : '',
    balance        : 0.00,
    playHistory      : {},     // 彩种对应 玩法记录
    doublePlayHistory: {},     // 双面盘 玩法记录
    lotteryMoneyMode : JSON.parse(localStorage.getItem('user.moneyMode') || '{}'),   // 元角分模式
    historyTags      : {},     // 根据彩种玩法记住标记
    playDesc         : [],     // 当前彩种玩法描述
}

// prettier-ignore
const betState = {
    moneyMode      : null,   // 元角分模式
    doubleSeries          : [],                                                // 双面盘目录下的玩法
    doubleDefaultAmount   : '',   // 双面盘默认金额
    doubleDefaultAmountEdited   : false,   // 是否编辑过双面盘快捷金额
    doubleAmountMode: '',
    clearDoubleData       : true,                                              // 双面盘清空数据
    doubleBetData      : {             // 双面盘数据
        list       : [],
        odds       : null,
        totalAmount: 0,
        totalBets  : 0,
    },
    currentOpenNumHistory: null,   // 当前历史开奖记录
    currentLastOpenIssue : null,   // 当前彩种上一期开奖期号，号码
    lotteryCounterMap    : {},     // 彩种倒计时map
    cancelSelectedBall   : null,   // 取消 一个 号码球
    quickMoney          : [],                   // 用户设置的快捷金额
}
const state = {
    isshowopen: true,
    lastNumberArray:[],
    lolMusic: true,         // 背景音乐
    lolBetConfirm: true,    // 投注确认弹窗
    amount: "", // 金额
    imgIndex: 1,
    currentOpenNumHistory: null, 
    ticketTypeList: [],         // 彩种类型列表 包括所有的彩种信息
    ticketInfo    : {},           // 彩种基础信息
    ticketId      : 268,         // 彩种id
    handicap      : 'double',     // 盘口玩法-标准盘
    currentStatus: null,          // 当前彩种状态
    maxMultiple  : 99999,        // 最大倍数
    moneyListMap : [],
    defaultQuickPlayList: [],   // 默认玩法
    playList: null,// 标准盘玩法列表
    play: null,   // 当前玩法
    playMap:{},
    betEntrance:'',
    ...betState,
    openMmcDraw: true,      // 是否启动开奖动画
    userInfo,
    dhistoryBetData:{},
    syncServerTimeGap: null,    // 与服务器时间的间隔
    recentPlayList   : [],      // 最近玩过
    showOrderTop     : false,   // 是否近期注单弹框
    pushData      : {},   // 推送的数据信息
    merchantAccout: '',
    lastPlayId: '',

    currencyChar: '',
    cancelOptions: '1',              // 1开启，2关闭
    lastBetData: null,
    configUrl:{},
    openBetReport: false,
}

export default {
    state,
    getters,
    mutations,
    actions,
}
