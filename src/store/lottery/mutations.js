import Vue from 'vue'

// 彩种
const LotteryMutation = {
    setTicketId(state, data) {
        state.ticketId = Number(data)
    },

    setTicketInfo(state, data) {
        const { ticketInfo } = data
        state.ticketInfo = {
            ...ticketInfo,
            ...data,
        }
    },
    setCurrentStatus(state, data) {
        state.currentStatus = data
    },
    setBetEntrance(state, data) {
        state.betEntrance = data
    },
    setCurrentOpenNumHistory(state, data) {
        state.currentOpenNumHistory = data
    },
    setLotteryCounterMap(state, data) {
        Vue.set(state.lotteryCounterMap, "268", data)
    },

    setCurrentLastOpenIssue(state, data) {
        state.currentLastOpenIssue = data
    },
    setAmount(state, data) {
        state.amount = data
    },
    setlastBetData(state,data){
        state.lastBetData = data;
    },
    setConfigUrl(state, data){
        state.configUrl = data;
    },
    SET_ISSHOWOPEN(state,data){
        state.isshowopen = data
    },
}


// 投注数据
const betMutation = {
    setDoubleAmountMode(state, data) {
        state.doubleAmountMode = data
    },
    setClearDoubleData(state, data) {
        state.clearDoubleData = data
    },
    setDoubleBetData(state, data) {
        state.doubleBetData = {
            ...data,
        }
    },
    setCancelSelectedBall(state, data) {
        state.cancelSelectedBall = data
    },
}
const mutations = {
    ...LotteryMutation,

    ...betMutation,
    setPushData(state, data) {
        state.pushData = data
    },
    setOpenBetReport(state, data){
        state.openBetReport = data;
    },
}

export default mutations
