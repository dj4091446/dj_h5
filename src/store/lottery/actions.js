import * as LotteryApi from '@/api/teselol'
import { Dialog } from 'vant'
import axios from "axios";
import i18n from '@/assets/i18n';
const lang = i18n.t('yxzh');

Dialog.setDefaultOptions({
    confirmButtonText: lang.queren
})

/**
 * 同步定时器
 * @param {*} counterObj 
 * @param {*} serverTime 
 */
function syncTimer(counterObj, timeGap) {
    // console.info(`CurrentIssue 倒计时时间调整计时器 菜种id=${counterObj.ticketId} 服务器时间差 ${timeGap} 倒计时结束时间 ${counterObj.endTime}`)
    const serverTime = new Date().getTime() + timeGap    
    counterObj.remain = Math.floor((counterObj.endTime - serverTime) / 1000 - (counterObj.defaultOpen && counterObj.advanceStopBetTime))
    counterObj.total = Math.floor((counterObj.endTime - counterObj.startTime) / 1000 - (counterObj.defaultOpen && counterObj.advanceStopBetTime));

    if (counterObj.remain <= 0) {
        // console.error(`CurrentIssue 倒计时时间错误 小于0 菜种id=${counterObj.ticketId} 服务器时间 ${serverTime} 倒计时结束时间 ${counterObj.endTime}`)
        counterObj.remain = 0
    }

    // 避免计时器误差太大，超过最大值，老架构线上出现过，以防万一
    if (counterObj.remain > counterObj.total) {
        // console.error(`CurrentIssue 定时器超过total总时长 修正中。。。 remain=${counterObj.remain} total=${counterObj.total}`)
        counterObj.remain = counterObj.total;
    }

    return counterObj
}

export default {
    async getCurrentOpenNumHistory({ commit, state, dispatch }) {
        const res = await LotteryApi.getRecentlylottery({
            ticketId: state.ticketId,
            num: 100,
        })
        if (res.data && res.data.data) {
            commit('setCurrentOpenNumHistory', res.data.data)
            commit('setCurrentLastOpenIssue', res.data.data[0])
        }
    },
    loadServerTime({commit,state,getters}){
        if(!window.serverTimerGap){
            return axios.get(`${state.configUrl.service_date_url}/date?_=${new Date().getTime()}`).then(res => {
                var curTime = new Date(res.headers.date).getTime()
                window.serverTimerGap = curTime - new Date().getTime()
                console.info(`CurrentIssue 从服务器拿到的时间差为 ${window.serverTimerGap}`)
                return window.serverTimerGap
            })
        }
        return window.serverTimerGap;
    },
    // 当前彩种
    async fetchCurrentIssue({ commit, state, dispatch }, param) {
        return new Promise((resolve, reject) => {
            async function getCurrentIssue(param) {
                LotteryApi.currentSaleIssue(param).then(async res => {
                    const { code } = res.data
                    if (code === 0) {
                        let data = res.data?.data?.[0];
                        const timeGap =  await dispatch("loadServerTime");
                        data = syncTimer(data, timeGap)
                        if (state.ticketId === data.ticketId) {
                            // 获取当前彩种奖期信息
                            console.debug('[Store] 设置当前奖期信息', param, data)
                            if (!data) {
                                reject('奖期数据异常')
                            }
                            commit('setCurrentStatus', data)
                        }
                        commit('setLotteryCounterMap', data)
                        resolve(data)
                        return
                    }

                    // resolve(null)
                    reject('当前奖期数据获取异常')
                }).catch(err => {
                    reject('您的网络异常，请检查看看')
                })
            }

            getCurrentIssue(param)
        })
    },
}
