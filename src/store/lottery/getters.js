import { judgeJKC } from '@/views/lottery/util/lottery'

export default {
    lastIssue: state => {
        const { currentLastOpenIssue } = state
        if (currentLastOpenIssue) {
            return currentLastOpenIssue.issue
        }

        return '-'
    },
    currentIssue: state => {
        const { currentStatus } = state
        if (currentStatus) {
            return currentStatus.planId
        }
        return '-'
    },
    ticketName: state => {
        return state.ticketInfo && state.ticketInfo.ticketName
    },
    ticketType: state => {
        return state.ticketInfo && state.ticketInfo.ticketType
    },
    moneyModeList: state => {
        if (state.ticketInfo && state.ticketInfo.moneyModeList) {
            return state.ticketInfo.moneyModeList.map(ele => {
                return {
                    text: ele.name,
                    value: ele.value,
                    key: ele.key,
                }
            })
        }
    },
    lastNumber: state => {
        const { currentLastOpenIssue } = state
        if (currentLastOpenIssue) {
            console.debug(`[Store] getter lastNumber --> ${currentLastOpenIssue.code}`)
            console.debug(`[Store] getter lastNumber --> ${currentLastOpenIssue.issue}`)
            return currentLastOpenIssue.code || ''
        }
        console.debug('[Store] getter lastNumber --> 为空')

        return ''
    },
    currentSale: state => {
        const { currentStatus } = state
        if (!currentStatus || currentStatus.sale === undefined) {
            return null // loading状态
        } else if (currentStatus && currentStatus.sale === false) {
            return false  //停售状态
        }

        // 默认为开奖状态
        return true
    },
    // 当前彩种的开奖间隔 返回秒
    currentIssueGap: state => {
        const { currentStatus } = state
        if (currentStatus) {
            const { endTime, startTime } = currentStatus
            const t = endTime - startTime
            return t / 1000
        }

        return 0
    },

    lastNumberArray: (_, getters) => getters.lastNumber && getters.lastNumber.split(' ') || [],

    group: state => {
        const { playList, play } = state
        if (play && playList) {
            for (let i = 0, len = playList.length; i < len; i++) {
                if (playList[i].playCode === play.groupCode) {
                    return playList[i]
                }
            }
        }
        return null
    },
    series: (state, getters) => {
        const { play } = state
        const { group } = getters
        if (play && group) {
            const groupList = group.list
            for (let i = 0, len = groupList.length; i < len; i++) {
                if (groupList[i].playCode === play.seriesCode) {
                    return groupList[i]
                }
            }
        }
        return null
    },
    groupCode: state => {
        return (state.play && state.play.groupCode) || null
    },
    seriesCode: state => {
        return (state.play && state.play.seriesCode) || null
    },
    playCode: state => (state.play && state.play.playCode) || null,

    balance: (state, getters, rootState) => {
        return rootState.user.userInfo.balance;
    },
    defaultQuickMoney: (state, getters, rootState) => {
        const {doubleLimitStatus, doubleBetRangeList} = rootState.setting
        let doublePlayBetMin = 10
        let doublePlayBetMax = 9999999999
        if(doubleLimitStatus) {
            const range = doubleBetRangeList.filter(v => v.current)[0]
            doublePlayBetMin = range.doublePlayBetMin
            doublePlayBetMax = range.doublePlayBetMax
        }
        return [1,5,10,50].map(v => {
            let t = doublePlayBetMin * v
            if(t > doublePlayBetMax) {
                t = doublePlayBetMax
            }
            return t
        })
    },
    isJKC: state => judgeJKC(state.ticketId),
    currentJKCState: state => state.mmcState[state.ticketId],
    lastBetData: state => state.lastBetData,
    lastPlayId: state => state.lastPlayId,
}
