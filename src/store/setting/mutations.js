export default {
    setShowNumberKeyboard(state, data) {
        state.showNumberKeyboard = data
    },

    setNumberKeyboardOrder(state, data) {
        state.numberKeyboardOrder = data
    },

    setNumberKeyboardValue(state, data) {
        state.numberKeyboardValue = data
    },
    setFeatureBetRangeList(state, data) {
        state.featureBetRangeList = data
    },
    setFeatureLimitStatus(state, data) {
        state.featureLimitStatus = data
    },
    setFeatureDelayed(state, data) {
        state.featureDelayed = data
    },
    setFeatureDelayedTime(state, data) {
        state.featureDelayedTime = data
    },
    setToken(state, data) {
        state.token = data;
    },
    setCountDownBeep(state, data) {
        state.countDownBeep = data;
    },
    setWinModal(state, data) {
        state.winModal = data;
    },
    setDoubleBetConfirm(state, data) {
        state.doubleBetConfirm = data;
    },
    setCpUserInfo(state,data){
        state.cpUserInfo = data;
    },
    setImgIndex(state, data) {
        state.imgIndex = data
    }
}
