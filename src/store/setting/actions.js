import * as LotteryApi from '@/api/teselol'

export default {
    restoreDoubleDefaultAmount({state, getters, rootState}) {
        state.quickMoney = getters.defaultQuickMoney
        state.doubleDefaultAmount = state.quickMoney[0]
    },
    fetchUserConfig({ commit, rootState, dispatch }) {
        return LotteryApi.getUserConfig().then(res => {
            if (!(res.data && res.data.data)) {
                // 获取接口数据失败, 暗示测试这是后台问题, 暗爽
                console.error('设置接口数据获取失败, 请联系后台')

                // 避免取值报错
                res.data = { data: {} }
            }

            // prettier-ignore
            const {
                featureBetRangeList,//特色彩限红
                featureLimitStatus,//是否开启特色彩限红
                featureDelayed,
                featureDelayedTime,
                winModal,
                // 这两个手机端无法实现
                winBeep,                  // 中奖提示音
                countDownBeep,            // 倒计时提示音
                doubleBetConfirm,
            } = res.data.data
            commit('setFeatureBetRangeList', featureBetRangeList)       
            commit('setFeatureLimitStatus', featureLimitStatus)   
            commit('setFeatureDelayed', featureDelayed)  
            commit('setFeatureDelayedTime', featureDelayedTime)  
            commit('setCountDownBeep', countDownBeep)
            commit('setWinModal',winModal)
            commit('setDoubleBetConfirm', doubleBetConfirm)
            if(!rootState.lottery.doubleDefaultAmountEdited && rootState.lottery.quickMoney.length === 0) {
                dispatch('restoreDoubleDefaultAmount')
            }
            if (!localStorage.getItem("setAmount") || ["undefined","null"," "].includes(localStorage.getItem("setAmount"))){
                commit('setAmount',featureBetRangeList.filter(v => v.current)[0]?.doublePlayBetMin)
            }else{
                commit('setAmount',localStorage.getItem("setAmount"))
            }
        })
    },
}
