import getters from './getters.js'
import mutations from './mutations.js'
import actions from './actions'

const defaultLang = localStorage.getItem('language') || 'cn';
// prettier-ignore
const state = {
    featureBetRangeList:null,//特色彩限红列表
    featureLimitStatus:false,//是否开启特色彩限红状态
    featureDelayed:false,//特色彩限红是否锁定
    featureDelayedTime:null,//特市场限红锁定时间
    showNumberKeyboard : false,   // 全局数字键盘
    numberKeyboardOrder: 0,       // 默认为 0 倍数 1 单式输入框
    numberKeyboardValue: '',      // 全局数字键盘 输入的值
    // 默认语言类型
    lang: defaultLang,
    token: localStorage.token,
    winModal: false,
    countDownBeep:false,
    doubleBetConfirm:false,
    cpUserInfo:{},
    imgIndex: 0,
}

export default {
    state,
    getters,
    mutations,
    actions,
}
