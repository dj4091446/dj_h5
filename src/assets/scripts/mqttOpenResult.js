import '@/assets/scripts/mqtt.min.js' 
import "../../store"
import { checkIsJSON } from '@/assets/scripts/utils.js'
class WS {
    constructor(vm) {
        this.vm = vm;
    }
    initWebSocket(){ 
        let me = this;
        let wsuri;
        var options = {
            keepalive : 60,
            reconnect : true,   // 断线自动重新连接
            reconnectPeriod : 1000, 
            cleanSession: true, // 不接受离线消息 
            clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8)
        }

        wsuri = me.vm.$store.state.lottery.configUrl.mqtt_url;

        if (!wsuri) {
            throw Error('mqtt 初始化 wsuri 地址为空')
        }

        // 连接mqtt
        var client = mqtt.connect(wsuri, options); 
        // 订阅频道  商户名称 + 用户名
        const { merchantAccout, memberId } = me.vm.$store.state.setting.cpUserInfo;
        client.subscribe(`BORACAY_UNICAST_MEMBER_TOPIC|${merchantAccout}|${memberId}`);
        // 广播 开奖消息推送
        client.subscribe(`SABANG_BROADCAST_MEMBER_TOPIC`);

        // 接受消息
        client.on("message", function (topic, payload) {
            // 解析消息
            let d = payload.toString()
            if(!checkIsJSON(d)){
                console.error("收到的消息不是json格式:", d)
                return
            }
            // 解析消息
            const data = JSON.parse(d);
            // console.log(data);
            let eventData;
            try {
                eventData = typeof data.data === 'string' ? JSON.parse(data.data) : data.data
            } catch(e) {
                eventData = data.data
            }
            if (data.msgType == 1) {
                // console.info( `[mqtt] 频道: ${topic} 收到消息：`, data )
                me.vm.$root.$store.commit('setPushData', {
                    type:  'EVENT_RECENT_BET',
                    data: eventData,
                })
            }

            // 余额推送
            // if( data.msgType == 5 ){
            //     let msgData = JSON.parse(data.data);
            //     console.log("余额推送", msgData)
            //     if(msgData && (memberAccount == msgData.memberAccount) && (merchantAccout == msgData.merchantAccount)){
            //         me.changlongBalance( msgData.balance || 0 )
                  
            //         me.vm.$root.$emit('fetch-user-balance', msgData.balance || 0)
            //     }
            //     else{
            //         me.vm.$store.dispatch('fetchUserInfo')
            //     }               
            // }

            /**
             * 开奖消息推送 
             * ticketId 彩种ID
             * ticketName 彩种名称
             * ticketPlanNo  期号.
             * openCode  开奖号码
             * messageType: 1:自动开奖消息  2:手动开奖 3、风控解锁消息.
             * openTime 实际开奖时间
             */
            if (data.msgType == 6) {
                // 发送开奖消息
                // console.info(`开奖事件 ==> ticketId=${eventData.ticketId} `, eventData)
                me.vm.$root.$store.commit('setPushData', {
                    type:  'EVENT_OPEN_PRIZE',
                    data: eventData,
                })
            }

            // 中奖推送
            // {
            //     "memberAcount": "aabbcc",
            //     "merchantAccount": "商户账号" string,
            //     "msgType": 消息类型 ，中奖消息是 7,
            //     "data": {
            //         "merchantAccount": 商户账号 string,
            //         "memberAccount": 会员账号 string,
            //         "ticketId":  彩种ID,
            //         "ticketName": "彩种名称" string,
            //         "ticketPlanNo": "奖期" string,
            //         "openNum": "开奖号码", String 
            //         "winBetNum": 中奖注数, int
            //         "WinAmount": 中奖金额, double
            //         "orderNum": 注单记录数量，int
            //     },
            // }
            if (data.msgType == 7) {
                console.info(`中奖事件 ==> ticketId=${eventData.ticketId} `, eventData)
                me.vm.$root.$store.commit('setPushData', {
                    type:  'EVENT_PRIZE',
                    data: eventData,
                })
            }
        })

        client.on("reconnect",  function () {
            // me.vm.$root.$store.commit('setPushData', {
            //     type:  'EVENT_RECONNECT',
            //     data: null,
            // })
        })
    }
}

export default WS;
