const derections = {
  // 点击除了指定区域外执行对应函数操作指令
  clickOutside: {
    bind(el, binding) {
      function clickHandler(e) {
        // 这里判断点击的元素是否是本身，是本身，则返回
        if (el.contains(e.target)) {
          return false;
        }
        // 判断指令中是否绑定了函数
        if (binding.expression) {
          // 如果绑定了函数 则调用那个函数，此处binding.value就是handleClose方法
          binding.value(e);
        }
        return true;
      }
      // 给当前元素绑定个私有变量，方便在unbind中可以解除事件监听
      el.vueClickOutside = clickHandler;
      document.addEventListener("click", clickHandler);
    },
    unbind(el) {
      // 解除事件监听
      document.removeEventListener("click", el.vueClickOutside);
      delete el.vueClickOutside;
    },
  },
  drag: {
    inserted(el) {
      const dragBox = el; //获取当前元素
      const action = (start, move, end) => {
        dragBox[start] = event => {
          let moveCounter = 0; //鼠标移动事件触发次数
          event.preventDefault(); //禁止页面滚动
          document[move] = e => {
            if (moveCounter > 4) {
              el.setAttribute('data-click', 0);
              //用鼠标的位置减去鼠标相对元素的位置，得到元素的位置
              let _el = null;
              if (start === 'onmousedown') {
                _el = e;
              } else {
                _el = e.changedTouches[0];
              }
              let left = _el.clientX - dragBox.offsetWidth / 2;
              let top = _el.clientY - dragBox.offsetHeight / 2;
              if (left < 0) {
                left = 0;
              }
              if (top < 0) {
                top = 0;
              }
              if (
                left >
                document.documentElement.clientWidth -
                  dragBox.offsetWidth
              ) {
                left =
                  document.documentElement.clientWidth -
                  dragBox.offsetWidth;
              }
              if (
                top >
                document.documentElement.clientHeight -
                  dragBox.offsetHeight
              ) {
                top =
                  document.documentElement.clientHeight -
                  dragBox.offsetHeight;
              }

              dragBox.style.left = left + 'px';
              dragBox.style.top = top + 'px';
            } else {
              moveCounter++;
            }
          };
          document[end] = e => {
            //鼠标弹起来的时候不再移动
            document[move] = null;
            //预防鼠标弹起来后还会循环（即预防鼠标放上去的时候还会移动）
            document[end] = null;
            if (moveCounter <=4) {
              el.setAttribute('data-click', 1)
              // dragBox.click();
              e.target.click();
            }
          };
        };
      };
      action('ontouchstart', 'ontouchmove', 'ontouchend');
      // action('onmousedown', 'onmousemove', 'onmouseup');
    },
    update(el) {
      const target = el.children[0];
      target.style.left = '';
      target.style.top = '';
    },
    unbind(el) {
    }
  }
};
export default derections;
