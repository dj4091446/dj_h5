import qs from "qs";
import lodash from "lodash";
import i18n from "@/assets/i18n/index.js"

// 检测字符串是否是json字符串
export function checkIsJSON(str) {
    if (typeof str == 'string') {
      try {
        var obj = JSON.parse(str);
        if (typeof obj == 'object' && obj) {
          return true;
        } else {
          return false;
        }
      } catch (e) {
        return false;
      }
    }
    return false;
}

export function timetrans(date) {
    var date = new Date(date * 1000); //如果date为13位不需要乘1000
    var Y = date.getFullYear() + "-";
    var M =
        (date.getMonth() + 1 < 10
            ? "0" + (date.getMonth() + 1)
            : date.getMonth() + 1) + "-";
    var D = (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + " ";
    var h =
        (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":";
    var m =
        date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();

    var s =
        date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

    return {
        full: Y + M + D + h + m + ":" + s,
        y: Y + M + D,
        m: M + D,
        d: h + m,
        s: s
    };
}

// 获取几天前
export function getNDate(n) {
    var now = new Date();
    var date = new Date(now.getTime() - n * 24 * 3600 * 1000);
    var year = date.getFullYear();
    var month = date.getMonth() + 1 > 9 ? date.getMonth() + 1 : "0" + (date.getMonth() + 1);
    var day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
    var date = year + "-" + month + "-" + day;
    return date;
}

// 获取周几
export function getxingqi(n) {
    var now = new Date();
    var date = new Date(now.getTime() - n * 24 * 3600 * 1000);
    var myxingqi = date.getDay();
    var b;
    switch (myxingqi) {
        case 0: b = i18n.t('homePage.chooseDate.zhouri'); break;
        case 1: b = i18n.t('homePage.chooseDate.zhouyi'); break;
        case 2: b = i18n.t('homePage.chooseDate.zhouer'); break;
        case 3: b = i18n.t('homePage.chooseDate.zhousan'); break;
        case 4: b = i18n.t('homePage.chooseDate.zhousi'); break;
        case 5: b = i18n.t('homePage.chooseDate.zhouwu'); break;
        case 6: b = i18n.t('homePage.chooseDate.zhouliu'); break;
        default: b = "/";
    }
    return b
}


// 数字再数组区间第几个
export function getTimeIndex(timeArr, time) {
    let timeIndex = 0;
    for (let i = 0; i < timeArr.length; i++) {
        const scrollHeight = timeArr[i];
        if (scrollHeight > time) {
            timeIndex = i;
            break;
        }
    }
    return timeIndex;
}

// 数字转化大写中文
export function digitalToChinese(digital = 1) {
   let t = i18n.t('homePage.digitalToChinese')
    return {
        1: t.number1,
        2: t.number2,
        3: t.number3,
        4: t.number4,
        5: t.number5,
        6: t.number6,
        7: t.number7,
        8: t.number8,
        9: t.number9,
        10: t.number10,
        11: t.number11,
    }[digital] || t.number1
}

// 获取上一个月时间,返回yyyy-MM-dd字符串,区分28,29,30,31
export function getLastMonthTime(date) {
    //  1    2    3    4    5    6    7    8    9   10    11   12月
    let daysInMonth = [0,31,28,31,30,31,30,31,31,30,31,30,31];
    let strYear = date.getFullYear();
    let strDay = date.getDate();
    let strMonth = date.getMonth() +1 ;
    //一、解决闰年平年的二月份天数   //平年28天、闰年29天//能被4整除且不能被100整除的为闰年,或能被100整除且能被400整除
    if (((strYear % 4) === 0) && ((strYear % 100)!==0) || ((strYear % 400)===0)){
        daysInMonth[2] = 29;
    }
    if(strMonth - 1 === 0) //二、解决跨年问题
    {
        strYear -= 1;
        strMonth = 12;
    }
    else
    {
        strMonth -= 1;
    }
    //  strDay = daysInMonth[strMonth] >= strDay ? strDay : daysInMonth[strMonth];
    strDay = Math.min(strDay,daysInMonth[strMonth]);//三、前一个月日期不一定和今天同一号，例如3.31的前一个月日期是2.28；9.30前一个月日期是8.30
 
    if(strMonth<10)//给个位数的月、日补零
    {
        strMonth="0"+strMonth;
    }
    if(strDay<10)
    {
        strDay="0"+strDay;
    }
    let datastr = strYear+"-"+strMonth+"-"+strDay;
    return datastr;
}

// 获取游戏已经进行的时间
export function gameAlreadyStart(date) {
    var date = new Date(date).getTime(); //如果date为13位不需要乘1000
    var now = new Date().getTime();
    return Math.abs(now - date) / 1000;
}
//公告格式转换 
export function ubb2html(str) {

    str = str.replace(/</ig, '&lt;');
    str = str.replace(/>/ig, '&gt;');
    str = str.replace(/\n/ig, '<br />');
    str = str.replace(/\[code\](.+?)\[\/code\]/ig, function ($1, $2) { return phpcode($2); });

    str = str.replace(/\[hr\]/ig, '<hr />');
    str = str.replace(/\[\/(size|color|font|backcolor)\]/ig, '</font>');
    str = str.replace(/\[(sub|sup|u|i|strike|b|blockquote|li)\]/ig, '<$1>');
    str = str.replace(/\[\/(sub|sup|u|i|strike|b|blockquote|li)\]/ig, '</$1>');
    str = str.replace(/\[\/align\]/ig, '</p>');
    str = str.replace(/\[(\/)?h([1-6])\]/ig, '<$1h$2>');


    str = str.replace(/\[(left|center|right)\]/ig, '<p style="text-align:$1">');
    str = str.replace(/\[\/(left|center|right)\]/ig, '</p>');
    str = str.replace(/\[align=(left|center|right|justify)\]/ig, '<p align="$1">');

    str = str.replace(/\[size=(\d+?)\]/ig, '<font size="$1">');
    str = str.replace(/\[color=([^\[\<]+?)\]/ig, '<font color="$1">');
    str = str.replace(/\[backcolor=([^\[\<]+?)\]/ig, '<font style="background-color:$1">');
    str = str.replace(/\[font=([^\[\<]+?)\]/ig, '<font face="$1">');
    str = str.replace(/\[list=(a|A|1)\](.+?)\[\/list\]/ig, '<ol type="$1">$2</ol>');
    str = str.replace(/\[(\/)?list\]/ig, '<$1ul>');

    str = str.replace(/\[s:(\d+)\]/ig, function ($1, $2) { return smilepath($2); });
    str = str.replace(/\[img\]([^\[]*)\[\/img\]/ig, '<img src="$1" border="0" />');
    str = str.replace(/\[url=([^\]]+)\]([^\[]+)\[\/url\]/ig, '<a href="$1">' + '$2' + '</a>');
    str = str.replace(/\[url\]([^\[]+)\[\/url\]/ig, '<a href="$1">' + '$1' + '</a>');
    return str;
}
const cubic = value => Math.pow(value, 3);
export function easeInOutCubic(value) {
    return value < 0.5
        ? cubic(value * 2) / 2
        : 1 - cubic((1 - value) * 2) / 2;
}
/**
 * @description 回到顶部
 * @param elClass String 滚动盒子类名
 * @param isVisibility bool 是否需要过渡效果，默认没有过渡效果
 */
export function scrollTop(elClass, isVisibility = false) {
    if (!elClass) return
    const el = document.querySelector(elClass);
    if (!isVisibility) {
        el.scrollTop = 0;
        return;
    }
    // 平滑滚动
    const beginTime = Date.now();
    const beginValue = el.scrollTop;
    const rAF =
        window.requestAnimationFrame || ((func) => setTimeout(func, 16));
    const frameFunc = () => {
        const progress = (Date.now() - beginTime) / 500;
        if (progress < 1) {
            el.scrollTop = beginValue * (1 - easeInOutCubic(progress));
            rAF(frameFunc);
        } else {
            el.scrollTop = 0;
        }
    };
    rAF(frameFunc);
}

/**
 * 为赛事列表内的赛事添加背景图，相邻不重复
 * @param {*} array 赛事列表
 * @param {*} gameImg 后台返回的背景图map
 */
export function formatH5Bg(arr, gameImg) {
    let gameMap = {};
    let array = lodash.cloneDeep(arr); // vuex 对象严格模式，不能直接修改对象vuex对象，否则抛出警告代码
    for(let i=0; i<array.length; i++){
        let item = array[i];
        if(gameImg[item.game_id] === undefined){
            console.error(`gameid：${item.game_id} 此游戏没有背景图`)
            continue;
        }
        let gameImgs = gameImg[item.game_id].h5_figure.split(',');

        //确保相同游戏的相邻赛事卡片背景图不同
        let index = gameMap[item.game_id] || 0;

        item.h5_bg = gameImgs[index]; // 赛事列表卡片的背景图
        index = (index+1)%gameImgs.length;
        
        gameMap[item.game_id] = index;
    }
    return array
}
// 参数处理json2getparam
export function getParams(data) {
    if (!(typeof data == "object")) {
        return data;
    }
    for (var key in data) {
        if (typeof data[key] != "number" && data[key] == "") {
            delete data[key];
        }
    }
    return qs.stringify(data);
}

/**
 * [groupingData 根据共同字段将数据分组]
 * @param {[type]} arr [数据源]
 * @param {[type]} field [字段名]
 */
export function groupingData(data,filed) {
    let map = {};
    let dest = [];
    data.forEach(item => {
        if(!map[item[filed]]) {
            dest.push({
                [filed]: item[filed],
                list: [item]
            });
            map[item[filed]] = item;
        } else {
            dest.forEach(dItem => {
                if (dItem[filed] == item[filed]) {
                    dItem.list.push(item);
                }
            });
        }
    })
    return dest;
}

// 是否显示中奖特效
// 单笔中奖金额大于等于5000元，每日仅弹出一次
export function canShowAwardAnimate(price, userid) {
    let lastTimeShowAnimate = window.localStorage.getItem(`AWARD_ANIMATE_LAST_TIME_${userid}`) || 0;
    return Number(price) >= 5000 && new Date(Number(lastTimeShowAnimate)).toDateString() !== new Date().toDateString()
}


// 让分大小盘口替换
export function oddStrTrans(oddsName){
    let str = oddsName;
    const symbolArr = lodash.filter(oddsName, function(v) {
        return /[\+\-\>\<]/i.test(v);
    });
    // 避免出现 SKT-1 + 0.75 这样数据
    const symbolStr = symbolArr.length == 0 ? false : symbolArr[symbolArr.length - 1];
    // 避免出现 SKT+1 + 0.75 这样数据
    const nameArr = oddsName.split(symbolStr);
    const odds2 = nameArr[nameArr.length-1];
    const odds1 = nameArr.filter( ele => ele != odds2 ).join(symbolStr);
    // Number(odds2) 避免出现 单注-大小 这样数据
    if (symbolStr && Number(odds2) && Number(odds2) % 0.5 !== 0 ){
        str = `${odds1}${symbolStr}${Number(odds2)-0.25}/${Number(odds2)+0.25}`;
    }
    return str 
}


// 是否显示基准分
export function hasJiZhunFen(match) {
    // FIFA的四种玩法增加基准分字段，包括：全场让分，上半场让分，剩余时间获胜，上半场剩余时间获胜
    // 仅在滚球中投注的注单
    // match.game_id = '258337013509741'
    // match.odd_type_id = '123677938025084368'
    // match.score_benchmark = "1:2"
    const odd_type_id = [
        '14650626566501040', // 全场让分
        '47957485339937812', // 上半场让分
        '123445693265569240', // 剩余时间获胜
        '53758685711873936', // 上半场剩余时间获胜
    ]
    const fifaGameId = '258337013509741' // FIFA游戏ID
    if (match.score_benchmark &&
        match.game_id === fifaGameId &&
        match.category !== 5 &&
        odd_type_id.includes(match.odd_type_id)) {
        return true
    }
    else {
        return false
    }
}

const localLangKey = localStorage.getItem('language') || 'cn'
// 转换多语言取值的key
export function i18nTransGameNameKeyUtil(data, prefix = '', suffix = '') {
    let langKey = localLangKey
     //繁体显示cn名字
    if (langKey === 'zh') {
        langKey = 'cn'
    }

    const nameKey = prefix + langKey + '_name' + suffix
    if ("undefined" !== typeof data[nameKey]) {
        return data[nameKey]
    }
    else {
        // fix 中文名不带cn的情况
        if (langKey === 'cn'){
            return data['name']
        }
        return data[prefix + 'en_name' + suffix] || (data[prefix + 'cn_name' + suffix] || '')
    }
}

// 数字转分秒
export function number2Time(times) {
    if (times <= 0) {
        return "0";
    } else {
        const mm = parseInt(times / 60);
        const ss = parseInt(times - mm * 60);
        return `${mm}'${ss}`;
    }
}

// 获取比分网游戏id
export function getAntDataGameId(id) {
    switch (id) {
        case '257154660915053' : //lol
            return 1; 
        case '257561197207055' : //王者
            return 3; 
        case '257289795134339' : //dota
            return 4; 
        case '257578064923863' : //CSGO
            return 2; 
    }
}


// 合并大小让分盘口


export function formatOptionType (array){
    let obj = {};
    array.map(e => {
        let key = `${e.option_type}_${e.name}_${e.status}`;
        if(obj[key]){
            let list = obj[key].list || [obj[key]];
            list.push(e);
            obj[key] = {
                ...obj[key],
                list
            }
            // delete obj[key].des
        }else {
            obj[key] = e;
        }
    })
    return Object.values(obj)
    
}