import '@/assets/scripts/mqtt.min.js' 
import lodash from "lodash";
import { checkIsJSON, canShowAwardAnimate } from '@/assets/scripts/utils.js'
import qs from "qs";
import i18n from "@/assets/i18n/index.js"

class WS {
    constructor(vm) {
        this.vm = vm;
        this.mqtt_client = ""; // mqtt对象
        this.mqtt_scribe = []; // mqtt订阅的频道
    }

    //初始化weosocket
    initWebSocket() {
        let me = this;
        const uid = me.vm.uid;
        const merchant_ID =  me.vm.merchant_ID;
        let wsuri = APP_MQTT_URL;
        var options = {
            username: appMQTTName,
            password: appMQTTPassword,
            keepalive: 60,
            reconnect: true, // 断线自动重新连接
            reconnectPeriod: 5000, // 5秒重新连接一次
            clean: true, // 断开后不清除会话
            protocolId: "MQTT",
            clientId: "mqttjs_dj" + uid,
        };
        // 连接mqtt
        var client = mqtt.connect(wsuri, options);
        window.mqtt_client = client;
        me.mqtt_client = client;
        me.mqtt_scribe = [
            "/match/status/live", // 比赛变更滚球状态
            "/match/status/finish", // 比赛结束
            "/match/action/suspended", // 比赛(暂停/取消暂停)
            "/match/action/visible", // 比赛(显示/隐藏)
            "/market/action/suspended", // 盘口(暂停/取消暂停)
            "/market/action/visible", // 盘口(显示/隐藏)
            "/market/status/update", // 盘口(关盘/开盘)
            "/market/add", // 增加盘口
            "/market/odds/update", // 盘口变赔
            "/match/status/update", // 比赛状态更新(关盘/开盘)
            "/odd/action/visible", // 投注项显示隐藏
            "/match/status/passoff", // 赛事是否可以串关
            "/match/score/update", // 赛事比分更新
            "/match/timer/update", // 比赛时间更新
            "/match/info/videoUrl", //直播地址变更
            "/market/reopen", // 重新开盘
            "/match/status/livesupport", // 赛事是否支持滚球
            "/market/sortCode/update", // 盘口排序推送
            "/match/info/startTime", // 比赛时间更新
            "/market/count/update", // 盘口数量更新
            "/match/recommend/update", // 推荐排序更新
            "/match/info/rndCompOddDscnt", // 赛事返还率
            "/match/risk/update", //  赛事返还率
            "/match/stat/update", // 赛事数量
            "/match/gameTour/update", // 联赛数量
            "/match/info/roundImages", //主播盘图片
            '/match/info/roundStartTime', //主播盘局数结束时间
            "/match/info/resetMatchLevel", // 赛事等级重置
            "/odd/action/insert", // 冠军盘新增盘口
            "/member/status/update", // 会员状态更新
            "/odd/action/suspended", // 投注项暂停
            "/market/action/passoff", // 盘口串关推送
            "/merchant/game/switch", // 游戏显示隐藏
            "/market/name/update", // 盘口名称和基准分
            "/match/videoUrl/update", // ty视频源
            "/merchant/activity/switch", // 商户活动开关
            "/setting/overrun/update", // 限额超限推送
            "/setting/game/match", // 赛事显隐
            "/setting/game/video", // 赛事视频显隐
            "/notice/maintain",    // 维护时间推送
            "/match/batchNo/update", // 赛果列表更新
            "/testMatch/stat/update", // 测试赛事数量
            "/market/status/default", // 首页展示盘口推送
            "/merchant/antData/update", // 前瞻分析,实时数据,动画视频开关状态更新
            "/match/ant/lock", // 赛事级别 前瞻分析,实时数据,动画视频开关状态更新
            "/match/ant/setLink", // 蚂蚁数据关联赛事推送
            "/market/recommend/update", // 冠军盘 盘口 推荐编号
            "/setting/game/sortCode", // 游戏列表更新
            "/match/videoType", // 直播类型推送
            "/setting/game/openEntrance", //开启/隐藏游戏入口更新
            "/setting/basic/oddGroup", // 首页赔率分组更新
            "/setting/basic/update", // 限额 赛事 局内 串关赔率上限推送
            "/merchant/forceRefresh", // 一键强制刷新

            `/member/balance/${uid}`,
            `/domain/change/${me.vm.$store.state.user.userInfo.top_merchant_account}`, // 域名变更
            `/member/prize/${uid}`,
            `/member/oddGroup/${uid}`,
            `/merchant/oddGroup/${merchant_ID}`,

        ]
        // 订阅频道
        let topAccount = me.vm.$store.state.user.userInfo.top_merchant_account // 当前商户ID
        const {ENV} = window;
        
        let isTestMerchant = false;
        if((ENV == 'fat'|| ENV == 'uat') && topAccount == 'bw') { // fat、uat的bw商户属于测试商户
            isTestMerchant = true
        }else if(ENV == 'prod' && topAccount == 'demo') { // 生产环境的demo商户是测试商户
            isTestMerchant = true;
        }

        if(isTestMerchant) {
            // 如果是测试商户，过滤掉正式赛事的赛事计数推送
            me.mqtt_scribe = me.mqtt_scribe.filter(item => item != '/match/stat/update')
            // 测试商户不显示虚拟赛事
            me.vm.$store.commit("SET_VIRTUAL_GAME_SHOW", false);
        }else {
            // 如果是正式商户，过滤掉测试赛事的赛事计数推送
            me.mqtt_scribe = me.mqtt_scribe.filter(item => item != '/testMatch/stat/update')
        }

        client.subscribe(me.mqtt_scribe);
        const updateMatch = lodash.debounce( () => {
            // 更新赛事列表
            me.vm.$store.dispatch("updateMatchList", {
                notload: true, //是否加载赛事列表刷新动画，默认false加载
            });
        }, 800);


        // 盘口列表
        function getMarketList (matchID){
            // 通知组件更新 当前局数下面得盘口列表
            me.vm.$bus.$emit("updateMktList");
        }

        const matchFn = lodash.debounce((res) => {
            const hasOpen = res.find( ele => ele.status == 5 );
            if(!hasOpen){ return }
            updateMatch();
        }, 800);

        const marketFn = lodash.debounce((res) => {
            const { matchID } = me.vm.$store.state.game;
            const matchIDList = res.map( ele => ele.match_id );
            const status = res[0].status;
            if (matchID && matchIDList.includes(matchID) && [6].includes(status) ) {
                getMarketList(matchID)
            }
        }, 800);

        const addMarket = lodash.debounce((res) => {
            const { matchID } = me.vm.$store.state.game;
            const obj = res.find( ele => ele.match_id == matchID )
            if(obj){
                getMarketList(matchID)
            }
        }, 800);

        // 请求推荐赛事列表
        const matchRecommend = lodash.debounce( (res) => {
            me.vm.$store.dispatch("fetchMatchRecommend");
        }, 800);

        // 请求盘口列表
        const marketList = lodash.debounce( (matchID) => {
            getMarketList(matchID)
        }, 800);

        // 赛事时间
        const matchTimer = lodash.debounce( () => {
            me.vm.$store.dispatch("fetchTimer")
        }, 800);

        // 游戏列表
        const gameList = lodash.debounce( () => {
            me.vm.$store.dispatch("fetchGameList")
        }, 800);

        // 赛事计数
        const matchState = lodash.debounce( (id) => {
            me.vm.$store.dispatch("fetchMatchState", id)
        }, 800);

        // 联赛数量
        const matchTourList = lodash.debounce( (res) => {
            me.vm.$store.dispatch("fetchMatchTourList", res)
        }, 800);

        // 更新域名池
        const updateDomain = async ({res, token}) => {
            let addr = localStorage.domainString;
            const { hostname, pathname } = location;
            const {h5:h5Str, api:apiStr, cdn:cdnStr,img_url:imgUrlStr} = res[localStorage.domainType];
            const h5 = h5Str?.split(',')[0];
            const api = apiStr?.split(',')[0];
            const cdn = cdnStr?.split(',')[0];
            const img_url = imgUrlStr?.split(',')[0];
            const isAddrChanged = api !== window.APP_BASE_URL || cdn !== window.APP_CDN_URL || img_url !== window.UPLOAD_URL;
            // 获取addr
            if(isAddrChanged){
                const targetUrl = await me.vm.$store.dispatch("fetchUpdateDomain", {
                    'domain': api,
                });
                addr = qs.parse(targetUrl.split('?')[1])?.addr;
            }
            if(h5 !== hostname || isAddrChanged){
                const i18nCom = i18n.t('common.tips')
                me.vm.$sysModal({
                    title: i18nCom.qiehuanxianlu,
                    message: i18nCom.qiehuanxianlutishi,
                    btnName: i18nCom.shuaxing,
                    link: `${h5||hostname}${pathname}?token=${token}&addr=${addr}`,
                });
            }
        }
        // 接受消息
        client.on("message", function(topic, payload) {
            const {
                matchFlag,
                tour
            } = me.vm.$store.state.game;

            const {
                token
            } = me.vm.$store.state.user;

            // 解析消息
            let data = payload.toString();
            // console.log(`[mqtt] 频道: ${topic} 收到消息：`, data);
            if(!checkIsJSON(data)){
                // console.error("收到的消息不是json格式:", data)
                return
            }
            const res = JSON.parse(data);

            switch (topic) {
                case "/setting/basic/update":
                    if( res.complex_max_odd_limit ){
                        me.vm.$store.commit("UPDATE_ODDS_LIMIT", res.complex_max_odd_limit);
                    }
                    if( res.round_complex_max_odd  ){
                        me.vm.$store.commit("UPDATE_STATION_MAXODDS", res.round_complex_max_odd);
                    }
                    if( res.credit_complex_max_odd_limit ){
                        me.vm.$store.commit("UPDATE_ODDS_LIMIT", res.credit_complex_max_odd_limit);
                    }
                    if( res.credit_round_complex_max_odd ){
                        me.vm.$store.commit("UPDATE_STATION_MAXODDS", res.credit_round_complex_max_odd);
                    }
                    if( res.secret_key ){
                        me.vm.$store.commit("UPDATE_SECRET_KEY", res.secret_key);
                    }
                    break;
                case "/market/status/default":
                    if( res.is_default == 1 ){
                        updateMatch();
                        matchRecommend();
                        let matchID = []; // 联赛赛事查询
                        for( let key in tour ){
                            const tourList = tour[key];
                            tourList.forEach( ele => {
                                matchID = [
                                    ...matchID,
                                    ...ele.matchId
                                ];
                            })
                        };
                        if( matchID.length > 0 ){
                            matchTourList({ matchId: matchID})
                        }
                    }
                    break;
                case "/match/info/resetMatchLevel": // 重置赛事等级
                    // 刷新盘口列表
                    const { matchID } = me.vm.$store.state.game;
                    console.log( matchID, res.match_id )
                    if( matchID == res.match_id ){
                        marketList(matchID)
                    };
                    // 刷新赛事列表
                    const MatchIds =  me.vm.$store.state.game.sourceMatch.map( ele => ele.id );
                    if(MatchIds.includes(res.match_id)){
                        updateMatch()
                    }
                    // 刷新推荐赛事列表
                    const recommendIDS = me.vm.$store.state.game.recommend.map( ele => ele.id );
                    if( recommendIDS.includes( res.match_id ) ){
                        matchRecommend();
                    };

                    break;

                case "/match/stat/update": // 赛事数量
                    const { userInfo:info2 } = me.vm.$store.state.user;
                    // let targetMatch = res.find(item => item.merchant_id == info2.merchant_id);
                    // if(!targetMatch){
                    //     targetMatch = res.find(item => !item.merchant_id);
                    // }
                    if( !Array.isArray(res) ){
                        for( let key in res ){
                            if( res[key].filters && res[key].filters.includes(String(info2.merchant_id)) ){ // 推送赛事数量包含商户id，代表游戏被禁用，所有数量全部改成0
                                res[key] = {
                                    ...res[key],
                                    "today":0,
                                    "live":0,
                                    "early":0,
                                    "complex":0,
                                    "champ":0,
                                    "anchor":0,
                                }
                                // 如果是fliter中的商户，赛事数量为0
                                res[key].total = 0;
                            }   
                        }
                        me.vm.$store.commit("SET_MATCH_STAT", res);
                    }
                    break;
                case "/testMatch/stat/update": // 测试赛事数量
                    const { userInfo:info3 } = me.vm.$store.state.user;
                    // let targetMatch = res.find(item => item.merchant_id == info2.merchant_id);
                    // if(!targetMatch){
                    //     targetMatch = res.find(item => !item.merchant_id);
                    // }
                    if( !Array.isArray(res) ){
                        for( let key in res ){
                            if( res[key].filters && res[key].filters.includes(String(info3.merchant_id)) ){ // 推送赛事数量包含商户id，代表游戏被禁用，所有数量全部改成0
                                res[key] = {
                                    ...res[key],
                                    "today":0,
                                    "live":0,
                                    "early":0,
                                    "complex":0,
                                    "champ":0,
                                    "anchor":0,
                                }
                            }   
                        }
                        me.vm.$store.commit("SET_MATCH_STAT", res);
                    }
                    break;
                case "/match/gameTour/update": // 联赛数量
                    me.vm.$store.commit("SET_MATCH_GAMETOUR", res);
                    break;
                // 推荐赛事更新    
                case "/match/recommend/update":
                    const recommendID = me.vm.$store.state.game.recommend.map( ele => ele.id );
                    if( !recommendID.includes( res.match_id ) ){
                        matchRecommend();
                    }
                    me.vm.$store.commit("SET_RECOMMEND_UPDATE", res);
                    me.vm.$store.commit("UPDATE_RECOMMEND_LIST");
                    break; 
                case "/market/count/update":
                    me.vm.$store.commit("SET_MARKET_COUNT", res);
                    break;
                // 盘口名称更新和基准分更新
                case "/market/name/update":
                    me.vm.$store.commit("SET_MARKET_NAME_UPDATE", res);
                    break;
                // 盘口变赔
                case "/market/odds/update":
                    if(res){
                        me.vm.$store.commit("SET_MARKET_ODDS", res);
                    }
                    break;
                // 更改赛事返还率    
                case "/match/risk/update":
                case "/match/info/rndCompOddDscnt":
                    me.vm.$store.commit("SET_MATCH_RNDDSCUT", res);
                    break;    
                // 盘口显示隐藏
                case "/market/action/visible":
                    me.vm.$store.commit("SET_MARKET_VISIBLE", res);
                    break;
                // 盘口显示隐藏
                case "/market/action/suspended":
                    me.vm.$store.commit("SET_MARKET_SUSPENDED", res);
                    break;
                // 投注项显示隐藏
                case "/odd/action/visible":
                    me.vm.$store.commit("SET_MARKET_BET_VISIBLE", res);
                    break;
                case "/odd/action/suspended":
                    me.vm.$store.commit("SET_MARKET_BET_SUSPENDED", res);
                    break;     
                //  赛事暂停取消
                case "/match/action/suspended":
                    me.vm.$store.commit("SET_MATCH_SUSPENDED", res);
                    break;
 
                case "/setting/overrun/update": // 超限限额最大投注额度
                    me.vm.$store.commit("UPDATE_OVERRUN", res);
                    break;   
                //  赛事显示隐藏
                case "/match/action/visible":
                    const Ids =  me.vm.$store.state.game.sourceMatch.map( ele => ele.id );
                    const notMatchId = [];
                    res.forEach( ele => {
                        if(!Ids.includes(ele.match_id)){
                            notMatchId.push(ele.match_id)
                        }
                    });
                    if( notMatchId.length > 0 ){
                        updateMatch();
                    }
                    me.vm.$store.commit("SET_MATCH_VISIBLE", res);
                    break;
                case "/market/sortCode/update":
                    me.vm.$store.commit("SET_MARKET_SORTCODE", res);
                    me.vm.$store.commit("UPDATE_MATCH_LIST");
                    break;    
                // 冠军盘 盘口排序
                case "/market/recommend/update":
                    me.vm.$store.commit("SET_MARKET_SORTREC", res);
                    break;   
                // 更新游戏列表排序
                case "/setting/game/sortCode":
                    me.vm.$store.commit("UPDATE_GAME_SORT_CODE", res);
                    break;   
                // 盘口关盘 不可逆操作
                case "/market/add":
                    if( matchFlag == 8 ){
                        updateMatch();
                    }
                    addMarket(res)
                    break;
                case "/market/status/update":
                    me.vm.$store.commit("SET_MARKET_UPDATE", res);
                    // marketFn(res);
                    break;
                // 赛事取消 不可逆操作
                case "/match/status/update":
                    me.vm.$store.commit("SET_MATCH_UPDATE", res);
                    if( matchFlag == 8 ){
                        updateMatch();
                    }else{
                        matchFn(res);
                    }
                    matchFn(res);
                    break;
                // 比赛变更滚球状 不可逆操作
                case "/match/status/live":
                    me.vm.$store.commit("SET_MATCH_LIVE", res);

                    const { sourceMatch, collect } = me.vm.$store.state.game
                    // 赛事ID
                    const matchIds = [
                        ...sourceMatch.map( ele => ele.id ),
                        ...collect.map( ele => ele.id ),
                    ];
                    
                    // 如果包含赛事ID 更新赛事时间
                    if(matchIds.includes(res.match_id)){
                        matchTimer()
                    }else{
                        updateMatch();
                    }
                    break;
                // 串关推送
                case "/match/status/passoff":
                    me.vm.$store.commit("SET_MATCH_PASS", res);
                    if( matchFlag == 4 ){
                        updateMatch();
                    }
                    break;
                // 盘口串关推送    
                case "/market/action/passoff":
                    me.vm.$store.commit("SET_MARKET_PASS", res);
                    break;    
                // 赛事比分更新
                case "/match/score/update":
                    me.vm.$store.commit("SET_MATCH_SCORE", res);
                    break;
                case "/match/timer/update":
                    me.vm.$store.commit("SET_TIMER_UPDATE", res);
                    break;
                case "/match/videoType":
                    me.vm.$store.commit("SET_MATCH_VIDEO_TYPE", res);
                    break;
                //直播地址变更
                case "/match/info/videoUrl":
                    me.vm.$store.commit("SET_MATCH_VIDEO_URL", res);
                    break;
                // 盘口开关盘 不可逆操作
                case "/market/reopen":
                    me.vm.$store.commit("SET_MARKET_REOPEN", res);
                    marketFn([ { match_id: res.match_id, status: 6 }]);
                    break;   
                // 赛事是否支持滚球
                case "/match/status/livesupport":
                    me.vm.$store.commit("SET_MATCH_LIVESUPPORT", res);
                    if( matchFlag == 3 ){
                        updateMatch();
                    }
                    // 在赛事详情页 刷新热门赛事列表
                    if(me.vm.$route.path.indexOf('/market') != -1){
                        matchRecommend()
                    }
                    break;  

                case "/match/info/startTime":
                    // 早盘 今日，串关修改比赛更新列表
                    if( matchFlag == 1 || matchFlag == 2 || matchFlag == 4){
                        updateMatch();
                    }
                    me.vm.$store.commit("SET_MATCH_STARTTIME", res);
                    break;

                case "/match/info/roundImages":
                    // 主播盘内容
                    setTimeout( () => {
                        me.vm.$store.commit("UPDATE_ANCHOR_INFO", res);
                    }, 500);
                    break;

                case "/match/info/roundStartTime":
                    // 主播盘局数结束时间
                    me.vm.$store.commit("UPDATE_ANCHOR_TIME", res);
                    break;
                case "/odd/action/insert": // 冠军盘 新增盘口
                    let resJson = JSON.stringify(res)
                    resJson = resJson.replace(/&nbsp;/ig, ' ');
                    me.vm.$store.commit("SET_MATCH_INSERTODD", JSON.parse(resJson));
                    break; 
                case "/member/status/update": // 会员状态更新
                    const { uid } = res;
                    const { userInfo } = me.vm.$store.state.user
                    if(uid === userInfo.uid){
                        me.vm.$store.dispatch("fetchUserInfo");
                    }
                    break;  
                case "/merchant/game/switch": // 游戏显隐
                    const { merchant_id, content } = res.data;
                    const { userInfo:info } = me.vm.$store.state.user;
                    const { matchGameID, gameID } = me.vm.$store.state.game;
                    const { path } = me.vm.$route;
                    const ids = content.map(item => {
                        const arr = []
                        if(item.status == 2){
                            arr.push( String(item.game_id) )
                        }
                        return arr
                    });
                    // 非变更商户
                    if(String(merchant_id) == String(info.merchant_id)){
                        // 更新游戏联赛数量
                        gameList()
                        // 更新收藏
                        matchState({
                            type: "collect"
                        })
                        // 刷新推荐赛事列表
                        matchRecommend()
                        // 从赛事详情回退到首页
                        if(ids.includes(String(gameID)) != -1 && path.indexOf('/market') != -1 ){
                            me.vm.$router.push("/home");
                        }
                        if(ids.includes("10158428472914228") != -1 && path.indexOf('/lottery') != -1 ){
                            me.vm.$router.push("/home");
                        }
                        // VR详情页面退回
                        if(ids.includes("1001") != -1 && path.indexOf('/vSports') != -1 ){
                            me.vm.$router.push("/home");
                        }
                        // 全部或者当前游戏显隐-更新赛事列表
                        if(ids.includes(String(gameID)) != -1 || gameID == 0){
                            updateMatch();
                        }
                        
                    }
                    break;   
                case "/match/ant/setLink":
                        me.vm.$store.commit("SET_MACTH_ANT_LINK", res);
                        break;    
                case "/match/videoUrl/update": // 体育视频源接口变更
                    me.vm.$store.commit("SET_TY_VIDEO_URL", res);
                    break;
                case "/merchant/activity/switch": //商户活动开关
                    me.vm.$store.commit("SET_ACTIVITY_ENTRANCE", !!res.data.status);
                    break;  
                // 比分网数据开关     
                case "/merchant/antData/update": 
                    const { merchant_id: mId, type, status } = res.data;
                    const { userInfo:uInfo } = me.vm.$store.state.user;
                    const { bifenBtnStatus } = me.vm.$store.state.game;
                    // 变更商户比分网按钮状态
                    if(mId == uInfo.merchant_id || mId === 'all') {
                        const { type, status } = res.data;
                        // 按钮类型: 0-前瞻分支 1-实时数据 2-动画视频
                        const dataKey = ['analyse_status', 'real_time_status', 'anime_video_status', 'live_status'];
                    
                        me.vm.$store.commit("SET_BIFEN_BTN_STATUS", {
                            ...bifenBtnStatus,
                            [dataKey[type]]: status
                        });
                    }
                    break; 
                case "/match/ant/lock":
                    me.vm.$store.commit("SET_BIFEN_LOCK", res);
                    break;     
                case "/setting/game/match": // 赛事显隐
                    me.vm.$store.commit("SET_GAME_MATCH_VISIBLE", res);
                    break;
                case "/setting/game/video": // 赛事视频显隐
                    me.vm.$store.commit("SET_GAME_VIDEO_VISIBLE", res);
                    break;
                case "/notice/maintain": // 维护时间推送
                    console.warn('维护时间推送:/notice/maintain==>',res);
                    me.vm.$store.commit("SET_USERINFO_MAIN_REMIN", res);
                    break;
                case "/match/batchNo/update": // 虚拟赛果更新
                    if(me.vm.$store.state.game.virtualLeague.id == res.tour_id){
                        console.log('虚拟赛果更新');
                        me.vm.$store.commit("SET_VIRTUAL_LEAGUE_STATUS");
                    }
                    break;
                case "/setting/game/openEntrance": // 开启/隐藏游戏入口更新
                    const vrList = JSON.parse(JSON.stringify(me.vm.$store.state.game.slotGameList))
                    const obj = vrList.filter(item => {
                        if(item.id === res.game_id){
                            item.is_open_entrance = Number(res.is_open_entrance)
                        }
                        return item
                    })
                    if(obj){
                        me.vm.$store.dispatch("updataVRList", obj)
                        const pathList = {
                            "1001": '/vSports',
                            "10158428472914228": '/lottery'
                        }
                        if(me.vm.$route.path.indexOf(pathList[res.game_id]) != -1 && Number(res.is_open_entrance) === 1){
                            me.vm.$router.replace("/home");
                        }
                    }
                    break;
                case "/setting/basic/oddGroup": // 首页赔率分组更新
                    
                    if( res.status == 0 ){ // 关闭赔率分组开关
                        me.vm.$store.commit("SET_ODD_GROUP", {
                            group: "g0",
                            switch: 0
                        });
                        me.vm.$store.commit("SET_BASIC_ODDGROUP");
                    }else{ // 开启开关
                        me.vm.$store.dispatch("fetchUserOddGroup").then( () => {
                            me.vm.$store.commit("SET_BASIC_ODDGROUP");
                        }); 
                    }
                    break;  
                case "/merchant/forceRefresh": // 一键强制刷新
                    if( res.refresh){
                        location.reload();
                        // me.vm.$store.commit("SET_KICK_OUT");
                    }
                    break;   
                default:
                    // {"pc_og_old":"g0","pc_og_new":"g1","h5_og_old":"g0","h5_og_new":"g1"}
                    if( topic.indexOf("/merchant/oddGroup/") != -1 ){ // 赔率分组推送
                        if( res.h5_og_new != res.h5_og_old){ // 赔率分组不相等 请求当前用户赔率
                            me.vm.$store.dispatch("fetchUserOddGroup").then( () => {
                                me.vm.$store.commit("SET_BASIC_ODDGROUP");
                            });
                        }
                    }
                    if( topic.indexOf("/member/oddGroup/") != -1 ){ // 赔率分组推送
                        const { oddGroup } = me.vm.$store.state.game;
                        me.vm.$store.commit("SET_ODD_GROUP", {
                            group: res.odd_group_key,
                            switch: oddGroup.switch
                        });
                    }
                    if (topic.indexOf("/domain/change/") != -1) {
                        updateDomain({res, token});
                    }
                    // 更新余额
                    if (topic.indexOf("/member/balance/") != -1) {
                        me.vm.$store.commit("UPDATA_BALANCE", res);
                    }
                    // 中奖金额
                    if (topic.indexOf("/member/prize/") != -1) {
                        // 显示中奖特效
                        const { userInfo } = me.vm.$store.state.user
                        // 暂时只在rmb中弹出
                        if (
                            canShowAwardAnimate(res.win_amount, userInfo.uid)
                            && userInfo.currency_code === 1   // 当前币种是人民币
                        ) {
                            me.vm.$store.commit("SET_AWARD_ANIMATE_STATUS", true);
                        }
                    }
                    break;
            }
        });
    }

    closeWebSocket(){
        if( this.mqtt_client ){
            this.mqtt_client.unsubscribe(this.mqtt_scribe); // 取消所有订阅
            this.mqtt_client.end(true); // 断开连接
        }
    }
}

export default WS;
