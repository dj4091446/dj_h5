
const pools = {};//音频管理map
let initSetting = {
  preload: "auto",// 缓冲音频文件
  autoplay: false,//缓存完了后自动播放
  volume: 0.0,//音量
  muted: false,
  src: '#',//资源
  canplaynow: false,//自定义属性是否可以播放
};
let abledBGAudio = false;//禁用背景音乐
let abledTouchAudio = false;//禁用交互音乐
let documentisHidden = false;//手机是否处于锁屏状态
//id和path的键值对，避免资源重复使用和出现重复ID
const keyMap={};
class Audios {
  static pools() {
    return pools;
  }
  static create(settingAttr = {}) {
    if (!settingAttr.id) {
      console.error(`音频缺少有效ID!!!!!`)
      return false;
    }
    else if(!settingAttr.path){
      console.error(`音频缺少必填项，path!!!!`)
      return false;
    }
    if(!/assets\/audios/ig.test(settingAttr.path)){
      console.error(`该音频的的路径【${settingAttr.path}】有误，请把音频存放再assets/audios`)
      return;
    }
    //出现重复id，但是路径不一样
    if(keyMap[settingAttr.id]&&keyMap[settingAttr.id]!=settingAttr.path){
      console.error(`该音频的id已经存在，但是路径不一致${settingAttr.path},请保持一致`)
      return false;
    }
    try {
      let setting = Object.assign({}, initSetting, settingAttr);
      // console.log("初始化", setting.id, setting)
      let dom = document.createElement("Audio");
      let id = setting.id;
      for (let key in setting) {
        if (key != 'id' && setting[key] != "#") {
          dom.setAttribute(key, setting[key]);
          dom[key] = setting[key];//只有直接复制才会有效，添加attribute无效果
        }
      }
      if (!pools[id]) {
        dom.id = "sicbo-audio-" + setting.id;
        document.querySelector("body").append(dom);
        pools[id] = dom;
        // //监听是否可以播放过，如果可以就添加自定义标签
        // //针对开奖音乐，背景音乐这种非抽动触发的音频
        let canPlayCall = function () {
          dom.setAttribute('canplaynow', true)
          dom.audioStatus = "playing";
          dom.setAttribute('audioStatus', "playing")
        }
        let paseStatus = function () {
          dom.audioStatus = "paused";
          dom.setAttribute('audioStatus', "paused")
        }
        dom.removeEventListener("playing", canPlayCall);
        dom.removeEventListener("pause", paseStatus);
        dom.addEventListener("playing", canPlayCall);
        dom.addEventListener("pause", paseStatus);
        keyMap[id]=settingAttr.path;//添加id 和path的键值对
      }
      else {
        console.error(`出现重复的音频节点:${id}，忽略`)
      }
    } catch (e) {
      console.error("Audios create", e)
    }

  }
  static play(id, setting = {}) {
    try {
      let item = Audios.pools()[id];
      //播放时候携带参数
      if (item) {
        for (let k in setting) {
          if (k != 'muted'){
            item[k] = setting[k];
          }
          
          // if (k == 'muted' && item[k] == false) {
          //   item.removeAttribute('muted')
          // }
          // else {
          //   item.setAttribute(k, setting[k])
          // }
        }
        // try {
        //   item.pause();
        // } catch (e) { };
        //===做禁止判断
        //是否是背景音乐
        if (Audios.pools()[id]&&Audios.pools()[id].isBg) {
          if (abledBGAudio && !documentisHidden) {
            // console.log("abledBGAudio",abledBGAudio)
            item.play();
          }
        }
        else {
          // if (abledTouchAudio && !documentisHidden) {
          //   item.play();
          // }
          item.play().then(_=>{
            console.log('播放音效')
        }).catch(error=>{
            // console.log(error)
        });
        }

      }
    } catch (e) {
      console.error("播放音频失败", e, id)
    }

  }
  static pause(id) {
    let item = Audios.pools()[id];
    if (item) {
      item.pause();
    }
  }
  static pauseAll() {
    let obj = Audios.pools();
    for (let id in obj) {
      let item = obj[id];
      if (item) {
        item.pause();
      }
    }
  }
  static remove(id) {
    let item = Audios.pools()[id];
    item.parentElement.remove(item);
  }
  static clear() {
    for (let key in Audios.pools()) {
      let item = Audios.pools()[key];
      item.parentElement.remove(item);
    }
  }
  static updateDisabled(type, val = false,id) {
    if (type == "abledBGAudio") {
      abledBGAudio = val;
      if (!val) {
        Audios.pause(id);
      }
      else {
        Audios.play(id);
      }
    }
    else if (type == "abledTouchAudio") {
      abledTouchAudio = val;
    }
  }
  static updateDocumentHidden(bool = false) {
    documentisHidden = bool;
    if (bool) {
      Audios.pauseAll();
    }
  }
}
export default Audios;