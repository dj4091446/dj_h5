import * as LotteryApi from '@/api/teselol'
import store from "@/store";
import WS from '@/assets/scripts/mqttOpenResult.js'

function lolApiDomainRace(str) {
    const list = window.atob(decodeURIComponent(str)).split('|');
    let path = '/boracay/pulic/festival';
    let method = 'post';
    let apiFailed = 0;

    return new Promise(function (resolve, reject) {
        var _loop = function (domain) {
            var url = domain + path;
            var xhr = new XMLHttpRequest();
            xhr.open(method, url);

            xhr.onload = function () {
                if (xhr.readyState === 4){
                    if (xhr.status === 200){
                        return resolve(domain);
                    } else {
                        console.error('api failed: ' + domain);
                    }
                }
            };

            xhr.onerror = function () {
                apiFailed++;
                if(apiFailed >= list.length){
                    reject('api test failed')
                }
            };

            xhr.send();
        };

        for (var i = 0; i < list.length; i++) {
            _loop(list[i]);
        }
    });
}

function goHome(){
    setTimeout(()=>{
        if(location.href.includes('/lottery')){
            location.href = 'home'
        }
    }, 2000)
}
// 进入英雄召唤之前，初始化
// 包括获取彩票api域名数组，api域名测速，用户信息，mqtt地址等
export async function initLoL() {
    const vm = global.rootVM;
    const res = await LotteryApi.getTokenAndDomain().catch(error=>{
        console.error(String(error))
        vm.$toast('进入电竞彩失败')

        goHome()
    });
    
    const data = res.data
    if(data.status == "true") {
        localStorage.lolToken = data.data.token;;
        const raceDomain = await lolApiDomainRace(data.data.api).catch(error=>{
            console.error(String(error))
            vm.$toast('lol api domain test failed')
            goHome()
        })

        localStorage.lolApiDomain = raceDomain;

        await LotteryApi.getFrontuserInfo().then(res =>{
            const { code, data } = res.data;
            store.commit('setCpUserInfo',data)
        })
        await LotteryApi.configQuery().then(res=>{
            const { code, data } = res.data;

            if(code === 0 || code === 200){
                vm.$store.commit("setConfigUrl",data)
                const ws = new WS(vm)
                ws.initWebSocket()
            }
        })

        return Promise.resolve();
    } else {
        vm.$toast(String(data.data))
        goHome()
    }

    return Promise.reject();;
}