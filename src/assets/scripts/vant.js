import Vue from "vue";
import {
    navBar,
    Icon,
    Tab,
    Tabs,
    Row,
    Col,
    Popup,
    List,
    Dialog,
    ActionSheet,
    DropdownMenu,
    DropdownItem,
    Divider,
    Sticky,
    NoticeBar,
    Swipe, 
    SwipeItem,
    Toast,
    DatetimePicker,
    Image,
    Lazyload,
    Button,
    Overlay,
    Pagination,
    Loading,
    Collapse,
    CollapseItem,
    Popover,
    Switch,
    Circle
} from "vant";

Vue.use(navBar)
    .use(Icon)
    .use(Tab)
    .use(Tabs)
    .use(Row)
    .use(Col)
    .use(Popup)
    .use(Dialog)
    .use(List)
    .use(ActionSheet)
    .use(DropdownMenu)
    .use(DropdownItem)
    .use(Divider)
    .use(Sticky)
    .use(NoticeBar)
    .use(Swipe)
    .use(SwipeItem)
    .use(Toast)
    .use(DatetimePicker)
    .use(Image)
    .use(Lazyload, {
        lazyComponent: true,
    })
    .use(Button)
    .use(Overlay)
    .use(Pagination)
    .use(Loading)
    .use(Collapse)
    .use(CollapseItem)
    .use(Popover)
    .use(Switch)
    .use(Circle)
