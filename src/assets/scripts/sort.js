// 赛事冠军篇排序
export function championMatchSort(arr, gameSortCode, mqMatch) {
    let newArr = [];
    arr.forEach( ele => {
        newArr.push({
            ...ele,
            ...mqMatch[ele.id],
        })
    })
    newArr.sort((a, b) => {
        const gameSortA = Number(gameSortCode[a.game_id]) || 1000;
        const gameSortB = Number(gameSortCode[b.game_id]) || 1000;
        return a.status - b.status || a.rec - b.rec || a.match_level_sort - b.match_level_sort || gameSortA - gameSortB || a.start_time - b.start_time || a.id - b.id;
    })
    return newArr;
}


// 赛事排序
export function matchSort(arr, gameSortCode, mqMatch) {
    let liveCommonMth = [];
    let liveAnchorMth = [];
    let noLiveMth = [];
    let newArr = [];
    arr.forEach( ele => {
        newArr.push({
            ...ele,
            ...mqMatch[ele.id],
        })
    })
    liveCommonMth = newArr.filter( ele => ele.is_live == 2 && ele.category !== 5);
    liveAnchorMth = newArr.filter( ele => ele.is_live == 2 && ele.category === 5);
    noLiveMth =  newArr.filter( ele => ele.is_live != 2 );
    liveCommonMth.sort((a, b) => {
        const gameSortA = Number(gameSortCode[a.game_id]) || 1000;
        const gameSortB = Number(gameSortCode[b.game_id]) || 1000;
        return a.match_level_sort - b.match_level_sort || gameSortA - gameSortB || a.start_time - b.start_time || a.id - b.id;;
    })
    liveAnchorMth.sort((a, b) => {
        const gameSortA = Number(gameSortCode[a.game_id]) || 1000;
        const gameSortB = Number(gameSortCode[b.game_id]) || 1000;
        return a.match_level_sort - b.match_level_sort || gameSortA - gameSortB || a.start_time - b.start_time || a.id - b.id;;
    })
    noLiveMth.sort((a, b) => {
        const gameSortA = Number(gameSortCode[a.game_id]) || 1000;
        const gameSortB = Number(gameSortCode[b.game_id]) || 1000;
        return a.start_time - b.start_time || a.match_level_sort - b.match_level_sort || gameSortA - gameSortB || a.id - b.id;;
    });
    newArr = [
        ...liveCommonMth,
        ...liveAnchorMth,
        ...noLiveMth
    ];
    return newArr;
}

// 推荐赛事篇排序
export function recommendSortMatch(data) {
    const systemMatch = data.filter(ele => ele.visible && ele.status == 5 && ele.rec == 1 && ele.is_open_match )
    systemMatch.sort((a, b) => {
        return Number(a.match_level_sort) - Number(b.match_level_sort) || Number(a.sort_code) - Number(b.sort_code) || Number(a.start_time) - Number(b.start_time) || a.id - b.id;;
    });
    // 操盘手赛事
    // ele.visible == 1 赛事是显示  ele.status == 5 赛事开盘
    const operaterMatch = data.filter(ele => ele.visible && ele.status == 5 && ele.rec != 0 && ele.rec != 1)
    operaterMatch.sort((a, b) => {
        return Number(a.rec) - Number(b.rec) || Number(a.match_level_sort) - Number(b.match_level_sort) || Number(a.sort_code) - Number(b.sort_code) || Number(a.start_time) - Number(b.start_time) || a.id - b.id;;
    });
    
    let _operater = [...operaterMatch];
    if (_operater.length > 2) {
        _operater = _operater.slice(0,2);
    }
    
    let _systems = [...systemMatch];
    _systems = _systems.slice(0,5 - _operater.length)
    return [..._systems, ..._operater];
}


// 正常盘口排序排序
export function marketRoundSort(data) {
    // 盘口排序
    data.sort((a, b) => {
        const splitNameA = Number(a.odds[0].name.split(">")[1]);
        const splitNameB = Number(b.odds[0].name.split(">")[1]);
        const splitTypeA = Number(a.odds[0].name.split("+")[1]) || -Number(a.odds[0].name.split("-")[1]);
        const splitTypeB = Number(b.odds[0].name.split("+")[1]) || -Number(b.odds[0].name.split("-")[1]);
        let statusSort = a.status - b.status;
        // 已结算位于本局最后，所有stauts:6, 7, 8, 10, 9
        if(a.status == 9 && b.status == 10){
            statusSort = 1;
        }
        if(a.status == 10 && b.status == 9){
            statusSort = -1;
        }
        return a.round - b.round || statusSort || a.tag_code - b.tag_code || a.sort_code - b.sort_code|| Number(a.odd_type_id) - Number(b.odd_type_id) || splitNameA - splitNameB || splitTypeA - splitTypeB;
    });
    return data
}

// 局内串关排序
export function marketSort(data) {
    data.sort((a, b) => {
        const splitNameA = Number(a.odds[0].name.split(">")[1]);
        const splitNameB = Number(b.odds[0].name.split(">")[1]);
        const splitTypeA = Number(a.odds[0].name.split("+")[1]) || -Number(a.odds[0].name.split("-")[1]);
        const splitTypeB = Number(b.odds[0].name.split("+")[1]) || -Number(b.odds[0].name.split("-")[1]);
        let optionTypeSort = 0
        // 复合玩法位于本局最后
        if(a.option_type == 12 && b.option_type != 12){
            optionTypeSort = 1;
        }
        if(a.option_type != 12 && b.option_type == 12){
            optionTypeSort = -1;
        }
        let statusSort = a.status - b.status;
        // 已结算位于本局最后，所有stauts:6, 7, 8, 10, 9
        if(a.status == 9 && b.status == 10){
            statusSort = 1;
        }
        if(a.status == 10 && b.status == 9){
            statusSort = -1;
        }
        return a.round - b.round || statusSort || optionTypeSort || a.comp_sub_num - b.comp_sub_num || a.sort_code - b.sort_code || Number(a.odd_type_id) - Number(b.odd_type_id) || splitNameA - splitNameB || splitTypeA - splitTypeB;
    })
    return data;
}


// 投注项排序
export function itemSort(data, type) {
    let odds = [];
    odds = data.sort((a, b) => {
        const splitNameA = Number(a.name.split(">")[1]) || Number(a.name.split("<")[1]);
        const splitNameB = Number(b.name.split(">")[1]) || Number(b.name.split("<")[1]);
        const splitTypeA = Number(a.name.split("+")[1]) || -Number(a.name.split("-")[1]);
        const splitTypeB = Number(b.name.split("+")[1]) || -Number(b.name.split("-")[1]);
        if( type == 2 ){
            return a.sort_id - b.sort_id || splitNameB - splitNameA || splitTypeB - splitTypeA
        }else{
            return a.sort_id - b.sort_id || splitNameA - splitNameB || splitTypeA - splitTypeB
        }
    });
    return odds
}

// 游戏列表排序
export function gameSort(data) {
    let gameList = [];
    gameList = data.sort( (a, b) => {
        return Number(a.sort_code) - Number(b.sort_code) || Number(a.id) - Number(b.id)
    });
    return gameList
}

/**
 * @亚运会专题排序规则
 * 
 * 【已结束】 >【滚球中】 >【即将开赛】
 * 已结束:【赛事等级的优先排序】（由小到大）＞【游戏排序】（排序数字小到数字大）＞比赛时间远近（由远到近）
 * 滚球中:【赛事等级的优先排序】（由小到大）＞【游戏排序】（排序数字小到数字大）＞比赛时间远近（由远到近）
 * 即将开赛: 【赛事等级的优先排序】（由小到大）＞【游戏排序】（排序数字小到数字大）＞比赛时间远近（由远到近）
 * 
 * 
 * */ 

// 推荐赛事篇排序
export function asiaDRecommendSortMatch(data, gameSortCode) {

    const end = [];
    const live = [];
    const early = [];

    data.forEach( ele => {
        if( ele.status > 5 ){
            end.push(ele)
        }else{
            if( ele.is_live == 2 ){
                live.push(ele)
            }else{
                early.push(ele)
            }
        }
    });

    end.sort((a, b) => {
        const gameSortA = Number(gameSortCode[a.game_id]) || 1000;
        const gameSortB = Number(gameSortCode[b.game_id]) || 1000;
        return a.match_level_sort - b.match_level_sort || gameSortA - gameSortB || a.start_time - b.start_time || a.id - b.id;;
    })

    live.sort((a, b) => {
        const gameSortA = Number(gameSortCode[a.game_id]) || 1000;
        const gameSortB = Number(gameSortCode[b.game_id]) || 1000;
        return a.match_level_sort - b.match_level_sort || gameSortA - gameSortB || a.start_time - b.start_time || a.id - b.id;;
    })

    early.sort((a, b) => {
        const gameSortA = Number(gameSortCode[a.game_id]) || 1000;
        const gameSortB = Number(gameSortCode[b.game_id]) || 1000;
        return a.match_level_sort - b.match_level_sort || gameSortA - gameSortB || a.start_time - b.start_time || a.id - b.id;;
    });

    return [
        ...end,
        ...live,
        ...early
    ];
}