
import {requestMerchantJson} from '@/api/game.js';

function setMerchantJson (mid){
    requestMerchantJson({
        merchant_id: mid, 
    }).then(res=>{
        let merchant = res.data.data;
        if( res.data.status == "true" && merchant.logo ){
            var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
            link.type = 'image/x-icon';
            link.rel = 'shortcut icon';
            link.href = UPLOAD_URL + merchant.logo;
            document.getElementsByTagName('head')[0].appendChild(link);
            
            document.title = merchant.title || ''
        }
    })
}

export default setMerchantJson

