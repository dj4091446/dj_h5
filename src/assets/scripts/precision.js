import { create, all } from 'mathjs'

// 精确计算 加减乘除
const config = { 
    number: 'BigNumber',
    precision: 64
}

const math = create(all, config);

// 加
function Add (arg1,arg2) {
    return math.number(math.add(math.bignumber(arg1), math.bignumber(arg2)));
}

// 减
function Sub (arg1,arg2) {
    return math.number(math.subtract(math.bignumber(arg1), math.bignumber(arg2)));
}

// 乘
function Ride (arg1, arg2) {      
    return math.number(math.multiply(math.bignumber(arg1), math.bignumber(arg2)));
}

// 除
function Except (arg1, arg2) {
    return math.number(math.divide(math.bignumber(arg1), math.bignumber(arg2)));
}

export {
    Add,
    Sub,
    Ride,
    Except
}