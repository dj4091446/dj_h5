//金额格式化  至少展示2位小数，不足补0，超过小数点后3位截断
Math.formatAmount = function (number, i = 3) {
    if(isNaN(number)) {
        if (i===2) {
            return 0.00;
        } else if (i===3) {
            return 0.000;
        } else if (i===0) {
            return 0;
        }
    }
    //为了避免精度问题，如 number = 1.89999999999999999
    number = String(parseFloat(number).toFixed(6));

    var index = number.indexOf('.') + i + (i === 0 ? 0 : 1);
    number = number.substring(0, index);
    return number;
}

// 组合 [1,2,3]
Math.arrange = function(arr, num) {
    var r = [];
    (function f(t, a, n) {
        if (n == 0) return r.push(t);
        for (var i = 0, l = a.length; i < l; i++) {
            f(t.concat(a[i]), a.slice(i + 1), n - 1);
        }
    })([], arr, num);
    return r;
}


function factorial(m, n){
    var num = 1;
    var count = 0;
    for( var i = m; i > 0; i-- ){
        if( count == n ){
            break;
        }
        num = num*i;
        count++;
    }
    return num;
}
    
Math.combination = function(m, n){
    return factorial(m,n)/factorial(n,n);
}


