export default {
	keywordsSearch: 'key search',
	tabMenu: {
		gunqiu: 'InPlay',
		chuanguan: 'Parlay',
		youxiguize: 'Hukum',
		jibenguize: 'Hukum am',
		guanjuntouzhu: 'Taruh Juara',
		jiesuanguize: 'Pengiraan eSports',
		wanfashuoming: 'Tentang mainan',
		youxiwanfa: 'Mainan',
		zhubowanfa: 'Anchor',
	},
	basicRule:
		`
        <div class="item">
            <div class="item-title">1 Date dan masa mula </div>
            <div class="item-content">Date dan masa mula E-Sports web kami sebagai rujukan. Masa mula sebenar mengikut situasi paparan rasmi. Contoh: Any acara suspend atau postpone, tiada rematch masa acara dalam 24 jam; slip terkait sebagai tak sah, refund</div>
        </div>

        <div class="item">
            <div class="item-title">2 Anggaran date dan masa mula:</div>
            <div class="item-content">Anggaran date dan masa mula sebagai rujukan. Date dan masa mula sebenar mengikut situasi paparan rasmi. Asalkan acara berlangsung pada akhirnya (bila-bila masa), semua slip sebagai sah</div>
        </div>
 
        <div class="item">
            <div class="item-title">3 Slip keputusan tak jelas</div>
            <div class="item-content">Kalau team Waiver sebelum mula atau semasa acara, slip berkeputusan jelas normal kira; keputusan tak jelas sebagai tak sah, refund</div>
        </div>
        <div class="item">
            <div class="item-title">4 Nama lawanan</div>
            <div class="item-content"> Kalau nama lawanan Inggeris dan bukan Inggeris berbeza dengan web, mengikut Inggeris rasmi</div>
        </div>
        <div class="item">
            <div class="item-title">5 Keputusan timeout</div>
            <div class="item-content">Kalau tiada keputusan rasmi acara atau tiada info bernas dalam 24 jam; slip tak sah, refund</div>
        </div>
        <div class="item">
            <div class="item-title">6 Kes luar biasa team</div>
            <div class="item-content">
                <p>a Kalau nama pemain atau team di E-Sports ralat (info team atau pemain di E-Sports ralat, tak diiktiraf); slip terkait tak sah, refund</p>
                <p>b Kalau team tak dapat sertai, atas sebab Waiver atau faktor lain; slip terkait dipertimbangkan batal, tak sah, refund</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">7 Pembetulan atau ubahan keputusan</div>
            <div class="item-content">
                <p>a Kalau any pembetulan atau ubahan keputusan dalam 36 jam dari masa mula, kami berhak mengubah keputusan slip terkira, menentukan keputusan mengikut badan terkait, skor web atau Live (video demo)</p>
                <p>b Tentang keputusan. Kalau keputusan terubah atau mencari keputusan tepat selepas 36 jam, pemain harus mencarinya/inkuiri platform rasmi sebagai rujukan</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">8 Tentang konflik pengumuman keputusan antara rasmi dan web kami </div>
            <div class="item-content">
                Kalau konflik pengumuman keputusan antara rasmi dan web kami, harus rujuk rekod video acara terkait untuk pengesahan. Kalau tiada rekod video tersedia, keputusan disahkan mengikut pengumuman pihak berkuasa acara terkait di web rasmi. Kalau web rasmi tak dapat membekalkan atau pengumuman rasmi ralat, kami berhak menentukan/betulkan dalam mengesahkan keputusan final dan konklusif
            </div>
        </div>
        <div class="item">
            <div class="item-title">9 Tentang rematch</div>
            <div class="item-content">
                Kalau suatu game perlu rematch atas sebab network atau teknikal ralat; taruhan terkait berkeputusan jelas kekal sah. Kalau rematch dalam 24 jam, kiraan slip belum kira mengikut keputusan ini. Kalau rematch lebih daripada 24 jam; slip belum kira batal. Parlays dikira sebagai "1"
            </div>
        </div>
        <div class="item">
            <div class="item-title">10 Tentang disconnected semasa acara</div>
            <div class="item-content">
                <p>Kalau dropped atau disconnected semasa acara, tapi tak dapat kembali; keputusan diiktiraf sah akan normal kira</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">11 Ubahan dan odds ralat</div>
            <div class="item-content">
                <p>a Odds ubah tiap masa. Taruhan mengikut paparan odds yang disahkan final</p>
                <p>b Kalau slip Parlays dibatal, odds dikira sebagai "1"</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">12 Market tak tutup</div>
            <div class="item-content">
                Kalau market tak tutup, slip taruhan selepas itu dipertimbangkan batal, tak sah, refund
            </div>
        </div>
        <div class="item">
            <div class="item-title">13 Taruhan delay (Sudden Death)</div>
            <div class="item-content">
                Taruhan delay (Sudden Death) - Iaitu taruhan preMatch yang diterima atas any faktor selepas bermula. Kalau inPlay tak tutup; slip taruhan selepas masa itu sebagai Sudden Death. Kami berhak menilai slip terkait sebagai tak sah
            </div>
        </div>
        <div class="item">
            <div class="item-title">14 Penganjur ad hoc ubah hukum </div>
            <div class="item-content">
                Kalau penganjur ad hoc ubah hukum, berbeza dengan bekalan hukum; slip seluruh market terkait tak sah. Game normal kira
            </div>
        </div>
        <div class="item">
            <div class="item-title">15 Muncul kes luar biasa semasa acara</div>
            <div class="item-content">
                Kalau BUG atau masalah teknikal semasa acara, kiraan sesi mengikut keputusan rasmi final
            </div>
        </div>
        <div class="item">
            <div class="item-title">16 Lain-lain</div>
            <div class="item-content">
                <p>a Kami berhak suspend market/atau membatalkan taruhan bila-bila masa</p>
                <p>b Kalau sumber video ralat, tak dapat mainkan video dll., tak menjejaskan kiraan acara</p>
                <p>c Taruhan melalui sistem bug, platform bukan rasmi, collude atau keraguan lain; kami berhak menentukan final</p>
                <p>d Slip tak dapat dibatal, void, ubah setelah disahkan; slip terkait sebagai bukti taruhan sah demi kepastian kami</p>
                <p>e Kalau sistem/teknikal ralat mengakibatkan odds ketara dibuka, semua taruhan terkait sebagai tak sah. Kalau taruhan inPlay ralat; kami berhak menilai taruhan tak sah, refund</p>
                <p>f Kalau sistem atau program ralat, semua taruhan tak sah. Anda boleh beritahu kami kalau ralat</p>
                <p>g Tentang kelewatan video E-Sports (contohnya date, masa, skor, stats, berita dll.) untuk rujukan sahaja. Syarikat tak bertanggungjawab atas ketepatan info ini</p>
                <p>h Kami berhak mengehad maks taruhan item acara atau menambah odds terbesar merchant, tanpa notis dan penjelasan</p>
                <p>i Kami berhak tarik balik any bayaran ekstra, betulkan akaun. Kami akan memberitahu secepat mungkin</p>
                <p>j Kami berhak menentukan dan mentaksir final serta mengubah hukum bila-bila masa bila perlu</p>

            </div>
        </div>
        <div class="item">
            <div class="item-title">17 Maks payout</div>
            <div class="item-content">
                <p>Maks payout: Setiap hari ahli mendapat maks payout USD 1,000,000 (kecuali modal taruhan), atau matawang lain setara</p>
            </div>
        </div>
        `,
	championBet:
		`
        <div class="item">
            <div class="item-title">Ramal Championship, liga atau acara Winner di Outrights. Final win team atau pemain terpilih sebagai layak payout, termasuk:</div>
            <div class="item-content">
                <p>a Keputusan final liga. Contoh: World Cup Champion</p>
                <p>b Win di preliminary. Contoh: World Cup Group Champion</p>
                <p>c Skor tertinggi</p>
                <p>d Pemain terbaik dll</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">Hukum:</div>
            <div class="item-content">
                <p>a Semua taruhan Winner mengikut keputusan final</p>
                <p>b Kalau semua taruhan pemain atau team sertai pertandingan, liga atau acara; slip terkait normal kira di any faktor sama ada disqualified, retired atau tak selesai acara</p>
                <p>c Winner mengikut layak keputusan penilaian terakhir sebagai pemenang pemain atau team</p>
                <p>d Kalau guna pemain lain atau "team lain" ganti nama peserta dalam apa jua situasi, sebagai tiada nama</p>
            </div>
        </div>
        `,
	cross:
		`
    <div class="item">
        <div class="item-content">
            <p>a Parlay ialah pilih 2 atau lebih acara dalam 1 slip. Semua acara harus win, maka slip dikira "win." Kalau salah 1 slip dalam Parlay combo win, akan tambah ke item seterusnya, hingga semua slip terkait win atau any acara kalah sebagai tamat. Any taruhan acara tak sah atau Draw, Parlay itu kekal sah. Item Draw/tak sah dikira sebagai "1"</p>
            <p>b Iaitu 2 atau lebih acara sebagai taruhan combo. Kalau semua pilihan win, taruhan Parlay terkait win dan dapat odds combo 2 taruhan. Kalau 1 (atau lebih) pilihan lose, taruhan "Parlay" sebagai lose. Kalau 1 (atau lebih) pilihan revoke atau batal, odds terkait kembali sebagai 1.00. Maks Parlay 10 slip</p>
            <p>c Setiap combo ada had taruh harian ketika user bertaruh X Parlay N. Kalau kuota hari ini telah habis, sistem akan update kuota pada 24:00:00</p>
        </div>
    </div>
    `,
	live:
		`
    <div class="item">
        <div class="item-title"> Taruhan inPlay ialah bertaruh semasa acara, penerimaan slip diteruskan semasa acara dan berhenti jual selepas Close Nota: Acara yang dialih ke inPlay, sama ada telah bermula, sistem kira semua taruhan sebagai "Slip inPlay"</div>
        <div class="item-content">
            <p>a Semua taruhan inPlay perlu pengesahan sistem. Ia mungkin mengakibatkan pengesahan lewat atau taruh gagal bagi setiap taruhan</p>
            <p>b Pengesahan sistem dinilai dalam 20 saat, sama ada semua taruhan dibatal atau disahkan</p>
            <p>c Kalau masalah teknikal atau kes luar biasa semasa acara, semua slip Sdg Dipasti tak akan disahkan dan sebagai taruh gagal</p>
            <p>d Semua info paparan inPlay (termasuk skor, masa dll.) sebagai rujukan sahaja. Semua info bukan sebagai asas kiraan dan taruhan</p>
        </div>
    </div>
    `,

	ESportsSettlementRules:
		`
        <div class="item">
        <div class="item-title">1 Untuk pengiraan</div>
            <div class="item-content">
                Tiada bukti berlangsung atau belum berlangsung atau postpone; sebagai tak sah/tiada keputusan/tak sertai. Slip terkait team/championship promoted atau Outright kekal sah, kalau berlangsung selepas 24 jam masa mula            
            </div>
        </div>
        <div class="item">
            <div class="item-title">2 Info team tersenarai ralat</div>
            <div class="item-content">
                Kalau info team tersenarai ralat, semua taruhan tak sah. Kalau nama pemain/team tersalah eja, paparan lencana ralat. Syarikat berhak betulkan dan semua taruhan kekal sah, kecuali ia memang salah          
			</div>
        </div>
        <div class="item">
            <div class="item-title">3 Info market ralat</div>
            <div class="item-content">
                Kalau info market tersenarai ralat, slip taruhan terkait tak sah. Syarikat berhak betulkan tajuk, masa acara ralat konsisten dengan liga/peserta/team sebenar, slip kekal sah. Kiraan mengikut keputusan sebenar
            </div>
        </div>
        <div class="item">
            <div class="item-title">4 Tukar nama team atau pemain</div>
            <div class="item-content">
                <p>a Kalau pemain atau team mengubah nama, asalkan dapat mengesahkan acara atau round yang mana, semua market yang menggunakan bekalan nama lama adalah sah. Kalau institusi benarkan substitusi pemain dan umumkan keputusan rasmi; taruhan sebagai normal, tak akan batal atas sebab ganti atau substitusi</p>
                <p>b Kalau team mengubah nama tapi lineup sama, taruhan team baru dan lama sah. Kalau 1 team diganti oleh lineup baru dan tiap pemain berbeza daripada team peserta asal, semua taruhan tak sah</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">5 Peserta bukan pemain rasmi</div>
            <div class="item-content">
                Kalau peserta bukan pemain rasmi (atau salah 1), tak mengumumkan perubahan peserta sebelum jadual diumumkan; kami berhak membatalkan slip, refund
            </div>
        </div>
        <div class="item">
            <div class="item-title">6 Acara tak berlangsung atau rematch atas faktor tak terhalang, di map tunggal</div>
            <div class="item-content">
                <p>a Acara tak berlangsung atas faktor tak terhalang, di map tunggal. Keputusan terkira kekal sah; taruhan keputusan belum kira refund</p>
                <p>b Rematch atas faktor tak terhalang, dalam map tunggal. Keputusan terkira kekal sah; taruhan keputusan belum kira mengikut keputusan rematch. Contoh: Slip belum kira batal, kalau 24 jam tiada rematch</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">7 Team map advantage</div>
            <div class="item-content">
                1 team ada 1 atau lebih map advantages. Contohnya pemenang Final mempunyai advantage, "Map 2" mainan sebenar Map 1; "Map 3" mainan sebenar Map 2 dan seterusnya
            </div>
        </div>
        <div class="item">
            <div class="item-title">8 Taruhan delay (Sudden Death)</div>
            <div class="item-content">
                Taruhan delay (Sudden Death): Iaitu taruhan Early yang diterima atas any faktor selepas bermula. Kalau inPlay tak tutup, slip taruhan selepas masa itu sebagai Sudden Death. Taruhan terkait refund         
			</div>
        </div>
        <div class="item">
            <div class="item-title">9 Hukum Kiraan lain</div>
            <div class="item-content">
                <p>a Kalau kurang konsisten dan bukti individu, atau konflik info bukti yang ketara, kiraan taruhan mengikut data individu terkait kami</p>
                <p>b Taruhan acara, siaran atau game. Kiraannya mengikut pengumuman keputusan rasmi badan pengurusan terkait; taruhan tak sah/tiada keputusan/tak sertai sebagai tak sah</p>
                <p>c Kalau kurang pasti terhadap keputusan, syarikat berhak suspend kiraan any market selama-lamanya</p>
                <p>d Pengumuman keputusan selepas 36 jam tak dapat diubah serta tak terima any inquiry selepas masa itu. Syarikat reset/betulkan kesilapan terhasil dari ralat manusia, sistem atau sumber rujukan yang mengakibatkan keputusan ralat; dalam 36 jam selepas pengumuman. Slip market yang terbukti tamat acara tapi tiada bukti keputusan, lebih daripada 36 jam, slip market terkait dikembalikan</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">10 Hukum kiraan refund</div>
            <div class="item-content">
                <p>Win: Refund = Taruhan * odds</p>
                <p>Lose: Refund = 0</p>
                <p>Half Win: Refund = Taruhan * ( ( odds - 1) / 2 + 1 )</p>
                <p>Half Lose: Refund = Taruhan * 50%</p>
                <p>Draw: Refund = Taruhan</p>
            </div>
        </div>
        `,
	gameShows:
		`
        <div class="item">
            <div class="item-title">1 1x2 market</div>
            <div class="item-content">
                1x2 market ialah tentang acara 1x2 dan Map 1x2. Mengikut keputusan, skor web rasmi atau Live (video demo). Kalau Games terkait ralat atau terubah, tak konsisten dengan web rasmi; slip terkait tak sah, refund
            </div>
        </div>
        <div class="item">
            <div class="item-title">2 Handicap market</div>
            <div class="item-content">
                Handicap market ialah mengikut keputusan, skor web rasmi atau Live (video demo). Handicap termasuk map, Games, kill. Kalau map acara ralat atau terubah, tak konsisten dengan web rasmi; slip terkait tak sah, refund
            </div>
        </div>
        <div class="item">
            <div class="item-title">3 Ketika hanya bekalkan Win/Lose, keputusan final sebagai Draw</div>
            <div class="item-content">
                Semua slip terkait tak sah, refund; ketika item market hanya bekalkan Win/Lose, keputusan final sebagai Draw (Extratime terakhir sebagai keputusan final acara kalau ada Extratime)
            </div>
        </div>
        <div class="item">
            <div class="item-title">4 Kalau tiada stats selepas acara</div>
            <div class="item-content">
                Payout mengikut keputusan paparan stats selepas acara, di League of Legends (LoL). Kiraan mengikut keputusan skor web rasmi, Live (video demo). Susunan dan paparan info stats terkait mungkin berbeza mengikut lokasi
            </div>
        </div>
        <div class="item">
            <div class="item-title">5 Match winner 2-Way Handicap:</div>
            <div class="item-content">
                <p>Kes 1. Taruh acara Draw (contohnya BO2 hanya main 2 games, BO1 tiada Extratime)</p>
                <p>Pemain harus ramal pemenang, tanpa Draw maka dapat win bonus. Kalau keputusan Draw, refund</p>
                <p>Kes 2. Acara yang ada Win/Lose</p>
                <p>Pemain harus ramal pemenang. Any bentuk Extratime dikira</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">6 1x2 3-Way Handicap:</div>
            <div class="item-content">
                Jenis taruhan ini tersedia dalam acara Draw (contohnya BO2 hanya main 2 games, BO1 tiada Extratime). Pemain harus ramal keputusan maka dapat win bonus
            </div>
        </div>
        <div class="item">
            <div class="item-title">7 Handicap:</div>
            <div class="item-content">
                <p>Ramal keputusan selepas Handicaps dikira, any bentuk Extratime dan Injury Time dikira</p>
                <p>Handicap ialah tetapan 1 nombor untuk ""seimbang"" probability, tambah ""-"" di awalan team atau pemain sebagai Handicap; tambah ""+"" sebagai Underdog. Skor tertinggi selepas Handicaps dikira sebagai win. 3 bentuk Handicap play seperti berikut:</p>
                <p>Whole ball handicap</p>
                <p>Handicaps sebagai integer. Contohnya -1, -2, -3 dll. Skor Handicap ditolak Handicaps. Kalau skor lebih daripada Underdog sebagai win, sebaliknya lose, sama sebagai tiada win mahupun lose (iaitu Draw)</p>
                <p>Contohnya A-1 dan B+1. Skor keputusan 2 teams A=3:B=2 sebab A(3)-1=2=B(2). Team AB tiada win mahupun lose</p>
                <p>Half ball handicaps</p>
                <p>Handicaps sebagai Half ball. Contohnya -0.5, -1.5, -2.5 dll. Skor Handicap ditolak Handicaps. Kalau skor lebih daripada Underdog sebagai win, sebaliknya lose</p>
                <p>Contohnya A-1.5 dan B+1.5. Skor keputusan 2 teams A=3:B=2, sebab A(3)-1.5=1.5&lt;B(2). Team B Underdog win</p>
                <p>Split ball handicap</p>
                <p>Kes 1: Handicaps .25. Contohnya -0/0.5, -1/1.5, -2/2.5 dll. Skor Handicap ditolak integer di Handicaps. Kalau skor lebih daripada Underdog sebagai Oer adalah win, sebaliknya lose; sama sebagai Handicap adalah Half Lose, Underdog adalah Half Win</p>
                <p>Contohnya A-1/1.5 dan B+1/1.5. Skor keputusan 2 teams A=1:B=0, sebab A(1)-1=0=B(0). Team A Half Lose, team B Half Win</p>
                <p>Kes 2: Handicaps .75. Contohnya -0.5/1, -1.5/2, -2.5/3 dll. Skor Handicap ditolak integer di Handicaps. Kalau skor lebih daripada Underdog sebagai win, sebaliknya lose; sama sebagai Handicap Half Win, Underdog Half Lose</p>
                <p>Contohnya A-0.5/1 dan B+0.5/1. Skor keputusan 2 teams A=1:B=0, sebab A(1)-1=0=B(0); team A Half Win, team B Half Lose</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">8 Over/Under:</div>
            <div class="item-content">

                <p> Ramal Total Skor lebih atau kurang daripada skor market, any bentuk Extratime dan Injury Time dikira</p>
                <p>3 bentuk Over/Under play seperti berikut:</p>
                <p> Whole ball handicap</p>
                <p>Skor market sebagai integer. Contohnya 26, 27, 28 dll. Total Goals ditolak skor market. Keputusan positif sebagai Over win, sebaliknya Under win; keputusan 0 sebagai tiada win mahupun lose (iaitu Draw)</p>
                <p>Contohnya Over 26 dan Under 26. Kalau Total Goals bagi 2 teams 26, sebab 26-26=0. Over 26 dan Under 26 sebagai tiada win mahupun lose</p>
                <p>Half ball handicaps</p>
                <p>Skor market sebagai Half ball. Contohnya 26.5, 27.5, 28.5 dll. Total Goals ditolak skor market. Keputusan positif sebagai Over adalah win, sebaliknya Under win</p>
                <p>Contohnya Over 26.5 dan Under 26.5. Kalau Total Goals bagi 2 teams 26, sebab 26-26.5=0.5. Under 26.5 sebagai win, Over 26.5 sebagai lose</p>
                <p>Split ball handicaps</p>
                <p>Kes 1: Skor market .25. Contohnya 26/26.5, 27/27.5, 28/28.5 dll. Total Goals ditolak skor market di Handicaps. Keputusan positif sebagai Over win, sebaliknya Under win; keputusan 0 sebagai Over adalah Half Lose, sebagai Under adalah Half Win</p>
                <p>Contohnya Over 26/26.5 dan Under 26/26.5. Kalau Total Goals bagi 2 teams 26, sebab 26-26=0. Over 26/26.5 sebagai Half Lose, Under 26/26.5 sebagai Half Win</p>
                <p>Kes 2: Skor market .75. Contohnya 26.5/27, 27.5/28, 28.5/29 dll. Total Goals ditolak integer di skor market. Keputusan positif sebagai Over win, sebaliknya Under win; keputusan 0 sebagai Over adalah Half Win, Under adalah Half Lose</p>
                <p>Contohnya Over 26.5/27 dan Under 26.5/27. Kalau Total Goals bagi 2 teams 27, sebab 27-27=0. Over 26.5/27 sebagai Half win, Under 26.5/27 sebagai Half Lose</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">9 Odd/Even:</div>
            <div class="item-content">
                Ramal keputusan final Odd/Even. Any bentuk tambahan masa atau Extratime dikira (Kalau keputusan 0, dikira sebagai Genap "Even")
            </div>
        </div>
        <div class="item">
            <div class="item-title">10 Masa:</div>
            <div class="item-content">
                (Tempoh/Event) - Masa map atau masa event; masa map mengikut masa di papan skor rasmi selepas tamat game; kalau tiada bekalan papan skor, mengikut kiraan masa di game; Kiraan masa event mengikut masa sebenar. 2 market Masa di atas, lebih daripada atau sama dengan masa market tertera, dikira sebagai lebih daripada
            </div>
        </div>
        <div class="item">
            <div class="item-title">11 No. of Kills:</div>
            <div class="item-content">
                Bilangan kill di map; mengikut bilangan selepas game tamat di papan skor rasmi; mengikut skor terpapar di game kalau tiada bekalan papan skor
            </div>
        </div>
        <div class="item">
            <div class="item-title">12 Towers Destroyed:</div>
            <div class="item-content">
                Semua towers di map. Kalau team serah kalah sebelum tamat acara, semua towers destroyed selepas menyerah adalah dikira. Creeps atau Deny destroyed pun dikira
            </div>
        </div>
        <div class="item">
            <div class="item-title">13 Role yang 1st tower destroyed:</div>
            <div class="item-content">
                Tower Role, termasuk Top/Mid/Bot. Total 6 towers
            </div>
        </div>
        <div class="item">
            <div class="item-title">14 Kills:</div>
            <div class="item-content">
                Bilangan kills team peserta, mengikut papan skor
            </div>
        </div>
        <div class="item">
            <div class="item-title">15 Assists:</div>
            <div class="item-content">
                Assists: Bilangan Assists pemain team, mengikut papan skor
            </div>
        </div>
        <div class="item">
            <div class="item-title">16 1st Blood:</div>
            <div class="item-content">
                Dapat skor pertama ialah blood pertama, mengikut papan skor
            </div>
        </div>
        <div class="item">
            <div class="item-title">17 1st Tower:</div>
            <div class="item-content">
                Team with 1st Tower Destroyed (Deny tower di DOTA2 sebagai denier destroy tower pihak lawan)
            </div>
        </div>
        <div class="item">
            <div class="item-title">18 N Kills:</div>
            <div class="item-content">
                Pihak dapat N Kills terdahulu. Contohnya 5 Kills, pihak yang dapat 5 Kills terdahulu
            </div>
        </div>
        <div class="item">
            <div class="item-title">19 Nth Kill:</div>
            <div class="item-content">
                Nth Kill: Pihak yang dapat Nth Kill. Contohnya 5th Kill: Pihak yang dapat 5th Kill
            </div>
        </div>
        <div class="item">
            <div class="item-title">20 Player special betting:</div>
            <div class="item-content">
				Map Winner refer to the player who gets the highest kill in a single map (Draw refund)
            </div>
        </div>
		<div class="item">
            <div class="item-title">21 Highest KDA:</div>
            <div class="item-content">
				Calculated as [K+A]/[D+1], K (kills), A (assists), D (deaths) (Calculate 2 decimal places) (Draw refund)
            </div>
        </div>
        `,
	gamePlay: [
		{
			title:'DOTA2',
			value:
				`
                <p>1 <span class="label">DOTA2 dropped semasa acara:</span></p>
                <p>Kes A Kalau dropped sebelum 1st blood, tapi reconnected dalam game; semua keputusan diiktiraf sah, akan normal kira</p>
                <p>Kes B Kalau dropped selepas 1st blood serta semasa acara tanpa Waiver dll.; semua keputusan diiktiraf sah, akan normal kira</p>
                <p>Kes C Kalau sebelum 1st blood, sebelah pihak dropped, masa seterusnya tak dapat reconnect hingga game tamat. Untuk keadilan dan kesaksamaan game; slip terkait tak sah</p>
                <p>2 <span class="label">Role pemain di game: </span>Role ID di Team Info (mengikut situasi sebenar, kalau Role ditukar)</p>
                <p>3 <span class="label">Support Role dapat 1st blood:</span> Jikalau pemain Role 4 dan 5 dapat 1st blood di Team Info</p>
                <p>4 <span class="label">Will 1st Blood Occur Before 3 Minutes: </span>Whether 1st Blood to happen within 3 minutes (00:00 - 02:59)</p>
                <p>5 <span class="label">1st tower Destroyer: </span>Deny tower di DOTA2 sebagai denier destroy tower pihak lawan</p>
                <p>6 <span class="label">Kill 1st Roshan:</span> Kiraan mengikut keputusan web rasmi atau Live (video demo)</p>
                <p>7 <span class="label">Mega Creeps spawn: </span>6 barracks bagi 1 team being destroyed dikira ""Ya""</p>
                <p>8 <span class="label">Team with Nth Kill: </span>Pihak yang dapat Nth Kill, bukannya N Kill terdahulu Contohnya Team with 20th Kill: Pihak yang dapat 20th Kill</p>
                <p>9 <span class="label">Team with 5 Kills Before 8 minutes:</span>Whether a team to accumulate 5 kills within 8 minutes (00:00 - 07:59)</p>
                <p>10 <span class="label">Use of most Smoke of Deceit (Consumables): </span>Use of most Smoke of Deceit (Consumables) team, tak termasuk Smoke of Deceit yang terhasil daripada Ninja Gear</p>
                <p>11 <span class="label">Use of Total Smoke of Deceit (Consumables): </span>Total usage Smoke of Deceit (Consumables) bagi 2 teams, tak termasuk Smoke of Deceit yang terhasil daripada Ninja Gear</p>
                <p>12 <span class="label">Consume 1st Aghs: </span>Team pertama consume Aghanim's Shard</p>
                <p>13 <span class="label">Total Aghs Consumed: </span>Total Aghanim's Shard consumed oleh 2 teams</p>
                <p>14 <span class="label">1st 4 Bounty Runes collected evenly: </span>Hanya kira sama ada 4 Bounty Runes yang pertama kali spawn di mulaan tanda minit 00:00, collected evenly oleh 2 teams</p>                
                <p>15 <span class="label">Player with highest Skor Creep (last hit): </span>Hanya kira bilangan Last Hits. Bilangan Denies tak dikira</p>
                <p>16 <span class="label">Kills Handicap Before 10 Minutes: </span>Cumulative Kill Handicap before 10 minutes (00:00 - 09:59)</p>
                <p>17 <span class="label">Total Kills Before 10 Minutes: </span>Cumulative Kills before 10 minutes (00:00 - 09:59)</p>
                <p>18 <span class="label">Destroy 1st Tormentor:  </span>Team yang destroy first Tormentor di game</p>
                <p>19 <span class="label">Highest Net Worth Before 10 Minutes: </span>Refers to the Net Worth gained before 10 minutes (00:00-09:59) (Draw refund)</p>
                <p>20 <span class="label">Highest Net Worth Before 20 Minutes: </span>Refers to the Net Worth gained before 20 minutes (00:00-19:59) (Draw refund)</p>
                <p>21 <span class="label">Water Rune Equally Divided: </span>Refers to whether the four water runes within 06:00 minutes in the game are divided equally</p>
                <p>22 <span class="label">The Same Team Picked the First Two Wisdom Runes within First 14 Minutes: </span>Only the first two wisdom runes are counted within 14:00 minutes of game (14:00 minutes is not counted)</p>
                <p>23 <span class="label">Most Last Hits of Position 2 within 5 Minutes: </span>Cumulative last hit (exclude deny) before 05:00 minutes (05:00 minutes is not counted)</p>

            `,
			icon:'game-logo-dota2'
		},
		{
			title:'League of Legends',
			value:
				`
                    <p>1 Role pemain di game (Top/Mid/Jungler/Support/ADC) mengikut pengumuman rasmi Early map</p>
                    <p>2 <span class="label">Rift Herald: </span>Hanya kira Kill bagi team ini, sebagai win</p>
                    <p>3 <span class="label">Towers destroyed Odd/Even: </span>Mengikut papan skor. Towers destroyed bagi 2 teams (termasuk Nexus Turret). Inhibitor dan Base (Nexus) tak dikira</p>
                    <p>4 <span class="label">Kiraan Elemental Dragon: </span>Hanya kira Elemental Dragon. Elder Dragon tak dikira</p>
                    <p>5 <span class="label">Total players dapat 1st Baron Nashor BUFF:</span> Kalau tiada Baron Nashor being killed, taruhan terkait refund</p>
                    <p>6 Destroy Inhibitor yang respawn tak dikira dalam Destroyed</p>
                    <p>7 <span class="label">Total Heroes Alive When Nexus Destroyed:</span> Mengikut heroes alive ketika Nexus destroyed (respawn semasa/selepas Ledakan tak dikira)</p>
                    <p>8 <span class="label">Total Kills Before 10 Minutes:</span>Cumulative Kills before 10 minutes (00:00 - 09:59)</p>
                    <p>9 <span class="label">1st blood market:</span> Kill team lawan terdahulu sebagai pemenang (mengikut papan skor)</p>
                    <p>10 Market berasaskan masa, kiraan slip mengikut progress game. Kira keputusan mengikut skor web rasmi dan stats selepas acara, Live (video demo)</p>
                    <p>11 <span class="label">Nth Elemental Dragon Attribute:</span> Pit dipapar Elemental Dragon seterusnya. Munculnya pratonton menandakan telah ada keputusan. Kiraan mengikut keputusan skor web rasmi atau Live (video demo)</p>
                    <p>12 <span class="label">Total Kills (00:00~10:00 minit, di masa ditetapkan): </span>Taruhan sah kalau di masa ditetapkan. Masa acara kalau tak melebihi masa ditetapkan, contohnya 10:00 minit; 00:00 ~ 10:00 minit sebagai tak sah. Kiraan mengikut keputusan skor web rasmi atau Live (video demo), mengikut pemasa di game</p>
                    <p>13 <span class="label">Baron Nashor being stolen:</span> Kalau belum ada Baron Nashor being killed, kiraan mengikut keputusan 0:0 bagi 2 teams sebagai ""Tak."" Kiraan keputusan mengikut skor web rasmi dan stats selepas acara, Live (video demo)</p>
                    <p>14 <span class="label">Last digit of Total Kills market:</span> Last digit of Total Kills bagi 2 teams ditambah nilai market dibuka dan dibandingkan. Contohnya keputusan 9:12 (Last digit of Total Kills bagi 2 teams ditambah 9+2=11). Kiraan keputusan mengikut skor web rasmi dan stats selepas acara, API atau Live (video demo)</p>
                    <p>15 Triple Kill: Papar selepas kill 3 musuh berturut. Quadra Kill: Papar selepas kill 4 musuh berturut</p>
                    <p>16 <span class="label">Jikalau Ace: </span>Semua ahli dalam 1 team mati atau muncul petua Ace dikira sebagai Ace</p> 
                    <p>17 <span class="label">Race ke Rift Herald's Eye: </span>Team pertama yang mendapat Rift Herald's Eye pertama. Tak terkait dengan Kill Rift Herald pertama</p>
                    <p>18 <span class="label">Bezaan ekonomi bagi Total dan Handicap 2 teams: </span>Kiraan keputusan mengikut skor web rasmi dan stats selepas acara, API atau Live (video demo). Nota: Bezaan ekonomi bagi Total dan Handicap terkait adalah konsisten; slip batal, tak sah (Draw), refund</p>
                    <p>19 <span class="label">10 Minit Kill Handicap: </span>Taruhan sah kalau di masa ditetapkan. Masa acara kalau tak melebihi masa ditetapkan, contohnya 10:00 minit; 00:00 ~ 10:00 minit sebagai tak sah. Kiraan mengikut keputusan skor web rasmi atau Live (video demo), mengikut pemasa di game</p>
                    <p>20 <span class="label">Total towers destroyed di Top: </span>Total towers destroyed di Top: Tak termasuk Nexus Turret bagi 2 teams</p>
                    <p>21 <span class="label">Total towers destroyed di Mid: </span>Total towers destroyed di Mid: Tak termasuk Nexus Turret bagi 2 teams</p>
                    <p>22 <span class="label">Total towers destroyed di Bot: </span>Total towers destroyed di Bot: Tak termasuk Nexus Turret bagi 2 teams</p>
                    <p>23 <span class="label">Role with most towers destroyed: </span>Play terkait hanya kira Top dan Bot. Role yang destroy towers terbanyak. Mid tak dikira (Draw No Bet)</p>
                    <p>24 <span class="label">Dapat most Gold: </span>Total ekonomi team peserta, mengikut papan skor</p>
                    <p>25 Role yang kill 1st Baron Nashor: 1st Baron Nashor being killed oleh any Role Champion di game, mengikut video. Slip tanpa Kill sebagai tak sah, refund.</p>
                    <p>26
                    Kill Handicap ketika 1 team dapat N Kills: ialah Total skor di papan skor ketika pihak dapat N Kills di papan skor terdahulu.
                    Contohnya market play【Team A Kill Handicap-1.5 ketika 1 team dapat 5 Kills】. Kalau 1 team dapat 5 Kills, skor papan skor 5:3, 5-1.5=3.5>3, bertaruh【Team A Kill Handicap-1.5】sebagai win</p>
                    <p>27
                    Total Kills ketika 1 team dapat N Kills: ialah Total skor di papan skor ketika pihak dapat N Kills di papan skor terdahulu.
                    Contohnya market play【Total Kills 8.5 ketika 1 team dapat 5 Kills】. Kalau skor Kills di papan skor 5:4 ketika 1 team dapat 5 Kills terdahulu, Jumlah Total Kills 9, bertaruh【Over】sebagai win</p>
                    <p>28
                    Total Kills ketika 1 team dapat N Kills: ialah Total skor di papan skor ketika pihak dapat N Kills di papan skor terdahulu.
                    Contohnya market play【Total Kills 8.5 ketika 1 team dapat 5 Kills】. Kalau skor Kills di papan skor 5:4 ketika 1 team dapat 5 Kills terdahulu, Jumlah Total Kills 9, bertaruh【Over】sebagai win</p>
                    <p>29
                    Role of last kill Champion: Champion Role yang dapat Kill terakhir dalam game, mengikut video</p>
                    <p>30 Role of Champion being killed last: Champion Role yang being killed terakhir di game, mengikut video</p>
                    <p>31 <span class="label">Highest Last Hit: </span>Refers to the highest number of last hit obtained by the players, the results are based on the scores on the official website and the post-tournament statistics panel (Draw refund)</p>
                    <p>32 <span class="label">Highest Last Hit Before 10 Minutes: </span>Refers to the highest number of last hit gained before 10 minutes (00:00 - 09:59) (Draw refund)</p>
                    <p>33 <span class="label">First to Obtain Mythic Item: </span>Refers to the player who gets the first Mythic Item (based on the Mythic Item provided by the official website)</p>
                    <p>34 <span class="label">Highest Kill Streak Will Be Quadra Kill or Penta Kill: </span>Only the highest kill streak is counted, e.g. if the highest is Quadra Kill then Quadra Kill wins, if the highest is Penta Kill then Penta Kill wins, the rest are considered as none</p>
                    <p>35 <span class="label">Epic Monsters: </span>Refers to Elemental Dragon, Baron Nashor, Rift Herald, and Elder Dragon</p>
                    <p>36 <span class="label">Elder Dragon Spawned: </span>Refers to whether the Elder Dragon is spawned. Result will be Yes as long as the Elder Dragon was spawned (regardless of whether it is killed or not)</p>
                    <p>37 <span class="label">Last Killed Champion Role upon Slaying the First Baron: </span>If Baron Nashor is not killed, it will be considered as no result</p>
                    <p>38 <span class="label">Slain the Nth Dargon Within 1 Minute After Respawned: </span>If the game ends within one minute after the respawn of the Nth dragon and it's not killed, result will be considered as No ; If the game ends before the Nth dragon respawn, it will be considered as no result</p>
                    <p>39 <span class="label">First Activated "Flash"/"Teleport": </span>Refers to the first player to activate "Flash"/"Teleport" skills. The skill must be selected by both players to be considered as effective bets. If one of the player does not choose the corresponding skill, example: Player A chosen "Ignite" and "Flash", Player B chosen "Flash" and "Teleport", then First Activated "Teleport" will be considered as no result</p>

                `,
			icon:'game-logo-lol'
		},
		{
			title:'CSGO/CS2',
			value:
				`
                    <p>1 <span class="label">1st Pistol round win:</span> Team pemenang Round 1 Pistol</p>
                    <p>2 <span class="label">Race ke Round 5/10 winner:</span> Team yang win Round 5/10 terdahulu</p>
                    <p>3 Odd/Even market, keputusan ""0"" sebagai Even</p>
                    <p>4 <span class="label">Definisi CSGO round:</span> Apabila round baru masuk ke buy time, round lama dikira tamat</p>
                    <p>5 <span class="label">Round N Total Headshots :</span> Hanya munculnya ikon/petua headshot dikira headshot. Semua headshots dalam apa jua situasi di round terkait dikira</p>
                    <p>6 <span class="label">1st Kill: </span>Harus kill team lawan. Kalau bunuh diri atau being killed oleh rakan, round terkait dinilai sebagai tiada 1st kill, taruhan refund</p>
                    <p>7 <span class="label">1st being killed: </span>Pemain pertama being killed di map, any kematian dikira</p>
                    <p>8 <span class="label">Pistol round Total Kills: </span>Kiraan mengikut skor web rasmi atau Live (video demo)</p>
                    <p>9 <span class="label">Kill market: </span>Kiraan mengikut skor web rasmi atau Live (video demo)</p>
                    <p>10 <span class="label">1st Half Winner/2nd Half Winner: </span></p>
                    <p>In CSGO First Half (Round 1 to Round 15) / Second Half (Round 16 to Round 30, draw refund) The teams with the highest score wins, each half is calculated separately</p>
                    <p>In CS2 First Half (Round 1 to Round 12, draw refund) / Second Half (Round 13 to Round 24, draw refund) The teams with the highest score wins, each half is calculated separately</p>
                    <p>11 <span class="label">Pistol round dan Round N Total Headshots: </span>Kiraan mengikut keputusan web rasmi, Live (video demo). Nota: Being headshot killed oleh team sendiri pun dikira Headshot</p>
                    <p>12 <span class="label">Pistol round dan Round N Total Kills: </span>Kiraan mengikut keputusan web rasmi, Live (video demo). Nota: Being killed daripada ledakan bom tak dikira Kills</p>
                    <p>13 <span class="label">Round N wining method:</span> Terdapat 4 jenis winning method, iaitu Timeout/Ledakan/Jinak bom/Eliminated. Penilaian play terkait ialah cara win bagi round terkait</p>
                    <p>14 <span class="label">Ada Knife Kill:</span> Ketika pemilihan Fortress, warmup tak dikira sebagai sah. Kalau being Knife killed oleh rakan team, pun dikira ""Ya""</p>
                    <p>15 <span class="label">Ada Grenade Kill:</span> Ketika pemilihan Fortress, warmup tak dikira sebagai sah. Hanya kira ""Grenade"" Kill. ""Molotov Cocktail,"" projektiles tak dikira. Kalau being Grenade killed oleh rakan team, pun dikira ""Ya""</p>
                    <p>16 <span class="label">1st Half winner/Map winner:</span> Item market itu harus penuhi syarat 1st Half win dan Map win, maka dikira sebagai win</p>
                    <p>17 <span class="label">Player with most Assists: </span>Semua jenis musuh kill Assist dikira di acara CSGO. Nota: Flashbang assist tak dikira</p>
                    <p>18 <span class="label">Total Clutches Won: Official calculation method: </span>The number of players in this round is reduced to 1 vs 1, and a total of 9 kills must be achieved in this round to be considered as a Clutch. Settlement will only be based on official post-match results showing "CLUTCHES WON"</p>
                    <p>19 <span class="label">Whether or not a player gets three kills in a single round (rounds N through N+1): </span>In rounds N through N+1, a single player with three or more kills in a single round (including kills of teammates) is considered to won the betting order. If rounds N through N+1 are not played, the order is considered null and void</p>
                    <p>20 <span class="label">Market with specified map: </span>If the selected map is not the specified map of the market, it will be considered as no result</p>
                    <p>21 <span class="label">Market with player's name only calculate the data of a single player. Example: </span>Player With Higher Kills : A=30 B=25, then the option including player A wins ; If there are multiple players, the option including the player with the highest corresponding data wins. Example: Player With Higher Assists : A=3/B=5, C=6/D=1, the highest is C, then option C/D including player C wins (Draw refund)</p>
                    <p>22 <span class="label">Correct Round Score(Exclude Overtime): </span></p>
                    <p>If the option is a one-way score, any team that obtains the corresponding score will be considered as win </p>
                    <p>For example in CSGO : 16:9, Team A=9 Team B=16, the option 16:9 is considered as win. Team A=16 Team B=9 Option 16:9 also considered as win ; </p>
                    <p>In CS2 : 13:5, Team A=13 Team B=5, the option 13:5 is considered as win </p>
                    <p>If the option is a two-way score, the corresponding team must obtain the correct score to win </p>
                    <p>For example in CSGO: 16:11 11:16, Team A=16 Team B=11, then option 16:11 will be considered as win, Team A=11, Team B=16, then option 11:16 will be considered as win (If it goes into overtime, 15:15 will be considered as win)</p>
                    <p>In CS2 : 13:7 7:13, Team A=13 Team B=7,  then option 13:7 will be considered as win (If it goes into overtime, 12:12 will be considered as win)</p>
                    <p>23 <span class="label">2nd Pistol Round - Winner: </span>In CSGO , Round 16 is the 2nd Pistol Round. In CS2 , Round 13 is the 2nd Pistol Round</p>
                `,
			icon:'game-logo-csgo'
		},
		{
			title:'King of Glory',
			value:
				`
                    <p>1 <span class="label">Destroy 1st Tower market: </span>Hanya destroyed Tower oleh team lawan di King Of Glory dikira. Nota: Base tak dikira</p>
                    <p>2 <span class="label">1st Blood market:</span> Kill team lawan terdahulu sebagai pemenang (mengikut papan skor)</p>
                    <p>3 <span class="label">Will 1st Blood Occur before 2 Minutes: </span>Whether 1st Blood to happen within 2 minutes (00:00 - 01:59)</p>
                    <p>4 <span class="label">1st Tyrant Killed Before 2:30: </span>Whether to kill the 1st Tyrant within 2:30 minutes (00:00 - 02:29)</p>
                    <p>5 Asas kiraan final Role pemain di game (Auxiliary/Mid/Jungle/Confrontation/Development) mengikut pengumuman rasmi map selepas acara</p>
                    <p>6 <span class="label">Lane with most towers destroyed: </span>Play terkait hanya kira Overlord dan Tyrant. Lane yang destroy towers terbanyak. Mid tak dikira (Draw No Bet)</p>
                    <p>7 <span class="label">Kill Overlord Odd/Even:</span> Semua Overlord dikira, termasuk Prophet Overlord and Dark Overlord</p>
                    <p>8 <span class="label">Kill Tyrant Odd/Even:</span> Semua Tyrant dikira, termasuk Tyrant dan Dark Tyrant</p>
                    <p>9 1st Tyrant Killed Before 4:30: Whether to kill the 1st Tyrant within 4:30 minutes (00:00 - 04:29)</p>
                    <p>10 <span class="label">Kills Handicap Before 6 Minutes: </span>Cumulative Kill Handicap before 06:00 minutes (06:00 minutes is not counted)</p>
                    <p>11 <span class="label">Total Kills Before 6 Minutes: </span>Cumulative Kills before 06:00 minutes (06:00 minutes is not counted)</p>
                    <p>12 <span class="label">First Tower Destroyed Before 5:30: </span>Refer to whether the first tower will be destroyed before 05:30 minutes (Exactly 05:30 minutes will be considered as No)</p>
                    <p>13 <span class="label">Ancient: </span>Ancient refer to Tyrant , Dark Tyrant , Overlord , Phophet Overlord , Dark Overlord and Storm Dragon</p>
                    <p>14 <span class="label">First Storm Dragon Spawn Location: </span>If Storm Dragon hasn't spawn yet will be cancelled and considered as no result</p>
                `,
			icon:'game-logo-aov'
		},
        {
            title:'Valorant',
            value:
                `
                <p>1st Pistol round win: Team pemenang Round 1 Pistol</p>
                <p>2nd Pistol round win: Team pemenang Round 13 Pistol</p>
                <p>1st Half win: Team yang menskor tertinggi di 1st Half (Round 1~12)</p>
                <p>2nd Half win: Team yang menskor tertinggi di 2nd Half (Round 13~24)</p>
                <p> Round N winning method: Terdapat 4 jenis winning method, iaitu Timeout/Ledakan/Jinak bom/Eliminated. Penilaian play terkait ialah cara win bagi round terkait</p>
                `,
            icon:'game-logo-Valorant'
        },
		{
			title:'Bola Keranjang',
			value:
				`
                    <p>1 Slip Fulltime NBA termasuk skor Extratime. Quarter, slip tak termasuk skor Extratime</p>
                    <p>2 Kiraan mengikut acara terkait atau skor rasmi pembekal atau pengumuman web rasmi. Kalau tiada skor rasmi pembekal atau web rasmi, atau terdapat bukti penting membuktikan skor rasmi pembekal atau data web rasmi adalah ralat; kiraan taruhan mengikut data individu terkait</p>
                    <p>3 Kalau acara belum berlangsung di date ditetapkan, belum rematch selepas 36 jam dari masa mula; semua slip terkait sebagai tak sah, refund</p>
                    <p>4 Kalau interrupted tanpa kembali berlangsung dalam 36 jam dari masa mula, taruhan berkeputusan kekal sah, sebaliknya tak sah</p>
                    <p>5 Kalau tempat acara berubah, semua taruhan tak sah</p>
                    <p>6 Taruhan Quarter/Halftime, harus selesai Quarter maka slip sebagai sah; kecuali dinyatakan sebaliknya dalam Hukum Sports individu</p>
                    <p>7 Kalau acara berlangsung awal dari masa ditetapkan; taruhan sebelum mula kekal sah dan sebaliknya tak sah (inPlay ialah perkara lain)</p>
                    <p>8 Taruhan delay (Sudden Death): Iaitu taruhan preMatch yang diterima atas any faktor selepas bermula. Kalau inPlay tak tutup; slip taruhan selepas masa itu sebagai Sudden Death</p>
                `,
			icon:'game-logo-basketball'
		},
		{
			title:'Bola Sepak',
			value:
				`
                    <p>1 Date dan masa mula Football di web sebagai rujukan. Kalau acara belum mula di masa ditetapkan, belum mula di hari sama (masa tempatan); taruhan terkait tak sah, refund. Kes pengecualian adalah sama ada acara suspend atau postpone, taruhan salah 1 team promoted atau Outrights di World Cup kekal sah</p>
                    <p>2 Kalau suspend ketika berlangsung, acara hari itu (dalam 36 jam) belum tamat, (waktu tempatan); taruhan keputusan terkait tak sah, refund</p>
                    <p>a Kalau suspend ketika berlangsung, acara hari itu belum tamat (waktu tempatan). Asalkan 1st Half telah tamat, taruhan terkait sah. Kalau acara yang suspend tamat di hari itu, (waktu tempatan), kebanyakan taruhan lain yang tak terkait dengan 1st Half sebagai tak sah. Ianya termasuk tanpa terhad kepada hal berikut: Corner, Offside, sent-off, Red/Yellow card, skor 2 teams, Correct Score, Masa Goal Ke-1, Clean Sheet dan pemain tertentu menskor/belum menskor</p>
                    <p>b Kalau 2 teams telah menskor tapi acara suspend, taruhan menskor tak sah. Kalau acara suspend, taruhan any pemain yang menskor/tak menskor tak sah. Sama ada goal menskor, kalau acara suspend, taruhan pada skor Goal Ke-1 bila-bila masa tak sah. Kalau acara suspend, taruhan semua Corner/Offside tak sah, sama ada skor telah melebihi Over/Under dibuka. Kalau pemain menskor, sama ada acara suspend, taruhan yang terkait dengan 1st Half tak sah. Kalau acara suspend, tapi ada 1 goal di sebelumnya. Tentang taruhan team menskor terdahulu/selepas itu sebagai sah</p>
                    <p>3 Kalau market ada item Draw, kami hanya membayar taruhan Draw ketika keputusan Draw, taruhan any pemenang team adalah lose</p>
                    <p>4 Kiraan taruhan Football mengikut masa Regular (termasuk Injury), kecuali dinyatakan sebaliknya; tak termasuk Extratime, keputusan Penalty Kick</p>
                    <p>5 Over/Under market: Harus selesai acara Fulltime, taruhan Total Goals sebagai sah</p>
                    <p>6 Kalau nilai Football Over/Under adalah sama, akan refund</p>
                    <p>7 Kalau tempat Home ditukar ke Neutral atau sebaliknya, semua taruhan kekal sah. Kalau Home diubah ke tempat asal Away, taruhan tak sah. Kalau nama Home dan Away terbalik, taruhan tak sah</p>
                    <p>8 Kiraan slip mengikut keputusan rasmi atau keputusan acara penilaian badan berkuasa</p>
                    <p>9 Taruhan delay (Sudden Death): Iaitu taruhan preMatch yang diterima atas any faktor selepas bermula. Kalau inPlay tak tutup; slip taruhan selepas masa itu sebagai Sudden Death</p>

                `,
			icon:'game-logo-football'
		},
		{
			title:'FIFA',
			value:
				`
                    <p>1 Kiraan keputusan Handicap inPlay dinilai mengikut antara ketika bertaruh dan langsungan acara/selepas tamat; iaitu skor masa tamat ditolak skor taruh semasa. Taruhan 1st Half inPlay Handicap mengikut keputusan tamat 1st Half</p>
                    <p>2 Kiraan baki masa win dinilai mengikut antara ketika bertaruh dan langsungan acara/selepas tamat; iaitu skor masa tamat ditolak skor taruh semasa. Taruhan 1st Half baki masa win mengikut keputusan tamat 1st Half</p>
                `,
			icon:'game-logo-fifa'
		},

	],
	zhuboPan: [
		{
			title:'1 Hukum',
			value:
				`
                    <p>
						Hukum berikut utk semua lawanan,pasaran/jenis taruh.Web umum "terma dan syarat" sesuai utk "peraturan" ini
                    </p>
                `,
			icon:false
		},
		{
			title:'2 Umum',
			value:
				`
                    <p>a Kami bekalkan info taruh secara ikhlas.Tak bertanggungjawab atas tarikh,masa,lokasi,saingan,Odds,keputusan,stats(tertera pada video langsung)atau taruh info ralat atau terkurang</p>
                    <p>b Kami berkuasa ubah ralat,mengambil tindakan demi pastikan integriti dlm kendalian pasaran.Pasaran definisi sbg jenis taruh game Anchor. Kami berkuasa dlm keputusan terakhir</p>													
                    <p>c Klien harus faham skor,info lawanan semasa dan pastikan situasi sebelum taruh</p>
                    <p>d Kami berkuasa sunting hukum dlm situasi khas,sah setelah umum di web</p>
                    <p>e Klien mengaku web skor,masa dll. dari pihak ketiga "siaran," tapi masih kelewatan atau tak tepat.Klien bertanggungjawab semua risiko taruh mengikut info ini.Syarikat tak pastikan ketepatan info,integriti dan masa,serta tak bertanggungjawab pada kerugian klien yang bergantung padanya(langsung atau tak langsung)</p>
                    <p>f Lawanan Anchor ialah Anchor padan lawanan Random Passerby melalui game rasmi Klien</p>
                    <p>g Syarat Anchor utk lawanan Anchor,bukan liga professional.Apabila hukum biasa dan game khas(Cth:LoL,PUBG,CS:GO dll.)berkonflik,mengikut hukum game khas </p>
                `,
			icon:false
		},
		{
			title:'3 Kesahan',
			value:
				`
                    <p>a Jika Anchor tak masuk(kecuali sebelum lawanan)dan keluar permainan formal,atau pemain lain keluar sebabkan game tak diteruskan,tak dikira sah.Pasaran ini utk lawanan sah,lawanan sah seterusnya masih sah</p>
                    <p>b Jika game lain mula,bukannya Anchor,game tak dikira sah.Pasaran ini utk lawanan sah,lawanan seterusnya masih sah</p>
                    <p>c Jika masa main Anchor < separuh tempoh game,game tak sah dan refund</p>												
                    <p>d Game ralat,pihak lain sengaja awal menyerah diri,taruhan mungkin dibatal</p>
                    <p>e Jika berniat hang sistem sebabkan masa main ralat,taruhan mungkin dibatal </p>
                    <p>f Jika berniat jahat atau sengaja dikalahkan,taruhan mungkin dibatal</p>
                `,
			icon:false
		},
		{
			title:'4 Hukum am',
			value:
				`
                    <p class="title">• Sah/sebahagian sah</p>													
                    <p>Anchor harus sendiri beroperasi > separuh masa game,sebaliknya refund</p>
                    <p class="title">• Refund,tertangguh</p>
                    <p>Anchor bukan game tertentu atau game tanpa pasaran sah,sbg belum mula</p>
                    <p class="title">• Payout</p>
                    <p>Mengikut keputusan rasmi</p>
                `,
			icon:false
		},
		{
			title:'5 League of Legends',
			value:
				`
                    <p class="title"> • Penentuan,tertangguh</p>
                    <p>a Menentukan game dlm 5minit jika sah</p>
                    <p>b Lawanan tamat sebelum 5min(boleh lawan semula),sbg tertangguh</p>
                    <p>c Harus main League of Legends 5 vs 5 klasik(hukum disunting/mod tak sah)</p>
                    <p class="title"> • Syarat pasaran：</p>
                    <p>Semua market harus mengikut kelayakan permainan,sebaliknya refund. Syarat LoL:</p>
                    <p>a Anchor rank:Anchor harus bermain mengikut rank poin dan deskripsi padanan kata,spt berikut:</p>
                    <p class="rules1 rules-image"></p>
                    <p>b Anchor solo:Padankan Anchor solo,mengikut urutan imej rasmi,spt berikut:</p>
                    <p class="rules2 rules-image"></p>
                    <p>c Anchor harus guna akaun tertentu kami,mengikut imej yang jelas bezakan akaun Anchor,spt berikut:</p>
                    <p class="rules3 rules-image"></p>
                    <p>d Anchor harus main LoL rasmi.Ia bukan lobi persendirian atau hukum game yang disunting,mengikut mod imej jelas,spt berikut(imej lobi persendirian):</p>
                    <p class="rules4 rules-image"></p>
                    <p class="title"> • Payout</p>
                    <p>a Pasaran taruh mengikut keputusan rasmi,spt berikut:</p>
                    <p class="rules5 rules-image"></p>
                    <p>b Mengikut imej terakhir jika tiada keputusan rasmi</p>
                    <p class="rules6 rules-image"></p>
                    <p>c Mengikut keputusan rasmi jika tiada imej terakhir atau imej berkonflik:</p>
                    <p class="rules7 rules-image"></p>
                    <p>d Keputusan Anchor sbg gagal jika petua Deserter(pemain lain tak dipengaruhi hukum),spt berikut:</p>
                    <p class="rules8 rules-image"></p>
                    <p>e Jika Anchor keluar,mengikut keputusan terakhir(Deserter tanda gagal). spt imej:</p>
                    <p class="rules8 rules-image"></p>
               `,
			icon:'game-logo-lol'
		},
		{
			title:'6 King of Glory',
			value:
				`															
                    <p class="title"> • Penentuan,tertangguh</p>
                    <p>a Menentukan game dlm 1minit jika sah</p>
                    <p>b Lawanan tamat dlm 1minit,tangguh ke seterusnya</p>
                    <p>c Harus main King of Glory 5 vs 5 klasik(hukum disunting/mod tak sah)</p>
                    <p class="title"> • Syarat pasaran</p>
                    <p>Semua market harus mengikut kelayakan permainan,sebaliknya refund.Syarat KoG:</p>
                    <p>a Anchor rank,mengikut urutan deskripsi,spt berikut:</p>
                    <p class="rules9 rules-image"></p>
                    <p>b Anchor harus guna akaun tertentu kami,mengikut imej yang jelas bezakan akaun Anchor,spt berikut:</p>
                    <p class="rules9 rules-image"></p>	
                    <p>c Anchor solo,mengikut urutan bil. pemain.Tak layak spt berikut:</p>
                    <p class="rules10 rules-image"></p>	
                    <p class="title"> • Payout</p>
                    <p>a Pasaran taruh mengikut keputusan rasmi,spt berikut:</p>
                    <p class="rules11 rules-image"></p>
                    <p>b Mengikut imej terakhir jika tiada keputusan rasmi:</p>
                    <p class="rules12 rules-image"></p>
                    <p>c Mengikut keputusan rasmi jika imej terakhir tak berasas atau imej berkonflik:</p>
                    <p class="rules11 rules-image"></p>	
                    <p class="title"> • Batal,refund dan kes khas:</p>
                    <p>Anchor keluar,tanpa papar keputusan dan menilai,keputusan lain mengikut imej terakhir sebelum keluar</p>
                `,
			icon:'game-logo-aov'
		},
	],
};
