export default {
	tabMenu: {
		gunqiu: 'InPlay',
		zaopan: 'Awal',
		chuanguan: 'Parlay',
		guanjun: 'Juara',
		zhubo: 'Anchor',
		shoucang: 'Kumpul',
		saiguo: 'Keputusan',
		sports: 'VR Football',
	},
	listItem: {
		gunqiu: 'InPlay',
		chuanguan: 'Parlay',
		zhubosaishi: 'Anchor',
		junei: 'Dlm',
		kaishouzhong: 'On sale',
		gunqiuzhong: 'Live',
		weikaishi: 'Belum mula',
		tigong: 'Beri',
		jiezhi: 'Tamat',
		dianjishouqi: 'Klik tutup',
		chakangengduo: 'Semak lagi',
		jijiangKaisai: "Akn mula",
		yujikaisai: "Expected to start",
		briefYuji: "Expected start time",
		type: "Taip",
        common: "All play",
        trend: "Trend",
        special: "Special",
        mix: "Mix" ,
		hoPTarlay: "Hot IG Parlay",
		Winner: "Menang"
	},
	endGame: {
		yiquxiao: 'Terbatal',
		zhubosaishi: 'Anchor',
		finished: 'Tiada lagi~',
		notData: 'Tiada lagi',
		notDataSub: 'Jom semak tempat lain~',
		yijieshu: 'Tamat',
		ping: 'Seri',
		servicing: 'Pembaikan',
	},
	chooseDate: {
		jinri: 'Hari ini',
		zhouri: 'Ahad',
		zhouyi: 'Isnin',
		zhouer: 'Selasa',
		zhousan: 'Rabu',
		zhousi: 'Khamis',
		zhouwu: 'Jumaat',
		zhouliu: 'Sabtu',
		shangyitian: 'Awal',
		xiayitian: 'Berikut',
		qitariqi: 'Tarikh lain',
		allriqi: 'Semua Tarikh',
	},
	gameCollapse: { queding: 'Pasti', quanxuan: 'Semua', qingchu: 'Padam' },
	list: {
		finished: 'Tiada lagi~',
		notData: 'Tiada lagi',
		notDataSub: 'Jom semak tempat lain~',
		collectNotData: 'Tiada lawanan',
		collectNotDataSub: 'Tiada lawanan',
	},
	matchFilter: {
		notData: 'Tiada lagi',
		finished: 'Jom semak tempat lain~',
		matchFilter: 'Pilihan liga',
		normal: 'Mainan',
		champion: 'Juara',
	},
	searchInput: { 
		search: 'Isi nama tim', 
		cancel: 'Batal',
        league: 'Liga',
		searchBtn: 'Cari'
	},
	invalid: 'Tak sah.Log masuk lagi',
	top: 'Atas',
	gameType: {
        common:'eSports',
        football:'VR Football',
    },
    sportsEndGame:{
        qi: '{no}th',
        finished: 'Tiada lagi~',
    },
	bet: 'bet',
};
