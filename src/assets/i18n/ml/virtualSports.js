export default {
    footballRanking: {
        Leaderboard: 'Rank',
        Team: 'Tim',
        MP: 'Lawanan',
        WDL: 'MKS',
        Pts: 'Poin',
    },
    groupKnockout: {
        Round1: '8th-finals',
        QuarterFinal: '1/4 final',
        Semifinals: 'Semifinals',
        Final: 'Final',
        zanwu: 'Tiada ',
    },
    groupMatches: {
        zu: 'Grup {no}',
        Team: 'Tim',
        MP: 'Lawanan',
        WDL: 'MKS',
        jin: 'GF',
        shi: 'GA',
        jingshengqiu: 'GD',
        Pts: 'Poin',
    },
    knockout: {
        GroupStage:'Grup lawanan',
        KnockoutStage:'Knockout',
    },
    video:{
        batchNo:'Pusingan {no}',
        dateNo:'{no} th',
        matchEnd:'Tlh tamat',
    },
    detail:{
        historyRecord:'Rekod',
    },
    endGame:{
        tips1:"Semak result OB E-Sports di sini",
        tips2:"Masuk ke {0} untuk semak keputusannya",
        tips3:"Tak akn papar lagi ",
    },
};
