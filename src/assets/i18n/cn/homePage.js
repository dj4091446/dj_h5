export default {
    tabMenu: {
        gunqiu: '滚球',
        zaopan: '早盘',
        chuanguan: '串关',
        guanjun: '冠军',
        zhubo: '主播',
        shoucang: '收藏',
        saiguo: '赛果',
        sports: 'VR足球',
    },
    listItem: {
        gunqiu: '滚球',
        chuanguan: '串关',
        zhubosaishi: '主播赛事',
        junei: '局内',
        kaishouzhong: '开售中',
        gunqiuzhong: '滚球中',
        weikaishi: '未开始',
        tigong: '提供',
        jiezhi: '截止',
        dianjishouqi: '点击收起',
        chakangengduo: '查看更多',
        jijiangKaisai: "即将开赛",
        yujikaisai: "预计开赛",
        briefYuji: "预计开赛",
        type: "玩法分类",
        common: "全部玩法",
        trend: "热门玩法",
        special: "特殊玩法",
        mix: "复合玩法" ,
        hoPTarlay: "热门局内串关",
        Winner: "获胜"
    },
    endGame: {
        yiquxiao: '已取消',
        zhubosaishi: '主播赛事',
        finished: '没有更多数据了～',
        notData: '暂无更多赛事',
        notDataSub: '去其它地方看看吧~',
        yijieshu: '已结束',
        ping: '平',
        servicing: "游戏维护中",
    },
    chooseDate: {
        jinri: '今日',
        zhouri: '周日',
        zhouyi: '周一',
        zhouer: '周二',
        zhousan: '周三',
        zhousi: '周四',
        zhouwu: '周五',
        zhouliu: '周六',
        shangyitian: '上一天',
        xiayitian: '下一天',
        qitariqi: '其他日期',
        allriqi: '全部日期',
    },
    gameCollapse: { queding: '确 定', quanxuan: '全选', qingchu: '清 除' },
    list: {
        finished: '没有更多数据了～',
        notData: '暂无更多赛事',
        notDataSub: '去其它地方看看吧~',
        collectNotData: '暂无收藏的赛事',
        collectNotDataSub: '暂无收藏的赛事',
    },
    matchFilter: {
        notData: '暂无更多赛事',
        finished: '去其它地方看看吧~',
        matchFilter: '联赛筛选结果',
        normal: '普通玩法',
        champion: '冠军玩法',
    },
    searchInput: {
        search: '请输入战队名进行搜索',
        cancel: '取消',
        league: '联赛',
        searchBtn: '搜索'
    },
    invalid: '登录失效，请重新登录',
    top: '顶部',
    gameType: {
        common:'普通赛事',
        football:'VR足球',
    },
    sportsEndGame:{
        qi: '{no}期',
        finished: '没有更多数据了～',
    },
    bet: '注单',
};
