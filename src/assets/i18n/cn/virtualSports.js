export default {
    footballRanking: {
        Leaderboard: '排行榜',
        Team: '球队',
        MP: '比赛',
        WDL: '胜/平/负',
        Pts: '积分',
    },
    groupKnockout: {
        Round1: '16强',
        QuarterFinal: '1/4决赛',
        Semifinals: '半决赛',
        Final: '决赛',
        zanwu: '暂无',
    },
    groupMatches: {
        zu: '{no} 组',
        Team: '球队',
        MP: '比赛',
        WDL: '胜/平/负',
        jin: '进',
        shi: '失',
        jingshengqiu: '净胜球',
        Pts: '积分',
    },
    knockout: {
        GroupStage:'小组赛',
        KnockoutStage:'淘汰赛',
    },
    video:{
        batchNo:'第{no}轮',
        dateNo:'{no}期',
        matchEnd:'已完赛',
    },
    detail:{
        historyRecord:'历史战绩',
    },
    endGame:{
        tips1:"一般电竞游戏赛果可在此查看",
        tips2:"如果查看电子游戏赛果，请进入{0}查看",
        tips3:"下次不再显示",
    },
};
