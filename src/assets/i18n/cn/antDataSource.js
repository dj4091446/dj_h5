/*
 * @Author: Supermark
 * @Date: 2022-08-03 17:49:57
 * @Description: 蚂蚁数据源中文简体翻译
 * @FilePath: /djGameH5/src/assets/i18n/cn/antDataSource.js
 */
export default {
  Bet: '投注',
  Scene: '实况',
  VideoLS: '视频',
  AnimationLS: '动画',
  Prescient: '前瞻',
  RecentData: '近期数据',
  HistoryFight: '历史交手',
  GameStat: '统计场次',
  RecentStat: '各队近期数据',
  FightMatchRecord: '交手/比赛记录',
  BigBattle: '大局',
  SmallBattle: '小局',
  FirstBloodRate: '一血率',
  FirstTowerRate: '一塔率',
  RoundNum265: '回合数>26.5',
  PentakillRate: '五杀率',
  DecakillRate: '十杀率',
  FirstBarrackRate: '首兵营率',
  AverageTotalDuration: '场均总时长',
  AverageKill: '场均击杀',
  Data: '数据',
  Baron: '大龙',
  DrakeDragon: '小龙',
  TurretTowerDive: '推塔',
  Crystal: '水晶',
  EconomyDifference: '经济差',
  NoData: '暂无数据',
  ExperienceDifference: '经验差',
  FirstBaronRate: '首小龙率',
  FirstDragonRate: '首大龙率',
  Kill: '击杀',
  FightData: '交手数据',
  Player: '选手',
  Output: '输出',
  Experience: '经验',
  LastHitDeny: '正/反补',
  TotalOutput: '总输出',
  DamageModifierRate: '伤害转化率',
  HeroOutput: '英雄输出',
  KillMonsters: '击杀野怪',
  LastHit: '补刀',
  VisionRemovalWard: '插眼/拆眼',
  DamageTaken: '承伤',
  TrueDamage: '真实伤害',
  MagicalOutput: '魔法输出',
  PhysicalOutput: '物理输出',
  FirstTyrantRate: '首暴君率',
  FirstOverlordRate: '首主宰率',
  Economy: '经济',
  ParticipationRate: '参团率',
  ItemSets: '出装',
  Situation: '战况',
  Round1: '第1回合',
  FirstBaron: '首小龙',
  FirstDragon: '首大龙',
  FirstPentakill: '首五杀',
  FirstDecakill: '首十杀',
  MatchStart: '比赛开始',
  All: '全部',
  Healing: '治疗',
  AbnormalDataInfoGameDataClosed: '赛事数据信息异常，已关闭本场赛事数据！',
  CloseDataWindow: '关闭数据窗口',
  RoundNo: '第{no}局',
  DestroyTowerCrystal: '摧毁防御塔/水晶',
  KillHero: '击杀英雄',
  Barrack: '兵营',
  RuleDescription: '数据展示规则说明',
  DescriptionDataDisplayRules: '此版面显示的所有直播内容仅供参考！会员亦可使用此内容为辅助指南。我们将尽最大努力确保显示的内容准确，如果有误，本公司将不承担任何责任。例如：赛事滚球时，让分盘口，将以投注时在注单中显示的正确分值为准！',
  DescriptionDataDisplayRulesRemark: '备注：展示的赛事数据信息仅供参考，不做赛果依据！',
  // 暂时缺失部分
  FirstBlood: '一血',
  FirstTower: '一塔',
  FirstHerald: '首先锋',
  FirstCrystal: '首水晶',
  FirstBarrack: '首兵营',
  Kill265: '击杀>26.5率',
  GameTimeOver33: '游戏时间>33',
  Kill475: '击杀>47.5',
  GameTimeOver35: '游戏时间>35',
  Kill235: '击杀>23.5',
  GameTimeOver19: '游戏时间>19',
  SecondHandgun: '第二手枪局胜率',
  More265Rounds: '回合>26.5',
  GlobalConference: '全局胜率',
  SingleConference: '单局胜率',
  FirstCrystalRate: '首水晶率',
  FirstMeatMountain: '首肉山',
  DrakeKill: '小龙击杀',
  PlayerKill: '队员击杀',
  RiftHeraldKill: '击杀先锋',
  DrakesKill: '击杀主宰',
  JoinGame: '加入比赛',
  QuitGame: '离开比赛',
  RoundStart: '回合开始',
  RoundWin: '回合胜利',
  Suicide: '自杀',
  Bomb: '放置炸弹',
  DisarmBomb: '炸弹拆除',
  CloseDataWindow : '关闭数据窗口吧～',
  BlueEconomy: '蓝方经济：',
  OnlyCurrentLg: '只看当前联赛',
  Last10Games: '近10场数据',
  zhengbu: '正补',
  fanbu: '反补',

  // 后续使用
  R1Gunwin: 'R1手枪胜',
  R16Gunwin: 'R16手枪胜',
  The1sWin5Rounds: '先胜五轮',
  The1stWin10Rounds: '先胜十轮',
  EnterTheOT: '进入加时赛',
  Overtime: '加时',
  // NoMap: '暂无地图',
  // Map: '地图',
  FirstHalf: '上半场',
  SecondHalf: '下半场',
  Annihilation: '全歼',
  BombDisposal: '拆弹',
  Explosion: '爆炸',
  TimeOut: '超时',
  

  // 暂时没使用
  // TotalKill: '总击杀',
  // OnlyCurrentLg: '只看当前联赛',
  // WinRate: '胜率',
  // FightRecord: '交手记录',
  // Last10Games: '近10场数据',
  // LiveStream: '直播',
  // ThisGameWinRate: '本场比赛胜率',
  // No: '无',
  // Processing: '进行中',
  // GameOver: '比赛已结束',
  // GameTime: '游戏时间',
  AddPoints: '加点',
  // NoAnimationLS: '暂无动画直播',
  Overlord: '主宰',
  Tyrant: '暴君',
  // RealTime: '实时',
  // Score: '比分',
  // GamesTime: '比赛时间',
  // SmallBattleSituation: '小局战况',
  // BigBattleSituation: '大局战况',
  // Round: '回合',
  // FirstTyrant: '首暴君',
  // FirstOverlord: '首主宰',
  // Topsolo: '上单',
  // Jungle: '打野',
  // Solo: '中单',
  // Support: '辅助',
  // Position1: '1号位',
  // Position2: '2号位',
  // Position3: '3号位',
  // Position4: '4号位',
  // Position5: '5号位',
  // bet: '投注',
  // Battle: '比赛',
  // Time: '时间',
  // Duration: '时长',
  // Schedule: '赛程',
  // GameDuration: '游戏时长',
  // BattleData: '比赛数据',
  // Close: '关闭',
  // PlsCloseGameData: '请关闭赛事数据',
  // OverView: '概况',
  AchieVements: '战绩',
  // RealTimeWinRate: '实时胜率',
  // LayAside: '收起',
  // UpdateTime: '更新时间',
  // RecentDataDetails: '近期数据详情',
  // Lead103K: '领先10.3K',

  Gamelogs: "比赛日志",
  Enter: "进入",
  Exit: "退出",
  Roundstart: "回合开始",
  Winround: "回合胜利",
  Suicide: "自杀",
  Placebomb: "放置炸弹",
  Defusebomb: "炸弹拆除",
};