export default {
    stationOrder: {
        corss: '串1 (局内串关)',
        return: '预计返还',
        placeholder: '输入金额',
        oddsChange: '您所选的投注项的盘口、赔率或有效性产生变化',
        total: '总投注额',
        maxReturn: '最高返还',
        stationTips: '局内串关，单局中最多选择10个选项',
        full: '(已满)',
        tips1: '提示: 单局串关注单数量需≥2',
        tips2: '提示: 同局中同名盘口，不能进行局内串关',
        tips3: '提示: 同盘口的投注项之间，暂不支持局内串关',
        tips4: '提示: 串关注单总赔率已经达到上限，不能添加更多选项了！ ',
        tips5: '提示: 投注超限盘口，暂不支持局内串关',
        limitOver:
            '当前局内串关已超限红，请选择其他盘口、或删减串关选项数量在进行投注',
        oddsNew: '自动接受<br/>最新赔率',
        showLimit5: 
            '单日派彩总金额超限，请查看预计返还金额，或调整投注金额，欢迎明日再来投注!',
    },
    betOrder: {
        return: '预计返还',
        betMax: '不能再选了，最多选择10个注单',
        placeholder: '输入金额',
        matchLimit: '赛事限额',
        marketLimit: '盘口限额',
        corssOptions: '过关选项',
        hasLight: '加亮的选项不可串成过关投注',
        oddsChange: '您所选的投注项的盘口、赔率或有效性产生变化',
        balance: '余额不足',
        showLimit1:
            '当前投注额已满, 请您选择其他盘口，赛事或删减串关选项数量再进行投注',
        showLimit2:
            '当前注单比赛的可投额度已达上限，请选择其它比赛的盘口进行串投',
        showLimit3:
            '当前投注额已满，请您选择其他盘口、赛事或删减串关选项数量再进行投注',
        showLimit4: '可投额度已达上限，请增加或删除串关单数，或明日再试',
        showLimit5: 
            '单日派彩总金额超限，请查看预计返还金额，或调整投注金额，欢迎明日再来投注!',
        length2: '至少选择2个以上的不同赛事',
        total: '总投注额',
        maxReturn: '最高返还',
        acceptOdds1: '自动接受最新赔率',
        acceptOdds2: '自动接受更高赔率',
        acceptOdds3: '永不接受赔率变动',
        daily: '今日',
        corss: '串',
    },
    index: {
        stationOrder: '局内串关',
        betOrder: '投注栏',
        delAll: '清空',
        balance: '余额',
    },
    timer: {
        Invalid: '已失效',
    },
};
