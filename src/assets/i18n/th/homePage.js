export default {
	tabMenu: {
		gunqiu: 'เกมสด',
		zaopan: 'เดิมพันล่วงหน้า',
		chuanguan: 'พาร์เลย์',
		guanjun: 'แชมป์',
		zhubo: 'ไลฟ์',
		shoucang: 'ชื่นชอบ',
		saiguo: 'ผลลัพธ์',
		sports: 'VR ฟุตบอล',
	},
	listItem: {
		gunqiu: 'เกมสด',
		chuanguan: 'Parlay',
		zhubosaishi: 'ไลฟ์',
		junei: 'IG',
		kaishouzhong: 'เปิดเดิมพัน',
		gunqiuzhong: 'อยู่ตอนนี้',
		weikaishi: 'ยังไม่เริ่ม',
		tigong: 'จัดเตรียม',
		jiezhi: 'ปิด',
		dianjishouqi: 'คลิกเพื่อปิด',
		chakangengduo: 'ดูเพิ่มเติม',
		jijiangKaisai: "เปิดการแข่งขันเร็วๆนี้",
		yujikaisai: "Expected to start",
		briefYuji: "Expected start time",
		type: "พิมพ์",
        common: "ทุกเกมส์",
        trend: "เกมส์ดัง",
        special: "เกมส์พิเศษ",
        mix: "เกมส์รวม" ,
		hoPTarlay: "Hot IG Parlay",
		Winner: "ทีมชนะเลิศ"
	},
	endGame: {
		yiquxiao: 'ยกเลิก',
		zhubosaishi: 'ไลฟ์',
		finished: 'ไม่มีข้อมูลเพิ่มเติม',
		notData: 'ไม่มีการแข่งขันเพิ่มเติม',
		notDataSub: 'ไปดูที่อื่น',
		yijieshu: 'จบ',
		ping: 'เสมอ',
		servicing: 'เกมอยู่ระหว่างการปรับปรุง',
	},
	chooseDate: {
		jinri: 'วันนี้',
		zhouri: 'อา',
		zhouyi: 'จ',
		zhouer: 'อ',
		zhousan: 'พ',
		zhousi: 'พฤ',
		zhouwu: 'ศ',
		zhouliu: 'ส',
		shangyitian: 'วันก่อน',
		xiayitian: 'วันถัดไป',
		qitariqi: 'วันอื่นๆ',
		allriqi: 'วันที่ทั้งหมด',
	},
	gameCollapse: { queding: 'OK', quanxuan: 'เลือกทั้งหมด', qingchu: 'ลบ' },
	list: {
		finished: 'ไม่มีข้อมูลเพิ่มเติม',
		notData: 'ไม่มีการแข่งขันเพิ่มเติม',
		notDataSub: 'ไปดูที่อื่น',
		collectNotData: 'ไม่มีรายการโปรด',
		collectNotDataSub: 'ไม่มีรายการโปรด',
	},
	matchFilter: {
		notData: 'ไม่มีการแข่งขันเพิ่มเติม',
		finished: 'ไปดูที่อื่น',
		matchFilter: 'ผลลีก',
		normal: 'วิธีเล่นปกติ',
		champion: 'วิธีเล่นชนะเดิมพัน',
	},
	searchInput: { 
		search: 'ป้อนชื่อทีมเพื่อค้นหา', 
		cancel: 'ยกเลิก',
        league: 'ลีก',
		searchBtn: 'ค้นหา'
	},
	invalid: 'เข้าสู่ระบบล้มเหลว กรุณาเข้าสู่ระบบอีกครั้ง',
	top: 'สูงสุด',
	gameType: {
        common:'Esports',
        football:'VR ฟุตบอล',
    },
    sportsEndGame:{
        qi: 'งวด{no}',
        finished: 'ไม่มีข้อมูลเพิ่มเติม',
    },
	bet: 'bet',
};
