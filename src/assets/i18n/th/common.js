const numbers = 'ทั้งแมตช์ | เกม 1 | เกม 2 | เกม 3 | เกม 4 | เกม 5 | เกม 6 | เกม 7 | เกม 8 | เกม 9 | เกม 10 | เกม 11 | เกม 12 | เกม 13'
const Mapnums = numbers.split('|')
export default {
	mixins: {
		live: 'เกมสด',
		early: 'เดิมพันล่วงหน้า',
		cross1: 'ไม้ 1',
		betFail: 'เดิมพันล้มเหลว',
		cross: 'ไม้',
		Global: 'ทั้งเกม',
		copy: 'คัดลอก',
		max30: 'บันทึกได้ถึง 30 เกม',
		same:
			'ในเกมเดียวกันคุณสามารถเพิ่มเดิมพันประเภทเดียวกันได้ 1 รายการเท่านั้น',
		upperLimit:
			'ราคาต่อรองรวมเดิมพันแบบพาร์เลย์ถึงขีดจำกัดสูงสุด ไม่สามารถเพิ่มตัวเลือกอื่นได้อีก!',
		finish: 'อยู่ระหว่างยืนยันเดิมพัน',
		FullTime: 'ทั้งแมตช์',
		HalfTime: 'ครึ่งแรก',
		OneQ: 'Q1',
		TwoQ: 'Q2',
		ThreeQ: 'Q3',
		FourQ: 'Q4',
		champion: 'แชมป์',
		overtime: 'เพิ่มเวลา',
		middleHalf: 'ครึ่งเกม',
		secondHalf: 'ครึ่งหลัง',
		rest1: 'Q1 พัก',
		rest2: 'Q2 พัก',
		rest3: 'Q3 พัก',
		overtime_first_half: 'เพิ่มเวลาครึ่งแรก',
		overtime_second_half: 'เพิ่มเวลาครึ่งหลัง',
		mid_rest: 'พักครึ่ง',
		Mapnum: (ctx) => Mapnums[ctx.named('num')],
		all: 'ทั้งหมด',
	},
	commonInput: {
		balance: 'ยอดเงินไม่เพียงพอ',
		oddsChange: 'อัตราต่อรองทั้งหมดมีการเปลี่ยนแปลง',
		redLimit: 'เกินขีดจำกัด',
	},
	message: { closeMessage: '( ปิดอัตโนมัติหลังจาก 2 วินาที)', close: 'ปิด' },
	betItem: {
		nocross: 'ตลาดไม่รองรับพาร์เลย์ โปรดเลือกเดิมพันอื่นที่มีเครื่องหมาย "ไม้"',
	},
	commonNotData: { golive: 'ไปดูที่ตารางแข่ง' },
	commonTabs: { global: 'ทั้งเกม' },
	listLoad: 'กำลัง โหลด~',
	numberBoard: { max: 'สูงสุด', thousand: 'k' },
	selectOdds: { select: 'เลือกวิธีรับ', changyong: 'เงินทั่วไป' },
	digitalToChinese: {
		number1: 1,
		number2: 2,
		number3: 3,
		number4: 4,
		number5: 5,
		number6: 6,
		number7: 7,
		number8: 8,
		number9: 9,
		number10: 10,
		number11: 11,
	},
	systemModel: { notice: 'Notice', confirm: 'OK', yes: "yes", no: "no" },
	awardAnimate: {
		jibai:
			'<span>คลิก</span>\n<span>ล้มเหลว</span>\n<span>99%</span>\n<span>ของ</span>\n<span>คน</span>',
		qiji:
			'<span>แปลก</span>\n<span>ติดตาม</span>\n<span>สมาคม</span>\n<span>สร้าง</span>\n<span>โดย</span>',
	},
	menu: { 
		all: 'ทั้งหมด',
		specials: 'Spesials',
	},
	tips: {
        max30days: "สามารถค้นหาสลิปเดิมพันภายใน 30 วันเท่านั้น",
        qiehuanxianlu: "เปลี่ยนเครือข่าย",
        qiehuanxianlutishi: "เครือข่ายปัจจุบันมีผู้ใช้หนาแน่น โปรดคลิกปุ่มรีเฟรชเพื่อเปลี่ยนเครือข่าย",
        shuaxing: "รีเฟรช"
    },
};
