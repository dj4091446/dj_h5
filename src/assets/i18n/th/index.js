import homePage from './homePage.js'
import rulesPage from './rulesPage.js'
import userPage from './userPage.js'
import marketPage from './marketPage.js'
import betCart from './betCart.js'
import common from './common.js'
import historyOrder from './historyOrder.js'
import guide from './guide.js'
import virtualSports from './virtualSports.js'
import antDataSource from './antDataSource.js'
import indexLottery from './indexLottery.js'
import asiad  from './asiad.js'
export default {
    homePage,
    rulesPage,
    userPage,
    marketPage,
    betCart,
    common,
    historyOrder,
    guide,
    virtualSports,
    antDataSource,
    asiad,
    ...indexLottery
}