export default {
    footballRanking: {
        Leaderboard: 'ลีดเดอร์บอร์ด',
        Team: 'ทีมฟุตบอล',
        MP: 'การต่อสู้',
        WDL: 'ชนะ/แพ้/เสมอ',
        Pts: 'จุด',
    },
    groupKnockout: {
        Round1: 'รอบ 1/16',
        QuarterFinal: 'รอบก่อนรอง',
        Semifinals: 'รอบรองชนะเลิศ',
        Final: 'รอบสุดท้าย',
        zanwu: 'ไม่มีข้อมูล',
    },
    groupMatches: {
        zu: 'ตาราง {no}',
        Team: 'Team',
        MP: 'MP',
        WDL: 'W/D/L',
        jin: 'GF',
        shi: 'GA',
        jingshengqiu: 'GD',
        Pts: 'Pts',
    },
    knockout: {
        GroupStage:'รอบแบ่งกลุ่ม',
        KnockoutStage:'รอบคัดเลือก',
    },
    video:{
        batchNo:'รอบ {no}',
        dateNo:'งวด{no}',
        matchEnd:'ที่เสร็จเรียบร้อย',
    },
    detail:{
        historyRecord:'บันทึก',
    },
    endGame:{
        tips1:"สามารถดูผลการแข่งขัน e-sports ทั่วไปได้ที่นี่",
        tips2:"หากดูผลลัพธ์ของSpesialsโปรดไปที่ {0} เพื่อดู",
        tips3:"ไม่แสดงในครั้งต่อไป",
    },
};
