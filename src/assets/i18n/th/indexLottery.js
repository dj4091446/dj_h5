
const teseLol = {
    heros: {
      "21": {
        "name": "Garen",
        "nickName": "พลังแห่งเดมาเซีย",
        "description": "Garen เป็นนักรบชั้นสูงผู้ที่ภาคภูมิใจในตัวตนของเขา เขาเป็นสมาชิกของ Dauntless Vanguard ซึ่งเป็นที่ชื่นชอบในหมู่ลูกน้อง เป็นที่เคารพยำเกรงในหมู่ศัตรู เขาเป็นทายาทของตระกูล Crownguard ที่มีเกียรติซึ่งถูกมอบหมายหน้าที่ให้ปกป้องผืนแผ่นดินและอุดมการณ์ของ Demacia ไว้ ด้วยเกราะต้านเวทมนตร์และดาบใหญ่ Garen พร้อมที่จะประจันหน้ากับนักเวทในสนามรบด้วยพายุหมุนโลหะแห่งความถูกต้อง"
      },
      "22": {
        "name": "Leona",
        "nickName": "เทพีแห่งรุ่งอรุณ",
        "description": "เต็มเปี่ยมไปด้วยเปลวเพลิงแห่งดวงตะวัน Leona เป็นผู้นำนักรบแห่งเผ่า Solari ผู้ปกป้องหุบเขา Mount Targon ด้วยดาบ Zenith Blade และโล่ Shield of Daybreak ที่เธอกวัดแกว่ง ผิวของเธอส่องประกายแสงแวววับไปด้วยแสงไฟดวงดารา ดวงตาลุกไหม้ด้วยพลังแห่งสรวงสวรรค์ที่อยู่ภายในร่างของเธอ สวมเกราะสีทองคำ และความรู้อันล้ำค่าที่สืบทอดกันมาแต่โบราณ Leona จักนำพาความรู้แจ้งมาให้ และก็จักนำความตายมาให้แก่ปรปักษ์"
      },
      "31": {
        "name": "Rammus",
        "nickName": "The Armordillo",
        "description": "มีหลายคนที่ชอบ แต่ก็มีบางคนที่ไม่สนใจ ในขณะเดียวกันก็ทำให้หลายคนต้องประหลาดใจ Rammus เป็นสิ่งมีชีวิตลึกลับ ที่มีการป้องกันตัวด้วยเปลือกหุ้มติดหนาม Rammus ก่อให้เกิดทฤษฎีเกี่ยวกับต้นกำเนิดของเขามากมายในทุกที่ที่เขาผ่านไป มีทั้งเป็นกึ่งเทพ เป็นทั้งเทพพยากรณ์ศักดิ์สิทธิ์ หรืออาจเป็นเพียงสัตว์ประหลาดที่มาจากพลังเวทมนตร์ ซึ่งไม่ว่าความจริงจะเป็นเช่นไร Rammus ก็ยังเดินทางไปบนทะเลทราย Shurima ด้วยวัตถุประสงค์ของตนเอง โดยไม่มีใครที่จะสามารถหยุดเขาได้"
      },
      "32": {
        "name": "Sejuani",
        "nickName": "ความโกรธเคืองแห่งแดนเหนือ",
        "description": "Sejuani เป็น Iceborn ผู้นำแห่งเผ่า Winter's Claw หนึ่งในเผ่าที่น่ากลัวที่สุดใน Frejord เพื่อต่อสู้และเอาชีวิตรอดในความหนาวเหน็บ เผ่า Winter's Claw โจมตีและปล้นเสบียงโดยไม่เลือกหน้า ไม่ว่าจะเป็น Noxus, Demacia หรือ Avarosan ต่างก็ต้องพบกับความโหดร้ายของ Sejuani ซึ่งจะเป็นผู้นำการจู่โจมอยู่บนหลังของ Bristle หมูป่า Drüvask คู่ใจของเธอ พร้อมกับเหวี่ยงลูกตุ้มที่ทำจาก True Ice ของเธอเพื่อแช่แข็งและทุบศัตรูของเธอให้แตกเป็นเศษละเอียด"
      },
      "41": {
        "name": "Lee Sin",
        "nickName": "พระตาบอด",
        "description": "ปรมาจารย์ศาสตร์การต่อสู้โบราณของ Ionia Lee Sin เป็นนักสู้ผู้ยึดมั่นในหลักการที่สามารถควบคุมปราณแห่งมังกรเพื่อใช้ในการต่อสู้ได้ ถึงเเม้ว่าเขาจะสูญเสียการมองเห็นไปเมื่อหลายปีก่อน พระนักรบผู้นี้ก็ได้อุทิศตนเพื่อปกป้องดินเเดนบ้านเกิดจากภัยทั้งหลาย ศัตรูที่ประมาทตอนที่เจอเขานั่งสมาธิ ก็จะพบกับหมัดสุดร้อนแรงและท่าเตะกวาดอันทรงพลังของเขา"
      },
      "42": {
        "name": "Irelia",
        "nickName": "นักเต้นใบมีด",
        "description": "การรุกรานของชาว Noxus เป็นสิ่งที่กำเนิดและสร้างวีรบุรุษของชาว Ionia มาหลายยุคสมัย หนึ่งในนั้นคือ Irelia แห่ง Navori นักรบผู้ปรับเปลี่ยนการเต้นระบำพื้นบ้าน ให้กลายเป็นศาสตร์แห่งการต่อสู้ที่ไม่เหมือนใคร ด้วยท่วงท่า การเคลื่อนไหวที่สง่างาม พลางควบคุมมีดที่ลอยอยู่รอบกายได้เป็นอย่างดี เมื่อผู้คนต่างยอมรับในความสามารถของเธอแล้ว กลุ่มต่อต้านก็ผลักดันให้เธอขึ้นเป็นผู้นำของพวกเขา และจนถึงทุกวันนี้ เธอก็ยังคงมุ่งมั่นที่จะดูแลรักษาบ้านเกิดของเธอให้ดีที่สุด"
      },
      "51": {
        "name": "Jax",
        "nickName": "ปรมาจารย์อาวุธ",
        "description": "ไม่มีผู้ใดสามารถทัดทานทักษะการใช้อาวุธที่แปลกประหลาดและวาจาที่เสียดสีของเขาได้ Jax คือปรมาจารย์ด้านการใช้อาวุธคนสุดท้ายของ Icathia หลังจากที่ดินเเดนบ้านเกิดของเขาถูกทำลายให้พังพินาศจากการปลดปล่อย Void Jax และพวกของเขาก็ได้สาบานที่จะปกป้องสิ่งที่ยังเหลืออยู่เพียงน้อยนิด และเมื่อมีการใช้เวทมนตร์บนโลกมากขึ้นเรื่อย ๆ ภัยอันตรายก็เริ่มที่จะก่อตัวอีกครั้ง Jax เดินทางไปทั่ว Valoran ถือตะเกียงที่ส่องแสงสุดท้ายของ Icathia ติดตัว และค้นหาผู้กล้าที่มีความเเข็งเเกร่งมากพอที่จะมายืนหยัดเคียงข้างเขา"
      },
      "52": {
        "name": "Vi",
        "nickName": "นายอำเภอพิลโทเวอร์",
        "description": "Vi อดีตอาชญากรบนถนนคนเถื่อนของ Zaun เธอเป็นคนเลือดร้อน มุทะลุ และ ผู้หญิงที่น่าเกรงกลัว แทบจะไม่มีความเคารพเจ้าหน้าที่แม้แต่น้อย ด้วยความที่เกิดมาตัวคนเดียว Vi จึงได้พัฒนาสัญชาตญาณในการเอาตัวรอดควบคู่ไปกับอารมณ์ขันอย่างร้ายกาจ ตอนนี้เธอทำงานร่วมกับเจ้าหน้าที่ของเมือง Piltover เพื่อรักษาความสงบ เธอสวมใส่หมัดเหล็ก Hextech ขนาดยักษ์ที่แข็งแกร่งขนาดที่ว่าต่อยทะลุกำแพงและผู้ต้องสงสัยได้ด้วยพละกำลังที่ไม่ต่างกัน"
      },
      "61": {
        "name": "Yasuo",
        "nickName": "นักดาบแห่งสายลม",
        "description": "Yasuo เป็นชาว Ionia ผู้แน่วแน่ เขาเป็นนักดาบที่แสนจะว่องไว ที่ใช้อากาศในการต่อสู้กับศัตรู เมื่อตอนที่เขายังหนุ่ม Yasuo เป็นคนทะนงตัว เขาถูกกล่าวหาว่าฆ่าอาจารย์ของตัวเอง—แต่เขาก็ไม่สามารถพิสูจน์ความบริสุทธิ์ของตัวเองได้ เขาถูกบังคับให้สังหารพี่ชายของตัวเองเพื่อเป็นการป้องกันตัว และถึงแม้ในที่สุดฆาตกรตัวจริงจะถูกเปิดเผย เขาก็ไม่สามารถให้อภัยสิ่งที่ตัวเองทำลงไปได้ และตอนนี้เขาได้เร่ร่อนไปในดินแดนบ้านเกิดของเขา โดยมีเพียงสายลมเท่านั้น ที่คอยนำทางดาบของเขา"
      },
      "62": {
        "name": "Riven",
        "nickName": "The Exile",
        "description": "Riven เป็นอดีตนักดาบแห่งกองทัพของ Noxus ตอนนี้เธอเนรเทศตัวเองมาอยู่ในดินแดนที่เธอเคยต่อสู้เพื่อที่จะครอบครอง เธอได้เลื่อนตำแหน่งขึ้นมาจากความแข็งแกร่งจากความทุ่มเทและประสิทธิภาพในการทำลายล้าง จนเธอได้รับรางวัลเป็นดาบรูนในตำนานและกองทัพของตัวเอง อย่างไรก็ตาม ในการสู้รบที่ Ionia ศรัทธาในบ้านเกิดของ Riven ได้ถูกทดสอบและถูกทำลายลงในที่สุด หลังจากตัดความสัมพันธ์ทั้งหมดกับจักรวรรดิ เธอตามหาที่ของตัวเองในโลกที่แตกหัก ถึงแม้เธอจะได้ยินข่าวลือว่า Noxus ได้เปลี่ยนไปแล้วก็ตาม..."
      },
      "71": {
        "name": "Zed",
        "nickName": "เจ้าแห่งศาสตร์มืด",
        "description": "Zed เป็นผู้ที่ไร้เมตตาและความปราณีโดยสิ้นเชิง เขาเป็นผู้นำแห่ง Order of Shadow องค์กรที่เขาสร้างขึ้นมาโดยมีจุดประสงค์ในการใช้เวทมนตร์และศิลปะการต่อสู้โบราณของ Ionia ในทางทหารเพื่อขับไล่ผู้รุกรานชาว Noxus ในช่วงสงคราม ความกดดันและไร้ทางสู้ได้บีบบังคับให้เขาปลดปล่อยร่างเงาที่ถูกเก็บเป็นความลับ วิญญาณแห่งความแค้นที่มีพลังมหาศาล แสนอันตรายและมีความสามารถในการชักนำจิตใจคนสู่ด้านมืด Zed ได้สำเร็จวิชาต้องห้ามทุกแขนงและจะทำลายทุกสิ่งที่เป็นภัยต่อประเทศชาติ และการปกครองของเขา"
      },
      "72": {
        "name": "Katarina",
        "nickName": "ดาบปีศาจ",
        "description": "ด้วยการตัดสินใจที่แน่วแน่และความสามารถในการสังหารคู่ต่อสู้ของเธอ Katarina เป็นมือสังหารที่เก่งกาจที่สุดของ Noxus และเป็นลูกสาวคนโตของแม่ทัพในตำนาน Du Couteau เธอมีเทคนิคในการสังหารเป้าหมายอย่างรวดเร็วโดยที่ฝ่ายตรงข้ามไม่ทันได้รู้สึกตัว อุดมการณ์ที่ร้อนแรงของเธอนั้นเป็นแรงผลักดันให้เธอเข้าท้าทายกับเป้าหมายที่ได้รับการคุ้มกันอย่างแน่นหนา แม้ว่ามันอาจจะเสี่ยงที่จะสร้างภัยอันตรายให้แก่พรรคพวกของเธอก็ตาม --- แต่ไม่ว่าจะเป็นภารกิจแบบใดก็ตาม ก็ไม่อาจทำให้ Katarina นั้นลังเลในการทำหน้าที่ท่ามกลางพายุคมมีดอันคมกริบของเธอได้"
      },
      "81": {
        "name": "Nocturne",
        "nickName": "ฝันร้ายนิรันดร์",
        "description": "ปีศาจที่เกิดขึ้นมาจากฝันร้ายของสิ่งมีชีวิตทั้งปวง Nocturne กลายเป็นจุดเริ่มต้นของความชั่วร้ายทั้งปวง รูปร่างของมันดูเป็นไอน้ำของเหลวที่ดูยุ่งเหยิง เงาที่ไร้ใบหน้าและดวงตาที่เยือกเย็นประกอบกับใบมีดที่ดูโหดร้ายที่ติดอยู่ที่แขน หลังจากที่มันหลุดพ้นออกจากดินแดนแห่งวิญญาณ Nocturne ก็ได้มายังผืนโลกใบนี้เพื่อที่จะดูดกลืนความหวาดกลัวที่จะเกิดขึ้นได้เฉพาะในความมืดที่เเท้จริงเท่านั้น"
      },
      "82": {
        "name": "Evelynn",
        "nickName": "อ้อมกอดแห่งความทุกข์ทรมาน",
        "description": "ภายใต้เงามืดแห่ง Runeterra ปีศาจร้าย Evelynn กำลังออกหาเหยื่อรายต่อไปของเธอ เธอหลอกล่อเหยื่อด้วยรูปร่างหน้าตาอันมีเสน่ห์ของเธอ เมื่อผู้ใดหลงในเสน่ห์เธอแล้ว ร่างที่แท้จริงของเธอก็จะปรากฏออกมา จากนั้นเหยื่อของเธอก็ต้องทรมานอย่างโหดร้ายที่ไม่มีอะไรเทียบเคียงได้ เธอชอบความเจ็บปวดของคนอื่น สำหรับเหล่าปีศาจแล้วการกระทำเหล่านี้ถือเป็นเรื่องปกติ แต่สำหรับ Runeterra แล้วมันเป็นนิทานของปีศาจที่เกิดจากความรักที่บิดเบี้ยวและเป็นสิ่งย้ำเตือนผู้คนเกี่ยวกับตัณหาต่าง ๆ"
      },
      "91": {
        "name": "Kha'Zix",
        "nickName": "นักฆ่าที่มองไม่เห็น",
        "description": "The Void เติบโต และปรับตัว—แต่ก็ไม่มีสิ่งใดเทียบได้กับ Kha'Zix การวิวัฒนาการคือสิ่งที่ผลักดันตัวผ่าเหล่าที่แสนน่ากลัวนี้ มันเกิดมาเพื่อมีชีวิตรอดและสังหารสิ่งที่แข็งแกร่งกว่า และเมื่อมันทำไม่ได้ มันก็จะสร้างวิธีที่มีประสิทธิภาพใหม่ ๆ เพื่อโต้กลับและสังหารเหยื่อของมัน แม้ตอนแรกมันจะเป็นสัตว์ที่ไร้สติปัญญา ตอนนี้มันสามารถที่จะวางแผนการล่า และแม้แต่ใช้ความกลัวที่มันสร้างขึ้นในก้นบึ้งจิตใจของเหยื่อผู้โชคร้ายของมันเป็นอาวุธ"
      },
      "92": {
        "name": "Akali",
        "nickName": "นักฆ่าผู้โดดเดี่ยว",
        "description": "จากการละทิ้งภาคีคินโคและตำแหน่งกำปั้นแห่งเงา ยามนี้อาคาลิได้ลุยเดี่ยว พร้อมที่จะกลายเป็นอาวุธสังหารโหดยามที่ผู้คนต้องการ แม้กระนั้นนางยังคงยึดมั่นในคำสอนของเชนผู้เป็นอาจารย์ของนางไว้ทั้งหมด นางได้กล่าวสัตย์ปฏิญาณไว้ว่าจะปกป้องไอโอเนียจากเหล่าปรปักษ์ สังหารเพียงหนึ่งคนต่อหนึ่งการลงมือ การจู่โจมของอาคาลิอาจเป็นไปอย่างเงียบสงัด แต่ข้อความที่นางกล่าวจะดังก้องและชัดเจน: จงหวาดกลัวมือสังหารผู้ไร้นาย"
      },
      "101": {
        "name": "Viktor",
        "nickName": "The Machine Herald",
        "description": "ผู้นำแห่งเทคโนโลยีของยุคใหม่ Viktor ใช้ทั้งชีวิตของเขาเพื่อการพัฒนาของมนุษยชาติ เขาเป็นนักอุดมคติที่พยายามจะยกระดับความเข้าใจของชาว Zaun ไปอีกขั้น เขาเชื่อว่ามีเพียงการยอมรับการวิวัฒนาการอันยิ่งใหญ่ของเทคโนโลยีเท่านั้นที่จะทำให้มนุษยชาติก้าวไปถึงขีดจำกัดของตัวเองได้ ด้วยร่างกายที่ถูกปรับแต่งด้วยโลหะและวิทยาศาสตร์ Viktor พยายามสร้างอนาคตแบบที่เขาต้องการด้วยศรัทธาเต็มเปี่ยม"
      },
      "102": {
        "name": "Lux",
        "nickName": "เลดี้แห่งความสดใส",
        "description": "Luxanna Crownguard มาจาก Demacia อาณาจักรอันโดดเดี่ยวที่มองความสามารถทางเวทมนตร์เป็นสิ่งชั่วร้ายและน่าเกรงกลัว เธอสามารถควบคุมแสงได้ดั่งใจนึก ทำให้เธอเติบโตขึ้นมาด้วยความกลัวว่าจะถูกจับได้และถูกเนรเทศ เธอถูกบังคับให้เก็บพลังของเธอเป็นความลับเพื่อปกป้องชือเสียงของครอบครัว แต่สุดท้าย ด้วยความที่ Lux เป็นคนที่มองโลกในแง่ดีและมีความคิดที่ยืดหยุ่น ทำให้เธอหันมายอมรับพลังของตัวเอง และตอนนี้เธอได้ใช้มันอย่างลับ ๆ เพื่อรับใช้บ้านเกิดของเธอ"
      },
      "111": {
        "name": "Ryze",
        "nickName": "นักเวทย์โบราณ",
        "description": "Ryze เป็นอาร์คเมจโบราณที่แข็งแกร่งผู้แบกรับภาระที่หนักหน่วงอย่างไม่น่าเชื่อไว้กับตัว Ryze ถือเป็นหนึ่งในจอมขมังเวทที่มีความเชี่ยวชาญมากที่สุดใน Runeterra ด้วยพลังมนตราที่มากมายและความแข็งแกร่งอันไร้ขอบเขต เขาตามหา World Rune โดยไม่รู้จักเหน็ดเหนื่อย ซึ่งเป็นชิ้นส่วนเวทมนตร์ดิบที่ครั้งหนึ่งได้ก่อกำเนิดโลกใบนี้ขึ้นจากความว่างเปล่า Ryze ต้องกอบกู้อักขระเหล่านี้ให้ได้ก่อนที่จะตกไปอยู่ในเงื้อมมือของคนชั่ว เนื่องจากเขารู้ดีว่าคนพวกนี้จะเป็นที่น่าสะพรึงกลัวสำหรับชาว Runeterra เพียงใด"
      },
      "112": {
        "name": "Ahri",
        "nickName": "จิ้งจอกเก้าหาง",
        "description": "Ahri นั้นถือกำเนิดมาโดยมีความเชื่อมโยงกับพลังเร้นลับแห่ง Runeterra นางเป็น Vastaya ที่สามารถเปลี่ยนรูปพลังเวทมนตร์ให้เป็นลูกบอลพลังงานดิบได้ นางชื่นชอบการเล่นสนุกกับเหยื่อด้วยการปรับเปลี่ยนความรู้สึกของพวกมันก่อนที่จะถูกนางกลืนกินวิญญาณ แม้จะมีสัญชาตญาณของนักล่าโดยธรรมชาติ แต่ Ahri ก็ยังคงสามารถเข้าอกเข้าใจผู้อื่นได้จากภาพความทรงจำที่ถูกแสดงเข้ามาจากวิญญาณแต่ละดวงที่นางกลืนกิน"
      },
      "121": {
        "name": "Rakan",
        "nickName": "The Charmer",
        "description": "ด้วยบุคลิกที่หุนหันพลันแล่นมากพอ ๆ กับเสน่ห์ประจำตัว Rakan เป็นนักก่อปัญหาผู้เลื่องชื่อและนักรบเริงระบำที่ยิ่งใหญ่ที่สุดในประวัติศาสตร์ของเผ่า Lhotlan สำหรับมนุษย์แห่งที่ราบสูง Ionia นั้น ชื่อของเขามีความหมายพ้องกับเทศกาลอันสนุกสุดเหวี่ยง งานปาร์ตี้ไร้ขอบเขต และเสียงดนตรีอันโกลาหล มีเพียงไม่กี่คนเท่านั้นจะคาดคิดว่านักเดินทางจอมโอ้อวดผู้อยู่ไม่สุขรายนี้เป็นคนรักของกบฏอย่าง Xayah แถมยังอุทิศตนให้กับนางอีกด้วย 。"
      },
      "122": {
        "name": "Lulu",
        "nickName": "นักเวทย์วิญญาณ",
        "description": "จอมเวทยอเดิ้ล Lulu เป็นที่รู้จักกันในเรื่องการใช้เวทมนตร์สร้างภาพลวงตาหรือสิ่งมีชีวิตที่มาจากความฝัน ขณะที่เธอตะลอนไปทั่ว Runeterra พร้อมกับ Pix แฟรี่ที่เป็นเพื่อนร่วมทางของเธอ Lulu เปลี่ยนรูปร่างของความจริงตามใจชอบ บิดเบือนโฉมหน้าของโลกใบนี้และอะไรก็ตามที่เธอมองว่าเป็นข้อจำกัดของโลกนี้ ในขณะที่คนอื่นอาจจะคิดว่าเวทมนตร์ของเธอนั้นผิดธรรมชาติและเป็นอันตรายอย่างที่สุด เธอเชื่อว่าทุกคนควรค่าเเก่การได้รับเวทมนตร์"
      },
      "131": {
        "name": "Lucian",
        "nickName": "The Purifier",
        "description": "Lucian ผู้เป็นองครักษ์แห่งแสง คือนักล่าวิญญาณผู้ชั่วร้ายและใช้ปืนคู่โบราณเพื่อตามล่าและกวาดล้างพวกมันอย่างไม่หยุดหย่อน หลังจากที่ภรรยาของเขาถูกสังหารโดย Thresh ผู้เป็นวิญญาณร้าย Lucian ก็ได้ออกเดินทางบนเส้นทางแห่งความเคียดแค้น แต่ถึงแม้ว่าภรรยาของเขาจะกลับมามีชีวิตอีกครั้ง ความโมโหโทโสของเขาก็ไม่ได้ลดลงเลย ทั้งเด็ดเดี่ยวและไร้ซึ่งความปราณี Lucian จะไม่ยอมให้อะไรมาขวางเพื่อปกป้องผู้คนจากความน่ากลัวของ Black Mist"
      },
      "132": {
        "name": "Jinx",
        "nickName": "The Loose Cannon",
        "description": "Jinx อาชญากรสาวผู้มีสภาพจิตผิดปกติจาก Zaun มีชีวิตอยู่เพื่อก่อความหายนะโดยไม่สนใจถึงผลลัพธ์ที่จะตามมา เธอใช้อาวุธยุทโธปกรณ์สุดโหดเพื่อระเบิดทุกสิ่งให้ดังสนั่นและสว่างไสว สร้างร่องรอยแห่งความวินาศสันตะโรไว้เป็นหลักฐานของการมีอยู่ของเธอ Jinx เกลียดชังความน่าเบื่อ เธอจึงเต็มใจที่จะสร้างความโกลาหลในแบบฉบับของเธอเองในทุก ๆ ที่ที่เธอย่างกรายไปถึง"
      },
      "temp": {}
    }
}
const yxzh = {
	buyushangtiao: 'ไม่ต้องเพิ่มขึ้น',
	message5: 'เดิมพันเกมเด่นได้เปลี่ยนเป็น',
	nomore: 'ไม่มีอีกแล้ว',
	norecords: 'ไม่มีบันทึก',
	_orders: 'ตั๋ว',
	xDraws: '{0} งวด',
	eventCash: 'เงินของขวัญโปรโมชั่น',
	qi: "",
	yuan: '',
	findTranceDetail: 'ดูรายละเอียดการติดตาม',
	cancelOrder: 'ยกเลิกตั๋ว',
	canceBtnText: 'ยกเลิก',
	watingOpen: 'รอเปิดจ้า',
	unWin: 'ไม่ถูก',
	winned: 'ชนะ',
	personalCancel: 'ยกเลิกเอง',
	systemCancel: 'ยกเลิกระบบ',
	help: {
        helpText: 'ช่วยเหลือ',
        gameIntro: 'แนะนำเกม',
        text1: 'นี่คือเกมอะไร',
        text2: 'Champion\'s Summon เป็นเกมปริศนาไพ่ วิธีการเล่นคือการทำนายคุณสมบัติของการ์ดฮีโร่ตัวต่อไป (เพศ ระยะทาง บทบาท)',
        text3: 'โปรดอ่านด้านล่างสำหรับข้อมูลเพิ่มเติมเกี่ยวกับวิธีการเล่น.',
        howToPlay: 'วิธีเล่น',
        betType: 'ประเภทเดิมพัน',
        bet: 'เดิมพัน',
        chase: 'ติดตามเลข',
        hero: 'ฮีโร่',
        limitIntro: 'คำอธิบายขีดจำกัด',
        limit1: '1. เงินเดิมพันเกมพิเศษ',
        limit11: 'เดิมพันขั้นต่ำและสูงสุด หากวงเงิน 5-100,000 หมายถึงเดิมพันขั้นต่ำ 5 เดิมพันสูงสุด 100,000',
        limt2: '2. รับรางวัลมากถึง 1 งวด',
        limit22: 'กำไรรวม (โบนัสทั้งหมด - เดิมพันทั้งหมด) ของคำสั่งที่วางโดยผู้ใช้คนเดียวกันในช่วงเวลารางวัลเดียวกันของลอตเตอรีเดียวกันต้องไม่เกินขีดจำกัดบนที่แพลตฟอร์มกำหนด และส่วนเกินจะถูกหักออกจากโบนัสโดยตรง',
        example: 'ตัวอย่าง:',
        example1: 'Champion\'s Summon กำไรสูงสุดในช่วงเวลาเดียวคือ 500,000 หยวน หยวน ผู้ใช้ A เดิมพัน 100,000 หยวนในซินเจียงบาคาร่า 001 และชนะโบนัส 1 ล้านหยวน กำไรสำหรับช่วงเวลานี้คือ 900,000 หยวน (100-10) ',
        example2: 'เกินขีดจำกัดกำไร จากนั้นโบนัสที่ผู้ใช้ A จะได้รับในช่วง 001 คือ 500,000 หยวน (10+40)',
        example3: 'เกมปัจจุบัน: ขีดจำกการชนะสูงสุดของซิ Champion\'s Summon 1 ช่วงเวลาคือ {0}'
    },
    history: {
        openResult: 'ผลลัพธ์',
        no: 'งวด',
        time: 'เวลา',
        result: 'ผลลัพธ์',
        placeholder: 'กรุณาใส่จำนวนงวดรางวัลที่ต้องการค้นหา',
        openDetail: 'รายละเอียดเปิดรางวัล',
        times: ['วันนี้', 'เมื่อวาน', 'วันก่อน'],
        watingOpen: 'รอเปิดรางวัล </br>···',
    },
    setting: {
        set: 'ตั้งค่า',
        betArea: 'ระดับการเดิมพัน',
        upLimit: 'ล็อคได้ถึง',
        voiceSwitch: 'เปิด/ปิดเสียง',
		winVoice: 'ประกาศรางวัล',
        betNowModalSwitch: 'ยืนยันเดิมพันตอนนี้'
    },
    staMap: {
        role: ['หญิง', 'ชาย'],
        distance: ['ระยะประชิด', 'ระยะไกล'],
    },
    play: {
        report: {
            betNo: 'งวด',
            betTime: 'เวลา',
            betContent: 'เนื้อหาเดิมพัน',
            orderStatus: 'เลขที่ตั๋ว',
            orderDetail: 'รายละเอียดการเดิมพัน',
            chaseDetail: 'รายละเอียดการติดตาม',
            chaseTime: 'เวลาติดตาม',
            chasePlay: 'วิธีการเล่น',
            totalBet: 'เดิมพัน/ทั้งหมด',
            betRecords: 'ประวัติเดิมพัน',
            chaseRecords: 'ป้อนหมายเลข',
            zhandouli: 'พลังต่อสู้:',
        },
        checkModal: {
            betContent: 'เนื้อหาเดิมพัน',
            betAmt: 'เดิมพัน',
            odds: 'อัตราต่อรอง',
            orderNo: 'เลขที่ตั๋ว',
            opera: 'ดำเนินงาน',
            profit: 'กำไรและขาดทุน',
            total: 'ทั้งหมด',
            chedan: 'ยกเลิกตั๋ว',
            cancelAll: 'ยกเลิกทั้งหมด',
            noOrders: 'ไม่มีเดิมพัน',
        },
        rangeSet: {
            cancel: 'ยกเลิก',
            confirm: 'ยืนยัน',
            rangeLimit: 'ตั้งค่าระยะการโจมตี',
            attckRange: 'ระยะค่าพลังโจมตี',
            minValue: 'ต่ำสุด≥2000',
            midValue: 'กลาง',
            maxValue: 'สูงสุด ≤13999',
            overRange: 'อยู่นอกขอบเขตการโจมตี โปรดกลับเข้ามาใหม่!',
            setSuccess: 'ตั้งค่าสำเร็จ!'
        },
        index: {
            minzhong: "ตี",
            qujian: 'ช่วงเวลา',
            fanwei: 'ขอบเขต',
            message1: 'เดิมพันตอนนี้ยืนยันป๊อปอัปเปิดแล้ว'
        }
    },
    config: {
        nvxin: 'หญิง',
        xinbie: 'เพศ',
        nanxin: 'ชาย',
        jinzhan: 'ระยะประชิด',
        yuanzhan: 'ระยะไกล',
        gongjijuli: 'ระยะโจมตี',
        tanke: 'แทงค์',
        zhanshi: 'นักสู้',
        cike: 'มือสังหาร',
        zhiye: 'ตำแหน่ง',
        fashi: 'นักเวทย์',
        fuzhu: 'ซัพพอร์ต',
        sheshou: 'มาคส์แมน',
    },
	header: {
		lang20: 'ขอแสดงความยินดี!',
		lang2: '',
		lang21: 'ชนะรางวัล',
	},
	wayMap: {
		tipsText: 'ยอดเงินไม่พอกรุณาเติมเงิน',
		canWinAmount: 'จำนวนเงินที่ชนะ',
		yuee: 'ยอด',
		errorTips1: 'เลือกอย่างน้อย 1 เดิมพัน',
		bet: 'เดิมพัน',
		dan: 'ตั๋ว',
		fengpaning: 'ช่วง {issue} ปิดแล้ว วางเดิมพันไม่ได้',
		tipsTitle: 'เตือน',
		errorTips2: 'ใส่จำนวนเงิน',
		chase: 'เลขติดตาม',
		confirmBet: 'ยืนยันการเดิมพัน',
	},
	bet: {
		zhushu: 'จำนวนเดิมพัน',
		beishu: 'ตัวคูณ',
		total: 'เดิมพันทั้งหมด',
		play: 'วิธีการเล่น',
		queren: 'ยืนยันการเดิมพัน',
		qihao: 'งวด',
		continue: 'เดิมพันต่อ',
		betAmount: 'เดิมพัน',
		giveUp: 'เลิกเดิมพัน',
		tingyong: {
			message:
				'สามารถเปิดใช้งานอีกครั้งได้ที่ตั้งค่า - การเล่นเกมสองด้านที่มุมบนขวา',
			title: 'ปิดการแจ้งเตือน "ยืนยันเดิมพัน"',
		},
		tingyong1: {
			message: 'สามารถเปิดใหม่ได้ในตั้งค่าด้านบน',
			title: 'ปิดยืนยันการเดิมพัน',
		},
		succeed: 'อยู่ระหว่างยืนยันเดิมพัน',
		allShowed: 'แสดงทั้งหมด',
		dontRepeat: 'เดิมพันโปรดอย่าส่งอีกครั้ง',
		tijiao: 'ส่งเดิมพัน...',
		submitFailed: 'ส่งไม่สำเร็จ',
		repeatContent:
			'ระบบรับรู้ตั๋วที่คุณส่งในครั้งนี้เป็นตั๋วที่ส่งเมื่อ 10 วินาทีที่แล้ว ยืนยันเดิมพันครั้งต่อไป?',
		zhankaimingxi: 'แสดงรายละเอียด',
		chedan: 'ยกเลิกตั๋ว',
		wuweijie: 'ไม่มีตั๋วค้างชำระ',
		wuyijie: 'ไม่มีตั๋วจ่าย',
		moshi: 'โหมด',
		repeatTitle: 'เตือนเดิมพันซ้ำ',
		zu: 'กลุ่ม',
		zuhao: 'กลุ่ม',
		zhengma: 'เลขหลัก',
		jinetongyiwei: 'จำนวนเงินเท่ากันคือ',
		meizujine: 'จำนวนต่อกลุ่ม',
		weijieshu: 'จำนวนตั๋วที่ยังไม่ได้ชำระ',
		all: 'ทั้งหมด',
		gong: 'ทั้งหมด',
		stopConfirm: 'ปิดยืนยันเดิมพัน (เปิดใช้งานได้ในตั้งค่า)',
		jine: 'ยอดเดิมพัน',
		needAmount: 'ยอดเดิมพันไม่สามารถเว้นว่างได้',
		haomazuhe: 'รวมตัวเลข',
		betStat: 'สถานะเดิมพัน',
		zhj: 'ได้รับรางวัล:',
		canWin: 'ประมาณการชนะ',
		keying: 'ชนะโดยประมาณ',
		orderFailed: 'ส่งตั๋วเดิมพันไม่สำเร็จ',
		querenqingkong: 'ยืนยันลบ?',
		qingkong: 'ลบเนื้อหา',
		timeout: 'คำขอหมดเวลา โปรดตรวจสอบเครือข่าย',
	},
	trace: {
		yuan: '',
		times: 'ตัวคูณ',
		quxiaoBtn: 'ยกเลิก',
		check: { total: 'เดิมพันทั้งหมด', title: 'ยืนยันติดตาม' },
		qizhi: 'ช่วง {firstPlanNo} ถึง {lastPlanNo}',
		loseStop: 'หยุดถ้าไม่ถูก',
		qishu: 'รอบ',
		qiNo: '',
		wanfaneirong: ['วิธีการเล่น', 'เนื้อหา'],
		winStop: 'หยุดเมื่อโดน',
		id: 'งวด',
		totalXyuan: '{0}',
		initialMuti: '{0} ครั้ง',
		giveUp: 'ยกเลิกติดตาม',
		stopWay: 'วิธีหยุดติดตาม',
		gapBet: 'แยก',
		tip:
			'หมายเหตุ: ทุกๆ [1] ช่วงของตัวคูณ X[1] ให้เพิ่มจำนวนเดียวกันกับตัวคูณเดียวกัน',
		noPlan: 'ไม่ได้สร้างแผน',
		continue: 'เดิมพันต่อ',
		tipProfit:
			'ถึงตัวคูณที่ใหญ่ที่สุดบนพื้นฐานของผลกำไรที่รับประกัน ตอนนี้กำหนดจำนวนสูงสุดที่จะเติบโต',
		submitting: 'เลขติดตามกำลังส่ง',
		hideDetail: 'ปิดข้อมูล',
		lianweishu: 'ท้าย {0}',
		showDetail: 'เพิ่มรายละเอียดติดตาม',
		current: 'ปัจจุบัน',
		start: 'ช่วงเวลาเริ่มต้น',
		tomorrow: 'วันที่ห่างกัน',
		today: 'ช่วงเวลาของวัน',
		gongzhui: 'ติดตาม {0} งวด:',
		succeed: 'อยู่ระหว่างยืนยันเดิมพัน',
		gongzhuiXqi: 'ติดตาม {0} งวด',
		yue: 'ยอดเงิน:',
		index: 'จำนวนงวด',
		num: 'จำนวนงวด',
		changed: 'จำนวนงวดที่เปลี่ยนไป งวดที่เปลี่ยนไป',
		startTimes: 'ตัวคูณเริ่มต้น',
		doubleEach: 'ตัวคูณ X',
		amount: 'เดิมพัน',
		fromTo: 'ถึง',
		maxiumTrace: 'สูงสุดไม่เกิน {0}',
		confirm: 'ยืนยันติดตาม',
		totalXXYuan: 'Total: {0}',
	},
	components: {
		cancel: 'ยกเลิก',
		ack: 'เข้าใจแล้ว',
		skip: 'ข้าม',
		confirmBet: 'ยืนยันการเดิมพัน',
		loading: 'กำลังโหลด',
		submitFailed: 'ส่งไม่สำเร็จ',
	},
	limit: {
		youshangjiao: '"เมนูด้านบนขวา - ตั้งค่า"',
		yibiaoji: 'ทำเครื่องหมาย',
		yishanchu: 'ถูกลบ',
		xiugai: 'เพื่อแก้ไข.',
		bufuhe: 'จำนวนเงินไม่อยู่ในช่วง',
		qujianwei: 'เดิมพันหนึ่งงวดคือ {0} คลิก',
	},
	tab: {
		betRecord: 'เดิมพัน',
		traceRecord: 'ป้อนหมายเลข',
		moneyRecord: 'โบนัส',
		orderNo: 'เลขที่ใบสั่งซื้อ',
		betDetail: 'รายละเอียดการเดิมพัน',
		traceDetail: 'รายละเอียดการติดตาม',
		dailyProfit: 'กำไรขาดทุนรายวัน',
		dailyProfitDetail: 'รายละเอียดกำไรขาดทุนรายวัน',
		moneyRecordDetail: 'รายละเอียดการเปลี่ยนบัญชี',
		geqizhuihao: 'ติดตามเลข',
		geqizhuihaoDetail: 'รายละเอียดหมายเลขติดตามผลแต่ละครั้ง',
		totalProfit: 'เงินรางวัลทั้งหมด',
		dayProfit: 'กำไรวันนี้',
		betContent2: 'เนื้อหาเดิมพัน',
	},
	status: {
		traceing: 'กำลังติดตาม',
		finished: 'เรียบร้อย',
		personalStopTrace: 'หยุดติดตามเอง',
		systemStopTrace: 'ระบบยกเลิก',
	},
	eachTrace: {
		bet: 'เดิมพัน',
		win: 'ถูก',
		period: 'งวด',
		times: 'ตัวคูณ',
		selectAll: 'เลือกทั้งหมด',
		cancelOrder: 'ยกเลิกเลขติดตาม (เลือกไว้ {0} รายการ)',
	},
	chaseRecords: {
		chakan: 'ดู',
		betMode: 'ประเภทการเดิมพัน',
		copy: 'คัดลอก',
		chaseStartTime: 'เวลาติดตาม',
		lottery: ['', 'เกม'],
		wanfa: ['', 'วิธีการเล่น'],
		startPeriod: 'ช่วงเวลาเริ่มต้น',
		chaseContent: 'เนื้อหาเลขตาม',
		chaseAllPeriods: 'จำนวนงวด',
		chaseTotalMoney: 'เดิมพันทั้งหมด',
		chaseFinishedPeriod: 'จำนวนงวดที่สำเร็จ',
		chaseFinishedAmount: 'ครบจำนวน',
		chaseCanceledPeriod: 'ยกเลิกการจับสลาก',
		chaseStatus: 'สถานะ',
		chaseCancelAmount: 'จำนวนเงินที่ยกเลิก',
		chaseRule: 'กฎการเพิ่มตัวเลข',
		winAmount: 'ยอดถูกลอตเตอรี่',
		eachDetails: 'ตรวจสอบสถานะติดตาม',
		cancelChase: 'ยกเลิกหมายเลขติดตาม',
	},
	cancelOrderJs: {
		cancelConfirmMessage: 'ยกเลิกเดิมพัน?',
		cancelSuccess: 'ยกเลิกสำเร็จ',
		cancelConfirmMessage2:
			'ช่วงเวลาปัจจุบัน {0} สามารถยกเลิก, ยกเลิกตั๋วเดิมพันทั้งหมดได้หรือไม่?',
		cancelConfirmMessage3:
			'ช่วงเวลาปัจจุบัน {0} อยู่ระหว่างดำเนินการ ไม่สามารถยกเลิกได้! ยกเลิกตั๋วช่วงอื่น?',
		cancelConfirmMessage4: 'คุณต้องการยกเลิกคำสั่งติดตามเหล่านี้หรือไม่?',
		confirmButtonText: 'ใช่',
		lastPeriodNonCancel: 'งวดปัจจุบันเป็นงวดสุดท้าย ยกเลิกไม่ได้!',
		cancelFail: 'ยกเลิกล้มเหลว โปรดลองอีกครั้งในภายหลัง',
	},
	orderStatusJs: {
		yes: 'ใช่',
		no: 'ไม่ใช่',
		non: 'ไม่ใช่',
		zhuizhongjiting: 'หยุดถ้าถูก',
		buzhongjiting: 'หยุดถ้าไม่ถูก',
		fanbeizhuihao: 'เพิ่มตัวเลขอื่นที่ไม่ใช่ตัวคูณ',
		tongbeizhuihao: 'เพิ่มตัวเลขที่มีตัวคูณเท่ากัน',
		lirunlvzhuihao: 'เพิ่มกำไร',
		systemCanceled: 'ระบบยกเลิก',
		systemException: 'ระบบผิดปกติ',
		weixiadan: 'ไม่มีการเดิมพัน',
		yixiadan: 'เดิมพัน',
		userCanceled: 'ผู้เล่นยกเลิก',
		yijiesuan: 'สรุปยอด',
		yifuzhi: 'คัดลอก',
	},
}
export default {
	yxzh,
    teseLol,
};