/*
 * @Author: Supermark
 * @Date: 2022-08-03 19:09:36
 * @Description: 蚂蚁数据源泰语翻译
 * @FilePath: /djUser/src/assets/i18n/th/antDataSource.js
 */
export default {
  Bet: 'เดิมพัน',
  Scene: 'เรียลไทม์',
  VideoLS: 'LS',
  AnimationLS: 'ANIM LS',
  Prescient: 'ดู',
  RecentData: 'ข้อมูล',
  HistoryFight: 'ประวัติต่อสู้',
  GameStat: 'สถิติเวลา',
  RecentStat: 'สถิติล่าสุด',
  FightMatchRecord: 'บันทึกต่อสู้/เหตุการณ์',
  BigBattle: 'ศึกใหญ่',
  SmallBattle: 'ศึกเล็ก',
  FirstBloodRate: 'เลือด1',
  RoundNum265: '>26.5 รอบ',
  FirstTowerRate: 'ป้อม1',
  PentakillRate: 'ฆ่า5',
  DecakillRate: 'ฆ่า10',
  FirstBarrackRate: 'ทัพแรก',
  AverageTotalDuration: 'เวลาต่อเกม',
  AverageKill: 'ฆ่าต่อเกม',
  Data: 'ข้อมูล',
  Baron: 'มังกรใหญ่',
  DrakeDragon: 'มังกรเล็ก',
  TurretTowerDive: 'ป้อม',
  Crystal: 'คริสตัล',
  EconomyDifference: 'GD',
  NoData: 'ไม่มีข้อมูล',
  ExperienceDifference: 'XP-D',
  FirstBaronRate: 'มังกรเล็กแรก',
  FirstDragonRate: 'มังกรใหญ่แรก',
  Kill: 'สังหาร',
  FightData: 'ข้อมูลต่อสู้',
  Player: 'ผู้เล่น',
  Output: 'ส่งออก',
  Experience: 'ประสบการณ์',
  LastHitDeny: 'CS/Deny',
  TotalOutput: 'ส่งออกทั้งหมด',
  DamageModifierRate: 'อัตราการแปลงความเสียหาย',
  HeroOutput: 'ฮีโร่',
  KillMonsters: 'ฆ่ามอนสเตอร์ป่า',
  LastHit: 'CS',
  VisionRemovalWard: 'ใส่วอร์ด/ทำลายวอร์ด',
  DamageTaken: 'บาดเจ็บ',
  TrueDamage: 'ความเสียหายที่แท้จริง',
  MagicalOutput: 'มายากล',
  PhysicalOutput: 'ฟิสิกส์',
  FirstTyrantRate: 'ทรราชครั้งแรก',
  FirstOverlordRate: 'ปกครองครั้งแรก',
  Economy: 'โกลด์',
  ParticipationRate: 'เข้าร่วม',
  ItemSets: 'สร้าง',
  Situation: 'สถานการณ์',
  Round1: 'รอบ1',
  FirstBaron: 'มังกรเล็กแรก',
  FirstDragon: 'มังกรใหญ่แรก',
  FirstPentakill: 'ฆ่า 5 ตัวแรก',
  FirstDecakill: 'ฆ่า 10 ตัวแรก',
  MatchStart: 'เริ่มเกม',
  All: 'ทั้งหมด',
  Healing: 'รักษา',
  AbnormalDataInfoGameDataClosed: 'ข้อมูลเกมไม่ปกติ ข้อมูลเกมถูกปิด!',
  CloseDataWindow: 'ปิดหน้าต่างข้อมูล',
  RoundNo: 'รอบที่ {no}',
  DestroyTowerCrystal: 'ทำลายป้อม/คริสตัล',
  KillHero: 'ฆ่าฮีโร่',
  Barrack: 'ทัพ',
  RuleDescription: 'กฎการแสดงข้อมูล',
  DescriptionDataDisplayRules: 'เนื้อหาทั้งหมดที่แสดงในหน้านี้ใช้สำหรับอ้างอิงเท่านั้น สมาชิกสามารถใช้เนื้อหานี้เป็นแนวทางได้ เราจะพยายามอย่างเต็มที่เพื่อรับรองความถูกต้องของเนื้อหาที่แสดง หากมีข้อผิดพลาดใดๆ บริษัทจะไม่รับผิดชอบใด ตัวอย่างเช่น: เมื่อเกมถ่ายทอดสด สำหรับตลาดแฮนดิแคป มูลค่าที่ถูกต้องที่แสดงบนใบเดิมพันจะมีผลเหนือกว่า!',
  DescriptionDataDisplayRulesRemark: 'หมายเหตุ: ข้อมูลเหตุการณ์ที่แสดงใช้เพื่อการอ้างอิงเท่านั้น ไม่ใช่สำหรับผลการแข่งขัน!',
  // 暂时缺失部分
  FirstBlood: 'เลือด1',
  FirstTower: 'ป้อม1',
  FirstHerald: 'กองหน้าแรก',
  FirstCrystal: 'คริสตัลแรก',
  FirstBarrack: 'กองทัพแรก',
  Kill265: 'สังหาร > 26.5',
  Kill475: 'สังหาร > 47.5',
  Kill235: 'สังหาร > 23.5',
  GameTimeOver33: 'เวลา > 33',
  GameTimeOver35: 'เวลา > 35',
  GameTimeOver19: 'เวลา > 19',
  SecondHandgun: 'ชนะรอบปืนพกที่สอง',
  More265Rounds: '> 26.5 รอบ',
  GlobalConference: 'ชนะทั้งเกม',
  SingleConference: 'ชนะเกมเดียว',
  FirstCrystalRate: 'คริสตัลแรก',
  FirstMeatMountain: 'โรชานแรก',
  DrakeKill: 'ฆ่ามังกร',
  PlayerKill: 'ฆ่าทีม',
  RiftHeraldKill: 'ฆ่าแนวหน้า',
  DrakesKill: 'ฆ่าผู้นำ',
  JoinGame: 'เข้าร่วมแข่งขัน',
  QuitGame: 'ออกจากเกม',
  RoundStart: 'เริ่ม',
  RoundWin: 'ชนะ',
  Suicide: 'ฆ่าตัวตาย',
  Bomb: 'วางระเบิด',
  DisarmBomb: 'ถอนระเบิด',
  CloseDataWindow : 'ปิดหน้าต่างข้อมูล～',
  BlueEconomy: 'เศรษฐกิจบลูสแควร์：',
  OnlyCurrentLg: 'ดูเฉพาะลีกปัจจุบัน',
  Last10Games: '10 เกมหลังสุด',
  zhengbu: 'CS',
  fanbu: 'Deny',

  // 后续使用
  R1Gunwin: 'ปืน R1 ชนะ',
  R16Gunwin: 'ปืน R16 ชนะ',
  The1sWin5Rounds: 'ชนะ5รอบแรก',
  The1stWin10Rounds: 'ชนะ10รอบแรก',
  EnterTheOT: 'เข้าสู่ช่วงต่อเวลา',
  Overtime: 'ต่อเวลา',
  // NoMap: '暂无地图',
  // Map: '地图',
  FirstHalf: 'ครึ่งแรก',
  SecondHalf: 'ครึ่งหลัง',
  Annihilation: 'คัดออก',
  BombDisposal: 'กำจัดระเบิด',
  Explosion: 'ระเบิด',
  TimeOut: 'เกินเวลา',
  

  // 暂时没使用
  // TotalKill: '总击杀',
  // OnlyCurrentLg: '只看当前联赛',
  // WinRate: '胜率',
  // FightRecord: '交手记录',
  // LiveStream: '直播',
  // ThisGameWinRate: '本场比赛胜率',
  // No: '无',
  // Processing: '进行中',
  // GameOver: '比赛已结束',
  // GameTime: '游戏时间',
  AddPoints: 'อัพเวล',
  // NoAnimationLS: '暂无动画直播',
  Overlord: 'ครอง',
  Tyrant: 'ทรราช',
  // RealTime: '实时',
  // Score: '比分',
  // GamesTime: '比赛时间',
  // SmallBattleSituation: '小局战况',
  // BigBattleSituation: '大局战况',
  // Round: '回合',
  // FirstTyrant: '首暴君',
  // FirstOverlord: '首主宰',
  // Topsolo: '上单',
  // Jungle: '打野',
  // Solo: '中单',
  // Support: '辅助',
  // Position1: '1号位',
  // Position2: '2号位',
  // Position3: '3号位',
  // Position4: '4号位',
  // Position5: '5号位',
  // bet: '投注',
  // Battle: '比赛',
  // Time: '时间',
  // Duration: '时长',
  // Schedule: '赛程',
  // GameDuration: '游戏时长',
  // BattleData: '比赛数据',
  // Close: '关闭',
  // PlsCloseGameData: '请关闭赛事数据',
  // OverView: '概况',
  AchieVements: 'บันทึก',
  // RealTimeWinRate: '实时胜率',
  // LayAside: '收起',
  // UpdateTime: '更新时间',
  // RecentDataDetails: '近期数据详情',
  // Lead103K: '领先10.3K',

  Gamelogs: "บันทึกเกม",
  Enter: "เข้า ",
  Exit: "ออก ",
  Roundstart: "เริ่ม",
  Winround: "รอบชนะ",
  Suicide: "ฆ่าตัวตาย",
  Placebomb: "วางระเบิด",
  Defusebomb: "ปลดชนวนระเบิด",
};