export default {
    footballRanking: {
        Leaderboard: 'Leaderboard',
        Team: 'Team',
        MP: 'MP',
        WDL: 'W/D/L',
        Pts: 'Pts',
    },
    groupKnockout: {
        Round1: 'Round 1',
        QuarterFinal: 'Quarter-final',
        Semifinals: 'Semifinals',
        Final: 'Final',
        zanwu: 'No ',
    },
    groupMatches: {
        zu: 'Group {no}',
        Team: 'Team',
        MP: 'MP',
        WDL: 'W/D/L',
        jin: 'GF',
        shi: 'GA',
        jingshengqiu: 'GD',
        Pts: 'Pts',
    },
    knockout: {
        GroupStage:'Group Stage',
        KnockoutStage:'Knockout Stage',
    },
    video:{
        batchNo:'Round{no}',
        dateNo:'{no}th',
        matchEnd:'Finished',
    },
    detail:{
        historyRecord:'Record',
    },
    endGame:{
        tips1:"General E-sports game results can be viewed here",
        tips2:"Pls go to {0} to view results",
        tips3:"Don't show this again",
    },
};
