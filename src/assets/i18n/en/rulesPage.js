export default {
	keywordsSearch: 'key search',
	tabMenu: {
		gunqiu: 'InPlay',
		chuanguan: 'Parlay',
		youxiguize: 'Game rule',
		jibenguize: 'Basic rule',
		guanjuntouzhu: 'Champion',
		jiesuanguize: 'eSports rule',
		wanfashuoming: 'About play',
		youxiwanfa: 'Games',
		zhubowanfa: 'Anchor play',
	},
	basicRule:
		`
		<div class="item">
				<div class="item-title">1.Start date, time</div>
				<div class="item-content">The starting date & time of E-sport matches on our website are for reference only, the specific time is based on the time displayed on the screen, if the match is suspended or postponed and not restarted within 24 hrs from the start, all bets on that match will be considered void & refunded.</div>
		</div>
		<div class="item">
            <div class="item-title">2.Estimated start time</div>
            <div class="item-content">The estimated start time are for reference only. The official final start time shall prevail. As long as the match is started normally (at any time), all bets will be valid.</div>
        </div>
        <div class="item">
            <div class="item-title">3.Unclear result bet</div>
            <div class="item-content">If team waiver before start or during match, clear result normal settle; Unclear result as void and refund.</div>
        </div>
        <div class="item">
            <div class="item-title">4.Name</div>
            <div class="item-content">If English and non-English match name differ in website, based on official english.</div>
        </div>
        <div class="item">
            <div class="item-title">5.Result info timeout</div>
            <div class="item-content">If the official result is awaited, and within 24 hrs after the actual time there is no match result or official information, the bets will be considered void & will be refunded.</div>
        </div>
        <div class="item">
            <div class="item-title">6.Team special case</div>
            <div class="item-content">
                <p>a.Bet as void and refund, if eSports player or team name error (Team or player info error in eSports will rejected).</p>
                <p>b.Bet will canceled, void and refund, if team can't participate for waiver or other reason.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">7.Result amend/change</div>
            <div class="item-content">
                <p>a.About settled bet. We reserve rights to amend result. Result based on related institution, web score or broadcast (video demo), if amend or change upon 36-hour start time.</p>
                <p>b.About match result. If result has changes after 36-hour or search accurate result, player may own inquiry or refer to official platform.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">8.About official and website result conflict</div>
            <div class="item-content">
			If official and our website result have conflict, may refer to related match video to confirm correct result. If no suitable video, may based on match institution official website. If official website can't provide result or its result error, we reserve rights to decide/amend in final and conclusive result.
            </div>
        </div>
        <div class="item">
            <div class="item-title">9.About rematch</div>
            <div class="item-content">
				If a game needs to rematch due to network problems or technical problem, all bets involved with a clear result will be considered valid. If the rematch takes place within 24 hrs, the unsettled result will be settled according to the rematch result; if the rematch time exceeds 24 hrs, the corresponding unsettled bets will be considered void, parlays will be calculated as "1".
			</div>
        </div>
        <div class="item">
            <div class="item-title">10.About the disconnection during the game</div>
            <div class="item-content">
                <p>If player dropped or lost connection, but no reversible, all result considered valid and normal settle.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">11.Change, error odds</div>
            <div class="item-content">
                <p>a.Odds change anytime. Bet will based on final odds confirmed.</p>
                <p>b.Odds counted as 1, if series bet canceled.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">12.Can't close before specific time</div>
            <div class="item-content">
			If market not close before specific time, bet after that session as cancel, void, refund.
            </div>
        </div>
        <div class="item">
            <div class="item-title">13.Late Bets(Past Post)</div>
            <div class="item-content">
			Late Bets(Past Post)- if a pre-match bet is accepted after a match has already started for any reasons. All betting in this case will be view as Past Posting. If InPlay didn't close on time, every bet is placed after this session will be viewed as Past Posting. We reserve rights to void bet.
            </div>
        </div>
        <div class="item">
            <div class="item-title">14.Ad hoc change rule</div>
            <div class="item-content">
				All market bet void, if organizer ad hoc amend rule during match. Game normal settle.
            </div>
        </div>
        <div class="item">
            <div class="item-title">15.Special case during match</div>
            <div class="item-content">
			Settle based on final official result, if BUG or temporary technical problem occur during match.
            </div>
        </div>
        <div class="item">
            <div class="item-title">16.Other</div>
            <div class="item-content">
                <p>a.We reserve rights to suspend/or cancel bet anytime.</p>
                <p>b.If video source error occur, video can't play etc. problem, won't affect match settlement.</p>
                <p>c.About system vulnerability bet, unofficial platform bet, Parlay or other suspicious bet. We reserve rights in decision.</p>
                <p>d.Bet as valid evidence for verification, if bet confirmed. Can't cancel, revoke and change.</p>
                <p>e. All bet as void, if system/technical error cause odds and odds time error. We reserve rights to void bet and refund, if inPlay error.</p>
                <p>f.All bet as void, if our system or program error occur. You may inform us if discover error.</p>
                <p>g.About eSports video delay (E.g. Date, time, score, stats, news etc.) as reference. Company not responsible on such info accuracy.</p>
                <p>h.We reserve rights to limit special match maximum bet or increase merchant biggest odds without prior notice.</p>
                <p>i.We reserve rights to revoke extra paid, adjust account to amend error and will notify as soon as reasonably practicable.</p>
                <p>j.We reserve rights in final decision, explanation and amend rule of need.</p>
            </div>
        </div>
		<div class="item">
            <div class="item-title">17. Max payout amount</div>
            <div class="item-content">
                <p>Max payout amt: For all match items or betting types, member daily max payout amt is 1000K$ (excl betting capital), or other equivalent currencies.</p>
            </div>
        </div>
        `,
	championBet:
		`
        <div class="item">
            <div class="item-title">Predict a tournament, league, or match Champion in Champions. Selected team or player win as qualified for payout, include:</div>
            <div class="item-content">
                <p>a.League. E.g. World Cup Champion;</p>
                <p>b.Preliminary. E.g. World Cup Group Champion;</p>
                <p>c.Highest score;</p>
                <p>d. Best player etc.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">Rules:</div>
            <div class="item-content">
                <p>a.Champion bet based on final match result;</p>
                <p>b.If bet player or team participate this competition, league or match; Even unqualified, quit or unable to finish match for any reason, bet normal settle;</p>
                <p>c.Champion, final judgment of player or team winner;</p>
                <p>d.If use other player or "other team" replace participant name, as unnamed;</p>
            </div>
        </div>
        `,
	cross:
		`
		<div class="item">
			<div class="item-content">
				<p>a.Parlay is select 2 or more match in 1bet. All Parlay match must win, bet as "win." If any bet of combo Parlay win, will add to next bet item, til all bet win or til any bet lose as end. Parlay still valid on any void or Tie bet. But Tie/void item counted as "1".</p>
				<p>b.It's 2 or more match bet as combo bet. Parlay win and get pay 2bet combo odds, if all selection are win. If 1 (or more) selection are lose, "Parlay" as lose. If 1 (or more) selection revoked or canceled, the odds will restore as 1.00. Maximum Parlay 10bet.</p>
				<p>c.When user bet XstringN, every combo have bet up limit at day. If today quota finished, system will update quota on 24:00:00.</p>
			</div>
		</div>
		`,
	live:
		`
    <div class="item">
        <div class="item-title">inPlay is bet during match. Will receive order during match and stop sell order after close.Note: Once match move to inPlay page, system counted all bet as "inPlay" whether if match started.</div>
        <div class="item-content">
            <p>a.All inPlay bet need system acceptance. May lead to bet delay or bet fail occur.</p>
            <p>b.System acceptance will determine if all bet canceled or confirmed in 20-second.</p>
            <p>c.Pending bet not confirmed or as fail, if technical problem or other special case during match.</p>
            <p>d.All inPlay bet info (include score, time etc.) as reference. All info neither as settle basis nor as bet basis.</p>
        </div>
    </div>
    `,
	ESportsSettlementRules:
		`
        <div class="item">
        <div class="item-title">1.For settle</div>
            <div class="item-content">
				For settlement purposes, a match without evidence of having taken place or confirmed not to take place or suspended will be considered void/no result/no participation, unless the involved match occurs within 24 hrs from the planned time. Bets on team/player to be qualified in the championship competition are always valid.
            </div>
        </div>
        <div class="item">
            <div class="item-title">2.Listed team info error</div>
            <div class="item-content">
			All bet as void, if listed match team info error. Company reserve rights to ammend, if misspelled player/team name, badge display error. All bet still valid, unless wrong in itself.
            </div>
        </div>
        <div class="item">
            <div class="item-title">3.MarketInfo error</div>
            <div class="item-content">
			The market bet as void, if all listed match info error. Company reserve rights to ammend match title,  time error to be consistent with real league/participant/team. All bet still valid and settle based on real result.
            </div>
        </div>
        <div class="item">
            <div class="item-title">4.Change team name or player</div>
            <div class="item-content">
                <p>a.Origin name provided are valid, if player or team changed name and able to confirm which match or game. Bet won't cancel due to replace or substitute player. All bet normal, if institution allow substitute player and announce official result.</p>
                <p>b.If a team changes name but same lineup, all new and old team name bet are valid. All bet as void, if a team substitute new lineup and every player differ from origin team.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">5.Unofficial player</div>
            <div class="item-content">
			If have unofficial player (or any one) not announce player changes before release schedule. Company reserve rights to cancel and refund.
            </div>
        </div>
        <div class="item">
            <div class="item-title">6.No match/rematch in 1map due to force majeure</div>
            <div class="item-content">
                <p>a.No match in 1map due to force majeure. Settled result valid. Unsettled result bet refund.</p>
                <p>b.In a map, if due to force majeure the match has to rematch, the settled result is still valid, unsettled bets will be based on the rematch result. If the rematch is not started within 24 hrs, all unsettled bets will be voided.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">7.Map Advantage</div>
            <div class="item-content">
				A team has 1 or more map advantages. E.g: A team that wins Upper Bracket Finals receives a map advantage, "Map 2" refers to the 1st map of current game, "Map 3" is 2nd map of current game and so on.
            </div>
        </div>
        <div class="item">
            <div class="item-title">8.Late Bets(Past Post)</div>
            <div class="item-content">
				Late Bets(Past Post)- if a pre-match bet is accepted after the match has already started for any reasons. All betting in this case will be view as Past Posting. If InPlay didn't close on time, every bet is placed after this session will be viewed as Past Posting. We reserve rights to void bet.
            </div>
        </div>
        <div class="item">
            <div class="item-title">9.Other rule</div>
            <div class="item-content">
                <p>a.Bet settle based on independant data, if have result inconsistent or conflict.</p>
                <p>b.Bet settle match, broadcast or game based on official result. Void/no result/unentered bet as void.</p>
                <p>c.Company reserve rights to suspend on settlement, if unsure result.</p>
                <p>d.Can't edit result and no accept inquery after 36-hour. Company will reset/correct result for human error in 36-hour. System error or result source reference error cause error result; Have prove match end but no prove of market result exceed 36-hour, bet refund.</p>
            </div>
        </div>
        <div class="item">
			<div class="item-title">10.Compute refund rule</div>
            <div class="item-content">
                <p>Win：refund = bet * odds</p>
                <p>Lose：refund = 0</p>
                <p>Half win：refund = bet * ( ( odds - 1 ) / 2 + 1 )</p>
                <p>Half lose：refund = bet * 50%</p>
                <p>Void：refund = bet</p>
            </div>
        </div>
        `,
	gameShows:
		`
        <div class="item">
            <div class="item-title">1.1x2 market</div>
            <div class="item-content">
				It's about match and map. Based on official match result, score web or broadcast (video demo). If match game error or have changes, inconsistent with website; Bet void and refund.
            </div>
        </div>
        <div class="item">
            <div class="item-title">2.Handicap</div>
            <div class="item-content">
			Based on official match result, web score or broadcast (video demo). Handicap include map, game, kill. If match map error or have changes, inconsistent with website. Bet void, refund.
            </div>
        </div>
        <div class="item">
            <div class="item-title">3.When Win Lose market final result as Tie</div>
            <div class="item-content">
				When the market only provides Win/Lose option which ends in a Tie in final result (if there is an Overtime, result of the last Overtime will be considered as game final result). All affected bets will be void and refunded.
            </div>
        </div>
        <div class="item">
            <div class="item-title">4.Post-match no stats</div>
            <div class="item-content">
				League of Legends(LOL) match, payout based on post-match stats. Next based on official website broadcast (video demo) to settle. Post-match stats modify and info, may different due to location.
            </div>
        </div>
        <div class="item">
            <div class="item-title">5.Two-Way Handicap winner:</div>
            <div class="item-content">
                <p>Case1. Bet on Tie match (E.g. BO2 play 2game, exclude OT BO1).</p>
                <p>Player need predict who will win. There will be no Tie to win bonus. Refund if result is Tie.</p>
                <p>Case 2. Win Lose match.</p>
                <p>Player need to predict who will win match. OT will counted.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">6.1X2 3Way Handicap:</div>
            <div class="item-content">
				For Tie match (E.g. BO2 play 2game, exclude OT BO1). Player need to predict result to win bonus.
            </div>
        </div>
        <div class="item">
            <div class="item-title">7.Handicap:</div>
            <div class="item-content">
				<p>Predict result after handicap, any OT or Injury counted</p>
				<p>Handicap, a number to "balance probability." "-" as team or player prefix is "Handicap"; "+" is Underdog. Highest handicap win. Handicap in 3 forms as follows:</p>
				<p>Integer</p>
				<p>Handicap as integer. E.g. -1, -2, -3 etc. Substract Handicap from team. If Over Underdog result as win, or else lose, equal as no win-lose (as Void)</p>
				<p>E.g.  A-1 & B +1; 2team result as A=3, B=2; A(3)-1=2=B(2), AB team are no win-lose</p>
				<p>Life & Death</p>

				<p>Handicap as half. E.g. -0.5, -1.5, -2.5 etc. Substract Handicap from team. If Over Underdog result as win, or else lose</p>

				<p>E.g. A-1.5 & B+1.5. 2team result as A=3:B=2, A(3)-1.5=1.5; A(3)-1.5=1.5&lt;B(2), team B Underdog win </p>
				
				<p>Combo</p>

				<p>Case1: Handicap as .25, e.g. -0/0.5, -1/1.5, -2/2.5 etc. Substract Handicap integer from team. If Over Underdog result as win, or else lose, equal is team as Half lose, Underdog as Half win</p> 

				<p>E.g. A-1/1.5 & B+1/1.5. 2team result as A=1:B=0, A(1)-1=0=B(0), team A Half lose, team B Half win</p>

				<p>Case2: Handicap as .75, e.g. -0.5/1, -1.5/2, -2.5/3 etc. Substract Handicap integer from team. If Over Underdog result as win, or else lose, equal is team as Half win, Underdog as Half lose </p>

				<p>E.g. A-0.5/1 & B+0.5/1. 2team result as A=1:B=0, A(1)-1=0=B(0), team A Half win, team B Half lose</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">8.Over/Under:</div>
            <div class="item-content">
				<p>Predict total Over or Under market value, any OT or Injury counted</p>

				<p>Over/Under in 3 form as follows:</p>

				<p>Integer</p>

				<p>Market value as integer. E.g. 26, 27, 28 etc. Substract market value from Goals. Result positive as Over win, or else Under win, zero as no win-lose (as Void)</p>

				<p>E.g. Over 26 and Under 26. If 2teams Goals as 26, 26-26=0, Over 26 and Under 26 are no win-lose</p>

				<p>Life & Death</p>

				<p>Market value as half. E.g. 26.5, 27.5, 28.5 etc. Substract market value from Goals. Result positive as Over win, or else Under win</p>

				<p>E.g. Over 26.5 and Under 26.5. If 2teams Goals as 26, 26-26.5=-0.5; Under 26.5 as win, Over 26.5 as lose </p>

				<p>Combo</p>

				<p>Case1: Market value as .25, e.g. 26/26.5, 27/27.5, 28/28.5 etc. Substract market value integer from Goals. Result positive as Over win, Or else Under win; Zero is Over as Half lose, or else Half win</p>

				<p>E.g. Over 26/26.5 and Under 26/26.5. If 2teams Goals as 26, 26-26=-0.5; Over 26/26.5 as Half lose, Under 26/26.5 as Half win </p>

				<p>Case2: Market value as .75, e.g. 26.5/27, 27.5/28, 28.5/29 etc. Substract market value integer from Goals. Result positive as Over win, or else Under win; Zero is Over as Half  win, or else Half lose</p>

				<p>E.g. Over 26.5/27 and Under 26.5/27. If 2teams Goals as 27, 27-27=0; Over 26.5/27 as Half win, or else Half lose </p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">9.Odd/Even:</div>
            <div class="item-content">
				Predict final result odd or even count. Extra Time or OT will counted. (If result show 0, settle as "Even")
            </div>
        </div>
        <div class="item">
            <div class="item-title">10.Time:</div>
            <div class="item-content">
				(Duration/Event time) - Match or event time; Match time based on official scoreboard end game time, next based on game; Event time based on time happend; Both duration, > or = time counted as >
            </div>
        </div>
        <div class="item">
            <div class="item-title">11.Total Kill:</div>
            <div class="item-content">
			Total Count in single game; Based on official scoreboard. Next based on score in game.
            </div>
        </div>
        <div class="item">
            <div class="item-title">12.Total Towers Destroyed:</div>
            <div class="item-content">
				If team surrender before the match ends, then all destroyed towers in map after surrender will be counted. Including destroyed by creeps or denying.
            </div>
        </div>
        <div class="item">
            <div class="item-title">13.Location of 1st Destroyed Tower:</div>
            <div class="item-content">
				Refers to the location of tower, consists Top/Mid/Bot, total 6 towers (Tier 1).
            </div>
        </div>
        <div class="item">
            <div class="item-title">14.Total Kills:</div>
            <div class="item-content">
				Refers to total kills of participating team. Based on scoreboard.
            </div>
        </div>
        <div class="item">
            <div class="item-title">15.Assists:</div>
            <div class="item-content">
				Assists: Refers to the number of assists by the teams players, the scoreboard shall prevail.
            </div>
        </div>
        <div class="item">
            <div class="item-title">16.1st blood:</div>
            <div class="item-content">
				Based on scoreboard. 1st score as 1st blood.
            </div>
        </div>
        <div class="item">
            <div class="item-title">17.1st tower:</div>
            <div class="item-content">
				The Team to Destroy The 1st Tower (Denying towers in DOTA 2 is considered as the Denier destroys the opposing towers).
            </div>
        </div>
        <div class="item">
            <div class="item-title">18.N kill:</div>
            <div class="item-content">
				Refers to one of the teams will be the first to reach N Kill. E.g: 5 Kills: First team to reach 5 Kill.
            </div>
        </div>
        <div class="item">
            <div class="item-title">19.Nth kill:</div>
            <div class="item-content">
				It's any team just get Nth kill. E.g. 5th kill: It's any team get 5th kill.
            </div>
        </div>
		<div class="item">
            <div class="item-title">20.Player special betting:</div>
            <div class="item-content">
				Map Winner refer to the player who gets the highest kill in a single map. (Draw refund)
            </div>
        </div>
		<div class="item">
            <div class="item-title">21.Highest KDA:</div>
            <div class="item-content">
				Calculated as [K+A]/[D+1], K (kills), A (assists), D (deaths) (Calculate 2 decimal places). (Draw refund)
            </div>
        </div>
        `,
	gamePlay: [
		{
			title:'DOTA2',
			value:
				`
						<p>1.<span class="label">DOTA2 dropped during game.</p>
						<p>Case A: If player dropped before 1st blood, but reconnected during the game. All result considered valid and normal settle.</p>
						<p>Case B: If player dropped after 1st blood, and the game is proceed normally, that means there is no withdrawal, all official valid results will be settled normally.</p>
						<p>Case C: If player dropped before 1st blood and reconnect fail until game end. Bet considered invalid to ensure fair play.</p>
						<p>2.<span class="label">Position:</span>Team Info Position ID (Based on real case, if origin Position exchanged).</p>
						<p>3.<span class="label">Support get 1st blood:</span>Whether if Team Info Position4 or 5 player get 1blood.</p>
						<p>4.<span class="label">Will 1st Blood Occur Before 3 Minutes: </span>Whether 1st Blood to happen within 3 minutes. (00:00 - 02:59)</p>
						<p>5.<span class="label">1st Tower Destroyer: </span> Denying towers in DOTA 2 is considered as the Denier destroys the opposing towers.</p>
						<p>6.<span class="label">1st Roshan Kill:</span>Settle based on website or broadcast (video demo) result.</p>
						<p>7.<span class="label">Will Mega Creeps Spawn :</span> 6 barracks destroyed counted as "Yes".</p>
						<p>8.<span class="label">Team to Score Nth Kill: </span> refer to any team who score Nth Kill just in time and isn't the first to score N Kill (E.g: Score 20th Kill, means one team managed to score 20th Kill).</p>
						<p>9.<span class="label">Team with 5 Kills Before 8 minutes:</span>Whether a team to accumulate 5 kills within 8 minutes. (00:00 - 07:59)</p>
						<p>10.<span class="label">Most Smoke of Deceit (Consumables):</span> Exclude Ninja Gear with Invisibility.</p>
											
						<p>11.<span class="label">Consume Smoke of Deceit (Consumables): </span>2 teams' consumption, exclude Smoke of Deceit of Ninja Gear.</p>
						<p>12.<span class="label">Consume 1st Aghs:</span>1st team consume Aghanim's Shard.</p>
						<p>13.<span class="label">Aghs Consumed:</span>2 teams' Aghanim's Shard consumption.</p>
						<p>14.<span class="label">1st 4 Bounty Runes Equal: </span>Whether 4 Bounty Runes (made at kickoff 00:00) with equal divide by 2 teams.</p>
						<p>15.<span class="label">Most Creep Score (Last Hit): </span>Only Last Hit will be counted, exclude Deny.</p>
						<p>16.<span class="label">Kills Handicap Before 10 Minutes: </span>Cumulative Kill Handicap before 10 minutes. (00:00 - 09:59)</p>
						<p>17.<span class="label">Total Kills Before 10 Minutes: </span>Cumulative Kills before 10 minutes. (00:00 - 09:59)</p>
						<p>18.<span class="label">Destroy first Tormentor:</span> Destroy first Tormentor team in game.</p>
						<p>19.<span class="label">Highest Net Worth Before 10 Minutes: </span>Refers to the Net Worth gained before 10 minutes. (00:00-09:59) (Draw refund)</p>
						<p>20.<span class="label">Highest Net Worth Before 20 Minutes: </span>Refers to the Net Worth gained before 20 minutes. (00:00-19:59) (Draw refund)</p>
						<p>21.<span class="label">Water Rune Equally Divided: </span>Refers to whether the four water runes within 06:00 minutes in the game are divided equally.</p>
						<p>22.<span class="label">The Same Team Picked the First Two Wisdom Runes within First 14 Minutes: </span>Only the first two wisdom runes are counted within 14:00 minutes of game. (14:00 minutes is not counted)</p>
						<p>23.<span class="label">Most Last Hits of Position 2 within 5 Minutes: </span>Cumulative last hit (exclude deny) before 05:00 minutes. (05:00 minutes is not counted)</p>
						
				`,
			icon:'game-logo-dota2'
		},
		{
			title:'League of Legends',
			value:
				`
						<p>1.Role (Top/Mid/Support/ADC) based on offical pre-match bracket.</p>
						<p>2.<span class="label">Rift Herald:</span>Only kill counted, this team kill as win.</p>
						<p>3.<span class="label">Total Turrets Destroyed O/E:</span> Total turrets destroyed of both teams (include Nexus Turret) based on scoreboard. Inhibitor & Nexus (Base) not included.</p>
						<p>4.<span class="label">Dragon Settlement: </span> Only elemental dragon counted. Elder dragon not included.</p>
						<p>5.<span class="label"> Total Players awarded 1st Baron Buff: </span> if there is no Baron slain, bet refund.</p>
						<p>6.Destroying the respawned Inhibitor isn't included in Turret Destroyed.</p>
						<p>7.<span class="label">Total Survivors when Nexus destroyed: </span> Based on a number of survivors, the moment Nexus is destroyed (Total respawned survivors when nexus explodes or after explosion aren't included).</p>
						<p>8.<span class="label">Total Kills Before 10 Minutes:</span>Cumulative Kills before 10 minutes. (00:00 - 09:59)</p>
						<p>9.<span class="label">1st Blood：</span>First killer as winner (based on scoreboard).</p>
						<p>10.<span class="label">Time-based market, settled bet：</span>Settle based on game progress. Settle based on website score & post-match stats, broadcast (video demo) result.</p>
						<p>11.<span class="label">Elemental Dragon Attribute:</span>Next Dragon element will show on broadcast. Broadcast info as result. Settle based on website score or broadcast (video demo) result.</p>
						<p>12.<span class="label">Total kills (00:00-10:00 minute, in fixed period)：</span>Safe bet as in fixed period. If match not exceed fixed time. E.g. 10:00-minute. 0-10minutes as void. Settle based on website score or broadcast (video demo) result, as per game timer.</p>
						<p>13.<span class="label">Whether if Elder Dragon stolen：</span> Settle based on 2team 0:0 result as No, if no Elder Dragon was killed. Settle based on website score & post-match stats, broadcast (video demo) result.</p>
						<p>14.<span class="label">Last Digit of Total Kills:</span> Predict the sum of last digit of total both teams kills will higher or lower than a quoted value. E.g: Result 9:12 (sum of last digit of total both teams kill 9+2=11). Settle based on score on offical website and post-match stats, API or broadcast (video demo) result.</p>
						<p>15.Triple Kill shows up after killing 3 enemy champions in a row; Quadra Kill shows up after killing 4 enemy champions in a row.</p>
						<p>16.<span class="label">Ace Occur: </span>the death of all members of a single team or the in-game prompt in the stream is counted as Ace Occur.</p>
						<p>17.<span class="label">Race To Herald's Eye:</span>1st team get 1st Rift Herald's Eye. Unrelated to killing Rift Herald.</p>
						<p>18.<span class="label">2 Teams Gold Difference Over/Under and Handicap market:</span>The settlement will base on the score on the official website and post-match stats, API or Livestream (video demo). Note: If Gold difference of Over/Under and Handicap markets are  the same as the one provided by us, the bets will be considered invalid (Draw) and refunded.</p>

						<p>19. <span class="label">10 Minutes Kill HDP: </span>Valid bet as in fixed period. If match not exceed fixed time. E.g. 10:00 minutes. 0~10 minutes as void. Settle based on game timer, website score or broadcast (video demo).</p>
						<p>20. <span class="label">Top TD Destroyed: </span>Exclude 2 teams' Nexus Turret.</p>
						<p>21. <span class="label">Mid TD Destroyed: </span>Exclude 2 teams' Nexus Turret.</p>
						<p>22. <span class="label">Bot TD Destroyed: </span>Exclude 2 teams' Nexus Turret.</p>
						<p>23. <span class="label">Role with the Most Turrets Destroyed: </span>Only Top and Bot Role are counted in which Role with the highest number of tower destroyed. Excluding Mid Role (Draw No Bet).</p>
						<p>24. <span class="label">Most Gold: </span>Team Golds based on scoreboard.</p>
						<p>25. <span class="label">Kill 1st Baron Nashor: </span>1st Baron killed by any Role Champion, will based on video. Bets are considered invalid and refunded if no kill.</p>
						<p>26.【Kill Handicap when one team gets N Kills】is Kill Handicap at scoreboard when lead side over N Kills at scoreboard.</p>
						For example, market play【Team A Kill Handicap-1.5 when one team gets 5 Kills】:
						If get 5 Kills, scoreboard score 5:3, 5-1.5=3.5>3 as【Team A Kill Handicap-1.5 win】</p>
						<p>27.【Total Kills when one team gets N Kills】is Total score at scoreboard when lead side over N Kills at scoreboard.
						For example, market play【Total Kills 8.5 when one team gets 5 Kills】:
						If Kills score at scoreboard 5:4 when lead side over 5 Kills, overall Total Kills 9, bet on【Over】as win</p>
						<p>28.【Total Kills when one team gets N Kills】is Total score at scoreboard when lead side over N Kills at scoreboard.
						For example, market play【Total Kills 8.5 when one team gets 5 Kills】:
						If Kills score at scoreboard 5:4 when lead side over 5 Kills, overall Total Kills 9, bet on【Over】as win</p>
						<p>29. <span class="label">Last Kill Champion Role: </span>Is last Kill Champion Role in a game, based on video.</p>
						<p>30. <span class="label">Last Death Champion Role: </span>Is last Death Champion Role in a game, based on video.</p>
						<p>31. <span class="label">Highest Last Hit: </span>Refers to the highest number of last hit obtained by the players, the results are based on the scores on the official website and the post-tournament statistics panel. (Draw refund)</p>
						<p>32. <span class="label">Highest Last Hit Before 10 Minutes: </span>Refers to the highest number of last hit gained before 10 minutes (00:00 - 09:59). (Draw refund)</p>
						<p>33. <span class="label">First to Obtain Mythic Item: </span>Refers to the player who gets the first Mythic Item. (based on the Mythic Item provided by the official website)</p>
						<p>34. <span class="label">Highest Kill Streak Will Be Quadra Kill or Penta Kill: </span>Only the highest kill streak is counted, e.g. if the highest is Quadra Kill then Quadra Kill wins, if the highest is Penta Kill then Penta Kill wins, the rest are considered as none.</p>
						<p>35. <span class="label">Epic Monsters: </span>Refers to Elemental Dragon, Baron Nashor, Rift Herald, and Elder Dragon.</p>
						<p>36. <span class="label">Elder Dragon Spawned: </span>Refers to whether the Elder Dragon is spawned. Result will be Yes as long as the Elder Dragon was spawned. (regardless of whether it is killed or not)</p>
						<p>37. <span class="label">Last Killed Champion Role upon Slaying the First Baron: </span>If Baron Nashor is not killed, it will be considered as no result.</p>
						<p>38. <span class="label">Slain the Nth Dargon Within 1 Minute After Respawned: </span>If the game ends within one minute after the respawn of the Nth dragon and it's not killed, result will be considered as No ; If the game ends before the Nth dragon respawn, it will be considered as no result.</p>
						<p>39. <span class="label">First Activated "Flash"/"Teleport": </span>Refers to the first player to activate "Flash"/"Teleport" skills. The skill must be selected by both players to be considered as effective bets. If one of the player does not choose the corresponding skill, example: Player A chosen "Ignite" and "Flash", Player B chosen "Flash" and "Teleport", then First Activated "Teleport" will be considered as no result.</p>
					`,
			icon:'game-logo-lol'
		},
		{
			title:'CSGO/CS2',
			value:
				`
						<p>1.<span class="label">1st Pistol win:</span> Win 1st round Pistol team.</p>
						<p>2.<span class="label">Lead 5/10 round winner:</span> Lead win 5/10 round team.</p>
						<p>3.Odd/Even, result counted as 0, as Even.</p>
						<p>4.<span class="label">CSGO round define:</span>If new round countdown by buying weapon and equipment, last round as end.</p>
						<p>5.<span class="label">N Round - Total Headshot:</span>Only headshot icon/hint occur as headshot kill. Any headshot will counted.</p>
						<p>6.<span class="label">1st kill:</span>Must kill by opponent. If suicide or teammate kill, decide as the round no 1st Kill, bet refund.</p>
						<p>7.<span class="label">1st killed:</span> 1round 1st killed player, any death counted.</p>
						<p>8.<span class="label">Pistol Round Total Kills:</span>Settle based on website score or broadcast (video demo).</p>
						<p>9.<span class="label">Kill:</span> Settle based on website score or broadcast (video demo).</p>
						<p>10.<span class="label">1st Half Winner/2nd Half Winner: </span></p>
						<p>In CSGO First Half (Round 1 to Round 15) / Second Half (Round 16 to Round 30, draw refund) The teams with the highest score wins, each half is calculated separately.</p>
						<p>In CS2 First Half (Round 1 to Round 12, draw refund) / Second Half (Round 13 to Round 24, draw refund) The teams with the highest score wins, each half is calculated separately.</p>
						<p>11.<span class="label">Pistol, RoundN total headshots:</span>Settle based on website result, broadcast (video demo). Note: Headshot killed by teammate also counted.</p>
						<p>12.<span class="label">Pistol, RoundN total kills:</span>Settle based on website result, broadcast (video demo).Note: Bomb killed not counted.</p>
						<p>13.<span class="label">Round N won: </span> There are 4 winning methods; Elimination/Bomb Explosion/Bomb Disposal/Time Runs Out. The round can be won using one of the above methods.</p>
						<p>14.<span class="label">Will There be a Knife kills: </span> Pick side,warmup not counted. Teammate knife kill, counted as "Yes".</p>
						<p>15.<span class="label">Grenade Kills:</span>  Picking a side and warmup aren't counted. Only "Grenade" Kills is counted, not any "Fire Bomb" or projectile. Killed by teammate using grenade is also counted as "Yes".</p>
						<p>16.<span class="label">1st half /map winner:</span> 1st half /map both have to win.</p>
						<p>17.<span class="label">Player with Most Assists: </span> All Kill assist in CSGO will be counted.Note: Flashbang assists not counted.</p>
						<p>18.<span class="label">Total Clutches Won: Official calculation method: </span>The number of players in this round is reduced to 1 vs 1, and a total of 9 kills must be achieved in this round to be considered as a Clutch. Settlement will only be based on official post-match results showing "CLUTCHES WON".</p>
						<p>19.<span class="label">Whether or not a player gets three kills in a single round (rounds N through N+1): </span>In rounds N through N+1, a single player with three or more kills in a single round (including kills of teammates) is considered to won the betting order. If rounds N through N+1 are not played, the order is considered null and void.</p>
						<p>20.<span class="label">Market with specified map: </span>If the selected map is not the specified map of the market, it will be considered as no result.</p>
						<p>21.<span class="label">Market with player's name only calculate the data of a single player. Example: </span>Player With Higher Kills : A=30 B=25, then the option including player A wins ; If there are multiple players, the option including the player with the highest corresponding data wins. Example: Player With Higher Assists : A=3/B=5, C=6/D=1, the highest is C, then option C/D including player C wins. (Draw refund)</p>
						<p>22.<span class="label">Correct Round Score(Exclude Overtime): </span></p>
						<p>If the option is a one-way score, any team that obtains the corresponding score will be considered as win. </p>
						<p>For example in CSGO : 16:9, Team A=9 Team B=16, the option 16:9 is considered as win. Team A=16 Team B=9 Option 16:9 also considered as win ; </p>
						<p>In CS2 : 13:5, Team A=13 Team B=5, the option 13:5 is considered as win. </p>
						<p>If the option is a two-way score, the corresponding team must obtain the correct score to win. </p>
						<p>For example in CSGO: 16:11 11:16, Team A=16 Team B=11, then option 16:11 will be considered as win, Team A=11, Team B=16, then option 11:16 will be considered as win. (If it goes into overtime, 15:15 will be considered as win)</p>
						<p>In CS2 : 13:7 7:13, Team A=13 Team B=7,  then option 13:7 will be considered as win. (If it goes into overtime, 12:12 will be considered as win)</p>
						<p>23.<span class="label">2nd Pistol Round - Winner: </span>In CSGO , Round 16 is the 2nd Pistol Round. In CS2 , Round 13 is the 2nd Pistol Round.</p>
					`,
			icon:'game-logo-csgo'
		},
		{
			title:'King of Glory',
			value:
				`
						<p>1.<span class="label">Turret Destroyed:</span> Which team is the first to destroy 1st Turret. Note: Base crystal isn't included.</p>
						<p>2.<span class="label">1st blood:</span>Lead kill opponent as winner (based on scoreboard).</p>
						<p>3.<span class="label">Will 1st Blood Occur before 2 Minutes: </span>Whether 1st Blood to happen within 2 minutes. (00:00 - 01:59)</p>
						<p>4.<span class="label">1st Tyrant Killed Before 2:30: </span>Whether to kill the 1st Tyrant within 2:30 minutes. (00:00 - 02:29)</p>
						<p>5.Player Position (Roam/Mid/Jungle/Clash Lane/Farm Lane) based on official post match bracket.</p>
						<p>6.<span class="label">Lane with the Most Turrets Destroyed: </span>Only Overlord Lane and Tyrant Lane are counted in which lane with the highest number of tower destroyed. Excluding Mid Lane (Draw No Bet).</p>
						<p>7.<span class="label">Total Overlord Slain: </span>All Overlords are counted, including Prophet and Shadow Overlord.</p>
						<p>8.<span class="label">Total Tryant Slain:</span> All Tyrants are counted, including Tyrant and Shadow Tyrant. </p>
						<p>9.1st Tyrant Killed Before 4:30: Whether to kill the 1st Tyrant within 4:30 minutes. (00:00 - 04:29)</p>
						<p>10.<span class="label">Kills Handicap Before 6 Minutes: </span>Cumulative Kill Handicap before 06:00 minutes. (06:00 minutes is not counted)</p>
						<p>11.<span class="label">Total Kills Before 6 Minutes: </span>Cumulative Kills before 06:00 minutes. (06:00 minutes is not counted)</p>
						<p>12.<span class="label">First Tower Destroyed Before 5:30: </span>Refer to whether the first tower will be destroyed before 05:30 minutes. (Exactly 05:30 minutes will be considered as No)</p>
						<p>13.<span class="label">Ancient: </span>Ancient refer to Tyrant , Dark Tyrant , Overlord , Phophet Overlord , Dark Overlord and Storm Dragon.</p>
						<p>14.<span class="label">First Storm Dragon Spawn Location: </span>If Storm Dragon hasn't spawn yet will be cancelled and considered as no result.</p>
					`,
			icon:'game-logo-aov'
		},
		{
            title:'Valorant',
            value:
                `
					<p>First Pistol round win: Win 1st Pistol Round team.</p>
					<p>Second Pistol round win: Win 13th Pistol Round team.</p>
					<p>First Half win: Highest score team win in First Half (1st~12th round).</p>
					<p>Second Half win: Highest score team win in Second Half (13th~24th round).</p>
					<p>Round N winning method: There are 4 winning methods such as Timeout/Bombed/Defused/Eliminated. Play determination is the round win by way of victory.</p>
                `,
            icon:'game-logo-Valorant'
        },
		{
			title:'Basketball',
			value:
				`
						<p>1.NBA Full Time bet include OT score. Quarter, bet exclude OT score.</p>
						<p>2.Settle, based on match or official score provider or official web. If official score provider or official web unexist or evidence prove that official provided data error, we will settle based on independent info.</p>
						<p>3.If match not ongoing at planned date, no rematch in 36-hour planned time, bet as void and refund.</p>
						<p>4.If match interrupted, no resume in 36-hour; bet with result still valid, bet with no result as invalid.</p>
						<p>5.Match venue have changes, bet as void.</p>
						<p>6.Quarter/HT bet. Match must finish, Quarter bet as valid. Unless play rule have other description.</p>
						<p>7.If match ongoing before legal time, bet before match still valid, bet after match as void (inPlay other discourse ).</p>
						<p>8.Late Bets(Past Post)- if a pre-match bet is accepted after a match has already started for any reasons. All betting in this case will be view as Past Posting. If InPlay didn't close on time, every bet is placed after this session will be viewed as Past Posting. We reserve rights to void bet.</p>
					`,
			icon:'game-logo-basketball'
		},
		{
			title:'Soccer',
			value:
				`
						<p>1.Match start date and time on website as reference. If match no start at planned time, not same day (current local time), bet as void and refund. Whether match if suspend or delay, World Cup qualified team or  Champion bet  always valid.</p>
						<p>2.If match suspend without full match time, the day (in 36-hour) unfinished, (current local time) bet as void and refund.</p>
						<p>a.If match suspend without full match time. If 1H end, bet valid. If suspend match no resume at (current local time) day, predict 1H unrelated bet void. Include and not limited to: Corner, Offside, Withdraw, Red/Yellow card, 2team score, Correct score, 1st Goal time, Clean Sheet and player score/not score.</p>
						<p>b.Both team scored, if match suspend, bet as void. Player scored, if match suspend, score/not score bet as void. If match suspend, 1st goal score bet, Corner/Offside bet, as void. If match suspend and team score, 1st or next score team predict bet as valid. If match suspend, but have 1goal before suspended, before or after score team bet as valid.</p>
						<p>3.If market have Draw item, result as Draw. We only pay Draw bet, any win team bet as lose.</p>
						<p>4.Unless other description, or else bet settle based on normal match time (include Injury time), exclude OT and PK result.</p>
						<p>5.Big/Small: Must finish full match, total goal bet as valid.</p>
						<p>6.If Big/Small equal, will refund.</p>
						<p>7. Unless other description, if home to neutral venue , or neutral to home venue, bet still valid. If home to origin away venue, bet as void. If home, away venue name reverse error, bet as void.</p>
						<p>8.Bet settle based on official or authority institution ruling result.</p>
						<p>9.Late Bets(Past Post)- if a pre-match bet is accepted after a match has already started for any reasons. All betting in this case will be view as Past Posting. If InPlay didn't close on time, every bet is placed after this session will be viewed as Past Posting. We reserve rights to void bet.</p>
					`,
			icon:'game-logo-football'
		},
		{
			title:'FIFA',
			value:
				`
						<p>1.InPlay handicap settle based on bet match til quarter result. It's substract current bet score from result. 1H inPlay handicap settle based on 1H result.</p>
						<p>2.Remaining time win settle based on bet match til quarter result. It's substract current bet score from result. 1H remaining quarter win settle based on 1H result.</p>
					`,
			icon:'game-logo-fifa'
		},

	],
	zhuboPan: [
		{
			title:'1.Rules',
			value:
				`
						<p>
							Below are all match, market/bet type common rule. Website "Terms & Conditions" set forth provision shall apply to the "Rules & Regulations".
						</p>
					`,
			icon:false
		},
		{
			title:'2.General',
			value:
				`
						<p>a.We provide bet info in sincere, but not responsible on date, time, venue, opponent, odds, result, sum data (display on realtime video) or other bet info error or lack.</p>
						<p>b.We reserve rights to amend error. We will take relevant action to ensure integrity in managing market. Market define as bet type provided by any Anchor game. We reserve rights in final decision.</p>													
						<p>c.Client should understand match score, match info anytime and confirm situation before bet.</p>
						<p>d.We reserve rights to amend rule of need. Amended rule valid upon release on website.</p>
						<p>e.Client admit website "broadcast" provided score, time and other data from third party, may delay or unaccurate. Client should responsible of all risk as bet based on these info.  Company not ensure info accuracy, integrity and in time. And not responsible to client on relying these results lead to any loss (direct or indirect loss) .</p>
						<p>f.Anchor match as Anchor direct pair Random Passerby match via official game Client.</p>
						<p>g.Anchor match terms for Anchor match, not for professional league. When common rule and special game (E.g. LoL, PUBG, CS:GO etc.) rule have conflict, based on the special game rule.</p>
					`,
			icon:false
		},
		{
			title:'3.Validity',
			value:
				`
						<p>a.If Anchor not join formal play scene (exclude before match) and quit match, or other player quit lead to game can't resume, not counted as valid game. The market only for valid match, next valid game still valid.</p>
						<p>b.If Anchor unstarted but start other game, not counted as valid game. The market only for valid match, next valid game still valid.</p>
						<p>c.If Anchor play time less than half of game duration, as void and refund.</p>												
						<p>d.Game error, one side deliberate early surrender, affected bet may canceled.</p>
						<p>e.If malice hang lead to game time error occur, affected bet may canceled.</p>
						<p>f.If player malice or deliberate being defeated, affected bet may canceled.</p>
					`,
			icon:false
		},
		{
			title:'4.Basic rule',
			value:
				`
						<p class="title">• Valid/partial valid</p>													
						<p>Anchor game must more than half (half) game time operate by Anchor itself, or else refund.</p>
						<p class="title">• Refund, postpone</p>
						<p>Anchor play unspecific game or specific game no valid market, as unstarted.</p>
						<p class="title">• Payout</p>
						<p>All based on official result if no special description.</p>
					`,
			icon:false
		},
		{
			title:'5.League of Legends',
			value:
				`
						<p class="title"> • Decide, postpone</p>
						<p>a.Game in 5-minute decide if match valid.</p>
						<p>b.Match end in 5-minute (may rematch) as postponed.</p>
						<p>c.Must play 5 vs 5 official classic League of Legends rule (edited rules/mode invalid).</p>
						<p class="title"> • Market terms:</p>
						<p>All market must based on different game, meet terms, or else refund. League of Legends terms as:</p>
						<p>a.Anchor rank: Anchor must play game based on rank point and match word description. As follows:</p>
						<p class="rules1 rules-image"></p>
						<p>b.Anchor solo arrange: Pair with Anchor in solo, based on official image when line up. As follows:</p>
						<p class="rules2 rules-image"></p>
						<p>c.Anchor use specific account: Must use our specific account, based on clear discern Anchor account image. As follows:</p>
						<p class="rules3 rules-image"></p>
						<p>d.Anchor must play official LoL. It's impersonal lobby or other edited rule mode, based on top left clear display game mode image. As follows (image as personal display):</p>
						<p class="rules4 rules-image"></p>
						<p class="title"> • Payout</p>
						<p>a.Bet market based on official result, as follows:</p>
						<p class="rules5 rules-image"></p>
						<p>b.Based on reliable final game image, if no official result</p>
						<p class="rules6 rules-image"></p>
						<p>c.Based on official result , if no final game image or conflict on final game image occur</p>
						<p class="rules7 rules-image"></p>
						<p>d.Anchor official result show deserter hint, game result as lose (other player not affected by the rule). As follows:</p>
						<p class="rules8 rules-image"></p>
						<p>e.If Anchor quit, all result based on official final result (deserter as fail). As shown:</p>
						<p class="rules8 rules-image"></p>
				   `,
			icon:'game-logo-lol'
		},
		{
			title:'6.King of Glory',
			value:
				`															
						<p class="title"> • Decide, postpone</p>
						<p>a.Decide game in 1-minute if match valid.</p>
						<p>b.Match end in 1-minute delay til next game.</p>
						<p>c.must play 5 vs 5 official classic King of Glory rule (any edited rules/mode invalid).</p>
						<p class="title"> • Market terms</p>
						<p>All market must based on different game meet terms, or else refund. King of Glory terms as:</p>
						<p>a.Anchor Rank: Based on line up description. As follows:</p>
						<p class="rules9 rules-image"></p>
						<p>b.Anchor use specific account: Must use our specific account, based on clear discern Anchor account image. As follows:</p>
						<p class="rules9 rules-image"></p>	
						<p>c. Anchor solo: Based on line up player count. Unqualified as follows:</p>
						<p class="rules10 rules-image"></p>	
						<p class="title"> • Payout</p>
						<p>a.Bet market based on official result, as follows:</p>
						<p class="rules11 rules-image"></p>
						<p>b.Based on final, if no final image. As follows:</p>
						<p class="rules12 rules-image"></p>
						<p>c.Based on official result, if unreliable or conflict on final game image occur. As shown:</p>
						<p class="rules11 rules-image"></p>	
						<p class="title"> • Cancel, refund, special case</p>
						<p>If Anchor quit and decide outcome without result page; Other result based on last image before quit.</p>
					`,
			icon:'game-logo-aov'
		},
	],
};
