export default {
    stationOrder: {
        corss: 'STR1(IG Parlay)',
        return: 'Slated Return',
        placeholder: 'Enter amount',
        oddsChange: 'Selected bet item, odds or validity have changes',
        total: 'Sum bet',
        maxReturn: 'Max return',
        stationTips: 'InGame Parlay. Max 10 options',
        full: '(Full)',
        tips1: 'Hint: Parlays must ≥2',
        tips2: "Hint: Same market & game, can't inGame Parlay",
        tips3: 'Hint: No inGame Parlay between bet item in 1market',
        tips4: "Hint: The total odds of parlay bets have reached the upper limit, can’t add more!",
        tips5: 'Hint: Bet exceed market, no inGame Parlay',
        limitOver:
            'InGame Parlay exceed limits. Select other market, or delete Parlay item to bet',
        oddsNew: 'Accept <br/> new odds',
        showLimit5: 
            "Exceed day payout. Check slated return, or adjust bet. Let's bet next day!",
    },
    betOrder: {
        return: 'Slated Return',
        betMax: "Can't select. Maximum 10bets",
        placeholder: 'Enter amount',
        matchLimit: 'Match limits',
        marketLimit: 'Market limits',
        corssOptions: 'Pass item',
        hasLight: "Highlights can't string as pass bet",
        oddsChange: 'Selected bet item, odds or validity have changes',
        balance: 'Lack fund',
        showLimit1:
            'Bet quota full. Select other market, match or clear Parlay item to bet',
        showLimit2: 'Bet match meet up limit. Select other match to Parlay',
        showLimit3:
            'Bet quota full. Select other market, match or clear Parlay item to bet',
        showLimit4: 'Meet bet quota. Add or delete Parlay, or retry next day',
        showLimit5: 
            "Exceed day payout. Check slated return, or adjust bet. Let's bet next day!",
        length2: 'At least >2 different match',
        total: 'Sum bet',
        maxReturn: 'Max return',
        acceptOdds1: 'Accept new odds',
        acceptOdds2: 'Accept higher odds',
        acceptOdds3: 'Reject odds changes',
        daily: 'Today',
        corss: 'String',
    },
    index: {
        stationOrder: 'IG Parlay',
        betOrder: 'Betting',
        delAll: 'Clear',
        balance: 'Balance',
    },
    timer: {
        Invalid: 'Invalid',
    },
};
