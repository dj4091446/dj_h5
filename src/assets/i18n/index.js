
import Vue from 'vue';
import VueI18n from 'vue-i18n'
import qs from 'qs'

Vue.use(VueI18n)

export const langMap = {
    cn: { title: "简体中文" , language:'cn'},
    zh: { title: "繁體中文" , language:'zh'},
    en: { title: "English", language:'en' },
    vi: { title: "Tiếng Việt", language:'vi' },
    th: { title: "ไทย", language:'th' },
    ml: { title: "Melayu", language:'ml' },
    ni: { title: "Indonesia", language:'ni' },
}

const query = window.location.search
if (query && query.length > 0) {
    const params = qs.parse(query, { ignoreQueryPrefix: true })
    if (params.lang) {
        localStorage.language =  langMap[params.lang] ? params.lang : "cn";
        let hrefStr = location.href.replace(`lang=${params.lang}`, '')
        history.replaceState(null, null, hrefStr);
    }
}

let defaultLanguage = localStorage.getItem('language') || 'cn'

const i18n = new VueI18n({
    locale: defaultLanguage,
    fallbackLocale: 'cn',
    messages: {}
});

export default i18n;