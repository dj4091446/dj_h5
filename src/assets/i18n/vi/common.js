const numbers = 'Cả trận | Ván 1 | Ván 2 | Ván 3 | Ván 4 | Ván 5 | Ván 6 | Ván 7 | Ván 8 | Ván 9 | Ván 10 | Ván 11 | Ván 12 | Ván 13'
const Mapnums = numbers.split('|')
export default {
    mixins: {
        live: 'Trực Tiếp',
        early: 'Cược Sớm',
        cross1: 'Xiên 1',
        betFail: 'Cược thất bại',
        cross: 'Xiên',
        Global: 'Cả trận',
        copy: 'Đã copy',
        max30: 'Tối đa 30 trận yêu thích',
        same: 'Cùng ván chỉ được thêm 1 kèo cùng loại',
        upperLimit:
            'Tổng tỷ lệ cược các đơn cược xiên đã đạt giới hạn trên, không thể chọn thêm nữa!',
        finish: 'Đang xác nhận đơn cược',
        FullTime: 'Cả trận',
        HalfTime: 'Hiệp 1',
        OneQ: 'Q1',
        TwoQ: 'Q2',
        ThreeQ: 'Q3',
        FourQ: 'Q4',
        champion: 'Cược Thắng',
        overtime: 'Hiệp phụ',
        middleHalf: 'Giữa hiệp',
        secondHalf: 'Hiệp 2',
        rest1: 'Nghỉ Q1',
        rest2: 'Nghỉ Q2',
        rest3: 'Nghỉ Q3',
        overtime_first_half: 'Hiệp phụ 1',
        overtime_second_half: 'Hiệp phụ 2',
        mid_rest: 'Nghỉ giữa hiệp',
        Mapnum: (ctx) => Mapnums[ctx.named('num')],
        all: 'Tất cả',
    },
    commonInput: {
        balance: 'Số dư không đủ',
        oddsChange: 'Tổng tỷ lệ cược đã thay đổi',
        redLimit: 'Vượt quá giới hạn',
    },
    message: { closeMessage: '(2s sau tự đóng)', close: 'Đóng' },
    betItem: {
        nocross:
            'Kèo không hỗ trợ cược xiên, mời chọn kèo đánh dấu "Xiên" khác',
    },
    commonNotData: { golive: 'Tới Trực tiếp xem~' },
    commonTabs: { global: 'Cả trận' },
    listLoad: 'Đang tải',
    numberBoard: { max: 'Tối đa', thousand: 'K' },
    selectOdds: { select: 'Chọn cách nhận', changyong: 'Tiền thường dùng' },
    digitalToChinese: {
        number1: 1,
        number2: 2,
        number3: 3,
        number4: 4,
        number5: 5,
        number6: 6,
        number7: 7,
        number8: 8,
        number9: 9,
        number10: 10,
        number11: 11,
    },
    systemModel: { notice: 'Thông báo', confirm: 'Xác nhận', yes: "yes", no: "no" },
    awardAnimate: {
        jibai:
            '<span>Hạ</span>\n  <span>gục</span>\n  <span>99%</span>\n  <span>người</span>',
        qiji:
            '<span>Người</span>\n  <span>tạo</span>\n  <span>kỳ</span>\n  <span>tích</span>',
    },
    menu: { 
        all: 'Tất cả',
        specials: 'Specials',
    },
    tips: {
        max30days: "Chỉ có thể tra cứu cược trong vòng 30 ngày",
        qiehuanxianlu: "Đổi tuyến",
        qiehuanxianlutishi: "Tuyến hiện tại đang bận, vui lòng ấn nút làm mới để đổi tuyến",
        shuaxing: "Làm mới"
    },
};
