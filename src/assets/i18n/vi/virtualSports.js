export default {
    footballRanking: {
        Leaderboard: 'Bảng xếp hạng',
        Team: 'Đội bóng',
        MP: 'Trận',
        WDL: 'T/H/B',
        Pts: 'Điểm',
    },
    groupKnockout: {
        Round1: 'Vòng 1/16',
        QuarterFinal: 'Vòng tứ kết',
        Semifinals: 'Vòng bán kết',
        Final: 'Chung kết',
        zanwu: 'Không có dữ liệu',
    },
    groupMatches: {
        zu: 'Bảng {no}',
        Team: 'Đội bóng',
        MP: 'Trận',
        WDL: 'T/H/B',
        jin: 'BT',
        shi: 'BB',
        jingshengqiu: 'HS',
        Pts: 'Điểm',
    },
    knockout: {
        GroupStage:'Vòng đấu bảng',
        KnockoutStage:'Vòng đấu loại',
    },
    video:{
        batchNo:'Vòng{no}',
        dateNo:'Kỳ{no}',
        matchEnd:'Đã kết thúc',
    },
    detail:{
        historyRecord:'Chiến tích lịch sử',
    },
    endGame:{
        tips1:"KQ trò chơi E-sports thông thường có thể xem tại đây",
        tips2:"Mời vào {0} để xem KQ",
        tips3:"Không nhắc nữa",
    },
};
