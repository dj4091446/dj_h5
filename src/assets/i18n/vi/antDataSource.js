/*
 * @Author: Supermark
 * @Date: 2022-08-03 19:09:38
 * @Description: 蚂蚁数据源越南语翻译
 * @FilePath: /djUser/src/assets/i18n/vi/antDataSource.js
 */
export default {
  Bet: 'Cược',
  Scene: 'Cảnh',
  VideoLS: 'LS',
  AnimationLS: 'ANIM LS',
  Prescient: 'Xem trước',
  RecentData: 'Dữ liệu',
  HistoryFight: 'Lịch sử',
  GameStat: 'Thống kê',
  RecentStat: 'Thống kê gần đây',
  FightMatchRecord: 'Bản ghi',
  BigBattle: 'Trận lớn',
  SmallBattle: 'Trận nhỏ',
  FirstBloodRate: 'Chiến công đầu',
  RoundNum265: '>26.5 trận',
  FirstTowerRate: 'Trụ đầu',
  PentakillRate: 'F5',
  DecakillRate: 'F10',
  FirstBarrackRate: 'Doanh trại đầu',
  AverageTotalDuration: 'Tổng lượng TB',
  AverageKill: 'Hạ gục TB',
  Data: 'Dữ liệu',
  Baron: 'Baron',
  DrakeDragon: 'Drake/Dragon',
  TurretTowerDive: 'Đẩy trụ',
  Crystal: 'Thủy tinh',
  EconomyDifference: 'Kinh tế',
  NoData: 'Không có dữ liệu',
  ExperienceDifference: 'XP',
  FirstBaronRate: 'Baron đầu',
  FirstDragonRate: 'Dragon đầu',
  Kill: 'Hạ gục',
  FightData: 'Dữ liệu',
  Player: 'Người chơi',
  Output: 'ST',
  Experience: 'XP',
  LastHitDeny: 'CS/Deny',
  TotalOutput: 'Tổng ST',
  DamageModifierRate: 'DMGCR',
  HeroOutput: 'ST tướng',
  KillMonsters: 'NMK',
  LastHit: 'CS',
  VisionRemovalWard: 'Cắm/phá ward',
  DamageTaken: 'Bị thương',
  TrueDamage: 'Vết Thương Sâu',
  MagicalOutput: 'STP',
  PhysicalOutput: 'STVL',
  FirstTyrantRate: 'Bạo chúa đầu',
  FirstOverlordRate: 'Thống trị đầu',
  Economy: 'Vàng',
  ParticipationRate: 'TG',
  ItemSets: 'Trang Bị',
  Situation: 'Tình thế',
  Round1: 'Trận 1',
  FirstBaron: 'Baron đầu',
  FirstDragon: 'Dragon đầu',
  FirstPentakill: 'Pentakill đầu',
  FirstDecakill: 'Decakill đầu',
  MatchStart: 'Bắt đầu',
  All: 'Tất cả',
  Healing: 'Hồi máu',
  AbnormalDataInfoGameDataClosed: 'Thông tin dữ liệu trận đấu dị thường, đã đóng dữ liệu trận đấu này!',
  CloseDataWindow: 'Đóng cửa sổ dữ liệu',
  RoundNo: 'Vòng {no}',
  DestroyTowerCrystal: 'Đẩy trụ/Trụ nhà lính',
  KillHero: 'Hạ gục tướng',
  Barrack: 'Doanh trại',
  RuleDescription: 'Quy tắc hiển thị dữ liệu',
  DescriptionDataDisplayRules: 'Tất cả ND livestream hiển thị trên trang này chỉ mang tính tham khảo! Hội viên có thể sử dụng ND này như HDSD. Chúng tôi sẽ cố gắng hết sức để đảm bảo tính chính xác của ND hiển thị, nếu có sai sót, công ty sẽ không chịu bất kỳ trách nhiệm nào. VD: Khi đang trong trận, đối với kèo cược, lấy thông tin chính xác hiển thị trên đơn cược làm chuẩn!',
  DescriptionDataDisplayRulesRemark: 'Ghi chú: Thông tin dữ liệu trận đấu chỉ mang tính tham khảo, không làm căn cứ cho kết quả!',
  // 暂时缺失部分
  FirstBlood: 'chiến công đầu',
  FirstTower: 'trụ đầu',
  FirstHerald: 'Vanguard đầu',
  FirstCrystal: 'Trụ nhà lính đầu',
  FirstBarrack: 'doanh trại đầu',
  Kill265: '>26.5 hạ gục',
  Kill475: '>47.5 hạ gục',
  Kill235: '>23.5 hạ gục',
  GameTimeOver33: '>33 Thời lượng',
  GameTimeOver35: '>35 Thời lượng',
  GameTimeOver19: '>19 Thời lượng',
  SecondHandgun: 'súng thứ 2 thắng trận',
  More265Rounds: '>26.5 trận',
  GlobalConference: 'Thắng toàn trận',
  SingleConference: 'Thắng trận đơn',
  FirstCrystalRate: 'Trụ nhà lính đầu',
  FirstMeatMountain: 'Roshan đầu',
  DrakeKill: 'Hạ gục Baron',
  PlayerKill: 'Hạ gục đội viên',
  RiftHeraldKill: 'Hạ gục Vanguard',
  DrakesKill: 'Hạ gục Thống trị',
  JoinGame: 'Vào trận',
  QuitGame: 'Rời trận',
  RoundStart: 'Bắt đầu',
  RoundWin: 'Thắng',
  Suicide: 'Tự tử',
  Bomb: 'Đặt bom',
  DisarmBomb: 'Gỡ bom',
  CloseDataWindow : 'Đóng cửa sổ dữ liệu～',
  BlueEconomy: 'Vàng bên xanh：',
  OnlyCurrentLg: 'Giải đấu hiện tại',
  Last10Games: 'Số liệu 10 ván trước',
  zhengbu: 'CS',
  fanbu: 'Deny',

  // 后续使用
  R1Gunwin: 'R1 thắng',
  R16Gunwin: 'R16 thắng',
  The1sWin5Rounds: 'Thắng 5 trận trước',
  The1stWin10Rounds: 'Thắng 10 trận trước',
  EnterTheOT: 'Vào trận phụ',
  Overtime: 'Trận phụ',
  // NoMap: '暂无地图',
  // Map: '地图',
  FirstHalf: '1H',
  SecondHalf: '2H',
  Annihilation: 'Tiêu diệt',
  BombDisposal: 'Gỡ bom',
  Explosion: 'Nổ',
  TimeOut: 'Hết giờ',
  

  // 暂时没使用
  // TotalKill: '总击杀',
  // WinRate: '胜率',
  // FightRecord: '交手记录',
  // LiveStream: '直播',
  // ThisGameWinRate: '本场比赛胜率',
  // No: '无',
  // Processing: '进行中',
  // GameOver: '比赛已结束',
  // GameTime: '游戏时间',
  AddPoints: 'SO',
  // NoAnimationLS: '暂无动画直播',
  Overlord: 'Overlord',
  Tyrant: 'Tyrant',
  // RealTime: '实时',
  // Score: '比分',
  // GamesTime: '比赛时间',
  // SmallBattleSituation: '小局战况',
  // BigBattleSituation: '大局战况',
  // Round: '回合',
  // FirstTyrant: '首暴君',
  // FirstOverlord: '首主宰',
  // Topsolo: '上单',
  // Jungle: '打野',
  // Solo: '中单',
  // Support: '辅助',
  // Position1: '1号位',
  // Position2: '2号位',
  // Position3: '3号位',
  // Position4: '4号位',
  // Position5: '5号位',
  // bet: '投注',
  // Battle: '比赛',
  // Time: '时间',
  // Duration: '时长',
  // Schedule: '赛程',
  // GameDuration: '游戏时长',
  // BattleData: '比赛数据',
  // Close: '关闭',
  // PlsCloseGameData: '请关闭赛事数据',
  // OverView: '概况',
  AchieVements: 'K/D/A',
  // RealTimeWinRate: '实时胜率',
  // LayAside: '收起',
  // UpdateTime: '更新时间',
  // RecentDataDetails: '近期数据详情',
  // Lead103K: '领先10.3K',

  Gamelogs: "Nhật ký trận đấu",
  Enter: "Vào ",
  Exit: "Thoát ",
  Roundstart: "Bắt đầu",
  Winround: "Thắng",
  Suicide: "Tự tử",
  Placebomb: "Đặt bom",
  Defusebomb: "Gỡ bom",

};