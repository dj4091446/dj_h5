export default {
    close: 'Bỏ qua hướng dẫn',
    goHome: 'Vào trang chính',
    tips1: 'Chủ nhân, chúng ta có chức năng mới, cùng đi xem đi…',
    next: 'Bước sau',
    tips2: 'Nhấp chuột để vào giải đấu có thể cược kết hợp',
    previous: 'Bước trước',
    tips3: 'Chọn kèo kết hợp yêu thích, cược đội sẽ thắng',
    tips4: 'Đội cược thắng tất cả cách chơi kết hợp là giành thưởng~',
    ikonw: 'Đã hiểu',
    tips5: 'Phiên bản hiện tại có thể cược riêng cược đơn và cược xiên~',
    continue: 'Xem tiếp',
    tips6:
        'Tới<span style="color:#E7BB02;">“Trực Tiếp/Cược Sớm/Cược Thắng/Yêu thích”</span>, có thể cược đơn, 1 lần được cược 1 kèo',
    tips7: 'Tới<span style="color:#E7BB02;">“Cược Xiên”</span>, có thể cược xiên kèo nhiều trận',
    tips8: 'Tới<span style="color:#E7BB02;">“Kèo cược xiên”</span>, có thể cược xiên/xiên trong ván',
    tips9:
        'Chọn<span style="color:#E7BB02;">“Cược Xiên”</span>, tới trận đấu đánh dấu<span style="color:#E7BB02;">“Xiên trong ván”</span>',
    tips10:
        'Tới<span style="color:#E7BB02;">“?"</span>xem giới thiệu trò chơi, bật<span style="color:#E7BB02;">“Xiên trong ván”</span>, bắt đầu cược xiên',
    tips11: 'Chọn kèo đúng tạo thành cược xiên trong trận theo Luật chơi',
    tips12: 'Nếu kèo chọn thắng hết, tiền thưởng sẽ tăng gấp bội',
    tips13: '"Champion\'s Summon" mới đã chính thức ra mắt! Tìm hiểu cách chơi ngay!',
    tips14: 'Trước tiên, vui lòng ấn vào "Trò chơi điện tử", Sau đó chọn "Champion\'s Summon"',
    tips15: 'Trang chủ được chia thành 4 khu vực',
    tips16: 'Khu vực thẻ: Hiển thị thời gian đếm ngược cho kỳ mở thưởng hiện tại và Anh hùng của kỳ trước đó',
    tips17: 'Dữ liệu tham khảo: Được chia thành Thống kê/Nam nữ/Khoảng cách công kích/bản đồ đường thường dùng, có thể giúp dự đoán kỳ mở thưởng tiếp theo',
    tips18: 'Khu vực cược cách chơi: Cược Giới tính/Từ xa & Cận chiến/ Vai trò',
    tips19: 'Cược phạm vi đánh: cược theo phạm vi số Sức mạnh công kích, có thể chọn phạm vi theo ý muốn',
    tips20: 'Cài đặt thanh thao tác: bao gồm Phiếu đã cược/ Tra cứu nuôi số, Xóa tùy chọn cược, Nuôi số cho tất cả mục cược đã chọn và Chọn nút Cược ngay',
    tips21: '20 giây mở thưởng một lần, cược và mở thưởng bất cứ lúc nào.'
};
