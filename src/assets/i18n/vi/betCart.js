export default {
    stationOrder: {
        corss: 'Xiên 1 (Xiên trong ván)',
        return: 'Dự kiến hoàn lại',
        placeholder: 'Nhập số tiền',
        oddsChange:
            'Kèo, tỷ lệ cược hoặc tính hợp lệ của mục cược bạn chọn thay đổi',
        total: 'Tổng cược',
        maxReturn: 'Hoàn tối đa',
        stationTips: 'Cược xiên trong ván 1 ván chọn tối đa 10 lựa chọn',
        full: '(Đã hết)',
        tips1: 'Nhắc nhở: Số cược xiên trong ván ≥2',
        tips2: 'Nhắc nhở: Kèo cùng tên cùng ván không được xiên trong ván',
        tips3: 'Nhắc nhở: Mục cược cùng kèo không hỗ trợ xiên trong ván',
        tips4:
            'Nhắc nhở: Tổng tỷ lệ cược các đơn cược xiên đã đạt giới hạn trên, không thể chọn thêm nữa!',
        tips5:
            'Nhắc nhở: Cược kèo vượt quá giới hạn, không hỗ trợ xiên trong ván',
        limitOver:
            'Xiên trong ván hiện đã quá giới hạn cược, mời chọn kèo khác hoặc giảm tùy chọn cược xiên rồi cược',
        oddsNew: 'Chấp nhận tỷ lệ cược mới nhất',
        showLimit5: 
        'Tổng trả thưởng ngày quá giới hạn, mời kiểm tra tiền dự kiến hoàn lại hoặc điều chỉnh tiền cược, chào mừng bạn ngày mai lại đến cược!',
    },
    betOrder: {
        return: 'Dự kiến hoàn lại',
        betMax: 'Không được chọn nữa, chọn tối đa 10 cược',
        placeholder: 'Nhập số tiền',
        matchLimit: 'Giới hạn trận đấu',
        marketLimit: 'Giới hạn kèo',
        corssOptions: 'Tùy chọn cược xiên',
        hasLight: 'Lựa chọn sáng màu không được cược xiên',
        oddsChange:
            'Kèo, tỷ lệ cược hoặc tính hợp lệ của mục cược bạn chọn thay đổi',
        balance: 'Số dư không đủ',
        showLimit1:
            'Đã đạt giới hạn cược. Chọn kèo, trận đấu khác hoặc giảm số kèo cược xiên rồi cược',
        showLimit2:
            'Giới hạn cược trận này đã đạt tối đa, chọn kèo trận khác để cược xiên',
        showLimit3:
            'Đã đạt giới hạn cược. Chọn kèo, trận đấu khác hoặc giảm số tùy chọn cược xiên rồi cược',
        showLimit4:
            'Đã đạt giới hạn cược, tăng hoặc giảm số kèo cược xiên hoặc ngày mai thử lại',
        showLimit5: 
            'Tổng trả thưởng ngày quá giới hạn, mời kiểm tra tiền dự kiến hoàn lại hoặc điều chỉnh tiền cược, chào mừng bạn ngày mai lại đến cược!',
        length2: 'Chọn ít nhất 2 trận khác nhau',
        total: 'Tổng cược',
        maxReturn: 'Hoàn tối đa',
        acceptOdds1: 'Chấp nhận tỷ lệ cược mới nhất',
        acceptOdds2: 'Chấp nhận tỷ lệ cược cao hơn',
        acceptOdds3: 'Không chấp nhận thay đổi tỷ lệ cược',
        daily: 'Hôm Nay',
        corss: 'Xiên',
    },
    index: {
        stationOrder: 'Xiên trong ván',
        betOrder: 'Vé cược',
        delAll: 'Xóa',
        balance: 'Số dư',
    },
    timer: {
        Invalid: 'Hết hiệu lực',
    },
};
