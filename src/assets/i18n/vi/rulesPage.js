export default {
	keywordsSearch: 'key search',
	tabMenu: {
		gunqiu: 'Trực Tiếp',
		chuanguan: 'Cược Xiên',
		youxiguize: 'Luật chơi',
		jibenguize: 'Luật chung',
		guanjuntouzhu: 'Cược Thắng',
		jiesuanguize: 'Luật thanh toán',
		wanfashuoming: 'Cách chơi',
		youxiwanfa: 'Trò chơi',
		zhubowanfa: 'Kèo Streamer',
	},
	basicRule:
		`
        <div class="item">
            <div class="item-title">1.Ngày, giờ bắt đầu</div>
            <div class="item-content">Thời gian bắt đầu thi đấu của kèo cược trận đấu E-sports trên trang web của chúng tôi chỉ mang tính chất tham khảo. Thời gian bắt đầu cụ thể lấy thông tin hiển thị chính thức làm căn cứ cuối cùng, nếu trận đấu tạm dừng hoặc bị hoãn và không bắt đầu lại trong vòng 24 giờ kể từ khi bắt đầu, các đơn cược cho trận đấu đó sẽ bị hủy và sẽ được hoàn tiền</div>
        </div>
		<div class="item">
            <div class="item-title">2.Thời gian bắt đầu dự kiến</div>
            <div class="item-content">Thời gian bắt đầu dự kiến chỉ mang tính tham khảo. Thời gian bắt đầu cụ thể tùy thuộc vào thời gian bắt đầu chính thức cuối cùng, miễn là trận đấu vẫn diễn ra bình thường (bất kể thời gian nào), tất cả cược vẫn sẽ có hiệu lực.</div>
        </div>
        <div class="item">
            <div class="item-title">3.Cược chưa xác định kết quả</div>
            <div class="item-content">Nếu có đội bỏ cuộc trước hoặc trong khi trận đấu diễn ra, cược có kết quả rõ ràng thanh toán bình thường, cược chưa xác định kết quả bị coi là vô hiệu và hoàn tiền cược.</div>
        </div>
        <div class="item">
            <div class="item-title">4.Tên trận đấu</div>
            <div class="item-content">Nếu có khác biệt giữa tên trận đấu bằng tiếng Anh và không phải tiếng Anh mà trang web sử dụng, dựa theo phiên bản tiếng Anh chính thức.</div>
        </div>
        <div class="item">
            <div class="item-title">5.Thông tin kết quả trận đấu quá thời gian chờ</div>
            <div class="item-content">Nếu chờ thông báo chính thức về kết quả trận đấu, và trong 24 giờ sau thời gian thi đấu thực tế không có kết quả trận đấu hoặc thông tin chính thức có hiệu lực, cược bị coi là vô hiệu và hoàn tiền cược.</div>
        </div>
        <div class="item">
            <div class="item-title">6.Tình huống đặc biệt của đội thi</div>
            <div class="item-content">
                <p>a) Nếu tên người chơi hoặc tên đội bị sai trong trận đấu E-sports (Trong trận đấu E-sports, thông tin đội hoặc người chơi không đúng và không được bên tổ chức trận đấu công nhận), cược liên quan bị coi là vô hiệu và hoàn tiền cược.</p>
                <p>b) Nếu đội không thể tham gia thi do bỏ cuộc hoặc các lý do khác, cược liên quan sẽ được cân nhắc hủy, coi là vô hiệu và hoàn tiền cược.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">7.Kết quả có sửa đổi hoặc thay đổi</div>
            <div class="item-content">
                <p>a) Đối với cược đã thanh toán, trong 36 giờ kể từ thời điểm bắt đầu trận đấu, nếu kết quả trận đấu có sửa đổi hoặc thay đổi, chúng tôi có quyền sửa kết quả trận đấu theo cơ quan liên quan, trang web tỷ số chính thức hoặc truyền hình trực tiếp chính thức (Video demo).</p>
                <p>b) Về kết quả thi đấu, nếu quá 36 giờ sau kết quả thay đổi hoặc tìm ra kết quả chính xác, người chơi cần tự tìm kiếm/tư vấn nền tảng thi đấu chính thức tham khảo.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">8.Kết quả chính thức và kết quả công bố trên trang web khác nhau</div>
            <div class="item-content">
				Khi kết quả chính thức và kết quả được công bố trong phần kết quả của trang web khác nhau, sẽ tham khảo bản ghi video liên quan của chúng tôi để xác định kết quả chính xác. Nếu không có bản ghi video, kết quả chính xác sẽ được xác định dựa trên kết quả được công bố bởi cơ quan quản lý có liên quan của trận đấu trên trang web chính thức. Nếu trang web chính thức không thể cung cấp kết quả hoặc kết quả công bố rõ ràng là sai, chúng tôi có quyền đưa ra quyết định/chỉnh sửa để xác định kết quả cuối cùng. Quyết định của chúng tôi là quyết định cuối cùng và mang tính kết luận.            </div>
        	</div>
        <div class="item">
            <div class="item-title">9.Trận đấu thi đấu lại</div>
            <div class="item-content">
				Nếu một ván, trận đấu cần thi đấu lại do vấn đề mạng hoặc sự cố kỹ thuật, tất cả cược liên quan đã có kết quả rõ ràng vẫn có giá trị. Nếu thời gian thi đấu lại diễn ra trong 24 giờ, kết quả chưa thanh toán sẽ thanh toán theo kết quả thi đấu lại; nếu thời gian thi đấu lại vượt quá 24 giờ, cược chưa thanh toán tương ứng bị hủy, cược xiên tính là "1".            
			</div>
        </div>
        <div class="item">
            <div class="item-title">10.Rớt mạng trong khi thi</div>
            <div class="item-content">
                <p>Nếu đội viên bị rớt mạng hoặc mất kết nối tạm thời trong khi tham gia thi đấu và không thể kết nối lại, tất cả kết quả của trận đấu được trang wed chính thức công nhận hợp lệ , vé cược của trận đấu sẽ được thanh toán bình thường.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">11.Tỷ lệ cược thay đổi và tỷ lệ cược sai</div>
            <div class="item-content">
                <p>a) Tất cả tỷ lệ cược có thể thay đổi bất cứ lúc nào, cược đã gửi sẽ dựa trên tỷ lệ cược xác nhận cuối cùng trên vé cược.</p>
                <p>b) Nếu cược xiên bị hủy, tỷ lệ cược tính là '1'.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">12.Kèo không đóng trước thời gian quy định</div>
            <div class="item-content">
				Nếu kèo không thể đóng trước thời gian quy định, vé cược sau thời gian này sẽ được cân nhắc hủy và hoàn tiền cược.
            </div>
        </div>
        <div class="item">
            <div class="item-title">13.Cược trễ (Cược sau)</div>
            <div class="item-content">
				Cược trễ (Cược sau) - Nếu cược trước trận đấu được chấp nhận sau khi trận đấu đã bắt đầu vì bất kỳ lý do gì, sẽ được coi là cược sau. Nếu kèo Trực tiếp không đóng trước thời gian quy định, cược đặt sau thời gian này được coi là cược sau. Chúng tôi có quyền đánh giá cược có liên quan vô hiệu.
            </div>
        </div>
        <div class="item">
            <div class="item-title">14.Bên tổ chức tạm thời thay đổi thể thức thi đấu</div>
            <div class="item-content">
				Trong khi thi đấu, nếu bên tổ chức tạm thời thay đổi thể thức thi đấu, khác với thể thức thi trước đó, cược cả trận có liên quan bị coi là vô hiệu. Cược 1 ván thanh toán bình thường.
            </div>
        </div>
        <div class="item">
            <div class="item-title">15.Trường hợp đặc biệt xảy ra trong khi thi đấu</div>
            <div class="item-content">
				Nếu có BUG hoặc sự cố kỹ thuật ngắn trong khi thi đấu, kết quả của khoảng thời gian thi đấu này thanh toán theo kết quả chính thức cuối cùng.
            </div>
        </div>
        <div class="item">
            <div class="item-title">16.Hướng dẫn khác</div>
            <div class="item-content">
                <p>a) Chúng tôi bảo lưu quyền tạm dừng kèo hoặc hủy cược vào bất kỳ lúc nào.</p>
                <p>b) Phát sai nguồn video, video không phát được… không ảnh hưởng đến việc thanh toán trận đấu.</p>
                <p>c) Chúng tôi có quyền quyết định xử lý cuối cùng đối với cược lợi dụng kẽ hở của hệ thống, dùng nền tảng trang web không chính thức để đặt cược bất thường, thông đồng cá cược hoặc đặt cược bất thường khác.</p>
                <p>d) Sau khi xác nhận cược, không thể hủy, thu hồi hoặc thay đổi vé cược, và vé cược sẽ được coi là bằng chứng quý khách cược hợp lệ để chúng tôi xác minh.</p>
                <p>e) Nếu xảy ra lỗi hệ thống/kỹ thuật dẫn đến tỷ lệ cược sai rõ ràng, tất cả cược đặt trong thời gian tỷ lệ cược sai bị coi là vô hiệu. Nếu cược Trực tiếp sai, chúng tôi có quyền hủy và hoàn tiền cược.</p>
                <p>f) Nếu hệ thống hoặc chương trình của chúng tôi bị lỗi hoặc trục trặc, tất cả cược sẽ bị hủy. Quý khách có nghĩa vụ thông báo cho chúng tôi khi phát hiện ra lỗi.</p>
                <p>g) Video E-sports chậm trễ (VD: Ngày, giờ, tỷ số, thống kê, tin tức...) chỉ mang tính chất tham khảo và công ty không chịu trách nhiệm về tính chính xác của thông tin đó.</p>
                <p>h) Chúng tôi có quyền giới hạn tiền cược tối đa cho trận đấu đặc biệt, giới hạn hoặc tăng tỷ lệ cược tối đa của khách hàng mà không cần thông báo và giải thích lý do.</p>
                <p>i) Chúng tôi có quyền thu hồi bất kỳ số tiền nào đã trả thừa và điều chỉnh tài khoản của quý khách để sửa lỗi. Chúng tôi sẽ nhanh chóng thông báo cho quý khách khi có thể.</p>
                <p>j) Chúng tôi có quyền ra quyết định và giải thích cuối cùng, sửa đổi quy tắc bất cứ lúc nào nếu cần thiết.</p>
            </div>
        </div>
		<div class="item">
            <div class="item-title">17.Số tiền trả thưởng tối đa</div>
            <div class="item-content">
                <p>Số tiền trả thưởng tối đa: Đối với tất cả các hạng mục trận đấu hoặc loại cược, số tiền trả thưởng tối đa cho mỗi thành viên mỗi ngày là 1 triệu $ (không bao gồm tiền vốn) hoặc số tiền tệ tương đương khác.</p>
            </div>
        </div>
        `,
	championBet:
		`
        <div class="item">
            <div class="item-title">Dự đoán người chiến thắng trong 1 trận thi đấu tranh giải, giải đấu hoặc trận đấu trong kèo Cược Thắng chỉ định. Nếu đội hoặc người chơi đã chọn giành chiến thắng cuối cùng là được trả thưởng, bao gồm:</div>
            <div class="item-content">
                <p>a) Kết quả cuối cùng của giải đấu, ví dụ: Vô địch World Cup;</p>
                <p>b) Thắng vòng sơ loại, ví dụ: Vô địch vòng bảng World Cup;</p>
                <p>c) Điểm cao nhất;</p>
                <p>d) Người chơi hay nhất…</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">Quy tắc chung:</div>
            <div class="item-content">
                <p>a) Tất cả Cược Thắng dựa trên kết quả cuối cùng của trận đấu.</p>
                <p>b) Nếu người chơi hoặc đội đặt cược đã tham gia cuộc đua, giải đấu hoặc trận đấu này, tất cả cược đặt người chơi hoặc đội này sẽ thanh toán bình thường dù họ bị truất quyền thi đấu, từ chức hoặc không thể hoàn thành thi đấu vì bất kỳ lý do gì.</p>
                <p>c) Người chiến thắng là người chơi hoặc đội chơi chiến thắng theo phán quyết cuối cùng.</p>
                <p>d) Người tham gia sử dụng "Người chơi khác" hoặc "Đội khác" thay cho tên sẽ được coi là vô danh, bất kể trường hợp nào.</p>
            </div>
        </div>
        `,
	cross:
		`
		<div class="item">
			<div class="item-content">
				<p>a) Cược xiên là chọn 2 hoặc nhiều trận đấu trong 1 vé cược. Tất cả trận đấu trong cược xiên thắng, vé cược mới được coi là "thắng". Nếu 1 cược trong tổ hợp cược xiên thắng, sẽ thêm vào kèo cược tiếp theo, cho đến khi tất cả cược của cược xiên đều thắng hoặc có 1 trận thua là kết thúc. Nếu cược xiên có trận đấu cược vô hiệu hoặc hòa, cược xiên này vẫn hợp lệ, kèo hòa/vô hiệu tính là 1.</p>
				<p>b) Chỉ phương thức đặt cược kết hợp cược 2 hoặc nhiều trận đấu. Nếu tất cả lựa chọn đều thắng, cược xiên thắng và giành tỷ lệ cược gộp của 2 cược. Nếu 1 (hoặc nhiều) lựa chọn bị thua, cược "Cược xiên" thua cuộc. Nếu 1 (hoặc nhiều) lựa chọn bị rút lại hoặc bị hủy bỏ, tỷ lệ cược cho lựa chọn đó sẽ khôi phục về 1.00. Cược xiên cược tối đa 10 cược.</p>
				<p>c) Khi người chơi cược X xiên N, mỗi cách kết hợp đều có giới hạn tối đa trong ngày. Nếu hạn mức hôm nay đã dùng hết, hệ thống sẽ cập nhật hạn mức vào 24:00:00 cùng ngày.</p>
			</div>
		</div>
		`,
	live:
		`
		<div class="item">
			<div class="item-title">
			Cược Trực tiếp là cược khi trận đấu đang diễn ra. Cược được nhận trong suốt trận đấu và ngừng nhận sau khi đóng cược. Lưu ý: Một khi trận đấu đã được chuyển đến Trực tiếp, bất kể trận đấu đã bắt đầu hay chưa, hệ thống sẽ tính tất cả cược là "Vé cược Trực tiếp"
			</div>
			<div class="item-content">
				<p>a) Tất cả cược Trực tiếp đều phải tuân theo quy trình nghiệm thu của hệ thống. Điều này có thể khiến việc xác nhận cược bị chậm hoặc thất bại.</p>
				<p>b) Tất cả cược sẽ đợi tối đa 20 giây để quy trình nghiệm thu của hệ thống xác định cược đã đặt thành công hay chưa, tức là cược có thể bị hủy hoặc được xác nhận.</p>
				<p>c) Nếu xảy ra sự cố kỹ thuật hoặc trường hợp đặc biệt khác trong khi trận đấu diễn ra, cược đang chờ xử lý sẽ không được xác nhận và bị coi là cược thất bại.</p>
				<p>d) Tất cả thông tin hiển thị tại cược Trực tiếp (Tỷ số và thời gian...) chỉ để tham khảo, không được dùng làm cơ sở thanh toán, cũng không được dùng làm cơ sở đặt cược.</p>
			</div>
		</div>
		`,
	ESportsSettlementRules :
		`
		<div class="item">
			<div class="item-title">1.Vì mục đích thanh toán</div>
			<div class="item-content">
				Vì mục đích thanh toán, trận đấu không có bằng chứng về việc đã diễn ra hoặc xác nhận không diễn ra hoặc tạm hoãn sẽ bị coi là vô hiệu/không có kết quả/không tham gia thi đấu, trừ khi trận đấu liên quan diễn ra trong 24 giờ kể từ thời gian bắt đầu ban đầu. Đơn cược đội/người chơi vào vòng trong thi đấu tranh giải hoặc vô địch luôn có hiệu lực.
			</div>
		</div>
		<div class="item">
			<div class="item-title">2.Thông tin đội được liệt kê sai</div>
			<div class="item-content">
				Nếu thông tin đội được liệt kê sai, tất cả cược bị coi là vô hiệu. Nếu tên cầu thủ/đội bị viết sai chính tả hoặc logo của đội hiển thị không đúng, công ty có quyền sửa lỗi và tất cả cược vẫn có hiệu lực, trừ khi đó rõ ràng là đối tượng sai.
			</div>
		</div>
		<div class="item">
			<div class="item-title">3.Thông tin kèo sai</div>
			<div class="item-content">
				Nếu thông tin kèo được liệt kê sai, tất cả cược đặt kèo này sẽ vô hiệu. Nếu sai sót về tiêu đề và thời gian của trận đấu phù hợp với trận đấu giải đấu/người tham gia/đội thi đấu thực tế, công ty có quyền sửa sai và tất cả cược vẫn có hiệu lực, thanh toán theo kết quả thi đấu thực tế.
			</div>
		</div>
		<div class="item">
			<div class="item-title">4.Đội đổi tên hoặc đổi người</div>
			<div class="item-content">
				<p>a) Nếu người chơi hoặc đội đổi tên, chỉ cần có thể xác định đối tượng cược là trận nào hoặc ván nào, kèo dùng tên trước đó đều có hiệu lực. Cược sẽ không hủy do đội có người thay thế hoặc cầu thủ tạm thời tham gia thi đấu, nếu ban tổ chức cuộc thi cho phép thay người và công bố kết quả thi đấu chính thức, tất cả cược đều bình thường.</p>
				<p>b) Nếu 1 đội đổi tên nhưng đội hình vẫn như cũ, tất cả cược đặt cho tên đội mới và cũ sẽ có hiệu lực trong suốt trận đấu. Nhưng nếu 1 đội được thay thế bằng 1 đội hình hoàn toàn mới và tất cả cầu thủ khác với đội tham gia ban đầu, tất cả cược bị coi là vô hiệu.</p>
			</div>
		</div>
		<div class="item">
			<div class="item-title">5.Người tham gia thi không phải tuyển thủ chính thức của đội</div>
			<div class="item-content">
				Nếu có người tham gia thi không phải tuyển thủ chính thức của đội (hoặc 1 trong 2), và chưa thông báo thay đổi người trước khi công bố lịch thi đấu, công ty có quyền hủy cược và hoàn tiền cược.
			</div>
		</div>
		<div class="item">
			<div class="item-title">6.Yếu tố bất khả kháng trong 1 bản đồ khiến trận đấu không thể diễn ra hoặc thi đấu lại</div>
			<div class="item-content">
				<p>a) Trong 1 bản đồ, nếu vì lý do bất khả kháng trận đấu không thể diễn ra, cược đã thanh toán vẫn có giá trị, cược kèo chưa thanh toán kết quả sẽ hoàn tiền cược.</p>
				<p>b) Trong một bản đồ, nếu vì lý do bất khả kháng trận đấu phải thi đấu lại, kết quả đã thanh toán vẫn có giá trị, cược kèo chưa thanh toán kết quả dựa theo kết quả thi đấu lại. Nếu trận đấu không được bắt đầu trong 24 giờ, hủy tất cả cược chưa thanh toán.</p>
			</div>
		</div>
		<div class="item">
			<div class="item-title">7.Lợi thế bản đồ đội</div>
			<div class="item-content">
				Lợi thế bản đồ khi 1 đội có 1 hoặc nhiều bản đồ. VD: Chiến thắng vòng chung kết có lợi thế, "Bản đồ 2" là bản đồ đầu tiên khi chơi thực tế, "Bản đồ 3" là bản đồ thứ 2 khi chơi thực tế, v.v.
			</div>
		</div>
		<div class="item">
			<div class="item-title">8.Cược trễ (Cược sau)</div>
			<div class="item-content">
				Cược trễ (Cược sau): Nếu vì bất kỳ lý do gì mà Cược sớm được chấp nhận sau khi trận đấu bắt đầu, sẽ được coi là cược sau. Nếu kèo Trực tiếp không đóng trước thời gian quy định, cược đặt sau thời gian đó sẽ được coi là cược sau. Các cược sau sẽ hoàn tiền cược.
			</div>
		</div>
		<div class="item">
			<div class="item-title">9.Luật thanh toán khác</div>
			<div class="item-content">
				<p>a) Nếu thiếu bằng chứng nhất quán và độc lập hoặc có bằng chứng mâu thuẫn rõ ràng, cược sẽ thanh toán theo dữ liệu độc lập có liên quan của riêng chúng tôi.</p>
				<p>b) Thanh toán cược dựa trên kết quả chính thức được công bố bởi cơ quan quản lý có liên quan của trận đấu, chương trình phát sóng hoặc trò chơi được chỉ định. Cược vô hiệu/không có kết quả/không tham gia thi bị coi là vô hiệu.</p>
				<p>c) Công ty có quyền tạm dừng thanh toán kèo bất kỳ vô thời hạn nếu không chắc chắn về bất kỳ kết quả nào.</p>
				<p>d) Tất cả kết quả đã công bố không thể thay đổi sau 36 giờ và không chấp nhận yêu cầu tra cứu sau khoảng thời gian này. Công ty chỉ thay thế/sửa kết quả bị sai do lỗi của con người, hệ thống hoặc nguồn tham khảo kết quả trong 36 giờ sau khi kết quả được công bố; đối với kèo cược có bằng chứng cho thấy trận đấu đã kết thúc nhưng không có bằng chứng về kết quả, sau 36 giờ, chúng tôi sẽ hoàn trả tiền cược.</p>
			</div>
		</div>
		<div class="item">
			<div class="item-title">10.Quy tắc tính tiền hoàn lại</div>
			<div class="item-content">
				<p>Thắng: Tiền hoàn lại = Tiền cược*Tỷ lệ cược</p>
				<p>Thua: Tiền hoàn lại = 0</p>
				<p>Thắng nửa: Tiền hoàn lại = Tiền cược*((Tỷ lệ cược-1)/2+1)</p>
				<p>Thua nửa: Tiền hoàn lại = Tiền cược*50%</p>
				<p>Hòa kèo: Tiền hoàn lại = Tiền cược</p>
			</div>
		</div>
		`,
	gameShows:
		`
        <div class="item">
            <div class="item-title">1.Đội thắng</div>
            <div class="item-content">
				Kèo đội thắng là trận đấu và bản đồ, theo kết quả trận đấu chính thức, trang web tỷ số chính thức hoặc truyền hình trực tiếp chính thức (Video demo). Nếu số ván của trận đấu không đúng hoặc thay đổi, khác với các trang web chính thức khác, cược bị coi là vô hiệu và hoàn tiền cược.
            </div>
        </div>
        <div class="item">
            <div class="item-title">2.Kèo chấp</div>
            <div class="item-content">
				Kèo chấp dựa trên kết quả trận đấu chính thức, trang web tỷ số chính thức hoặc truyền hình trực tiếp chính thức (Video demo). Kèo chấp gồm bản đồ, số ván, lần hạ gục. Nếu bản đồ trận đấu không đúng hoặc thay đổi, khác với trang web chính thức khác, cược bị coi là vô hiệu và hoàn tiền cược.
            </div>
        </div>
        <div class="item">
            <div class="item-title">3.Khi kèo chỉ có tùy chọn thắng/thua, kết quả cuối cùng là hòa</div>
            <div class="item-content">
				Khi kèo chỉ có tùy chọn thắng/thua, kết quả cuối cùng là hòa (Nếu có hiệp phụ, kết quả hiệp phụ cuối cùng là kết quả cuối cùng), tất cả cược kèo đó bị hủy và hoàn tiền cược.
            </div>
        </div>
        <div class="item">
            <div class="item-title">4.Nếu không có bảng thống kê sau trận đấu</div>
            <div class="item-content">
				Trận đấu Liên Minh Huyền Thoại thanh toán theo kết quả hiển thị trong bảng thống kê sau trận đấu. Nếu không có bảng thống kê sau trận đấu, chúng tôi sẽ thanh toán theo kết quả trên trang web chính thức, truyền hình trực tiếp chính thức (Video demo). Cách bố trí và hiển thị thông tin của bảng thống kê sau trận đấu có thể khác nhau tùy theo khu vực.
            </div>
        </div>
        <div class="item">
            <div class="item-title">5.Kèo Đội thắng trận 2 cửa:</div>
            <div class="item-content">
                <p>TH1. Cược trận đấu có thể hòa (VD BO2 chỉ có 2 hiệp, BO1 không có hiệp phụ).</p>
                <p>Người chơi cần dự đoán 1 bên sẽ thắng, không hòa mới giành tiền thưởng. Nếu kết quả hòa, hoàn tiền cược.</p>
                <p>TH2. Trận đấu sẽ phân thắng thua.</p>
                <p>Người chơi cần dự đoán ai thắng trận, tính mọi hình thức bù giờ.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">6.Kèo 1X2 3 cửa thắng, hòa và thua:</div>
            <div class="item-content">
				Loại cược này phù hợp với trận đấu có kết quả hòa (VD BO2 chỉ có 2 hiệp, BO1 không có hiệp phụ), người chơi dự đoán chính xác kết quả trận đấu mới giành được tiền thưởng.
            </div>
        </div>
        <div class="item">
            <div class="item-title">7.Kèo chấp:</div>
            <div class="item-content">				
				
				<p>Dự đoán kết quả sau khi tính số chấp. Tính mọi hình thức thêm giờ hoặc bù giờ.</p>


				<p>Kèo chấp là 1 con số do công ty đặt ra để "cân bằng xác suất". "-" nằm trước đội hoặc người chơi là chấp và "+" là được chấp. Sau khi tính tỷ lệ chấp, bên điểm cao thắng. Kèo chấp có 3 kiểu dưới đây:</p>


				<p>Kèo số nguyên</p>


				<p>Số chấp là số nguyên, ví dụ -1, -2, -3 v.v, điểm bên chấp trừ số chấp, nếu lớn hơn điểm bên được chấp là thắng, nhỏ hơn là thua, bằng là không thắng không thua (Hòa kèo).
				Ví dụ: A-1 và B+1, kết quả thi của 2 đội là A=3:B=2, A(3)-1=2=B(2) nên 2 đội AB không thắng không thua.</p>
				
				<p>Kèo sống chết</p>
				
				<p>Số chấp là nửa số, ví dụ -0.5, -1.5, -2.5 v.v, điểm bên chấp trừ số chấp, nếu lớn hơn điểm bên được chấp là thắng, nhỏ hơn là thua.</p>
				
				<p>Ví dụ: A-1.5 và B+1.5, kết quả thi của 2 đội là A=3:B=2, A(3)-1.5=1.5&lt;B(2) nên đội được chấp B thắng.</p>
				
				
				
				<p>Kèo tổ hợp</p>
				
				<p>TH1: Số chấp là .25, ví dụ -0/0.5, -1/1.5, -2/2.5 v.v, điểm bên chấp trừ số nguyên trong số chấp, nếu lớn hơn điểm bên được chấp là thắng, nhỏ hơn là thua, bằng nhau là bên chấp thua nửa, bên được chấp thắng nửa.</p>
				
				<p>Ví dụ: A-1/1.5 và B+1/1.5, kết quả thi của 2 đội là A=1:B=0, A(1)-1=0=B(0) nên đội A thua nửa, đội B thắng nửa.</p>
				
				<p>TH2: Số chấp là .75, ví dụ -0.5/1, -1.5/2, -2.5/3 v.v, điểm bên chấp trừ số nguyên trong số chấp, nếu lớn hơn điểm bên được chấp là thắng, nhỏ hơn là thua, bằng nhau là bên chấp thắng nửa, bên được chấp thua nửa.</p>
				
				<p>Ví dụ: A-0.5/1 và B+0.5/1, kết quả thi của 2 đội là A=1:B=0, A(1)-1=0=B(0) nên đội A thắng nửa, đội B thua nửa.</p>



            </div>
        </div>
        <div class="item">
            <div class="item-title">8.Trên/Dưới:</div>
            <div class="item-content">



				<p>Dự đoán tổng tỷ số trận đấu Trên hoặc Dưới giá trị kèo, tính mọi hình thức thêm giờ hoặc bù giờ.</p>

				<p>Kèo Trên/Dưới có 3 kiểu dưới đây:</p>
					
					
					
				<p>Kèo số nguyên</p>
					
				<p>Giá trị kèo là số nguyên, ví dụ 26, 27, 28 v.v, lấy tổng bàn thắng trong trận đấu trừ giá trị kèo, kết quả là số dương là Trên thắng, số âm là Dưới thắng, 0 là không thắng không thua (Hòa kèo).</p>
					
				<p>VD: Trên 26 và Dưới 26, tổng bàn thắng của 2 đội là 26, 26-26=0 nên Trên 26 và Dưới 26 không thắng không thua.</p>
					
					
					
				<p>Kèo sống chết</p>
					
				<p>Giá trị kèo là nửa số, ví dụ 26.5, 27.5, 28.5, v.v, lấy tổng bàn thắng trong trận đấu trừ giá trị kèo, kết quả là số dương là Trên thắng, số âm là Dưới thắng.</p>
					
				<p>VD: Trên 26.5 và Dưới 26.5, tổng bàn thắng của 2 đội là 26, 26-26.5=-0.5 nên Dưới 26.5 là thắng, Trên 26.5 là thua.</p>
					
					
					
				<p>Kèo tổ hợp</p>
					
				<p>TH1: Giá trị kèo là .25, ví dụ 26/26.5, 27/27.5, 28/28.5 v.v, lấy tổng bàn thắng trong trận đấu trừ số nguyên trong giá trị kèo, kết quả là số dương là Trên thắng, số âm là Dưới thắng, 0 là Trên thua nửa, Dưới thắng nửa.</p>
					
				<p>VD: Trên 26/26.5 và Dưới 26/26.5, tổng bàn thắng của 2 đội là 26, 26-26=0 nên Trên 26/26.5 là thua nửa, Dưới 26/26.5 là thắng nửa.</p>
					</p>
				<p>TH2: Giá trị kèo là .75, ví dụ 26.5/27, 27.5/28, 28.5/29 v.v, lấy tổng bàn thắng trong trận đấu trừ số nguyên trong giá trị kèo, kết quả là số dương là Trên thắng, số âm là Dưới thắng, 0 là Trên thắng nửa, Dưới thua nửa.</p>
					
				<p>VD: Trên 26.5/27 và Dưới 26.5/27, tổng bàn thắng của 2 đội là 27, 27-27=0 nên Trên 26.5/27 là thắng nửa, Dưới 26.5/27 là thua nửa.</p>

              
            </div>
        </div>
        <div class="item">
            <div class="item-title">9.Lẻ/Chẵn:</div>
            <div class="item-content">
				Dự đoán số lượng lẻ chẵn trong kết quả cuối cùng của trận đấu, bao gồm mọi hình thức thêm giờ hoặc hiệp phụ. (Nếu kết quả có 0, thanh toán là số chẵn "Chẵn")
            </div>
        </div>
        <div class="item">
            <div class="item-title">10.Kèo thời gian:</div>
            <div class="item-content">
				(Thời lượng trận đấu/thời gian sự kiện) - Thời gian thi đấu 1 trận/thời gian xảy ra sự kiện trận đấu; thời gian thi đấu 1 trận dựa theo thời gian trên bảng điểm chính thức sau khi trò chơi kết thúc; nếu không có bảng điểm, dựa vào thời gian trong trận đấu; kèo thời gian sự kiện tính theo thời gian xảy ra thực tế; thời lượng 2 loại kèo thời gian trên lớn hơn hoặc bằng thời gian đánh dấu của kèo là trên.
            </div>
        </div>
        <div class="item">
            <div class="item-title">11.Tổng hạ gục:</div>
            <div class="item-content">
				Số lần hạ gục trong 1 trận; dựa vào số trên bảng điểm chính thức sau khi trận đấu kết thúc. Nếu không có bảng điểm, dựa theo hiển thị trong trò chơi.
            </div>
        </div>
        <div class="item">
            <div class="item-title">12.Số trụ bị phá hủy:</div>
				Trong bản đồ, nếu đội thi đầu hàng trước khi trận đấu kết thúc, tính tất cả trụ bảo vệ bị phá hủy sau khi có đội đầu hàng. Mục tiêu do lính phá hủy hoặc tự phá hủy cũng được tính.
            </div>
        </div>
        <div class="item">
            <div class="item-title">13.Vị trí phá hủy trụ đầu tiên:</div>
            <div class="item-content">
				Chỉ vị trí của trụ bảo vệ, gồm Đường trên/Đường giữa/Đường dưới, tổng cộng 6 trụ 1 tầng.
            </div>
        </div>
        <div class="item">
            <div class="item-title">14.Tổng hạ gục:</div>
            <div class="item-content"> 
				Chỉ số lần hạ gục của cầu thủ các đội tham gia, dựa theo bảng điểm.
            </div>
        </div>
        <div class="item">
            <div class="item-title">15.Trợ công:</div>
            <div class="item-content">
				Trợ công: Chỉ số lần trợ công của người chơi của các đội tham gia, lấy bảng điểm làm chuẩn.
            </div>
        </div>
        <div class="item">
            <div class="item-title">16.Chiến công đầu:</div>
            <div class="item-content">
				Dựa theo bảng điểm, giành điểm đầu tiên là chiến công đầu.
            </div>
        </div>
        <div class="item">
            <div class="item-title">17.Trụ đầu:</div>
            <div class="item-content">
				Đội phá hủy trụ bảo vệ đầu tiên (Trong DOTA2, tự phá hủy trụ bảo vệ được tính là đội đối phương phá hủy trụ).
            </div>
        </div>
        <div class="item">
            <div class="item-title">18.N mạng</div>
            <div class="item-content">
				Chỉ 1 đội giành N mạng trước. VD: 5 mạng: Chỉ 1 đội giành 5 mạng trước.
            </div>
        </div>
        <div class="item">
            <div class="item-title">19.Lần hạ gục thứ N:</div>
            <div class="item-content">
				Chỉ đội giành lần hạ gục thứ N, VD: Lần hạ gục thứ 5: Chỉ đội nào giành lần hạ gục thứ 5.
            </div>
        </div>
		<div class="item">
            <div class="item-title">20.Player special betting:</div>
            <div class="item-content">
				Map Winner refer to the player who gets the highest kill in a single map. (Draw refund)
            </div>
        </div>
		<div class="item">
            <div class="item-title">21.Highest KDA:</div>
            <div class="item-content">
				Calculated as [K+A]/[D+1], K (kills), A (assists), D (deaths) (Calculate 2 decimal places). (Draw refund)
            </div>
        </div>
        `,
		gamePlay: [
			{
				title:'DOTA2',
				value:
					`
						<p>1.<span class="label">Rớt mạng trong DOTA2</p>
						<p>Trường hợp A: Nếu rớt mạng trước chiến công đầu, nhưng kết nối lại trong khi chơi, tất cả kết quả hợp lệ được chính thức công nhận đều thanh toán bình thường.</p>
						<p>Trường hợp B: Nếu rớt mạng sau chiến công đầu, và trò chơi đang diễn ra bình thường, tức là không có ai bỏ cuộc, tất cả kết quả hợp lệ được chính thức công nhận đều thanh toán bình thường.</p>
						<p>Trường hợp C: Trước chiến công đầu nếu 1 bên rớt mạng và và cho đến khi trò chơi kết thúc cũng không thể kết nối lại, để bảo đảm công bằng, vé cược của trận đấu bị coi là vô hiệu.</p>
						<p>2.<span class="label">Vị trí người chơi đảm nhiệm trong trò chơi: </span>Chỉ mã vị trí trong thông tin đội (Nếu thực tế đổi vị trí, dựa theo tình hình thực tế).</p>
						<p>3.<span class="label">Vị trí Trợ thủ đoạt chiến công đầu: </span>Chỉ người chơi vị trí 4 và 5 trong thông tin đội giành chiến công đầu không.</p>
						<p>4.<span class="label">Will 1st Blood Occur Before 3 Minutes: </span>Whether 1st Blood to happen within 3 minutes. (00:00 - 02:59)</p>
						<p>5.<span class="label">Bên phá hủy trụ đầu tiên: </span>Trong DOTA2, tự phá hủy trụ bảo vệ được tính là đội đối phương phá hủy trụ.</p>
						<p>6.<span class="label">Tiêu diệt Roshan đầu tiên: </span> Thanh toán theo kết quả trang web chính thức hoặc truyền hình trực tiếp chính thức (Video demo).</p>
						<p>7.<span class="label">Xuất hiện Lính siêu cấp: </span>Nếu 6 nhà lính của 1 đội đều bị phá hủy tính là "Có".</p>
						<p>8.<span class="label">Đội giành lần hạ gục thứ N:</span>Chỉ bên nào giành lần hạ gục thứ N, không phải bên đầu tiên giành N lần hạ gục. (VD: Đội giành lần hạ gục thứ 20, chỉ bên nào giành lần hạ gục thứ 20)</p>
						<p>9.<span class="label">Team with 5 Kills Before 8 minutes:</span>Whether a team to accumulate 5 kills within 8 minutes. (00:00 - 07:59)</p>
						<p>10.<span class="label">Dùng Smoke of Deceit nhiều nhất (Tiêu hao):</span>Đội dùng nhiều Smoke of Deceit nhất (Tiêu hao), không bao gồm Smoke of Deceit được tạo ra bởi đồ ninja.</p>
						<p>11.<span class="label">Tổng Smoke of Deceit (Tiêu hao) Tài/Xỉu:</span> Tổng Smoke of Deceit (Tiêu hao) mà 2 đội sử dụng Tài/Xỉu, không bao gồm Smoke of Deceit được tạo ra bởi đồ ninja.</p>
						<p>12.<span class="label">Tiêu hao pha lê đầu tiên:</span>Đội đầu tiên tiêu hao pha lê Aghanim.</p>
						<p>13.<span class="label">Tổng pha lê tiêu thụ Tài/Xỉu:</span>Tổng tiêu hao pha lê Aghanim của 2 đội Tài/Xỉu.</p>
						<p>14.<span class="label">4 Bounty Rune đầu tiên có chia đều không:</span>chỉ 4 Bounty Rune được tạo ra lúc 00:00 có được chia đều cho 2 đội hay không.</p>
						<p>15.<span class="label">Số CS cá nhân cao nhất:</span>chỉ số CS, không bao gồm số Deny.</p>
						<p>16.<span class="label">Kills Handicap Before 10 Minutes: </span>Cumulative Kill Handicap before 10 minutes. (00:00 - 09:59)</p>
						<p>17.<span class="label">Total Kills Before 10 Minutes: </span>Cumulative Kills before 10 minutes. (00:00 - 09:59)</p>
						<p>18.<span class="label">Phá hủy Khổ hình đầu tiên: </span>Chỉ đội phá hủy Khổ hình đầu tiên trong trò chơi</p>
						<p>19.<span class="label">Highest Net Worth Before 10 Minutes: </span>Refers to the Net Worth gained before 10 minutes. (00:00-09:59) (Draw refund)</p>
						<p>20.<span class="label">Highest Net Worth Before 20 Minutes: </span>Refers to the Net Worth gained before 20 minutes. (00:00-19:59) (Draw refund)</p>
						<p>21.<span class="label">Water Rune Equally Divided: </span>Refers to whether the four water runes within 06:00 minutes in the game are divided equally.</p>
						<p>22.<span class="label">The Same Team Picked the First Two Wisdom Runes within First 14 Minutes: </span>Only the first two wisdom runes are counted within 14:00 minutes of game. (14:00 minutes is not counted)</p>
						<p>23.<span class="label">Most Last Hits of Position 2 within 5 Minutes: </span>Cumulative last hit (exclude deny) before 05:00 minutes. (05:00 minutes is not counted)</p>
					`,
			icon:'game-logo-dota2'
		},
		{
			title:'Liên Minh Huyền Thoại',
			value:
				`
						<p>1.Vị trí của người chơi trong trò chơi (Đường trên/Đường giữa/Đi rừng/Trợ thủ/ADC), dựa theo bản đồ cược sớm công bố chính thức.</p>
						<p>2.<span class="label">Sứ Giả Khe Nứt: </span>Chỉ tính hạ gục, đội này hạ gục là thắng.</p>
						<p>3.<span class="label">Tổng số trụ bị đẩy Lẻ/Chẵn：</span> Dựa theo bảng điểm cuối trận, tổng số trụ bị đẩy của 2 đội (bao gồm trụ nhà chính), không tính nhà lính và nhà chính (Căn cứ).</p>
						<p>4.<span class="label">Định nghĩa thanh toán cho tất cả kèo Rồng nhỏ: </span>Chỉ tính rồng nguyên tố, không tính rồng cổ đại.</p>
						<p>5.<span class="label">Số tướng được hưởng bùa lợi Rồng Baron đầu tiên: </span> Nếu không có rồng Baron nào bị tiêu diệt, sẽ hoàn cược đặt kèo này.</p>
						<p>6. Nhà lính bị phá hủy và tái sinh không tính vào số lượng phá hủy.</p>
						<p>7.<span class="label"> Tổng số tướng còn sống khi nhà chính bị phá hủy: </span> Dựa theo số tướng sống sót tại thời điểm phá hủy nhà chính. (Không tính tướng sống lại khi nổ/sau vụ nổ).</p>
						<p>8.<span class="label">Total Kills Before 10 Minutes:</span>Cumulative Kills before 10 minutes. (00:00 - 09:59)</p>
						<p>9.<span class="label">Chiến công đầu: </span>Bên đầu tiên hạ gục đối thủ là bên thắng, dựa theo bảng điểm.</p>
						<p>10.<span class="label">Thanh toán vé cược kèo dựa trên thời gian: </span>Thanh toán theo thời gian diễn biến trận đấu, kết quả trận đấu dựa theo tỷ số trên trang web chính thức, bảng thống kê sau trận đấu và truyền hình trực tiếp chính thức (Video demo).</p>
						<p>11.<span class="label">Đoán thuộc tính rồng nguyên tố thứ N:</span> Thuộc tính rồng nguyên tố tiếp theo sẽ hiển thị trên vách đá, chỉ cần thông báo là đã có kết quả. Thanh toán theo kết quả của trang web chính thức hoặc truyền hình trực tiếp chính thức (Video demo).</p>
						<p>12.<span class="label">Tổng hạ gục (Phút 00:00-10:00, trong thời gian quy định):</span> Cược hợp lệ trong thời gian quy định, nếu thời gian thi đấu không vượt quá thời gian quy định, VD 10:00 phút, khoảng thời gian từ 0-10 phút là vô hiệu. Theo đồng hồ tính giờ trong trò chơi, kết quả trận đấu thanh toán theo tỷ số của trang web chính thức hoặc truyền hình trực tiếp chính thức (Video demo).</p>
						<p>13.<span class="label">Rồng lớn bị cướp không: </span>Nếu trận đấu không có rồng lớn bị tiêu diệt thì thanh toán theo kết quả 2 đội 0:0 là "Không", kết quả trận đấu thanh toán theo tỷ số của trang web chính thức hoặc truyền hình trực tiếp chính thức (Video demo).</p>
						<p>14.<span class="label">Tổng số cuối hạ gục: </span>Số cuối số lần hạ gục của 2 bên cộng lại so với giá trị kèo. VD kết quả trận đấu là 9:12 (Số cuối số lần hạ gục của 2 bên cộng lại 9+2=11), kết quả trận đấu dựa theo tỷ số trên trang web chính thức và bảng thống kê sau trận đấu, API hoặc truyền hình trực tiếp chính thức (Video demo).</p>
						<p>15.Triple Kill hiển thị sau khi hạ gục 3 tướng địch liên tiếp, Quadra Kill hiển thị sau khi hạ gục 4 tướng địch liên tiếp.</p>
						<p>16.<span class="label">Quét sạch:</span>Tất cả thành viên của một đội bị hạ gục hoặc trò chơi có thông báo quét sạch đều tính là Quét sạch.</p>
						<p>17.<span class="label">Đội đầu tiên lấy được Con mắt của Herald:</span>Chỉ đội đầu tiên có được Con mắt của Rift Herald, không liên quan đến việc hạ gục Rift Herald đầu tiên.</p>
						<p>18.<span class="label">Tổng chênh lệch vàng giữa 2 bên Trên/Dưới và Kèo chấp:</span>KQ trận đấu được thanh toán dựa trên tỷ số trên trang web chính thức và bảng thống kê sau trận đấu, API hoặc Livestream chính thức (video demo) Lưu ý: Nếu chênh lệch vàng của trận đấu Trên/Dưới và kèo chấp điểm giống với hệ số do chúng tôi cung cấp, cược sẽ bị hủy vì không hợp lệ (hòa) và hoàn tiền.</p>
						<p>19.<span class="label">Kèo chấp hạ gục 10p:</span>Cược hợp lệ là cược trong thời gian quy định. Nếu thời gian trận đấu không vượt quá thời gian quy định, VD 10:00 phút, khoảng thời gian từ 0 - 10p là không hợp lệ. Dựa theo công cụ tính giờ trong trò chơi, trận đấu được thanh toán dựa trên tỷ số trên trang web chính thức hoặc Livestream chính thức (video demo).</p>
						<p>20.<span class="label">Tổng số trụ bị phá hủy Đường trên Tài/Xỉu:</span>Tổng số trụ bị phá hủy ở đường trên Tài/Xỉu : Không bao gồm trụ nhà chính của 2 bên.</p>
						<p>21.<span class="label">Tổng số trụ bị phá hủy Đường giữa Tài/Xỉu:</span>Tổng số trụ bị phá hủy ở đường giữa Tài/Xỉu: Không bao gồm trụ nhà chính của 2 bên.</p>
						<p>22.<span class="label">Tổng số trụ bị phá hủy Đường dưới Tài/Xỉu:</span>Tổng số trụ bị phá hủy ở đường dưới Tài/Xỉu: Không bao gồm trụ nhà chính của 2 bên.</p>
					



						<p>23. <span class="label">Đường nào phá hủy nhiều trụ nhất: </span>Cách chơi này chỉ tính Đường trên và Đường dưới, xem đường nào phá được nhiều trụ nhất, không tính Đường giữa (Hòa hoàn tiền).</p>

						<p>24. <span class="label">Giành nhiều vàng nhất: </span>Chỉ tổng vàng của người chơi của các đội tham gia, bảng điểm làm chuẩn.</p>
						
						<p>25. <span class="label">Vị trí hạ gục Rồng Baron đầu tiên: </span>Chỉ con Rồng đầu tiên bị hạ gục bởi một anh hùng ở một vị trí nhất định, lấy video làm chuẩn. Nếu không hạ gục, cược sẽ bị coi là không hợp lệ và hoàn tiền.</p>
						
						
						<p>26. Kèo chấp hạ gục khi Đội đạt N mạng: chỉ hệ số chấp trên bảng điểm khi một bên đạt được N mạng trên bảng điểm.
						VD - Kèo cược là 【Kèo chấp hạ gục khi Đội đạt 5 mạng, Team A chấp hạ gục -1.5】:
						Khi một đội đạt 5 mạng, và tỷ số trên bảng điểm là 5:3, thì 5-1,5=3,5>3 tức là 【Team A chấp hạ gục -1.5】sẽ thắng."</p>
						
						<p>27. Tổng hạ gục khi Đội đạt N mạng Trên/Dưới:chỉ tổng hạ gục trên bảng điểm khi một bên đạt được N mạng trên bảng điểm.
						VD - Kèo cược là 【Tổng hạ gục khi Đội đạt 5 mạng Trên/Dưới 8.5】:
						Khi một đội đạt 5 mạng, và tỷ số trên bảng điểm lúc đó là 5:4, tức tổng hạ gục là 9 và cược【Trên】 sẽ thắng.</p>
						
						<p>28. Thời gian trận đấu khi Đội đạt N mạng Trên/Dưới: chỉ thời gian trận đấu trên bảng điểm khi một bên đạt được N mạng trên bảng điểm. VD - Kèo cược là 【Thời gian trận đấu khi Đội đạt 5 mạng Trên/Dưới 11】: Khi một đội đạt 5 mạng, nếu thời gian trên bảng điểm là 11:00, cược【Trên】sẽ thắng, nếu thời gian trên bảng điểm là 10:59, cược【Dưới】sẽ thắng.</p>
						
						<p>29. <span class="label">Vị trí Tướng hạ gục cuối cùng: </span>chỉ vị trí tướng hạ gục cuối cùng trong trò chơi, lấy video làm chuẩn.</p>
						
						<p>30. <span class="label">Vị trí Tướng bị hạ gục cuối cùng: </span>chỉ vị trí tướng bị hạ gục cuối cùng trong trò chơi, lấy video làm chuẩn.</p>
						<p>31. <span class="label">Highest Last Hit: </span>Refers to the highest number of last hit obtained by the players, the results are based on the scores on the official website and the post-tournament statistics panel. (Draw refund)</p>
						<p>32. <span class="label">Highest Last Hit Before 10 Minutes: </span>Refers to the highest number of last hit gained before 10 minutes (00:00 - 09:59). (Draw refund)</p>
						<p>33. <span class="label">First to Obtain Mythic Item: </span>Refers to the player who gets the first Mythic Item. (based on the Mythic Item provided by the official website)</p>
						<p>34. <span class="label">Highest Kill Streak Will Be Quadra Kill or Penta Kill: </span>Only the highest kill streak is counted, e.g. if the highest is Quadra Kill then Quadra Kill wins, if the highest is Penta Kill then Penta Kill wins, the rest are considered as none.</p>
						<p>35. <span class="label">Epic Monsters: </span>Refers to Elemental Dragon, Baron Nashor, Rift Herald, and Elder Dragon.</p>
						<p>36. <span class="label">Elder Dragon Spawned: </span>Refers to whether the Elder Dragon is spawned. Result will be Yes as long as the Elder Dragon was spawned. (regardless of whether it is killed or not)</p>
						<p>37. <span class="label">Last Killed Champion Role upon Slaying the First Baron: </span>If Baron Nashor is not killed, it will be considered as no result.</p>
						<p>38. <span class="label">Slain the Nth Dargon Within 1 Minute After Respawned: </span>If the game ends within one minute after the respawn of the Nth dragon and it's not killed, result will be considered as No ; If the game ends before the Nth dragon respawn, it will be considered as no result.</p>
						<p>39. <span class="label">First Activated "Flash"/"Teleport": </span>Refers to the first player to activate "Flash"/"Teleport" skills. The skill must be selected by both players to be considered as effective bets. If one of the player does not choose the corresponding skill, example: Player A chosen "Ignite" and "Flash", Player B chosen "Flash" and "Teleport", then First Activated "Teleport" will be considered as no result.</p>

					`,
			icon:'game-logo-lol'
		},
		{
			title:'CSGO/CS2',
			value:
				`
						<p>1.<span class="label">Thắng vòng súng lục 1:</span> Đội thắng ở vòng đấu súng lục đầu tiên.</p>
						<p>2.<span class="label">Đội đầu tiên thắng 5/10 vòng:</span> Đội thắng 5/10 vòng đấu sớm nhất.</p>
						<p>3.Đối với cược lẻ/chẵn, khi kết quả là "0", tính là chẵn.</p>
						<p>4.<span class="label">Định nghĩa các vòng CSGO: </span>Khi 1 vòng mới bắt đầu đếm ngược cược, vòng trước đó coi như kết thúc.</p>
						<p>5.<span class="label">Tổng số Headshot vòng N: </span>Xuất hiện biểu tượng/thông báo Headshot (bắn trúng đầu) mới được tính là hạ gục bằng Headshot, tính tất cả Headshot được tạo ra trong vòng đấu.</p>
						<p>6.<span class="label">Hạ gục đầu tiên: </span> Phải hạ gục đối thủ, nếu tự sát hoặc đồng đội hạ gục, ván này được coi là không có hạ gục đầu tiên, hoàn tiền cược liên quan.</p>
						<p>7.<span class="label">Bị hạ gục đầu tiên: </span>Người đầu tiên bị hạ gục trong 1 vòng, bất kỳ phương pháp chết nào cũng được tính.</p>
						<p>8.<span class="label">Tổng hạ gục vòng súng lục: </span>Thanh toán theo tỷ số trên trang web chính thức hoặc truyền hình trực tiếp chính thức (Video demo).</p>
						<p>9.<span class="label">Kèo hạ gục: </span>Thanh toán theo tỷ số trên trang web chính thức hoặc truyền hình trực tiếp chính thức (Video demo).</p>
						<p>10.<span class="label">1st Half Winner/2nd Half Winner: </span></p>
						<p>In CSGO First Half (Round 1 to Round 15) / Second Half (Round 16 to Round 30, draw refund) The teams with the highest score wins, each half is calculated separately.</p>
						<p>In CS2 First Half (Round 1 to Round 12, draw refund) / Second Half (Round 13 to Round 24, draw refund) The teams with the highest score wins, each half is calculated separately.</p>
						<p>11.<span class="label">Tổng số Headshot vòng súng lục và vòng N: </span>Thanh toán theo kết quả của trang web chính thức, truyền hình trực tiếp chính thức (Video demo). Lưu ý: Bị thành viên đội mình bắn trúng đầu chết cũng là Headshot.</p>
						<p>12.<span class="label">Tổng hạ gục vòng súng lục và vòng N: </span>Thanh toán theo kết quả của trang web chính thức, truyền hình trực tiếp chính thức (Video demo). Lưu ý: Bị bom hạ gục không tính vào tổng hạ gục.</p>
						<p>13.<span class="label">Cách thắng vòng N: </span> 4 cách thắng: Hết thời gian/Nổ bom/Gỡ bom/Quét sạch đối phương, kèo này phán đoán vòng này sẽ thắng bằng cách nào.</p>
						<p>14.<span class="label">Hạ gục bằng dao:</span> Chọn bên và khởi động không tính là hợp lệ, nếu bị hạ gục bằng dao của đồng đội cũng tính là “Có”.</p>
						<p>15.<span class="label">Hạ gục bằng lựu đạn: </span>Chọn bên và khởi động không tính là hợp lệ, chỉ tính hạ gục bằng "lựu đạn". "Bom cháy" và các loại vật ném khác không được tính, nếu bị đồng đội hạ gục bằng lựu đạn, cũng tính là "Có".</p>
						<p>16.<span class="label">Đội thắng hiệp 1/Đội thắng bản đồ:</span> Kèo này phải cùng đáp ứng điều kiện thắng hiệp 1 và thắng bản đồ mới được coi là thắng.</p>
						<p>17.<span class="label">Người chơi được nhiều trợ công nhất:</span>Trong trận đấu CSGO, tất cả các loại trợ công hạ gục đối phương đều được tính vào trợ công của người chơi. Lưu ý: Không tính trợ công bom choáng.</p>
						<p>18.<span class="label">Total Clutches Won: Official calculation method: </span>The number of players in this round is reduced to 1 vs 1, and a total of 9 kills must be achieved in this round to be considered as a Clutch. Settlement will only be based on official post-match results showing "CLUTCHES WON".</p>
						<p>19.<span class="label">Whether or not a player gets three kills in a single round (rounds N through N+1): </span>In rounds N through N+1, a single player with three or more kills in a single round (including kills of teammates) is considered to won the betting order. If rounds N through N+1 are not played, the order is considered null and void.</p>
						<p>20.<span class="label">Market with specified map: </span>If the selected map is not the specified map of the market, it will be considered as no result.</p>
						<p>21.<span class="label">Market with player's name only calculate the data of a single player. Example: </span>Player With Higher Kills : A=30 B=25, then the option including player A wins ; If there are multiple players, the option including the player with the highest corresponding data wins. Example: Player With Higher Assists : A=3/B=5, C=6/D=1, the highest is C, then option C/D including player C wins. (Draw refund)</p>
						<p>22.<span class="label">Correct Round Score(Exclude Overtime): </span></p>
						<p>If the option is a one-way score, any team that obtains the corresponding score will be considered as win. </p>
						<p>For example in CSGO : 16:9, Team A=9 Team B=16, the option 16:9 is considered as win. Team A=16 Team B=9 Option 16:9 also considered as win ; </p>
						<p>In CS2 : 13:5, Team A=13 Team B=5, the option 13:5 is considered as win. </p>
						<p>If the option is a two-way score, the corresponding team must obtain the correct score to win. </p>
						<p>For example in CSGO: 16:11 11:16, Team A=16 Team B=11, then option 16:11 will be considered as win, Team A=11, Team B=16, then option 11:16 will be considered as win. (If it goes into overtime, 15:15 will be considered as win)</p>
						<p>In CS2 : 13:7 7:13, Team A=13 Team B=7,  then option 13:7 will be considered as win. (If it goes into overtime, 12:12 will be considered as win)</p>
						<p>23.<span class="label">2nd Pistol Round - Winner: </span>In CSGO , Round 16 is the 2nd Pistol Round. In CS2 , Round 13 is the 2nd Pistol Round.</p>

					`,
			icon:'game-logo-csgo'
		},
		{
			title:'Vương Giả Vinh Diệu',
			value:
				`
						<p>1.<span class="label">Phá hủy trụ:</span> Đối phương phá hủy trụ bảo vệ đầu tiên mới được tính. Lưu ý: Không tính nhà chính.</p>
						<p>2.<span class="label">Chiến công đầu:</span> Bên đầu tiên hạ gục đối thủ là bên thắng (Theo bảng điểm).</p>
						<p>3.<span class="label">Will 1st Blood Occur before 2 Minutes: </span>Whether 1st Blood to happen within 2 minutes. (00:00 - 01:59)</p>
						<p>4.<span class="label">1st Tyrant Killed Before 2:30: </span>Whether to kill the 1st Tyrant within 2:30 minutes. (00:00 - 02:29)</p>
						<p>5.Vị trí của người chơi trong trò chơi (Trợ thủ, đường giữa, đi rừng, đường đối kháng, đường phát triển) lấy bản đồ công bố chính thức sau trận đấu làm cơ sở thanh toán cuối cùng.</p>
						<p>6.<span class="label">Đường nào phá nhiều trụ nhất: </span>Cách chơi này chỉ tính đường Overlord và đường Tyrant, xem đường nào phá được nhiều trụ nhất, không tính Đường giữa (Hòa hoàn tiền).</p>
						<p>7.<span class="label">Tổng số Overlord bị tiêu diệt Lẻ/Chẵn:</span> Tính tất cả Overlord, bao gồm Prophet Overlord và Dark Overlord.</p>
						<p>8.<span class="label">Tổng số Tyrant bị tiêu diệt Lẻ/Chẵn: </span>Tính tất cả Tyrant, bao gồm Tyrant và Dark Tyrant.</p>
						<p>9.1st Tyrant Killed Before 4:30: Whether to kill the 1st Tyrant within 4:30 minutes. (00:00 - 04:29)</p>
						<p>10.<span class="label">Kills Handicap Before 6 Minutes: </span>Cumulative Kill Handicap before 06:00 minutes. (06:00 minutes is not counted)</p>
						<p>11.<span class="label">Total Kills Before 6 Minutes: </span>Cumulative Kills before 06:00 minutes. (06:00 minutes is not counted)</p>
						<p>12.<span class="label">First Tower Destroyed Before 5:30: </span>Refer to whether the first tower will be destroyed before 05:30 minutes. (Exactly 05:30 minutes will be considered as No)</p>
						<p>13.<span class="label">Ancient: </span>Ancient refer to Tyrant , Dark Tyrant , Overlord , Phophet Overlord , Dark Overlord and Storm Dragon.</p>
						<p>14.<span class="label">First Storm Dragon Spawn Location: </span>If Storm Dragon hasn't spawn yet will be cancelled and considered as no result.</p>
					`,
			icon:'game-logo-aov'
		},
		{
            title:'Valorant',
            value:
                `
				<p>Thắng vòng súng lục 1: Đội thắng ở vòng đấu súng lục đầu tiên.</p>
				<p>Thắng Vòng súng lục 2: Đội thắng ở vòng đấu súng lục thứ 16.</p>
				<p>Đội thắng hiệp 1: Đội đạt được điểm số cao hơn trong hiệp 1 (tức vòng 1-12).</p>
				<p>Đội thắng hiệp 2: Đội đạt được điểm số cao hơn trong hiệp 1 (tức vòng 13-24).</p>
				<p>Cách thắng vòng N: 4 cách thắng: Hết giờ/Nổ bom/Gỡ bom/Quét sạch đối phương, kèo này phán đoán vòng này sẽ thắng bằng cách nào.</p>
                `,
            icon:'game-logo-Valorant'
        },
		{
			title:'Bóng Rổ',
			value:
				`
						<p>1.Cược cả trận NBA bao gồm tỷ số hiệp phụ. Cược 1 quarter không bao gồm tỷ số hiệp phụ.</p>
						<p>2.Thanh toán theo dữ liệu được công bố bởi nhà cung cấp tỷ số chính thức hoặc trang web chính thức của trò chơi hoặc trận đấu có liên quan. Khi nhà cung cấp tỷ số chính thức hoặc trang web chính thức không tồn tại hoặc có bằng chứng quan trọng để chứng minh rằng dữ liệu của nhà cung cấp tỷ số chính thức hoặc trang web chính thức sai, chúng tôi sẽ thanh toán dựa trên thông tin độc lập có liên quan.</p>
						<p>3.Nếu trận đấu không diễn ra vào ngày đã định, và không được bắt đầu lại trong 36 giờ kể từ thời gian bắt đầu ban đầu, tất cả cược của trận đấu bị coi là vô hiệu và hoàn tiền cược.</p>
						<p>4.Nếu trận đấu bị gián đoạn và không được tiếp tục trong 36 giờ kể từ thời gian bắt đầu, cược có kết quả vẫn có giá trị và cược không có kết quả bị coi là vô hiệu.</p>
						<p>5.Nếu địa điểm thi đấu thay đổi, tất cả cược bị coi là vô hiệu.</p>
						<p>6. Đối với cược 1 quater/1 hiệp, trận đấu phải hoàn thành quarter/hiệp mới hợp lệ, trừ khi có quy định khác trong luật chơi riêng.</p>
						<p>7.Nếu trận đấu diễn ra trước thời gian quy định, cược đặt trước khi trận đấu bắt đầu vẫn có giá trị, tất cả cược sau khi trận đấu bắt đầu bị coi là vô hiệu (Cược Trực tiếp khác).</p>
						<p>8.Cược trễ (Cược sau): Nếu cược trước trận đấu được chấp nhận sau khi trận đấu đã bắt đầu vì bất kỳ lý do gì, sẽ được coi là cược sau. Nếu kèo Trực tiếp không đóng trước thời gian quy định, cược đặt sau thời gian này được coi là cược sau.</p>
					`,
			icon:'game-logo-basketball'
		},
		{
			title:'Bóng Đá',
			value:
				`
						<p>1.Ngày giờ bắt đầu của các trận đấu bóng đá được cung cấp trên trang web chỉ mang tính tham khảo, nếu trận đấu không bắt đầu theo lịch trình và không bắt đầu cùng ngày (giờ địa phương tại nơi thi đấu), cược không hợp lệ và hoàn tiền cược. Ngoại lệ là cược đội vào vòng sau hoặc giành cúp luôn có giá trị dù trận đấu bị tạm dừng hay hoãn.</p>
						<p>2.Nếu trận đấu bị tạm dừng trước khi hết thời gian, và trận đấu không kết thúc trong cùng ngày (trong 36 giờ), (giờ địa phương tại nơi thi đấu) cược đặt cho kết quả trận đấu bị coi là vô hiệu và hoàn tiền cược.</p>
						<p>a) Nếu trận đấu bị tạm dừng trước khi hết giờ, và không kết thúc vào cùng ngày (giờ địa phương của nơi thi đấu), miễn là hiệp 1 đã kết thúc, cược cho hiệp 1 vẫn có hiệu lực. Nếu trận đấu bị tạm dừng không kết thúc vào cùng ngày (giờ địa phương tại nơi thi đấu), hầu hết cược dự kiến không liên quan đến hiệp 1 khác đều vô hiệu. Điều này sẽ bao gồm nhưng không giới hạn ở: Phạt góc, việt vị, cầu thủ bị đuổi khỏi sân, thẻ đỏ/vàng, 2 đội ghi bàn, tỷ số chính xác, thời gian ghi bàn đầu tiên, giữ sạch lưới và cầu thủ cụ thể ghi bàn/không ghi bàn.</p>
						<p>b) Nếu 2 đội đã ghi bàn, nếu trận đấu bị tạm dừng, cược ghi bàn vô hiệu. Tương tự, nếu trận đấu bị tạm dừng, cược đặt 1 cầu thủ ghi bàn/không ghi bàn vô hiệu, dù cầu thủ đó đã ghi bàn. Tương tự, nếu trận đấu bị tạm dừng, cược thời điểm bàn thắng đầu tiên được ghi sẽ vô hiệu, dù bàn thắng được ghi hay không. Tương tự, nếu trận đấu bị tạm dừng, tất cả cược phạt góc/việt vị sẽ vô hiệu, dù số điểm vượt quá kèo trên/dưới đưa ra. Nếu đội ghi bàn, dù trận đấu bị tạm dừng, cược dự đoán đội ghi bàn lần đầu hoặc lần ghi bàn tiếp theo sẽ có hiệu lực. Nếu trận đấu bị tạm dừng nhưng vẫn có 1 bàn thắng trước khi tạm dừng, cược đội ghi bàn đầu tiên hoặc cuối cùng có hiệu lực.</p>
						<p>3.Nếu kèo có cửa Hòa, khi kết quả của trận đấu là hòa, chúng tôi chỉ trả tiền cược Hòa, cược đội nào thắng là thua.</p>
						<p>4.Trừ khi có quy định khác, thanh toán cược bóng đá sẽ dựa trên thời gian chính thức của trận đấu (bao gồm thời gian bù giờ), không bao gồm kết quả hiệp phụ và đá phạt đền.</p>
						<p>5.Trên/Dưới: Cược tổng số bàn thắng chỉ có hiệu lực khi kết thúc cả trận đấu.</p>
						<p>6.Nếu kèo Trên/Dưới của bóng đá bằng nhau, hoàn tiền cược.</p>
						<p>7.Trừ khi có quy định khác, nếu nơi thi đấu thay đổi từ sân nhà sang trung lập, hoặc từ sân trung lập đã đề cập trước đó thành sân nhà, tất cả cược vẫn có giá trị. Cược bị vô hiệu nếu địa điểm được thay đổi từ sân nhà sang sân khách ban đầu. Cược cũng bị vô hiệu nếu tên đội nhà và tên đội khách bị đảo ngược sai.</p>
						<p>8.Cược thanh toán theo kết quả chính thức hoặc kết quả do cơ quan có thẩm quyền của trận đấu quyết định.</p>
						<p>9. Cược trễ (Cược sau): Nếu cược trước trận đấu được chấp nhận sau khi trận đấu đã bắt đầu vì bất kỳ lý do gì, sẽ được coi là cược sau. Nếu kèo Trực tiếp không đóng trước thời gian quy định, cược đặt sau thời gian này được coi là cược sau.</p>
					`,
			icon:'game-logo-football'
		},
		{
			title:'FIFA',
			value:
				`
						<p>1.Kèo chấp Trực tiếp được thanh toán dựa trên kết quả từ khi cược tới khi kết thúc trận đấu/hiệp, nghĩa là dựa theo tỷ số kết thúc trận đấu trừ đi tỷ số khi cược. Cược kèo chấp Trực tiếp hiệp 1 thanh toán theo kết quả hiệp 1.</p>
						<p>2.Kèo Thắng trận trong thời gian còn lại được thanh toán dựa trên kết quả từ khi cược tới khi kết thúc trận đấu/hiệp, nghĩa là dựa theo tỷ số kết thúc trận đấu trừ đi tỷ số khi cược. Cược Thắng trận trong thời gian còn lại của hiệp 1 thanh toán theo kết quả hiệp 1.</p>
					`,
			icon:'game-logo-fifa'
		},

	],
	zhuboPan: [
		{
			title:'1.Quy tắc chung',
			value:
				`
						<p>
							Các quy tắc chung sau đây áp dụng cho tất cả kèo và trận đấu/loại cược và phải được tuân thủ đầy đủ khi đặt cược. Nếu có thể, các quy định và định nghĩa được nêu trong "Điều khoản và Điều kiện" được công bố trên trang web sẽ được áp dụng cho các "Quy tắc và Quy định" này.
						</p>
					`,
			icon:false
		},
		{
			title:'2.Vấn đề chung',
			value:
				`
						<p>a) Tất cả thông tin cược của công ty đều được cung cấp một cách thiện chí. Công ty không chịu trách nhiệm về các sai sót hoặc thiếu sót liên quan đến ngày, giờ, địa điểm, đối thủ cạnh tranh, tỷ lệ cược, kết quả, thống kê (hiển thị trong video trực tiếp) hoặc các thông tin cược khác.</p>
						<p>b) Công ty có quyền sửa bất kỳ lỗi rõ ràng nào và sẽ thực hiện tất cả hành động hợp lý để đảm bảo kèo được quản lý trung thực và minh bạch, kèo được định nghĩa là các loại cược khác nhau do 1 Streamer truyền hình trực tiếp trò chơi cung cấp. Công ty có quyền đưa ra quyết định cuối cùng.</p>													
						<p>c) Khách hàng có trách nhiệm tìm hiểu tỷ số trận đấu và tất cả thông tin liên quan đến trận đấu bất kỳ lúc nào, khách hàng nên xác nhận tình trạng trận đấu trước khi cược.</p>
						<p>d) Trong trường hợp bất thường, công ty có quyền sửa đổi quy tắc bất cứ lúc nào và vì bất kỳ lý do gì. Các quy tắc sửa đổi có hiệu lực kể từ thời điểm chúng được đăng trên trang web.</p>
						<p>e) Khách hàng thừa nhận rằng điểm số hiện tại, thời gian sử dụng và các dữ liệu khác cung cấp trên trang web dù do bên thứ ba cung cấp dưới dạng "trực tiếp", vẫn có thể bị trì hoãn hoặc không chính xác, khách hàng sẽ chịu mọi rủi ro khi đặt cược dựa trên những dữ liệu này. Khi cung cấp những dữ liệu này, công ty không đảm bảo tính chính xác, đầy đủ hoặc kịp thời và không chịu trách nhiệm về bất kỳ tổn thất nào (trực tiếp hoặc gián tiếp) mà khách hàng phải chịu do phụ thuộc vào những dữ liệu này.</p>
						<p>f) Trận đấu Streamer là trận đấu Streamer kết hợp ngẫu nhiên người chơi qua đường thông qua ứng dụng chính thức của trò chơi tương ứng.</p>
						<p>g) Các điều khoản của trận đấu Streamer áp dụng cho tất cả trận đấu Streamer nhưng không áp dụng cho giải đấu chuyên nghiệp thông thường. Khi quy tắc chung của trận đấu Streamer xung đột với quy tắc trận đấu Streamer của trò chơi đặc biệt (VD Liên minh huyền thoại, PUBG, CS: GO...), căn cứ theo quy tắc trận đấu Streamer của trò chơi đặc biệt tương ứng.</p>
					`,
			icon:false
		},
		{
			title:'3.Tính hợp lệ của trận đấu',
			value:
				`
						<p>a) Nếu Streamer chưa vào màn hình chơi chính thức của trò chơi tương ứng (trừ chuẩn bị trước trận đấu) đã thoát ván hoặc người chơi khác cùng ván thoát ra khiến trò chơi không thể tiến hành, sẽ không tính là ván hợp lệ. Kèo trận tương ứng chỉ áp dụng cho trận đấu hợp lệ, tất cả kèo ván này vẫn có giá trị cho ván hợp lệ tiếp theo.</p>
						<p>b) Nếu Streamer không chơi trò chơi tương ứng mà chơi trò chơi khác, sẽ không tính là ván hợp lệ. Kèo trận tương ứng chỉ áp dụng cho trận đấu hợp lệ, tất cả kèo ván này vẫn có giá trị cho ván hợp lệ tiếp theo.</p>
						<p>c) Nếu Streamer chơi ít hơn một nửa thời gian của ván, ván vô hiệu và hoàn tiền cược tất cả cược.</p>												
						<p>d) Trò chơi xảy ra tình huống bất thường, một bên cố tình bỏ cuộc trước, cược bị ảnh hưởng trong thời gian này có thể bị hủy.</p>
						<p>e) Nếu có hành vi ác ý sử dụng phần mềm gian lận khiến thời gian trò chơi bất thường, cược bị ảnh hưởng trong thời gian này có thể bị hủy.</p>
						<p>f) Nếu trong khi thi đấu, người chơi ác ý hoặc cố ý để nhân vật của mình chết, cược bị ảnh hưởng trong thời gian này có thể bị hủy.</p>
					`,
			icon:false
		},
		{
			title:'4.Quy tắc cơ bản',
			value:
				`
						<p class="title">• Trò chơi có hiệu lực/1 phần trò chơi có hiệu lực</p>													
						<p>Trò chơi của Streamer phải có ít nhất 1 nửa thời gian chơi do Streamer thao tác, nếu không hoàn tiền cược tất cả kèo.</p>
						<p class="title">• Hoàn tiền cược và tạm hoãn</p>
						<p>Nếu Streamer chơi trò không được chỉ định hoặc chơi trò được chỉ định mà không có kèo hợp lệ, sẽ coi như chưa bắt đầu.</p>
						<p class="title">• Trả thưởng</p>
						<p>Tất cả kết quả dựa trên trang kết quả chính thức trừ khi có quy định khác.</p>
					`,
			icon:false
		},
		{
			title:'5.Liên Minh Huyền Thoại',
			value:
				`
						<p class="title"> • Nhận định trò chơi và tạm hoãn</p>
						<p>a) Có 5 phút chơi nhận định trận đấu hợp lệ không.</p>
						<p>b) Trận đấu kết thúc trong 5 phút đầu (nếu có thể thi đấu lại) sẽ được coi là tạm hoãn.</p>
						<p>c) Phải chơi trò chơi quy tắc Liên Minh Huyền Thoại cổ điển chính thức 5 vs 5 (Mọi quy tắc/chế độ đã sửa đổi đều không hợp lệ).</p>
						<p class="title"> • Nhận định điều kiện kèo：</p>
						<p>Tất cả kèo phải đáp ứng 1 số điều kiện tùy trò chơi, nếu không sẽ hoàn tiền cược kèo tương ứng. Điều kiện của Liên Minh Huyền Thoại như sau:</p>
						<p>a) Xếp hạng Streamer: Streamer phải chơi theo điểm xếp hạng, dựa trên mô tả văn bản khi kết hợp. Như hình dưới:</p>
						<p class="rules1 rules-image"></p>
						<p>b) Xếp hàng Streamer solo: Streamer ghép đôi khi solo, dựa theo màn hình chính thức khi xếp hàng. Như hình dưới:</p>
						<p class="rules2 rules-image"></p>
						<p>c) Streamer dùng tài khoản được chỉ định: Streamer phải dùng tài khoản trong danh sách chỉ định của chúng tôi, màn hình trong trò chơi có thể phân biệt rõ tên tài khoản của Streamer. Như hình dưới:</p>
						<p class="rules3 rules-image"></p>
						<p>d) Streamer không chơi trò chơi cá nhân, phải chơi Liên Minh Huyền Thoại chính thức, không phải sảnh cá nhân hoặc chế độ trò chơi đã sửa quy tắc khác, hình ảnh phía trên bên trái hiển thị rõ chế độ trò chơi. Như hình dưới (hình trưng bày cá nhân):</p>
						<p class="rules4 rules-image"></p>
						<p class="title"> • Kết quả trả thưởng</p>
						<p>a) Nếu có kết quả chính thức, kèo cược dựa trên kết quả chính thức. Như hình dưới:</p>
						<p class="rules5 rules-image"></p>
						<p>b) Nếu không có kết quả chính thức, dựa trên ảnh chụp màn hình phút cuối đáng tin cậy trong trò chơi.</p>
						<p class="rules6 rules-image"></p>
						<p>c) Nếu không có ảnh chụp màn hình phút cuối trong trò chơi hoặc có tranh chấp về kết quả của ảnh chụp màn hình phút cuối trong trò chơi, dựa theo trang truy vấn kết quả chính thức.</p>
						<p class="rules7 rules-image"></p>
						<p>d) Nếu Streamer hiển thị biểu tượng đào ngũ trong kết quả chính thức, kèo thắng thua đều là thua bất kể kết quả của trò chơi (người chơi khác không bị ảnh hưởng bởi quy tắc này). Như hình dưới:</p>
						<p class="rules8 rules-image"></p>
						<p>e) Nếu Streamer bỏ trận giữa chừng, tất cả kết quả sẽ dựa trên kết quả cuối cùng chính thức (người đào ngũ kèo thắng thua là thất bại). Như hình:</p>
						<p class="rules8 rules-image"></p>
				   `,
			icon:'game-logo-lol'
		},
		{
			title:'6.Vương Giả Vinh Diệu',
			value:
				`															
						<p class="title"> • Nhận định trò chơi và tạm hoãn</p>
						<p>a) Có 5 phút chơi nhận định trận đấu hợp lệ không.</p>
						<p>b) Trận đấu kết thúc trong 1 phút đều tạm hoãn tới lần sau.</p>
						<p>c) Phải chơi trò Vương Giả Vinh Diệu quy tắc cổ điển chính thức 5 vs 5 (Mọi quy tắc/chế độ đã sửa đổi đều không hợp lệ).</p>
						<p class="title"> • Nhận định điều kiện kèo</p>
						<p>Tất cả kèo phải đáp ứng 1 số điều kiện tùy trò chơi, nếu không sẽ hoàn tiền cược kèo tương ứng. Điều kiện của Vương Giả Vinh Diệu như sau:</p>
						<p>a) Streamer cày xếp hạng, căn cứ vào mô tả hiển thị khi xếp hạng. Như hình dưới:</p>
						<p class="rules9 rules-image"></p>
						<p>b) Streamer dùng tài khoản được chỉ định: Streamer phải dùng tài khoản trong danh sách chỉ định của chúng tôi, màn hình trò chơi phân biệt rõ tên tài khoản của Streamer. Như hình dưới:</p>
						<p class="rules9 rules-image"></p>	
						<p>c) Xếp hàng Streamer solo, dựa theo số người chơi hiển thị khi xếp hàng. Như hình dưới là không phù hợp quy tắc:</p>
						<p class="rules10 rules-image"></p>	
						<p class="title"> • Kết quả trả thưởng</p>
						<p>a) Nếu có kết quả chính thức, kèo cược dựa trên kết quả chính thức, như hình dưới:</p>
						<p class="rules11 rules-image"></p>
						<p>b) Nếu không có kết quả chính thức, dựa trên ảnh chụp màn hình phút cuối đáng tin cậy trong trò chơi.</p>
						<p class="rules12 rules-image"></p>
						<p>c) Nếu ảnh chụp màn hình phút cuối trong trò chơi không đáng tin cậy hoặc có tranh chấp về kết quả, căn cứ vào kết quả chính thức, như hình dưới:</p>
						<p class="rules11 rules-image"></p>	
						<p class="title"> • Hủy kèo, hoàn tiền cược và trường hợp đặc biệt:</p>
						<p>Nếu Streamer thoát ra giữa chừng và không hiển thị kết quả thì thắng thua coi là thua, kết quả khác vẫn dựa theo hình ảnh cuối trước khi thoát.</p>
					`,
			icon:'game-logo-aov'
		},
	],
};
