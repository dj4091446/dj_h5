export default {
    footballRanking: {
        Leaderboard: 'Urutan juara',
        Team: 'Tim',
        MP: 'Laga',
        WDL: 'M/S/K',
        Pts: 'Pts',
    },
    groupKnockout: {
        Round1: 'Ronde 1',
        QuarterFinal: '1/4 final',
        Semifinals: 'Semifinal',
        Final: 'Final',
        zanwu: 'Blm ada ',
    },
    groupMatches: {
        zu: 'Grup {no}',
        Team: 'Tim',
        MP: 'Laga',
        WDL: 'M/S/K',
        jin: 'Gol',
        shi: 'Miss',
        jingshengqiu: 'GD',
        Pts: 'Pts',
    },
    knockout: {
        GroupStage:'Penyisihan grup',
        KnockoutStage:'Gugur',
    },
    video:{
        batchNo:'Ronde-{no}',
        dateNo:'Fase {no}',
        matchEnd:'Selesai',
    },
    detail:{
        historyRecord:'Rekap',
    },
    endGame:{
        tips1:"Hasil OB E-sport bisa dicek disini",
        tips2:"Masuk ke {0} untuk semak keputusannya",
        tips3:"Jgn tampilkan lagi",
    },
};
