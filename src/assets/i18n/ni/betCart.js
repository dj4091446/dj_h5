export default {
	stationOrder: {
		corss: 'Set1 (inGame Parlay)',
		return: 'Prediksi balik',
		placeholder: 'Isi saldo',
		oddsChange: 'Slot, odds atau validitas yg dipilih berubah',
		total: 'Jlh',
		maxReturn: 'Maks balik',
		stationTips: 'InGame Parlay, maks 10 item',
		full: '(Penuh)',
		tips1: 'Tips: tiket di Parlys harus ≥2',
		tips2: 'Tips: Nama bursa sama tdk bs main inGame Parlay',
		tips3: 'Tips: Item di bursa sama tdk dukung main inGame Parlay',
		tips4: 'Tips: Nominal Odds capai batas, tdk bisa tambah item lagi!',
		tips5: 'Tips: Taruhan capai batas bursa, tdk dukung inGame Parlay',
		limitOver: 'InGame Parlay lewat batas, pilih bursa lain atau kurangi item',
		oddsNew: 'Auto odds terbaru',
		showLimit5: 'Payout harian capai batas, cek perkiraan balik atau atur jlh taruhan, silahkan taruh bsk!'
	},
	betOrder: {
		return: 'Prediksi balik',
		betMax: 'Tdk bs pilih lagi, maks 10 tiket',
		placeholder: 'Isi saldo',
		matchLimit: 'Limit laga',
		marketLimit: 'Limit bursa',
		corssOptions: 'Item lolos',
		hasLight: 'Item ditandai tdk bs jadi objek taruhan',
		oddsChange: 'Slot, odds atau validitas yg dipilih berubah',
		balance: 'Saldo tdk cukup',
		showLimit1:
			'Slot penuh, pilih bursa, tanding atau batal item Pralay lalu mulai taruh lagi',
		showLimit2: 'Slot tiket capai batas, pilih bursa lain utk Parlay',
		showLimit3:
			'Slot penuh,pilih bursa, lomba atau batal item Pralay lalu mulai taruh lagi',
		showLimit4:
			'Slot capai batas, tambah atau kurangi item, atau coba lagi besok',
		showLimit5: 'Payout harian capai batas, cek perkiraan balik atau atur jlh taruhan, silahkan taruh bsk!',
		length2: 'Pilih min> 2 laga beda',
		total: 'Jlh',
		maxReturn: 'Maks balik',
		acceptOdds1: 'Auto odds terbaru',
		acceptOdds2: 'Auto odds tertinggi',
		acceptOdds3: 'Tolak odds berubah',
		daily: 'Hari ini',
		corss: 'Set',
	},
	index: {
		stationOrder: 'InGame Parlay',
		betOrder: 'Kolom slot',
		delAll: 'Bersihkan',
		balance: 'Saldo',
	},
	timer: {
        Invalid: 'Tdk sah',
    },
};
