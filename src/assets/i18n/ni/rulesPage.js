export default {
    keywordsSearch: 'key search',
    tabMenu: {
		gunqiu: 'InPlay',
		chuanguan: 'Parlay',
		youxiguize: 'Aturan game',
		jibenguize: 'Aturan dasar',
		guanjuntouzhu: 'Taruhan Juara',
		jiesuanguize: 'Aturan hitungan',
		wanfashuoming: 'Deskripsi trik',
		youxiwanfa: 'Trik main game',
		zhubowanfa: 'Streamer'
    },
    basicRule:
        `
        <div class="item">
            <div class="item-title">1. Tgl dan waktu mulai</div>
            <div class="item-content">Tgl dan waktu mulai laga pd bursa Esport di web hny sbg referensi. Waktu detail mulai laga mengikut tampilan resmi.Jika slh satu laga berhenti/diundur atau tdk dimulai dlm 24 jam waktu yg ditentukan, maka tiket laga tsbt dianggap tak sah dan dana dikembalikan.</div>
        </div>
        <div class="item">
            <div class="item-title">2. Estimasi tgl dan waktu mulai</div>
            <div class="item-content">Estimasi tgl dan waktu mulai hanya sebagai referensi, rincian tgl dan waktu laga akan berdasarkan waktu resmi terakhir. Selama laga tetap berlangsung (waktu tidak dibatasi), maka semua tiket tetap sah.</div>
        </div>
        <div class="item">
            <div class="item-title">3. Tiket yg hasilnya blm terklarifikasi</div>
            <div class="item-content">Jika tim sblm laga atau di tengah laga tarik diri, tiket yg hasil tlh terklarifikasi dihitung, tiket yg hasil blm terklarifikasi dianggap tak sah dan akan dibalikkan dana.</div>
        </div>
        <div class="item">
            <div class="item-title">4. Nama laga</div>
            <div class="item-content">Jika nama laga inggris ataupun bukan terdpt perbedaan di web, ttp akan mengikuti versi inggris sbg patokan resmi</div>
        </div>
        <div class="item">
            <div class="item-title">5. Info hasil laga kadarluasa</div>
            <div class="item-content">Tiket dianggap tak sah dan balik dana jika tdk ada hasil dlm 24 jam ataupun pihak resmi tdk bisa menyediakan info bersangkutan.</div>
        </div>
        <div class="item">
            <div class="item-title">6. kasus khusus tim laga</div>
            <div class="item-content">
                <p>a. Jika nama pemain atau tim di E-sport error (info lawan atau tim salah di E-sport, maka akan dibatalkan) tiket yg terlibat dianggap tak sah dan balik dana.</p>
                <p>b. Jika tim tdk bisa ikut serta, dgn alasan tarik diri maupun yg lainnya, maka tiket akan dibatalkan, tak sah dan balik dana.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">7. Hasil laga ada perbaikan atau pergantian</div>
            <div class="item-content">
                <p>a. Mengenai tiket yg tlh dihitung, jika hasil ada perbaikan atau pergantian dlm 36 jam laga dimulai, maka platform berhak memperbaiki hasil, hasil akan ditetapkan mengikut institusi bersangkutan, skor web atau siaran resmi (video demo).</p>
                <p>b. Terkait hasil laga, jika terjadi perubahan pada hasil stlh 36 jam atau pencarian hasil akurat, pemain hrs cari bantuan/ berkonsultasi di platfrom laga resmi.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">8. Terkait saat hasil laga resmi bentrok dgn pengumunan hasil di web</div>
            <div class="item-content">
				Saat pengumuman hasil resmi dan web bentrok, merujuk ke video laga di platfrom utk memastikan hasil. Jika tidak ada video tersedia, merujuk ke pengumuman hasil di web resmi institusi. Jika web resmi tdk bs menyediakan hasil atau hasil yg diumumkan salah, maka kami berhak memutuskan/memperbaiki hasil akhir. Di aspek ini, keputusan kami adlh final dan mutlak.
            </div>
        </div>
        <div class="item">
            <div class="item-title">9. Terkait tanding ulang</div>
            <div class="item-content">
                Jika terjadi kendala internet atau teknis pd suatu laga, maka diperlukan tanding ulang. Semua tiket yg hasilnya tlh terklarifikasi dianggap sah. Jika tanding ulang terjadi dlm 24 jam, hasil yg blm dihitung disesuaikan dgn hasil tanding ulang; jika tanding ulang lebih dari 24 jam, tiket yg blm dihitung dibatalkan, tiket berseri dihitung "1"
            </div>
        </div>
        <div class="item">
            <div class="item-title">10. Terkait koneksi putus saat laga.</div>
            <div class="item-content">
                <p>Jika saat laga terjadi pemain putus koneksi atau temporer hilang koneksi, tp tdk bs diulang, maka laga ttp sah dan dihitung.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">11. Odds berubah dan Odds error</div>
            <div class="item-content">
                <p>a. Semua Odds bs berubah setiap saat, taruhan yg diajukan akan mengikut tampilan konfirmasi Odds terakhir</p>
                <p>b. Jika tiket berseri batal, maka Odds dihitung "1"</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">12. Bursa tdk bs tutup sblm waktu ditentukan.</div>
            <div class="item-content">
				Jika bursa tdk bs tutup sblm waktu ditentukan, tiket stlh sesi itu batal, tak sah dan balik dana
            </div>
        </div>
        <div class="item">
            <div class="item-title">13.Taruhan Terlambat (Past-Post)</div>
            <div class="item-content">
                Taruhan Terlambat (Past-Post) adalah taruhan pre-match yang diterima setelah laga dimulai karena alasan apapun.
                Taruhan dalam kondisi ini dianggap sebagai taruhan tertunda. Jika bursa In-Play tidak tutup tetap waktu, maka taruhan setelah sesi tersebut dianggap taruhan terlambat dan akan balik dana.
            </div>
        </div>
        <div class="item">
            <div class="item-title">14. Panitia mendadak mengubah aturan</div>
            <div class="item-content">
                Jika panitia mendadak mengubah aturan di tengah laga dan berbeda dgn aturan semula, maka semua tiket di bursa terlibat dianggap tak sah, sedangkan babak tunggal akan dihitung seperti biasa.
            </div>
        </div>
        <div class="item">
            <div class="item-title">15. kasus khusus muncul di tengah laga</div>
            <div class="item-content">
				Jika di tengah laga muncul BUG atau kendala teknis temporer, hasil laga akan mengikut hasil final resmi dihitung.
            </div>
        </div>
        <div class="item">
            <div class="item-title">16. Lain-lainnya</div>
            <div class="item-content">
                <p>a. Kami berhak utk menghentikan bursa/ atau membatalkan taruhan apapun dan kapanpun.</p>
                <p>b. Jika muncul error pada sumber video, video tidak bisa diputar dll, hal ini tidak akan mempengaruhi perhitungan hasil laga.</p>
                <p>c. Memanfaatkan kerentanan sistem atau platform tdk resmi utk memulai taruh, kami berhak mengambil keputusan akhir terkait Parlay maupun taruhan yg dicurigai.</p>
                <p>d. Jika tiket telah terkonfirmasi, maka tiket tidak akan dibatalkan, dicabut ataupun diubah serta tiket tersebut akan menjadi bukti taruhan Anda telah terverifikasi oleh platform.</p>
                <p>e. Jika terjadi sistem/teknis error hingga muncul Odds error, maka semua tiket saat waktu Odds error dihitung tak sah. Jika error terjadi di inPlay, kami berhak memutuskan taruhan tak sah dan balik dana.</p>
                <p>f. Semua tiket dianggap tak sah saat sistem atau program kami error, Anda bisa menginformasikan kami saat muncul error.</p>
                <p>g.Terkait video E-sport tertunda (cth: tgl, waktu, skor, stats, berita dll) sbg peringatan, perusahaan tdk bertanggung jawab atas ketepatan waktu info.</p>
                <p>h. Kami berhak membatasi limit maks taruh pada laga tertentu ataupun menaikkan maks Odds merchant tertentu tanpa pemberitahuan.</p>
                <p>i. Kami berhak menarik balik pembayaran berlebih serta meyetel akun Anda dgn tujuan memperbaiki error. Kami akan menindak secara rasional dan mengabari Anda secepat mungkin.</p>
                <p>j. Kami berhak membuat keputusan akhir, penjelasan serta perbaikan aturan kapanpun saat diperlukan.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">17. Maks payout</div>
            <div class="item-content">
                <p>Maks payout: Semua item taruh atau jenis taruhan, setiap pemain dpt bonus maks hariannya hanya 1juta usd (tdk termasuk modal taruhan), atau mata uang lainnya.</p>
            </div>
        </div>
        `,
    championBet:
        `
        <div class="item">
            <div class="item-title">Prediksi turnamen, liga atau laga juara dlm bursa juara. Tim atau pemain yg dipilih menang dianggap sesuai dgn pantokan payout, termasuk :</div>
            <div class="item-content">
                <p>a. Hasil liga, cth : Juara Piala Dunia</p>
                <p>b. Menang dari pra-eliminasi, contoh Piala Dunia grup stage</p>
                <p>c. Skor tertinggi</p>
                <p>d. Pemain terbaik, dll</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">Aturan :</div>
            <div class="item-content">
                <p>a. Semua taruhan juara berbasis hasil akhir laga</p>
                <p>b. Jika pemain atau tim yg ditaruh ikut serta dlm turnamen, liga atau laga, semua tiket terlibat akan ttp dihitung secara normal, meskipun diskualifikasi, diberhentikan, atau tdk menyelesaikan laga, tdk peduli apapun alasannya.</p>
                <p>c. Juara adlh tim atau pemain yg dinyatakan sesuai dgn penilaian akhir menang</p>
                <p>d. Tdk peduli situasi apapun, jika menggunakan pemain lain atau "tim lain" mengganti nama partisipan, maka dianggap tdk ada nama.</p>
            </div>
        </div>
        `,
    cross:
        `
    <div class="item">
        <div class="item-content">
            <p>a. Parlay adlh memilih 2 atau lebih laga dlm 1 tiket. Di dlm Parlay, tiket dinyatakan "Menang" saat semua laga menang. Jika slh satu taruhan di Combo Parlay menang, maka akan tambah item berikutnya, sampai semua taruhan menang atau salah satunya kalah sbg akhir. Parlay ttp sah saat laga di parlay tak sah atau seri, tapi taruhan yg seri atau tak sah dihitung sbg "1"</p>
            <p>b. Merujuk kombinasi dari 2 atau lebih partai laga. Jika semua pilihan menang, maka taruhan parlay dianggap menang serta memperoleh Odds 2 taruhan kombinasi. Jika satu (atau lebih) pilihan kalah, maka taruhan parlay dianggap kalah Jika satu (atau lebih) pilihan dibatalkan atau dicabut, maka maka odd yang dipilih akan kembali menjadi 1.00 odds. Maksimal tiket parlay 10 item.</p>
            <p>c. Ketika pengguna sdg taruh XsetN, setiap taruh kombo ada kouta harian taruhan. Jika kouta sdh habis digunakan, sistem akan setiap 24:00:00 perbaharui kouta.</p>
        </div>
    </div>
    `,
    live:
        `
    <div class="item">
        <div class="item-title"> inPlay adlh taruhan saat laga berlgsg. Selama laga berlgsg akan trs menerima pesanan dan berhenti menerima saat bursa tutup. Note: Ketika laga pindah ke laman inPlay, tidak peduli laga tlh mulai atau tidak, sistem akan menghitung semua taruhan sbg "inPlay".</div>
        <div class="item-content">
			<p>a. Semua taruh inPlay perlu melewati verifikasi sistem. Hal ini mungkin akan menyebabkan konfirmasi tiket tertunda atau gagal taruh.</p>
			<p>b. Semua tiket plg lama 20 mnt melewati verifikasi sistem untuk menentukan taruh berhasil atau tidak. Ini berarti tiket mungkin dibatal atau dikonfirmasi.</p>
			<p>c. Jika terjadi kendala teknis atau kasus khusus di tengah laga, semua tiket yang menunggu konfirmasi akan dianggap gagal.</p>
			<p>d. Semua info inPlay (termasuk skor, waktu dll) hanya sbg referensi, semua info tdk digunakan sbg dasar hitungan maupun taruhan.</p>
        </div>
    </div>
    `,

    ESportsSettlementRules:
        `
        <div class="item">
        <div class="item-title">1. Terkait hitungan</div>
            <div class="item-content">
            Terkait hitungan, tanpa bukti laga berlgsg atau blm berlgsg atau ditunda, maka dianggap tdk sah/tdk ada hasil/tdk ikut serta, kecuali laga bisa mulai dlm 24 jam waktu semula ditetapkan.Tiket laga tim/turnamen atau juara ttp sah.
            </div>
        </div>
        <div class="item">
            <div class="item-title">2. Info tim yg terdaftar error</div>
            <div class="item-content">
            Jika info tim yg terdaftar error, maka semua taruh dianggap tak sah. Perusahaan berhak memperbaiki jika pengejaan nama tim/pemain salah, tampilan logo error dan semua taruhan ttp sah, terkecuali kesalahan target.
            </div>
        </div>
        <div class="item">
            <div class="item-title">3. Info bursa error</div>
            <div class="item-content">
				Jika info bursa yg terdaftar error, semua tiket yg terlibat tak sah. Perusahaan berhak memperbaiki titel laga, waktu laga error menjadi konsisten dgn liga/partisipan/tim yg nyata berlgsg dan semua tiket akan ttp sah serta dihitung sesuai dgn hasil laga berlgsg.
            </div>
        </div>
        <div class="item">
            <div class="item-title">4. Ganti nama tim atau pemain</div>
            <div class="item-content">
                <p>a. Jika pemain atau tim ganti nama, selama bisa memastikan target taruh adlh laga atau game mana, maka menggunakan nama semula yg disediakan bursa semuanya sah. Taruhan tdk akan batal krn pergantian pemain. Jika institusi membolehkan pergantian pemain dan mengumumkan hasil resmi laga, maka taruhan dianggap normal.</p>
                <p>b.Jika tim ganti nama tapi lineup ttp sama, maka nama baru dan lama ttp sah. Semua taruhan akan tak sah, jika satu tim  diganti dgn formasi baru serta pemain beda dr tim semula. </p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">5. Pemain tak resmi</div>
            <div class="item-content">
				Jika terdpt pemain tak resmi (atau salah satunya) dan tdk mengumumkan perubahan pemain sblm jawdal keluar, perusahaan berhak membatalkan tiket laga itu dan balik dana.
            </div>
        </div>
        <div class="item">
            <div class="item-title">6. Keadaan kahar 1map hingga laga tdk bisa dilgsgkan atau tanding ulang.</div>
            <div class="item-content">
                <p>a. Keadaan kahar 1map hingga laga tdk bisa dilgsgkan, tp hasil laga yg sdh dihitung ttp sah dan yg blm dihitungkan akan balik dana.</p>
                <p>b. Keadaan kahar 1map hingga laga tdk bs dilangsungkan, hasil yg sdh dihitung ttp sah, hasil yg blm dihitung disesuaikan dgn hasil tanding ulang. cth dlm 24 jam, blm ada tanding ulang, maka semua tiket blm dihitung batal.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">7.Map Advantage</div>
            <div class="item-content">
            Satu tim memiliki 1 atau lebih map advantange, contoh: pemenang upper bracket final akan menerima map advantange, dimana "Map 2" adalah map 1 dari game saat ini, "Map 3" adalah map ke-2 dari game saat ini dan seterusnya.
            </div>
        </div>
        <div class="item">
            <div class="item-title">8. Taruhan Terlambat (Past-Post)</div>
            <div class="item-content">
                Taruhan Terlambat (Past-Post) adalah taruhan pre-match yang diterima setelah laga dimulai karena alasan apapun.
                Taruhan dalam kondisi ini dianggap sebagai taruhan tertunda. Jika bursa In-Play tidak tutup tetap waktu, maka taruhan setelah sesi tersebut dianggap taruhan terlambat dan akan balik dana.
            </div>
        </div>
        <div class="item">
            <div class="item-title">9. Aturan lainnya</div>
            <div class="item-content">
                <p>a. Jika terjadi kurangnya bukti independen atau muncul data bukti yg tdk konsisten, kami akan menghitung taruh sesuai data independen.</p>
                <p>b. Hitungan taruh akan disesuaikan dgn laga, siaran, hasil yg diumumkan secara resmi. Taruhan laga tak sah/tdk ada hasil/tdk ikut serta dianggap tidak sah.</p>
                <p>c. Perusahaan berhak menghentikan hitungan bursa manapun, jika tidak yakin dgn hasil laga.</p>
                <p>d. Semua hasil yg diumumkan stlh 36 jam tdk dpt diubah dan dipantau. Perusahaan hanya akan mereset/memperbaiki human error, sistem error atau sumber hasil error dlm 36 jam stlh hasil diumumkan; sedangkan mengenai bursa dengan bukti laga sdh selesai tapi tdk dengan hasil laga serta melampaui 36 jam, kami akan mengembalikan tiket.</p>
            </div>
        </div>
        <div class="item">
			<div class="item-title">10. Aturan hitung pengembalian dana</div>
			<div class="item-content">
				<p>Menang：balik dana = taruh* odds</p>
				<p>Kalah：balik dana = 0</p>
				<p>Menang setengah：balik dana= taruh * ( ( odds - 1 ) / 2 + 1 )</p>
				<p>Kalah setengah：balik dana = taruh * 50%</p>
				<p>Batal：balik dana=  taruh </p>
			</div>
        </div>
        `,
    gameShows:
        `
        <div class="item">
            <div class="item-title">1.Bursa 1x2</div>
            <div class="item-content">
            Bursa 1x2 adlh ttg laga dan map. Sesuai dgn hasil laga resmi, skor web atau siaran resmi (video demo). Jika babak laga tdk benar atau berubah, tdk sama dgn web resmi, tiket yg terlibat dianggap tak sah dan balik dana.
            </div>
        </div>
        <div class="item">
            <div class="item-title">2. HDP</div>
            <div class="item-content">
				Tentang HDP sesuai dgn hasil laga resmi, skor web atau siaran resmi (video demo). HDP termasuk map, babak, kill. Jika map tdk benar atau berubah, tdk sama dgn web resmi, maka tiket dianggap tak sah dan balik dana. 
            </div>
        </div>
        <div class="item">
            <div class="item-title">3. Bursa yg hanya menyediakan opsi menang, kalah, saat ketemu hasil akhir seri</div>
            <div class="item-content">
				Saat bursa yg hanya menyediakan opsi menang kalah ketemu  hasil akhir seri (jika ada overtime, maka hasil overtime dianggap hasil akhir), maka semua tiket di bursa itu tak sah dan balik dana.
            </div>
        </div>
        <div class="item">
            <div class="item-title">4. Jika usai laga tdk ada stats </div>
            <div class="item-content">
				Payout laga League of Legends(LOL) akan mengikuti hasil yg ditampilkan di stat usai laga. Jika tdk ada stats, kami akan hitung sesuai siaran web resmi (video demo). Layout dan info stat yg ditampilkan mungkin akan sedikit berbeda karena perbedaan area.
            </div>
        </div>
        <div class="item">
            <div class="item-title">5. Pemenang 2 way HDP:</div>
            <div class="item-content">
                <p>kasus1. Taruh terdpt laga seri (mis BO2 main 2babak, BO1 tdk ada overtime).</p>
                <p>Pemain perlu prediksi siapa menang, tdk akan ada kasus seri, barulah bisa menang bonus. Jika hasil laga seri, balik dana.</p>
                <p>kasus2. Laga wajib ada menang kalah </p>
                <p>Pemain perlu prediksi siapa menang di laga,overtime dlm bentuk apapun jg terhitung.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">6. 1x2 3way HDP:</div>
            <div class="item-content">
				Taruh jenis ini cocok pada laga ada seri (mis BO2 main dua babak, BO1 tdk ada overtime), pemain perlu prediksi hasil, barulah bisa menang bonus 
            </div>
        </div>
        <div class="item">
            <div class="item-title">7. HDP:</div>
            <div class="item-content">
                <p>Prediksi hitung hasil stlh HDP, segala bentuk overtime atau injury time dihitung.</p>
                <p>HDP adalah sebuah angka yang digunakan oleh perusahaan "menyeimbangkan" peluang. Awalan "-" di tim atau pemain melambangkan Handicap, "+" melambangkan Underdog, Nilai tertinggi setelah handicap dianggap menang.Trik main HDP ada 3 jenis:</p> 
                <p>Integer </p>
                <p>Angka HDP adlh bil. bulat, cth:-1,-2,-3 dll, skor tim favorit kurang HDP. Jika yg skor tim favorit > tim underdog, maka menang dan jg sebaliknya, skor seri maka batal (void)</p>
                <p>Cth:A-1 dan B+1.Skor 2 tim A=3,B=2; A(3)-1=2=B(2),tim A&B tdk menang maupun kalah</p>
                <p>Hidup/Mati</p>
                <p>Angka HDP adalah setengah bil., cth:-0.5,-1.5,-2.5 dll,skor tim favorit kurang HDP.Jika yg skor tim favorit > tim underdog, maka menang dan jg sebaliknya.</p>
                <p>Cth:A-1.5 dan B+1.5.Skor 2 tim A=3:B=2,A(3)-1.5=1.5;A(3)-1.5=1.5&lt;B(2),tim B Underdog menang</p>
                <p>Combo</p>
                <p>kasus1. Handikap sbg .25,cth:-0/0.5,-1/1.5,-2/2.5 dll,skor tim favorit kurang HDP.Tim favorit> underdog, maka menang,dan juga sebaliknya.Jika seri, tim favorit kalah setengah dan underdog menang setengah.</p>
                <p>Cth:A-1/1.5 dan B+1/1.5.Skor 2 tim A=1:B=0,A(1)-1=0=B(0),tim A kalah setengah,tim B menang setengah</p>
                <p>kasus2. HDP sbg .75.Cth: -0.5/1,-1.5/2,-2.5/3 dll,skor tim favorit kurang HDP. Tim favorit > Underdog menang dan juga sebaliknya.Jika seri, tim favorit menang setengah dan underdog kalah setengah.</p>
                <p>Cth:A-0.5/1 dan B+0.5/1.Skor 2tim A=1:B=0,A(1)-1=0=B(0),tim A menang setengah ,tim B kalah setengah</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">8. Over/Under:</div>
            <div class="item-content">
				<p>Prediksi skor laga atas/bawah nilai bursa, overtime atau injury time dlm bentuk apapun dihitung. </p>
				<p>3 trik main O/U :</p>
				<p>Integer </p>
				<p>Nilai bursa adlh bil. bulat, cth "26,27,28" dll, skor gol kurang nilai bursa, jika hasil positif atas (over) menang, hasil negatif bawah (under) menang, hasil nol tidak menang/kalau (batal)</p>
				<p>cth :  Over 26 dan Under 26, jika total gol 2 tim 26, maka 26-26=0, maka over dan under tidak menang/kalah</p>
				<p>Hidup/mati</p>
				<p>Nilai bursa adlh separuh bil., cth : 26.5, 27.5, 28.5, dll, skor gol kurang nilai bursa, hasil positif atas (over) menang, hasil negatif bawah (under) menang</p>
				<p>cth : Over 26.5 dan Under 26.5,jika total gol 2 tim 26, maka 26-26.5=-0.5 , maka under menang, over kalah.</p>
				<p>Combo </p>
				<p>Kasus1: nilai bursa .25, cth :26/26.5,26/26.5,27/27.5,28/28.5 dll, skor gol kurang nilai bursa, hasil positif atas (over) menang, hasil negatif bawah (under) menang, over kalah setengah dan under menang setengah jika hasil 0</p>
				<p>cth : Over 26/26.5 dan Under 26/26.5, jika total gol 2 tim 26, 26-26=0, maka over 26/26.5 kalah setengah dan under 26/26.5 menang setengah</p>
				<p>Kasus2: nilai bursa .75, cth :26.5/27,27.5/28,28.5/29 dll,skor gol kurang nilai bursa,hasil positif atas (over) menang, hasil negatif bawah (under) menang, over menang setengah dan under kalah setengah jika hasil 0</p>
				<p>cth : Over 26.5/27 dan Under 26.5/27, jikat total gol 2 tim 27, 27-27=0, maka over 26.5/27 menang setengah dan under kalah setengah.</p>
            </div>
        </div>
        <div class="item">
            <div class="item-title">9 3 trik main O/U :</div>
            <div class="item-content">
                Prediksi hsl akhir laga O/U, overtime dlm btk apapun atau extra time dihitung (jk hsl muncul , maka dihitung sbg "genap").
            </div>
        </div>
        <div class="item">
            <div class="item-title">10. Waktu:</div>
            <div class="item-content">
                (Durasi/waktu event) - waktu single game/waktu event berlangsung; waktu single game berdasarkan scoreboard seusai game selesai. Jika scoreboard tidak tersedia, maka berdasarkan timer di dalam game. Perhitungan waktu event akan berdasarkan realtime. Jika durasi 2 tipe bursa waktu diatas lebih besar atau sama dengan bursa waktu standar, maka dianggap lebih besar.
            </div>
        </div>
        <div class="item">
            <div class="item-title">11. Total kill:</div>
            <div class="item-content">
            Jumlah kill dalam 1 babak: berdasarkan scoreboard resmi. Jika skor resmi belum tersedia, maka berdasarkan tampilan game sebagai patokan.
            </div>
        </div>
        <div class="item">
            <div class="item-title">12. Total Turret Hancur:</div>
            <div class="item-content">
            Jika tim menyerah sebelum laga selesai, maka semua turret hancur setelah menyerah akan dihitung. Termasuk tower yang dihacurkan oleh Creep maupun Deny.
            </div>
        </div>
        <div class="item">
            <div class="item-title">13.  Lane Turret ke-1 Hancur:</div>
            <div class="item-content">
                Mengacu pada Lane dimana tower hancur, terdiri dari Top/Mid/Bot. Total 6 tower pada Tier 1.
            </div>
        </div>
        <div class="item">
            <div class="item-title">14. Total Kill :</div>
            <div class="item-content">
                Jumlah kill pemain berdasarkan scoreboard.
            </div>
        </div>
        <div class="item">
            <div class="item-title">15.  Assists:</div>
            <div class="item-content">
            Assist: Berdasarkan scoreboard, jumlah assist pada tim.
            </div>
        </div>
        <div class="item">
            <div class="item-title">16. First Blood:</div>
            <div class="item-content">
                Berdasarkan scoreboard, skor pertama yang dicetak adalah First Blood.
            </div>
        </div>
        <div class="item">
            <div class="item-title">17. Satu Tower:</div>
            <div class="item-content">
            Tim yang menghancurkan Tower pertama( dalam DOTA2, tindakan deny tower dianggap sebagai menghancurkan tower musuh).
            </div>
        </div>
        <div class="item">
            <div class="item-title">18. N kill:</div>
            <div class="item-content">
            Mengacu salah satu pihak berhasil memperoleh total N kill terlebih dahulu.
            Contoh: 5 Kill, berarti satu pihak berhasil memperoleh total 5 kill terlebih dahulu.
            </div>
        </div>
        <div class="item">
            <div class="item-title">19. Kill ke-N:</div>
            <div class="item-content">
            Mengacu tim mana yang berhasil memperoleh kill ke-N terlebih dahulu. Contoh: Kill ke-5, berarti satu pihak berhasil memperoleh kill ke-5 terlebih dahulu.
            </div>
        </div>
        <div class="item">
            <div class="item-title">20. Player special betting:</div>
            <div class="item-content">
				Map Winner refer to the player who gets the highest kill in a single map. (Draw refund)
            </div>
        </div>
		<div class="item">
            <div class="item-title">21. Highest KDA:</div>
            <div class="item-content">
				Calculated as [K+A]/[D+1], K (kills), A (assists), D (deaths) (Calculate 2 decimal places). (Draw refund)
            </div>
        </div>
        `,
    gamePlay: [
        {
            title:'DOTA2',
            value:
                `
                    <p>1.<span class="label">DOTA2 koneksi putus saat laga</p>
                    <p>KasusA. Jika terjadi putus koneksi sblm first blood, tp kembali tersambung, maka hasil yg dianggap sah ttp dihitung.</p>
                    <p>KasusB. Jika usai first blood terjadi putus koneksi dan laga berberlgsg normal, tidak terjadi tindak tarik diri, maka hasil laga dikonfirmasi sah akan ttp dihitung.</p>
                    <p>KasusC. Jika slh satu pemain putus koneksi sblm first blood dan tdk dpt sambung kembali sampai game selesai, maka agar adil, tiket dianggap tak tak sah.</p>

                    <p>2.<span class="label">Posisi：</span>Info tim posisi ID (Jika terjadi pergantian posisi, ttp akan mengikut kondisi sebenarnya)</p>
                    <p>3.<span class="label">Role support dpt first blood：</span>di info tim apakah posisi pemain no 4 dan 5 dpt first blood.</p>
                    <p>4.<span class="label">Will 1st Blood Occur Before 3 Minutes: </span>Whether 1st Blood to happen within 3 minutes. (00:00 - 02:59)</p>
                    <p>5.<span class="label">Penghancur Tower ke-1: </span>Dalam DOTA2, tindakan deny tower dianggap sebagai menghancurkan tower musuh.</p>
                    <p>6.<span class="label">Roshan Kill: </span>Perhitungan berdasarkan hasil dari situs resmi atau siaran resmi (video demo). </p>
                    <p>7.<span class="label">Apakah muncul Mega Creep：</span>6 barrack dihancurkan dihitung "Ya". </p>
                    <p>8.<span class="label">Tim yang memperoleh Kill ke-N: </span>mengacu tim yang berhasil memperoleh Kill ke-N, dan bukan terlebih dahulu memperoleh N Kill (Contoh: Memperoleh Kill ke-20, berarti satu tim berhasil memperoleh Kill ke-20).</p>
                    <p>9.<span class="label">Team with 5 Kills Before 8 minutes:</span>Whether a team to accumulate 5 kills within 8 minutes. (00:00 - 07:59)</p>
                    <p>10.<span class="label">Pemakaian Smoke of Deceit(Consumable) terbanyak：</span> Tim yang memakai Smoke of Deceit (Consumable) terbanyak, tidak mencakup Ninja Gear yang menghasilkan Smoke of Deceit.</p>
                    <p>11.<span class="label">Total pemakaian Smoke of Deceit (Consumable) O/U：</span>Pemakaian Smoke of Deceit (Consumable) 2 tim O/U, tidak mencakup Ninja Gear yang menghasilkan Smoke of Deceit.</p>
                    <p>12.<span class="label">Pakai Aghanim's Shard：</span>Tim yang memakai Aghanim's Shard.</p>
                    <p>13.<span class="label">Total pemakaian Aghanim's Shard O/U：</span>Total pemakaian Aghanim's Shard 2 tim O/U.</p>
                    <p>14.<span class="label">Apakah 4 Bounty Rune terbagi rata：</span>hanya menghitung apakah 4 Bounty Rune yang terbentuk di pembukaan 00:00 terbagi sama rata pada kedua tim.</p>
                    <p>15.<span class="label">Total solo last hitting tertinggi (Last Hit)：</span>Hanya menghitung jumlah last hit, tidak termasuk Deny.</p>
                    <p>16.<span class="label">Kills Handicap Before 10 Minutes: </span>Cumulative Kill Handicap before 10 minutes. (00:00 - 09:59)</p>
                    <p>17.<span class="label">Total Kills Before 10 Minutes: </span>Cumulative Kills before 10 minutes. (00:00 - 09:59)</p>
                    <p>18.<span class="label">Menghancurkan Tormentors ke-1: </span>Tim yang berhasil menghancurkan Tormentors pertama.</p>
                    <p>19.<span class="label">Highest Net Worth Before 10 Minutes: </span>Refers to the Net Worth gained before 10 minutes. (00:00-09:59) (Draw refund)</p>
                    <p>20.<span class="label">Highest Net Worth Before 20 Minutes: </span>Refers to the Net Worth gained before 20 minutes. (00:00-19:59) (Draw refund)</p>
                    <p>21.<span class="label">Water Rune Equally Divided: </span>Refers to whether the four water runes within 06:00 minutes in the game are divided equally.</p>
                    <p>22.<span class="label">The Same Team Picked the First Two Wisdom Runes within First 14 Minutes: </span>Only the first two wisdom runes are counted within 14:00 minutes of game. (14:00 minutes is not counted)</p>
                    <p>23.<span class="label">Most Last Hits of Position 2 within 5 Minutes: </span>Cumulative last hit (exclude deny) before 05:00 minutes. (05:00 minutes is not counted)</p>
                `,
            icon:'game-logo-dota2'
        },
        {
            title:'League of Legends',
            value:
                `
                    <p>1. Role (Top/Mid/Support/ADC) berdasarkan pre-match bracket resmi.</p>
                    <p>2. <span class="label">Rift Herald：</span>Hanya kill dihitung,selama tim ini bunuh maka dinyatakan menang.</p>
                    <p>3. <span class="label">Total Turret Hancur O/E: </span>Berdasarkan papan skor, jumlah turret yang dihancurkan oleh kedua tim (termasuk Nexus Turret). Tidak termasuk Inhibitor dan Nexus (Base).</p>
                    <p>4. <span class="label">Perhitungan Dragon:</span> Semua Elemental Dragon dihitung, Elder Dragon tidak termasuk.</p>
                    <p>5. <span class="label"> Total pemain mendapatkan Baron Buff:</span> Jika tidak terjadi Baron Slain, maka dana akan dikembalikan.</p>
                    <p>6. Menghancurkan respawn inhibitor tidak termasuk dalam total turret hancur.</p>
                    <p>7. <span class="label">Total Survivor ketika Nexus Hancur: </span>Berdasarkan jumlah survivor ketika Nexus hancur (tidak termasuk total survivor ketika Nexus meledak atau setelah ledakan).</p>
                    <p>8. <span class="label">Total Kills Before 10 Minutes:</span>Cumulative Kills before 10 minutes. (00:00 - 09:59)</p>
                    <p>9. <span class="label">First Blood: Berdasarkan scoreboard, pemenang adalah pihak pertama yang berhasil membunuh lawan. </p>
                    <p>10. <span class="label">Semua bursa berbasis waktu, hitungan tiket：</span>Hitungan mengikut progres game, hasil laga mengikut skor web, stat usai laga, siaran resmi (video demo) dihitung.</p>
                    <p>11. <span class="label">Tebak tipe elemen dragon ke-N：</span>Pit akan tampilkan tipe elemen dragon, saat ada notifikasi berarti ada hasil. Hitungan mengikut web skor atau siaran resmi (video demo).</p>
                    <p>12. <span class="label">Total kill (00:00-10:00 mnt,waktu ditetapkan)：</span>Tiket sah di waktu ditetapkan, jika laga tdk lewat dr waktu ditetapkan, cth: 10:00 mnt, durasi 0-10 mnt dianggap tak sah. Sesuai timer dlm game, hitungan hasil mengikut web skor atau siaran resmi (video demo).</p>
                    <p>13. <span class="label">Apakah Elder Dragon dirampas：</span>Jika tdk ada dragon terbunuh, maka hitung hasil 2 tim 0:0 adlh "tak", hasil laga dihitung mengikut web skor, stat usai laga dan siaran resmi (video demo).</p>
                    <p>14. <span class="label">Digit akhir Total Kill:</span> Prediksi apakah jumlah digit terakhir total kill lebih besar/kecil dari nilai bursa. Contoh: Hasil 9:12 ( digit akhir total kill 9+2=11) Hasil laga berdasarkan skor situs resmi, post-match stats, API atau siaran resmi (video demo).</p>
                    <p>15. Triple Kill muncul setelah membunuh 3 musuh secara berurutan; Quadra Kill muncul setelah membunuh 4 musuh secara berurutan. </p>
                    <p>16. <span class="label">Apakah muncul Ace：</span>semua pemain mati di satu tim atau muncul tanda Ace dihitung Ace.</p>
                    <p>17.<span class="label">Duluan peroleh Eye of the Herald:</span>merujuk pada tim pertama yang memperoleh Eye of Herald dan tidak ada kaitanya dengan membunuh Rift Herald.</p>
                    <p>18.<span class="label"> Perbedaan ekonomi 2 tim O/U & HDP:</span> Hitungan hasil mengikut skor situs resmi, Stats usai laga, API atau siaran resmi (video demo). Note: misalkan perbedaan ekonomi pada laga terkait dengan yang kami sediakan sama, maka tiket akan dibatalkan, dianggap tak sah (Seri) dan dana dikembalikan.</p>
                    <p>19.<span class="label">HDP kill 10 min:</span>Tiket sah di waktu ditetapkan, jika laga tdk lewat dr waktu ditetapkan, cth: 10:00 mnt, durasi 0-10 mnt dianggap tak sah. Sesuai timer dlm game, hitungan hasil mengikut web skor atau siaran resmi (video demo).</p>
                    <p>20.<span class="label">Total turret hancur Top Lane O/U:</span>Total turret yang hancur di Top Lane O/U: Tidak mencakup Nexus Turret kedua tim.</p>
                    <p>21.<span class="label">Total turret hancur Mid Lane O/U:</span>Total turret yang hancur di Mid Lane O/U: Tidak mencakup Nexus Turret kedua tim.</p>
                    <p>22.<span class="label">Total turret hancur Bot Lane O/U:</span>Total turret yang hancur di Bot Lane O/U: Tidak mencakup Nexus Turret kedua tim.</p>
                    
                    <p>23. <span class="label">Lane dengan Turret hancur terbanyak: </span>permainan Lane dengan Turret hancur terbanyak hanya menghitung Top Lane dan Bot Lane, Mid Lane tidak termasuk (Draw no Bet).</p>
                    <p>24.<span class="label">Peroleh gold tertinggi: </span>merujuk pada total gold tim pemain dan mengacu papan skor sebagai patokan.</p>
                    <p>25. <span class="label">Role bunuh Baron Nashor: </span>merujuk pada role yang membunuh naga pertama. Mengacu video sebagai patokan, tiket dianggap tak sah dan dana dikembalikan jika gagal membunuh.</p>

                    <p>26. HDP Kill ketika satu tim berhasil memperoleh N Kill: merujuk salah satu pihak berhasil memperoleh N Kill lebih dulu pada scoreboard.
                    Contoh: Bursa taruhan【HDP Kill Team A ketika berhasil memperoleh 5 Kill -1.5】Tampilan scoreboard ketika satu tim berhasil memperoleh 5 Kill 5:3, dimana 5 -1.5=3.5>3, maka【HDP Kill Team A -1.5 menang】.</p>

                    <p>27. Total Kill ketika satu tim berhasil memperoleh N Kill: merujuk salah satu pihak berhasil memperoleh N Kill lebih dulu pada scoreboard. 
                    Contoh: Bursa taruhan【Total Kill ketika satu tim berhasil memperoleh N Kill 8.5】Tampilan scoreboard ketika satu tim berhasil memperoleh 5 Kill 5:4, maka Total Kill 9, maka memasang【Over】menang.</p>

                    <p>28. Total Kill ketika satu tim berhasil memperoleh N Kill: merujuk salah satu pihak berhasil memperoleh N Kill lebih dulu pada scoreboard. 
                    Contoh: Bursa taruhan【Total Kill ketika satu tim berhasil memperoleh N Kill 8.5】Tampilan scoreboard ketika satu tim berhasil memperoleh 5 Kill 5:4, maka Total Kill 9, maka memasang【Over】menang.</p>

                    <p>29. Role memperoleh Last Kill: merujuk Role Champion yang memperoleh Last Kill di akhir game berdasarkan video.</p>

                    <p>30. Role memperoleh Last Death: merujuk Role Champion yang mendapatkan Last Death di akhir game berdasarkan video.</p>
                    <p>31. <span class="label">Highest Last Hit: </span>Refers to the highest number of last hit obtained by the players, the results are based on the scores on the official website and the post-tournament statistics panel. (Draw refund)</p>
                    <p>32. <span class="label">Highest Last Hit Before 10 Minutes: </span>Refers to the highest number of last hit gained before 10 minutes (00:00 - 09:59). (Draw refund)</p>
                    <p>33. <span class="label">First to Obtain Mythic Item: </span>Refers to the player who gets the first Mythic Item. (based on the Mythic Item provided by the official website)</p>
                    <p>34. <span class="label">Highest Kill Streak Will Be Quadra Kill or Penta Kill: </span>Only the highest kill streak is counted, e.g. if the highest is Quadra Kill then Quadra Kill wins, if the highest is Penta Kill then Penta Kill wins, the rest are considered as none.</p>
                    <p>35. <span class="label">Epic Monsters: </span>Refers to Elemental Dragon, Baron Nashor, Rift Herald, and Elder Dragon.</p>
                    <p>36. <span class="label">Elder Dragon Spawned: </span>Refers to whether the Elder Dragon is spawned. Result will be Yes as long as the Elder Dragon was spawned. (regardless of whether it is killed or not)</p>
                    <p>37. <span class="label">Last Killed Champion Role upon Slaying the First Baron: </span>If Baron Nashor is not killed, it will be considered as no result.</p>
                    <p>38. <span class="label">Slain the Nth Dargon Within 1 Minute After Respawned: </span>If the game ends within one minute after the respawn of the Nth dragon and it's not killed, result will be considered as No ; If the game ends before the Nth dragon respawn, it will be considered as no result.</p>
                    <p>39. <span class="label">First Activated "Flash"/"Teleport": </span>Refers to the first player to activate "Flash"/"Teleport" skills. The skill must be selected by both players to be considered as effective bets. If one of the player does not choose the corresponding skill, example: Player A chosen "Ignite" and "Flash", Player B chosen "Flash" and "Teleport", then First Activated "Teleport" will be considered as no result.</p>
                   
                `,
            icon:'game-logo-lol'
        },
        {
            title:'CSGO/CS2',
            value:
                `
                    <p>1. <span class="label">Pistol ke-1 menang：</span>Tim yg menang pistol ronde 1</p>
                    <p>2. <span class="label">Pemenang ronde 5/10：</span>Tim yg pertama menangkan ronde 5/10</p>
                    <p>3. O/E, saat hasil dihitung "0" dianggap Even </p>
                    <p>4. <span class="label">Definisi ronde CSGO：</span>Saat ronde baru masuk buy time, ronde sblmnya dianggap selesai.</p>
                    <p>5. <span class="label">Total headshot ronde ke-N：</span>Segala headshot dihitung selama muncul tanda/notifikasi headshot</p>
                    <p>6. <span class="label">Kill ke-1：</span>Harus bunuh lawan, jika bunuh diri atau dibunuh rekan maka ronde ini dianggap tdk ada kill dan taruhan balik dana.</p>
                    <p>7. <span class="label">Terbunuh ke-1：</span>ronde 1 pemain ke-1 dibunuh, kematian apapun dihitung. </p>
                    <p>8. <span class="label">Total kill di Pistol：</span>Hitungan mengikut web skor atau siaran resmi (video demo) </p>
                    <p>9. <span class="label">Kill：</span>Hitungan mengikut web skor atau siaran resmi (video demo) </p>
                    <p>10. <span class="label">1st Half Winner/2nd Half Winner: </span></p>
                    <p>In CSGO First Half (Round 1 to Round 15) / Second Half (Round 16 to Round 30, draw refund) The teams with the highest score wins, each half is calculated separately.</p>
                    <p>In CS2 First Half (Round 1 to Round 12, draw refund) / Second Half (Round 13 to Round 24, draw refund) The teams with the highest score wins, each half is calculated separately.</p>
                    <p>11. <span class="label">Pistol, Total headshot rondeN：</span>Hitungan mengikut web skor, siaran resmi (video demo). Note: Mati di-headshot oleh rekan tim dihitung sbg total headshot.</p>
                    <p>12. <span class="label">Pistol, total kill rondeN：</span>Hitungan mengikut web skor, siaran resmi (video demo). Note: Mati kena ledakan tdk dihitung.</p>
                    <p>13. <span class="label">Cara menang ronde N:</span>Terdapat 4 cara untuk mencapai kemenangan, yaitu waktu habis/ledakan/jinakkan/pembasmian. Penilaian kemenangan round terkait tergantung pada cara menang.</p>
                    <p>14. <span class="label">Apakah muncul knife kill：</span>Knife round pilih faksi dan warmup tdk dihitung. Jika dibunuh rekan dgn pisau, jg dihitung "Ya"</p>
                    <p>15. <span class="label">Apakah muncul grenade kill：</span>Pilih faksi dan warmup tdk dihitung, hanya hitung kill"granat"."Fire bomb" serta bom lainya tdk dihitung, jika dibunuh rekan dgn granat dihitung "Ya".</p>
                    <p>16. <span class="label">Pemenang FH/map：</span>Item bursa harus memenuhi syarat FH dan map menang baru dpt hadiah.</p>
                    <p>17. <span class="label">Pemain dengan Assist Terbanyak: </span>Semua Kill Assist dalam CSGO akan dihitung.NB: Flashbang Assist tidak termasuk. </p>
                    <p>18.<span class="label">Total Clutches Won: Official calculation method: </span>The number of players in this round is reduced to 1 vs 1, and a total of 9 kills must be achieved in this round to be considered as a Clutch. Settlement will only be based on official post-match results showing "CLUTCHES WON".</p>
                    <p>19.<span class="label">Whether or not a player gets three kills in a single round (rounds N through N+1): </span>In rounds N through N+1, a single player with three or more kills in a single round (including kills of teammates) is considered to won the betting order. If rounds N through N+1 are not played, the order is considered null and void.</p>
                    <p>20.<span class="label">Market with specified map: </span>If the selected map is not the specified map of the market, it will be considered as no result.</p>
                    <p>21.<span class="label">Market with player's name only calculate the data of a single player. Example: </span>Player With Higher Kills : A=30 B=25, then the option including player A wins ; If there are multiple players, the option including the player with the highest corresponding data wins. Example: Player With Higher Assists : A=3/B=5, C=6/D=1, the highest is C, then option C/D including player C wins. (Draw refund)</p>
                    <p>22.<span class="label">Correct Round Score(Exclude Overtime): </span></p>
                    <p>If the option is a one-way score, any team that obtains the corresponding score will be considered as win. </p>
                    <p>For example in CSGO : 16:9, Team A=9 Team B=16, the option 16:9 is considered as win. Team A=16 Team B=9 Option 16:9 also considered as win ; </p>
                    <p>In CS2 : 13:5, Team A=13 Team B=5, the option 13:5 is considered as win. </p>
                    <p>If the option is a two-way score, the corresponding team must obtain the correct score to win. </p>
                    <p>For example in CSGO: 16:11 11:16, Team A=16 Team B=11, then option 16:11 will be considered as win, Team A=11, Team B=16, then option 11:16 will be considered as win. (If it goes into overtime, 15:15 will be considered as win)</p>
                    <p>In CS2 : 13:7 7:13, Team A=13 Team B=7,  then option 13:7 will be considered as win. (If it goes into overtime, 12:12 will be considered as win)</p>
                    <p>23.<span class="label">2nd Pistol Round - Winner: </span>In CSGO , Round 16 is the 2nd Pistol Round. In CS2 , Round 13 is the 2nd Pistol Round.</p>
                `,
            icon:'game-logo-csgo'
        },
        {
            title:'King of Glory',
            value:
                `
                    <p>1. <span class="label">Tower Hancur:</span> Tim mana yang pertama menghancurkan Tower. NB: Base crystal tidak termasuk.</p>
                    <p>2. <span class="label">First Blood:</span> Berdasarkan scoreboard, pemenang adalah pihak pertama yang berhasil membunuh lawan.</p>
                    <p>3. <span class="label">Will 1st Blood Occur before 2 Minutes: </span>Whether 1st Blood to happen within 2 minutes. (00:00 - 01:59)</p>
                    <p>4. <span class="label">1st Tyrant Killed Before 2:30: </span>Whether to kill the 1st Tyrant within 2:30 minutes. (00:00 - 02:29)</p>
                    <p>5. Posisi Pemain(Roam/Mid/Jungle/Clash/Farm) berdasarkan pengumuman reesmi post match bracket.</p>
                    <p>6. <span class="label">Lane dengan Turret hancur terbanyak:</span> permainan Lane dengan Turret hancur terbanyak hanya menghitung Overlord Lane dan Tyrant Lane, Mid Lane tidak termasuk (Draw no Bet).</p>
                    <p>7. <span class="label">Total Overlord Slain: </span>Semua Overlord dihitung, termasuk Prophet dan Shadwo.</p>
                    <p>8. <span class="label">Total Tyrant Slain: </span>Semua Tyrant dihitung termasuk Tyrant dan Shadow Tryant.</p>
                    <p>9. 1st Tyrant Killed Before 4:30: Whether to kill the 1st Tyrant within 4:30 minutes. (00:00 - 04:29)</p>
                    <p>10. <span class="label">Kills Handicap Before 6 Minutes: </span>Cumulative Kill Handicap before 06:00 minutes. (06:00 minutes is not counted)</p>
                    <p>11. <span class="label">Total Kills Before 6 Minutes: </span>Cumulative Kills before 06:00 minutes. (06:00 minutes is not counted)</p>
                    <p>12. <span class="label">First Tower Destroyed Before 5:30: </span>Refer to whether the first tower will be destroyed before 05:30 minutes. (Exactly 05:30 minutes will be considered as No)</p>
                    <p>13. <span class="label">Ancient: </span>Ancient refer to Tyrant , Dark Tyrant , Overlord , Phophet Overlord , Dark Overlord and Storm Dragon.</p>
                    <p>14. <span class="label">First Storm Dragon Spawn Location: </span>If Storm Dragon hasn't spawn yet will be cancelled and considered as no result.</p>
                `,
            icon:'game-logo-aov'
        },
        {
            title:'Valorant',
            value:
                `
                    <p>Pemenang Pistol Round Pertama: Tim yang berhasil memenangkan pistol round ke-1.</p>
                    <p>Pemenang Pistol Round Kedua: Tim yang berhasil memenangkan pistol round ke-13.</p>
                    <p>Pemenang 1st Half: Tim yang berhasil meraih skor tertinggi di First Half (ronde1-12 ).</p>
                    <p>Pemenang 2st Half: Tim yang berhasil meraih skor tertinggi di Second Half (ronde13-24).</p>
                    <p>Cara menang ronde N:Terdapat 4 cara untuk mencapai kemenangan, yaitu waktu habis/ledakan/jinakkan/pembasmian. Penilaian kemenangan round terkait tergantung pada cara menang. </p>
                `,
            icon:'game-logo-Valorant'
        },
        {
            title:'Bola Basket',
            value:
                `
                    <p>1. Tiket NBA FT termasuk skor overtime. babak, tiket tdk termasuk skor overtime.</p>
                    <p>2. Hitungan mengikut laga, penyedia skor resmi atau data diumumkan oleh web resmi. Saat penyedia skor atau web resmi tdk ada atau bukti membuktikan data yg disediakan salah, kami akan hitung sesuai data independen.</p>
                    <p>3. Jika laga tdk berlgsg di waktu ditetapkan, tdk ada tanding ulang dlm 36 jam waktu semula, maka taruhan terlibat dianggap tak sah, balik dana.</p>
                    <p>4. Jika laga ditengah berhenti dan tdk balik berlgsg dlm 36 jam, maka taruh yg ada hasil ttp sah, yg tnp hasil dianggap tak sah.</p>
                    <p>5. Jika tmpt laga ganti, maka taruhan terlibat dianggap tak sah.</p>
                    <p>6. babak/Taruh FH, laga harus selesai barulah tiket dianggap sah, kecuali ada uraian lain di aturan main.</p>
                    <p>7. Jika laga berlgsg sblm waktu yg ditetapkan, maka taruhan sblm laga mulai ttp sah dan  taruhan stlh laga mulai dianggap tak sah.(inPlay adlh kasus lain)</p>
                    <p>8. Taruhan Terlambat (Past-Post) adalah taruhan pre-match yang diterima setelah laga dimulai karena alasan apapun.
                    Taruhan dalam kondisi ini dianggap sebagai taruhan tertunda. Jika bursa In-Play tidak tutup tetap waktu, maka taruhan setelah sesi tersebut dianggap taruhan terlambat dan akan balik dana.</p>
                `,
            icon:'game-logo-basketball'
        },
        {
            title:'Sepak Bola',
            value:
                `
                    <p>1. Tgl dan waktu mulai laga yg ada di web hanya sbg referensi, jika laga blm mulai, bkn di hari sama (waktu lokal) mulai, maka tiket terlibat tak sah dan balik dana. Terkecuali, taruhan tim dlm piala dunia ttp sah tdk peduli laga ditunda atau diundur.</p>
                    <p>2. Jika laga berhenti sblm selesai, dan hari itu (dlm 36 jam) tak selesai, (waktu lokal) taruhan terkait hasil laga dianggap tak sah dan balik dana.</p>
                    <p>a. Jika laga berhenti sblm selesai dan hari itu tidak selesai (waktu lokal) selama FH sdh selesai, taruhan FH ttp sah. Jika laga yg terhenti pd hari itu tdk selesai, (waktu lokal) maka taruhan yg tak terkait dgn FH dianggap tdk sah, termasuk dan tdk dibatasi :  Tendangan sudut, Offside, dikeluarkan,kartu merah/kuning, 2 tim dpt poin , skor absah, waktu dpt poin , tdk bobol dan pemain andalan dpt poin/tanpa poin.</p>
                    <p>b. Jika 2tim sdh dpt skor dan laga berhenti, maka taruhan yg dpt skor tak sah. JIka pemain dpt skor dan laga berhenti, taruhan pemain dpt skor/tdk dpt skor jg tak sah. Tdk peduli gol ada skor/tdk, jika laga berhenti, taruhan yg ada skor tdk sah. Jika laga berhenti, semua taruhan tdgn sudut, offside tak sah, meskipun lampaui nilai bursa HDP. Meskipun laga berhenti, jika tim dpt skor, taruhan pd prediksi bursa skor ke-1 atau skor seterusnya ttp sah. Jika laga berhenti tp sblm berhenti, cetak gol, maka taruhan tim sblm dpt skor atau stlh adlh sah.</p>
                    <p>3. Jika bursa ada opsi seri, saat hasil seri, kami hanya bayar taruhan seri,sdgkan taruh tim mana menang adlh kalah.</p>
                    <p>4. Kecuali ada deskripsi lain, hitungan taruh akan mengikut waktu biasa laga (termasuk injury time), tdk termasuk overtime dan adu penalti.</p>
                    <p>5. O/U: Laga harus selesai FT, taruhan gol sah.</p>
                    <p>6. Jika O/U bursa ekuivalen, balik dana.</p>
                    <p>7. Kecuali ada deskripsi lain, jika lokasi dari kandang sendiri ke venue netral atau venue netral ke kandang sendiri,  semua taruhan ttp sah. Jika  home ke kandang lawan semula, maka taruhan batal. Jika nama kandang sendiri dan kandang lawan terbalik, maka taruhan batal.</p>
                    <p>8. Hitungan tiket mengikut hasil resmi atau hak institusi memutuskan hasil.</p>
                    <p>9. Taruhan Terlambat (Past-Post) adalah taruhan pre-match yang diterima setelah laga dimulai karena alasan apapun.
                    Taruhan dalam kondisi ini dianggap sebagai taruhan tertunda. Jika bursa In-Play tidak tutup tetap waktu, maka taruhan setelah sesi tersebut dianggap taruhan terlambat dan akan balik dana.</p>
                `,
            icon:'game-logo-football'
        },
        {
            title:'FIFA',
            value:
                `
                    <p>1. Ketentuan hitungan HDP inPlay mengikut taruh sampai laga/hasil akhir babak, lalu skor laga kurang skor taruhan. Hitungan HDP H1 inPlay mengikut akhir hasil laga HT.</p>
                    <p>2. Hitungan sisa waktu menang mengikut ketentuan taruh sampai laga/hasil akhir babak, lalu skor laga kurang skor taruhan. Hitungan taruh sisa babak FH menang mengikut hasil akhir FH. </p>
                `,
            icon:'game-logo-fifa'
        },

    ],
    zhuboPan: [
        {
            title:'1. Aturan umum',
            value:
                `
                    <p>
						Aturan di bwh utk semua laga/jenis taruhan
                    </p>
                `,
            icon:false
        },
        {
            title:'2. Umum',
            value:
                `
                    <p>a. Kami menyediakan semua info taruh secara jujur, Namun, kami tidak bertanggung jawab atas tgl, waktu, lokasi, lawan, odds, hasil, info stats(tampil di video secara live) atau info kurang/error.</p>
                    <p>b. Kami berhak memperbaiki error dan mengambil tindakan utk memastikan integritas dlm kelola bursa. Bursa didefiniskan sbg jenis taruhan yg disediakan  streamer game. Kami berhak mengambil keputusan akhir.</p>													
                    <p>c. Klien memiliki kewajiban untuk memahami skor laga dan info laga kapanpun dan harap klien memastikan keadaan laga sblm taruh.</p>
                    <p>d. Saat muncul situasi aneh, kami berhak utk memperbaiki aturan kapanpun. Aturan yg sdh diperbaiki otomatis berlaku stlh diumumkan di web.</p>
                    <p>e. Klien mengakui data yg disediakan di web, waktu serta data yg berasal dr pihak ketiga dlm bentuk"siaran",  jika ada waktu yg tertunda atau kondisi tdk akurat dan mengikut data itu lakukan taruh, maka klien yg menanggung resiko.Perusahaan tdk menjamin akurasi, integritas atau ketepatan waktu saat menyediakan data, maka tidak bertanggung jawab akan kerugian ( lgsg atau tidak lgsg) klien yg mengikuti data itu.</p>
                    <p>f. Laga streamer adlh laga dmn streamer melalui sistem matchmaking temukan pemain lewat.</p>
                    <p>g. Regulasi laga steamer hanya utk semua streamer dan bkn liga profesional umumnya, saat aturan umum laga streamer bentrok dgn aturan khusus game (spt: LoL, PUBG, CS:GO,dll), ikuti aturan khusus game sbg patokan.</p>
                `,
            icon:false
        },
        {
            title:'3. Kesahan',
            value:
                `
                    <p>a. Jika streamer blm masuk laman main (kecuali persiapan tanding) dan keluar, atau pemain lain keluar yg menyebabkan game tdk bs dimulai,maka dianggap tak sah. Bursa hanya utk laga sah, dan laga berikut msh sah.</p>
                    <p>b. Jika streamer blm mulai game bersangkutan dan mulai game lain, maka diaggap tak sah. Bursa hanya utk laga sah, dan laga berikut msh sah.</p>
                    <p>c. Jika waktu main streamer krg dr setengah durasi game, maka dianggap tak sah dan dana taruhan dikembalikan.</p>												
                    <p>d. Pihak sengaja taruh saat game dlm kondisi aneh, maka taruhan mungkin dibatalkan.</p>
                    <p>e. Jika muncul niat memakai alat peretasan yg menyebabkan waktu game aneh, maka taruhan mungkin dibatalkan.</p>
                    <p>f. Jika pemain berniat/sengaja kalah, maka taruhan mungkin dibatalkan. </p>
                `,
            icon:false
        },
        {
            title:'4. Aturan dasar',
            value:
                `
                    <p class="title">• Sah/ sebagian yg sah</p>													
                    <p>Streamer hrs ada lbh dr setengah waktu game dioperasikan sendiri, jika tidak, balik dana.</p>
                    <p class="title">• Balik dana atau tertunda</p>
                    <p>Streamer bermain bkn game spefisik atau game tanpa bursa, maka dianggap sbg blm mulai.</p>
                    <p class="title">• Payout</p>
                    <p>Semua hasil atau tdk ada deskripsi spefisik mengikut halaman hasil resmi sbg patokan.</p>
                `,
            icon:false
        },
        {
            title:'5. League of Legends',
            value:
                `
                    <p class="title"> • Penentuan dan diundur </p>
                    <p>a. Menentukan main LoL dlm 5 mnt sah/tdk.</p>
                    <p>b. Laga yg waktu main selesai 5 mnt awal (dmn bisa tanding ulang) dianggap diundur.</p>
                    <p>c. Harus main 5 vs 5, aturan umum LoL (Perbaikan apapun pd aturan/ mode tak sah)</p>
                    <p class="title">• Syarat bursa</p>
                    <p>Semua bursa hrs memenuhi bbrp syarat pd tiap game berbeda, jika tidak, balik dana. Syarat LoL dibagi:</p>
                    <p>a. Rank streamer: Streamer harus main sesuai peringkat poin dan deskripsi saat matchmaking. Sbg berikut: </p>
                    <p class="rules1 rules-image"></p>
                    <p>b. Solo rank steamer:  Steamer matchmaking sesuai personal status dan urutan imej resmi. Sbg berikut:</p>
                    <p class="rules2 rules-image"></p>
                    <p>c.Steamer hrs memakai akun spefisik: Streamer harus memakai akun spefisik kami agar bisa membedakan nama akun streame. Sbg berikut:</p>
                    <p class="rules3 rules-image"></p>
                    <p>d. Steamer harus main LoL resmi, yakni bkn lobby personal/ perbaikan aturan mode game, mengikut tampilan imej mode game </p>
                    <p class="rules4 rules-image"></p>
                    <p class="title"> • Hasil payout</p>
                    <p>a. bursa taruh mengikut hasil resmi. sbg berikut:</p>
                    <p class="rules5 rules-image"></p>
                    <p>b. Jika tdk ada hasil resmi, ikuti screenshot akhir sbg patokan.	</p>
                    <p class="rules6 rules-image"></p>
                    <p>c. Jika tdk ada screenshot akhir/screenshot dgn hasil  ada bentrok, maka ikuti laman cek hasil.</p>
                    <p class="rules7 rules-image"></p>
                    <p>d. Jika hasil resmi muncul tanda streamer AFK, tdk peduli menang/kalah ttp dianggap kalah.</p>
                    <p class="rules8 rules-image"></p>
                    <p>e.Jika streamer ditengah game keluar, maka ikuti hasil akhir resmi sbg patokan. (AFK dianggap gagal). spt gbr:</p>
                    <p class="rules8 rules-image"></p>
               `,
            icon:'game-logo-lol'
        },
        {
            title:'6. King of Glory',
            value:
                `															
                    <p class="title"> • Penentuan dan diundur</p>
                    <p>a. KoG akan di dtk terakhir tentukan sah atau tidak</p>
                    <p>b. Laga selesai dlm 1 mnt akan diundur sampai game selanjutnya.</p>
                    <p>c. Harus main 5 vs 5, aturan umum KoG (Perbaikan apapun pd aturan/ mode tak sah)</p>
                    <p class="title"> • Syarat bursa</p>
                    <p>Semua bursa hrs memenuhi bbrp syarat pd tiap game berbeda, jika tidak, balik dana. Syarat koG dibagi:</p>
                    <p>a. Rank streamer sesuai dgn urutan deskripsi .Spt berikut: </p>
                    <p class="rules9 rules-image"></p>
                    <p>b. Streamer memakai akun ditentukan, streamer hrs memakai akun yg kami tentukan agar bisa membedakan dgn jls nama akun sbg patokan.spt berikut:</p>
                    <p class="rules9 rules-image"></p>	
                    <p>c. Streamer solo, mengikut urutan jlh pemain. Dibwh ini cth yg tdk sesuai dgn aturan:</p>
                    <p class="rules10 rules-image"></p>	
                    <p class="title"> • Hasil payout</p>
                    <p>a. Bursa taruh mengikut hasil resmi, spt berikut:</p>
                    <p class="rules11 rules-image"></p>
                    <p>b. Jika tdk ada imej maka mengikut akhir game. Spt berikut:</p>
                    <p class="rules12 rules-image"></p>
                    <p>c. Jika screenshot tdk relevan atau hasil bentrok, maka mengikut hasil game resmi. Spt berikut:</p>
                    <p class="rules11 rules-image"></p>	
                    <p class="title"> • Bursa batal, balik dana dan kasus khusus</p>
                    <p>Streamer keluar dan tdk tampil menang/kalah di laman hasil, maka dinyatakan kalah. Hasil lainnya mengikut imej terakhir sblm keluar.</p>
                `,
            icon:'game-logo-aov'
        },
    ],
};
