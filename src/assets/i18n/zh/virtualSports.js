export default {
    footballRanking: {
        Leaderboard: '排行榜',
        Team: '球隊',
        MP: '比賽',
        WDL: '勝/平/負',
        Pts: '積分',
    },
    groupKnockout: {
        Round1: '16強',
        QuarterFinal: '1/4決賽',
        Semifinals: '半決賽',
        Final: '決賽',
        zanwu: '暫無',
    },
    groupMatches: {
        zu: '{no} 組',
        Team: '球隊',
        MP: '比賽',
        WDL: '勝/平/負',
        jin: '進',
        shi: '失',
        jingshengqiu: '淨勝球',
        Pts: '積分',
    },
    knockout: {
        GroupStage:'小組賽',
        KnockoutStage:'淘汰賽',
    },
    video:{
        batchNo:'第{no}輪',
        dateNo:'{no}期',
        matchEnd:'已完賽',
    },
    detail:{
        historyRecord:'歷史戰績',
    },
    endGame:{
        tips1:"一般電競遊戲賽果可在此查看",
        tips2:"如果查看電子遊戲賽果，請進入 {0} 查看",
        tips3:"下次不再顯示",
    },
};
