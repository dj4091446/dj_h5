const numbers = '全局 | 第一局 | 第二局 | 第三局 | 第四局 | 第五局 | 第六局 | 第七局 | 第八局 | 第九局 | 第十局 | 第十一局 | 第十二局 | 第十三局'
const Mapnums = numbers.split('|')
export default {
    mixins: {
        live: '滾球',
        early: '早盤',
        cross1: '串1',
        betFail: '投注失敗',
        cross: '串',
        Global: '全局',
        copy: '複製成功',
        max30: '最多收藏30場比賽',
        same: '同局內，相同類型盤口只能添加一個',
        upperLimit: '串關註單總賠率已經達到上限，不能添加更多選項了！',
        finish: '註單確認中',
        FullTime: '全場',
        HalfTime: '上半場',
        OneQ: '第一節',
        TwoQ: '第二節',
        ThreeQ: '第三節',
        FourQ: '第四節',
        champion: '冠軍',
        overtime: '加時',
        middleHalf: '中場',
        secondHalf: '下半場',
        rest1: '第1節休息',
        rest2: '第2節休息',
        rest3: '第3節休息',
        overtime_first_half: '加時上半場',
        overtime_second_half: '加時下半場',
        mid_rest: '中場休息',
        Mapnum: (ctx) => Mapnums[ctx.named('num')],
        all: '全部',
    },
    commonInput: {
        balance: '餘額不足',
        oddsChange: '總賠率已變化',
        redLimit: '超出限紅',
    },
    message: { closeMessage: '( 2s後自動關閉 )', close: '關閉' },
    betItem: { nocross: '當前盤口不支持串關，請選擇其他標識"串"的盤口' },
    commonNotData: { golive: '去滾球看看吧~' },
    commonTabs: { global: '全局' },
    listLoad: '正在努力加載中～',
    numberBoard: { max: '最大', thousand: '千' },
    selectOdds: { select: '選擇接收方式', changyong: '常用金額' },
    digitalToChinese: {
        number1: '一',
        number2: '二',
        number3: '三',
        number4: '四',
        number5: '五',
        number6: '六',
        number7: '七',
        number8: '八',
        number9: '九',
        number10: '十',
        number11: '十一',
    },
    systemModel: { notice: '公告', confirm: '確定', yes: "是", no: "否" },
    awardAnimate: {
        jibai:
            '<span>擊</span>\n            <span>敗</span>\n            <span>99%</span>\n            <span>的</span>\n            <span>人</span>',
        qiji:
            '<span>奇</span>\n            <span>跡</span>\n            <span>締</span>\n            <span>造</span>\n            <span>者</span>',
    },
    menu: { 
        all: '全部',
        specials: '電子遊戲',
    },
    tips: {
        max30days: "隻能查詢30天之內的注單",
        qiehuanxianlu: "切換線路",
        qiehuanxianlutishi: "當前線路擁擠，請點擊刷新按鈕切換路線",
        shuaxing: "刷新"
    },
};
