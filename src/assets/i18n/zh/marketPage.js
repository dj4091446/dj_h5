export default {
    marketHeader: {
        yiquxiao: '已取消',
        yijieshu: '已結束',
        hot: '熱門賽事',
        zhunbei: '準備中',
        jinxing: '進行中 等待確認有效性',
        youxiao: '有效',
        wuxiao: '當前視頻賽事無效',
        bifenpanding: "比分判定中"
    },
    index: {
        station: '局內串關',
        shouqi: '點擊收起',
        zanwutishi: '去其它地方看看吧~',
        tips:
            '温馨提示：當前直播比賽的盤口已處於等待結算狀態，您可投注的盤口為主播的下一局有效比賽',
        lock: '鎖定播放',
        unlock: '取消鎖定',
        result: '賽果照片',
        noresult: '暫無更多賽果推薦',
        zhankai: '點擊展開',
        jiesuan: '已結算/待結算',
        market: '盤口',
        huchuan: '以上盤口可以自由互串',
        same: '以上盤口為相同類型',
        zanwu: '暫無更多盤口',
        shuoming: '局內串關規則説明',
        chuanguanzuhe: '您可以在同場賽事且同一單局內完成串關組合',
        iknow: '我知道了',
        openStation: '已為您開啓局內串關！',
        nextRound1: "跳轉第一局",
        nextRound2: "跳轉第二局",
        nextRound3: "跳轉第三局",
        nextRound4: "跳轉第四局",
        nextRound5: "跳轉第五局",
        nextRound6: "跳轉第六局",
        nextRound7: "跳轉第七局",
        nextRound8: "跳轉第八局",
        nextRound9: "跳轉第九局",
        nextRound10: "跳轉第十局",
        nextRound11: "跳轉第十一局",
        nextRound0: "跳轉全局",
        MarketBet: "<span>盤</span><span>口</span><span>下</span><span>注</span>",
        toNext0: '跳轉全場',
        toNext100: '跳轉上半場',
        toNext1: '跳轉第一節',
        toNext2: '跳轉第二節',
        toNext3: '跳轉第三節',
        toNext4: '跳轉第四節',
        All: "全部",
        Mix: "復合",
        Handicap: "讓分",
        TotalGoals: "大小",
        guanbizhibo: '關閉直播',
        stationBet: "<span>局內</span> <span>串關</span>",
        touzhulan: '<span>投注</span><span>欄</span>',

        endMatchHome:"<p>賽事已結束,</p><p>是否跳轉回首頁</p>", 
        eventEnded: "本賽事已結束",
        eventsRecommended: "以下為您推薦的熱門賽事"
    },
    tobesettled: '待結算',
    recommend: { zanwutishi: '去其它地方看看吧~', zanwu: '暫無熱門賽事' },
    stationtips: [
        { key: '1', value: '不同賽事之間，暫不支持局內串關' },
        { key: '2', value: '全局盤口暫不支持局內串關' },
        { key: '3', value: '不同局數的盤口，暫不支持局內串關' },
        { key: '4', value: '相同類型盤口，互不支持局內串關' },
        { key: '5', value: '同一盤口中的多個投注項，不支持局內串關' },
        { key: '6', value: '局內串關賠率以實時顯示的總賠率為準' },
    ],
    timer: {
        kaipan: '開盤',
        day: '天',
        hour: '時',
        minute: '分',
        second: '秒',
    },
};
