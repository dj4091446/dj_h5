import Vue from "vue";
import VueRouter from "vue-router";


const home = () => import(/* webpackChunkName: "home" */ "@/views/home");
const betCart = () => import(/* webpackChunkName: "betCart" */ "@/views/betCart");
const markets = () => import(/* webpackChunkName: "markets" */ "@/views/markets");
const marketsVSports = () => import(/* webpackChunkName: "marketsVSports" */ "@/views/marketsVSports");
const vSports = () => import(/* webpackChunkName: "virtualSports" */ "@/views/home/virtualSports");
const matchFilter = () => import(/* webpackChunkName: "matchFilter" */ "@/views/matchFilter");
const tokenInvalid = () => import(/* webpackChunkName: "tokenInvalid" */ "@/views/tokenInvalid");
const historyOrder = () => import(/* webpackChunkName: "historyOrder" */ "@/views/user/historyOrder/index");
const operateActivity = () => import(/* webpackChunkName: "activity" */ "@/views/user/operateActivity/activity");
const notice = () => import(/* webpackChunkName: "notice" */ "@/views/user/notice");
const rules = () => import(/* webpackChunkName: "rules" */ "@/views/user/rules");
const guide = () => import(/* webpackChunkName: "guide" */ "@/views/user/guide");
const accountChange = () => import(/* webpackChunkName: "accountChange" */ "@/views/user/accountChange");
const selectLanguage = () => import(/* webpackChunkName: "selectLanguage" */ "@/views/user/selectLanguage");
const selectHandicap = () => import(/* webpackChunkName: "selectHandicap" */ "@/views/user/selectHandicap");
const selectAvatar = () => import(/* webpackChunkName: "selectAvatar" */ "@/views/user/selectAvatar");
const lottery = () => import(/* webpackChunkName: "lottery" */ "@/views/lottery/index");
const asiad = () => import(/* webpackChunkName: "lottery" */ "@/views/asiad");

Vue.use(VueRouter);
/**
 * 主要页面
 */
const pages = [
    {
        path: "/",
        redirect: "/home"
    },
    {
        path: "/home",
        name: "home",
        components: {
            default: home,
            a: betCart
        },
        meta: {
            keepAlive: true,
        }
    },

    {
        path: '/markets/:id',
        name: "markets",
        components: {
            default: markets,
            a: betCart
        },
        meta: {
            keepAlive: false,
        }
    },

    {
        path: '/vSports/:id',
        name: "marketsVSports",
        components: {
            default: vSports,
            a: betCart
        },
        meta: {
            keepAlive: false,
        }
    },
    
    {
        path: '/lottery',
        name: "lottery",
        components: {
            default: lottery,
        },
        meta: {
            keepAlive: false,
        }
    },

    {
        path: '/marketsVSports/:id',
        name: "marketsVSports",
        components: {
            default: marketsVSports,
            a: betCart
        },
        meta: {
            keepAlive: false,
        }
    },

    {
        path: "/matchFilter",
        name: "matchFilter",
        components: {
            default: matchFilter,
            a: betCart
        }
    },

    {
        path: "/tokenInvalid",
        name: "tokenInvalid",
        component: tokenInvalid
    },

    {
        path: "/historyOrder",
        name: "historyOrder",
        component: historyOrder
    },
    
    // 运营活动
    {
        path: "/operateActivity",
        name: "operateActivity",
        component: operateActivity
    },

    {
        path: "/notice",
        name: "notice",
        component: notice
    },

    {
        path: "/rules",
        name: "rules",
        component: rules,
        meta: {
            keepAlive: false,
        }
    },

    {
        path: "/guide",
        name: "guide",
        component: guide,
        meta: {
            keepAlive: false,
        }
    },

    {
        path: "/accountChange",
        name: "accountChange",
        component: accountChange,
        meta: {
            keepAlive: false,
        }
    },

    {
        path: "/selectLanguage",
        name: "selectLanguage",
        component: selectLanguage,
        meta: {
            keepAlive: false,
        }
    },
    
    {
        path: "/selectHandicap",
        name: "selectHandicap",
        component: selectHandicap
    },
    {
        path: "/selectAvatar",
        name: "selectAvatar",
        component: selectAvatar,
        meta: {
            keepAlive: false,
        }
    },
    {
        path: "/asiad",
        name: "asiad",
        component: asiad,
    },
];

const router = new VueRouter({
    mode: 'history',
    routes: [...pages],
    // 切换路由 自动滚动到顶部
    // scrollBehavior(to, from, savedPosition) {
    //     if (savedPosition) {
    //         return savedPosition;
    //     } else {
    //         return { x: 0, y: 0 };
    //     }
    // }
});

// 全局守卫
router.beforeEach((to, from, next) => {
    // console.log( to, from )
    console.info(`路由[router]: from: ${from.path} ==> to: ${to.path}`)
    next();
});

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}

export default router;
