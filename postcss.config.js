module.exports = {
    plugins: {
        'postcss-preset-env': {},
        'postcss-px-to-viewport': {
            unitToConvert: 'px',
            viewportWidth: 375,
            unitPrecision: 5,
            propList: ['*'],
            viewportUnit: 'vw',
            fontViewportUnit: 'vw',
            selectorBlackList: ['.no-vw'],
            minPixelValue: 1,
            mediaQuery: false,
            replace: true,
            exclude: [],
            landscape: true,
            landscapeUnit: 'vw',
            landscapeWidth: 780,
          
        },
        cssnano: {},
    },
}
