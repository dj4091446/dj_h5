const VueLoaderPlugin = require("vue-loader/lib/plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
const compressionWebpackPlugin = require('compression-webpack-plugin')


const CopyWebpackPlugin = require('copy-webpack-plugin');
const gitCommitMessage = require('./version.js')
const version = gitCommitMessage.split(':')[1].slice(0, 8);

console.log(gitCommitMessage)

const path = require("path");
const webpack = require("webpack");

const isDevMode = process.env.NODE_ENV == "development";

module.exports = (env, args) => {
	const BASE = args.base || "FAT";
	console.info("环境:", BASE);
	const server = {
        DEV: {
			APP_BASE_URL: "http://h5.ybdj.online",
			APP_CORON_URL: "https://dev-bob-wap.emkcp.com",
            APP_MQTT_URL: "ws://fat-mqtt.ybdj.online:9001/mqtt",
            APP_CDN_URL: "http://dl.ybdj.online/",
			APP_MQTT_NAME: "'admin'",
            APP_MQTT_PASSWORD: "'Qazqaz123...'",
        },
        FAT: {
            APP_BASE_URL: "http://api.zxjlbvip.org",
			APP_CORON_URL: "http://fatbob-api-1.yblott.com",
            // APP_MQTT_URL: "ws://fat-mqtt.ybdj.online:9001/mqtt",
            APP_MQTT_URL: "wss://djmqfat.ouligk.com:8084/mqtt",
            APP_CDN_URL: "http://dl.pai500.org/",
			APP_MQTT_NAME: "'admin'",
            APP_MQTT_PASSWORD: "'Qazqaz123...'",
			UPLOAD_URL: 'http://dl1.zxjlbvip.org/',
        },
        UAT: {
			APP_BASE_URL: "https://duatbob-hwapi.i9cge.com",
			APP_CORON_URL: "https://uat-wap.obcp.io",
            APP_MQTT_URL: "wss://uat-djwss.kluiqe1d.com:8084/mqtt",
            APP_CDN_URL: "https://dj-uat.klsdfjoiuwe23.com:8011/",
			APP_MQTT_NAME: "'admin'",
            APP_MQTT_PASSWORD: "'Qazqaz123...'",
			UPLOAD_URL: 'https://uat-uptxcdn1.sq5595.com/',
        },
        PROD: {
            APP_BASE_URL: "https://alidjh.gdnmstpt.com",
			APP_CORON_URL: "https://h5.bobcp.vip",
            APP_MQTT_URL: "wss://djmpro.ouligk.com:8084/mqtt",
            APP_CDN_URL: "https://txgzdj.91dkwg.com:8012/",
			APP_MQTT_NAME: "'admin'",
            APP_MQTT_PASSWORD: "'Qazqaz123...'",
			UPLOAD_URL: 'https://uptx-cdn2.q0n5k964.com/',
        }
    };

	const APP_BASE_URL = server[BASE].APP_BASE_URL;
	const APP_MQTT_URL = server[BASE].APP_MQTT_URL;
	const APP_CDN_URL = server[BASE].APP_CDN_URL;
	const UPLOAD_URL =  server[BASE].UPLOAD_URL;
	const APP_CORON_URL =  server[BASE].APP_CORON_URL;

	
	let publicPath = '/';
    //如果是build，使用cdn域名
    if (!isDevMode) {
        publicPath = '/h5/';
        console.log('使用cdn域名：', publicPath);
    }else{
        console.log('使用cdn域名：', false);
	}

	let defineObj = {
        isDevMode: isDevMode,
		appMQTTName: server[BASE].APP_MQTT_NAME,
        appMQTTPassword: server[BASE].APP_MQTT_PASSWORD
    }
    if(isDevMode){
        defineObj.APP_CDN_URL = `"${APP_CDN_URL}"`;
        defineObj.APP_MQTT_URL = `"${APP_MQTT_URL}"`;
        defineObj.UPLOAD_URL = `"${UPLOAD_URL}"`;
    }

	const config = {
		entry: {
			build: "./src/main.js",
		},

		output: {
			path: path.resolve(__dirname, "./dist"),
			publicPath: publicPath,
			filename: `static/js/[name].${version}.js`,
			chunkFilename: `static/js/[id].${version}.js`,
		},
		stats: "errors-only",
		devtool: "#source-map",
		devServer: {
			// host: "0.0.0.0", //允许外部ip访问设定0.0.0.0
			host: '127.0.0.1',
			historyApiFallback: true,
			hot: true,
			inline: true,
			open: true,
			stats: {
			colors: true,
			},
			port: 8083,
			disableHostCheck: true,
			proxy: [
                // 接口
                {
                    context: ["/game", "/handicap", "/activity"],
                    target: APP_BASE_URL,
                    changeOrigin: true,
                    logLevel: "debug",
                    secure: false
                }, 
                // 静态图片资源
                {
                    context: ["/upload"],
                    target: UPLOAD_URL,
                    changeOrigin: true,
                    logLevel: "debug",
                    secure: false
                },
				// 彩票
				{
					context: ["/coron", "/boracay"],
					target: APP_CORON_URL,
                    changeOrigin: true,
                    logLevel: "debug",
                    secure: false
				},
			],
		},

		module: {
			rules: [
				{
					test: /\.vue/,
					loader: "vue-loader",
					options: {
						transformToRequire: {
							audio: "src",
						},
					},
				},
				{
                    test: /\.(png|jpg|gif)$/,
                    //use: "url-loader?limit=80000&&esModule=false&name=static/images/[name][hash].[ext]?"
                    use: [{
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                            esModule: false,
                            name: 'static/images/[hash].[ext]',
                            publicPath: isDevMode ? '/' : '/h5/',
                            postTransformPublicPath: (p) => {
                                // 动态加载的文件添加cdn前缀
                                return isDevMode ? `${p}` 
                                : `(global.APP_CDN_URL || '../../../') + ${p}`
                            },
                        },
                    }],
                },
                {
                    test: /\.(woff|woff2|svg|eot|ttf)\??.*$/,
                    //use: "url-loader?limit=80000&esModule=false&name=static/fonts/[name].[ext]"
                    use: [{
                        loader: 'url-loader',
                        options: {
                            limit: 1,
                            esModule: false,
                            name: 'static/fonts/[name].[ext]',
                            publicPath: isDevMode ? '/' : '/h5/'
                        },
                    }],
                },
				{
					test: /\.(sa|sc)ss$/,
					use: [
						isDevMode ? "vue-style-loader" : MiniCssExtractPlugin.loader,
						"css-loader",
						"postcss-loader",
						// "sass-loader",
                        {
                            loader: "sass-loader",
                            options: {
                              implementation: require("sass")//使用dart-sass代替node-sass
                            }
                        }
					],
				},
				{
					test: /\.less$/,
					use: [
						isDevMode ? "vue-style-loader" : MiniCssExtractPlugin.loader,
						"css-loader",
						"postcss-loader",
						"less-loader",
					],
				},
				{
					test: /\.css$/,
					use: [
						isDevMode ? "vue-style-loader" : MiniCssExtractPlugin.loader,
						"css-loader",
						"postcss-loader",
					],
				},
				{
					test: /\.js|\.jsx$/,
					use: "babel-loader",
					exclude: /node_modules/, 
				},
				{
					test: /\.mp3|\.ogg(\?.*)?$/,
					loader: "url-loader",
					options: {
						limit: 10000,
						name: "static/audios/[name]-[hash].[ext]",
						// publicPath: isDevMode ? '/' : '/h5/'
					}
				}
			],
		},
		resolve: {
			alias: {
			vue$: "vue/dist/vue.esm.js",
			"@": path.resolve(__dirname, "./src/"),
			},
			extensions: [".js", ".json", ".vue"],
		},
		plugins: [
			new VueLoaderPlugin(),
			new HtmlWebpackPlugin({
                filename: "./index.html",
                template: "./src/index.html",
                inject: isDevMode,
                hash: false,
                minify: {
                    collapseWhitespace: true,
                    keepClosingSlash: true,
                    removeComments: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true,
                    useShortDoctype: true,
                    minifyJS: true,
                    minifyCSS: true,
                },
                gitCommitMessage: gitCommitMessage
            }),
			
			new CleanWebpackPlugin(["dist/**/*.*"], {
				root: path.resolve(__dirname, "."),
				exclude: [],
				verbose: true,
				dry: false,
			}),

			new MiniCssExtractPlugin({
				filename: isDevMode ? "[name].css" : `static/css/[id].${version}.css`,
				chunkFilename: isDevMode ? "[name].css" : `static/css/[id].${version}.css`,
			}),

			new webpack.DefinePlugin(defineObj),

			new CopyWebpackPlugin({
                patterns: [
                    {
                        from: path.posix.join(
                            path.resolve(__dirname).replace(/\\/g, '/'),
                            'src/assets/images/upload/*' 
                        ),

                        to: path.resolve(__dirname, 'dist/static/images/'),
                        flatten: true
                    },
					{
                        from: path.posix.join(
                            path.resolve(__dirname).replace(/\\/g, "/"),
                            "src/assets/video/"
                        ),

                        to: path.resolve(__dirname, "dist/static/video/"),
                        flatten: true,
                    }, 
                ]
            }),

			new compressionWebpackPlugin({
				filename: '[path].gz[query]', 
				algorithm: 'gzip', 
				test: /\.(js|css|svg)$/,
				threshold: 10240,
				minRatio: 0.8, 
				deleteOriginalAssets: false 
			}),
			
		],
		optimization: {
			splitChunks: {
			chunks: "all",
			cacheGroups: {
				commons: {
				chunks: "initial",
				minChunks: 2,
				name: "commons",
				maxInitialRequests: 5,
				minSize: 0, // 默认是30kb,
				priority: 2,
				},
			},
			},
			runtimeChunk: {
				name: "manifest",
			},
			minimizer: [
				new TerserPlugin({
					terserOptions: {
					compress: {
						drop_console: true
						// pure_funcs:["console.log"],
					},
					},
				}),
				new OptimizeCSSAssetsPlugin({}),
				new BundleAnalyzerPlugin({
					analyzerMode:
					process.env.npm_config_report == "true" ? "server" : "disabled",
				}),
			],
		},
	};
	return config;
};
