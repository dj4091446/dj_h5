const originPath = './src/assets/i18n/';
const exportPath = './_i18nExport';

const importPath = `./_i18nImport`;

const fs = require('fs');
const xlsx = require('xlsx');
const path = require('path');
const prettier = require('prettier');

const workBook = {
    SheetNames: [],
    Sheets: {},
};

const langArray = ['cn', 'zh', 'en', 'vi', 'ml', 'th', 'ni'];

const columnArray = ['key', ...langArray];

const xlsxPath =  './电竞H5端语言包.xlsx';

const sheetName = '电竞H5多语言JS脚本'
/**
 * 将对象內的属性拉平
 */
const flattenObj = (obj, parent, res = {}) => {
    for (const key of Object.keys(obj)) {
        const propName = parent ? `${parent}.${key}` : key;
        if (typeof obj[key] === 'object') {
            if(obj[key] == null){
                console.log(key, propName, parent)
            } else {
                flattenObj(obj[key], propName, res);
            }
        }else if (typeof obj[key] === 'function') {
            res[propName] = String(obj[key]);
        } else {
            res[propName] = obj[key];
        }
    }
    return res;
};

// 不是数组，却用数字做key的
const isObjectKey = ['cancellations', 'heros'];

function unflatten(data) {
    const result = {};
    for (var i in data) {
        var keys = i.split('.');
        keys.reduce(
            (r, e, j) => 
            r[e] || (r[e] = 
                (isNaN(Number(keys[j + 1])) || isObjectKey.includes(e))
                ? (keys.length - 1 == j ? data[i] : {}) : []), 
        result);
    }
    return result;
}

/*
 * 复制目录、子目录，及其中的文件
 * @param src {String} 要复制的目录
 * @param dist {String} 复制到目标目录
 */
function copyDir(src, dist, callback) {
    fs.access(dist, err => {
        if (err) {
            // 目录不存在时创建目录
            fs.mkdirSync(dist);
        }
        _copy(null, src, dist);
    });

    function _copy(err, src, dist) {
        if (err) {
            callback(err);
        } else {
            fs.readdir(src, (err, paths) => {
                if (err) {
                    callback(err);
                } else {
                    paths.forEach(path => {
                        const _src = `${src}/${path}`;
                        let _dist = `${dist}/${path}`;

                        if (_dist.endsWith('index.js') || _dist.endsWith('DS_Store')) {
                            return;
                        }
                        _dist = _dist.replace(/.js/g, '.mjs');
                        fs.stat(_src, (err, stat) => {
                            if (err) {
                                callback(err);
                            } else {
                                // 判断是文件还是目录
                                if (stat.isFile()) {
                                    fs.writeFileSync(_dist, fs.readFileSync(_src));
                                    translate(_dist);
                                } else if (stat.isDirectory()) {
                                    // 当是目录是，递归复制
                                    copyDir(_src, _dist, callback);
                                }
                            }
                        });
                    });
                }
            });
        }
    }
}

// 导出的数据
let exportData = [];
/**
 *
 * @param {*} file 要翻译的mjs文件
 */
function translate(file) {
    const parsePath = path.parse(file);
    const fileName = parsePath.name;
    const lang = langArray.find(e => parsePath.dir.includes(e));

    import(file).then(result => {
        const data = flattenObj(result.default);

        for (const _key in data) {
            let key = fileName + '.' + _key;
            const _index = exportData.findIndex(e => e.key == key);
            const obj = _index == -1 ? {} : exportData[_index];

            obj.key = key;
            obj[lang] = data[_key];

            if (_index == -1) {
                exportData.push(obj);
            } else {
                exportData[_index] = obj;
            }
        }
    });
}

function exportExcel() {
    copyDir(originPath, exportPath, error => {
        console.log(error);
    });

    // 将workBook写入文件
    setTimeout(() => {
        const jsonWorkSheet = xlsx.utils.json_to_sheet(exportData, {header: columnArray});

        workBook.SheetNames.push(sheetName);
        workBook.Sheets[sheetName] = jsonWorkSheet;

        xlsx.writeFile(workBook, xlsxPath);
        console.log('done');
    }, 4000);
}

function formatByPrettier(content) {
    return prettier.format(content, {
        printWidth: 80, // 单行长度
        tabWidth: 4, //缩进长度
        // useTabs: true, // 使用空格代替tab缩进
        semi: true, // 句末使用分号
        singleQuote: true, // 使用单引号
        quoteProps: 'as-needed', // 仅在必需时为对象的key添加引号
        jsxSingleQuote: true, // jsx中使用单引号
        trailingComma: 'all', // 多行时尽可能打印尾随逗号
        bracketSpacing: true, // 在对象前后添加空格-eg: { foo: bar }
        jsxBracketSameLine: true, // 多属性html标签的‘>’折行放置
        arrowParens: 'always', // 单参数箭头函数参数周围使用圆括号-eg: (x) => x
        requirePragma: false, // 无需顶部注释即可格式化
        insertPragma: false, // 在已被preitter格式化的文件顶部加上标注
        proseWrap: 'always', // 不知道怎么翻译
        htmlWhitespaceSensitivity: 'ignore', // 对HTML全局空白不敏感
        vueIndentScriptAndStyle: false, // 不对vue中的script及style标签缩进
        endOfLine: 'lf', // 结束行形式
        embeddedLanguageFormatting: 'auto', // 对引用代码进行格式化
        parser: 'babel',
    });
}


function getModuleObj(arr) {
    let moduleObj = {};
    let moduleList = Array.from(new Set(arr.map(e => e.key.split('.')[0])));

    moduleList.map(e => {
        let _key = `${e}.`;
        moduleObj[e] = [];
        arr.map(item => {
            if(item.key.startsWith(_key)){
                moduleObj[e].push(
                    {
                        ...item,
                        key: item.key.substring(_key.length)
                    }
                );
            }
        })

    })
    return moduleObj;
}

function importExcel() {
    const wb = xlsx.readFile(xlsxPath);

    let lastLang = langArray[langArray.length-1];
    fs.access(`${importPath}/${lastLang}`, err => {
        if (err) {

            langArray.map(e => {
                fs.mkdirSync(`${importPath}/${e}`);
            })
        }
        for (const fileName in wb.Sheets) {
            const arr = xlsx.utils.sheet_to_json(wb.Sheets[fileName], {
                header: columnArray,
            });

            const moduleObj = getModuleObj(arr);
            for (const fileName in moduleObj) {
                sheetToI18n(moduleObj[fileName], fileName);
            }
        }
    });

}

const sheetToI18n = function (arr, fileName) {
    langArray.map(e => {
        let obj = {};
        for (let i = 0; i < arr.length; i++) {
            obj[arr[i].key] = arr[i][e];
        }
        obj = unflatten(obj);
    
        // let varContent = '';
        // let exportContent = '';
        // for (const _key in obj) {
        //     varContent += `const ${_key} = 
        //         ${JSON.stringify(obj[_key])}
        //     ;`;
        //     exportContent += `${_key},`;
        // }
        // exportContent = `export default {
        //     ${exportContent}
        // };`;
        // let content = varContent + exportContent;
        let content =  `export default 
                ${JSON.stringify(obj)}
            ;`;
        content = formatByPrettier(content)
        
        try {
            fs.writeFileSync(`${importPath}/${e}/${fileName}.js`, content)
            // 文件写入成功。
        } catch (err) {
            console.error('1111',err);
        }
    })
    
};

importExcel();
// exportExcel();
